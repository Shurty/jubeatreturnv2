Jubeat Return
======

Rhythm music arcade game made from the Jubeat (KONAMI) concept. Select a music and play it in rhythm by pressing on blinking buttons and try to make the highest score.

Features
--------

* Specific controller - Blueprints included
* Electronic controller with Arduino - Code included

Platforms
--------

* PC (Keyboard)
* PC (Specific controller)

Usage
-------

Win32/Win64 Versions (CMake crossplatform was WIP)

* Load the project with Visual Studio
* Build the project with engine and libs
* Include all DLLs from Dev/Src/DLL in Dev/Bin/JRExe/(Release/Debug)/

Development Quality
-------

Documentation: None - Feel free to make one  
Code quality: Medium - Prototype

Dependancies & Engines
-------

* SFML
* tinyXML2

Licence
-------
This project is under MIT Licence.

The MIT License (MIT)

Copyright (c) 2015 Jeremy Dubuc

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.