

int inputStates[16];

void setup() { // initialize the buttons' inputs:
 
  
  for (int i = 0; i < 16; i++)
  {
    inputStates[i] = 0;    
  }
  for (int i = 2; i <= 13; i++)
  {
    pinMode(i, INPUT); 
  }
  
  Serial.begin(9600);
}

void loop() {

  /* Digital reading: PIN2-13 <=> 1 2 3 4 5 6 7 8 9 10 11 12 */
  for (int i = 2; i < 14; i++)
  {
     int buttonState = digitalRead(i);
     if (buttonState != inputStates[i-2])
     {
       if (buttonState == 1) {
         Serial.print("+");         
       } else {
         Serial.print("-");
       }
       Serial.print(19 - i);
       inputStates[i-2] = buttonState;
     }
  }

  /* Analog reading: (4) PIN A2-A5 <=> 16 15 14 13 - */
  for (int i = 0; i < 4; i++)
  {
    int sensorValue;
    int buttonState;
    if (i == 0)
      sensorValue = analogRead(A5);
    if (i == 1)
      sensorValue = analogRead(A4);
    if (i == 2)
      sensorValue = analogRead(A3);
    if (i == 3)
      sensorValue = analogRead(A2);
      
    buttonState = 0;
    if (sensorValue > 10) {
      buttonState = 1;
    }
    
    if (buttonState != inputStates[i + 12]) {
       if (buttonState == 1) {
         Serial.print("+");         
       } else {
         Serial.print("-");
       }
       Serial.print(i + 2);
       inputStates[i + 12] = buttonState;
    }
  }
  delay(20);

}

