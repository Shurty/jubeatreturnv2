/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"
#include "GradeDef.h"
#include "BeatResultDef.h"
#include "ScoreLogger.h"

class ScoreManager
{
protected:
	double m_scoreForBeat[BEATRESULT::BR_MAX_RESULTS];
	unsigned int m_beatResults[BEATRESULT::BR_MAX_RESULTS];

	/*
	 * Bonus score multiplier (from 0 to 1)
	 */
	double	m_bonusCounter;

	/*
	 * Current score (non bonus included)
	 */
	double	m_currentScore;

	/*
	 * potential score, correct score when all beats have been done (numberOfBeatsDone == numberOfnotes
	 */
	double	m_calculatedFinalScore;

	double	m_calculatedBonusScore;

	GRADE	m_calculatedFinalGrade;

	/*
	 * Number of notes of the played beatmap
	 */
	unsigned int	m_numberOfNotes;
	
	/*
	 * Maximum score value of a beat depending of number of notes
	 */
	double			m_beatValue;

	/*
	 * current combo (number of notes without breaking combo with a miss or bad)
	 */
	unsigned int	m_bonusNotesCounter;

	unsigned int	m_numberOfBeatsDone;


	/*
	 * some stats
	 */
	unsigned int m_maxCombo;


	/*
	 * detailed logging (every notes, every current score and predicted)
	 */
	bool m_logThisSession;
	ScoreLogger* scoreLogger;

public:
	ScoreManager();
	~ScoreManager(void);

	// bool parameter is optional, if true, saves every notes and save them when we call logScores().
	void reset(unsigned int numberOfNotes, bool logThisSession = false);
	// writes every previous notes into a timestamped file, so designed to be called only once (at the end of a scoring session).
	// calls logScores(const std::string&) with a timestamp filename
	std::string logScores() const;

	const std::string& logScores(const std::string&) const;
	// writes every previous notes into a timestamped file, so designed to be called only once (at the end of a scoring session).
	std::vector<ScoreLogger::Data> readScores(const std::string&) const;

	void scoreBeat(BEATRESULT);
	unsigned int getScoreWithoutCombo() const;
	unsigned int getBonusScore() const;
	unsigned int computePotentialBonusScore() const;
	unsigned int getScoreWithCombo() const;
	double getBonusScoreMultiplier() const;
	unsigned int getBonusNotesCount() const;
	unsigned int getMaxCombo() const;
	// returns total potential score with bonus
	unsigned int getPotentialScore() const;
	unsigned int getPotentialGrade() const;
	unsigned int getNbBeatResult(BEATRESULT br) const;

	static const unsigned int minimumScoreForGrade[GRADE::NUMBER_GRADES];
	static unsigned int testPerfectNotes(unsigned int numberOfNotes);
	static GRADE getGradeForFinalScore(unsigned int finalScore);
};

