/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrshared.h>


class AAPIGetScore : public Command
{
private:

public:
	AAPIGetScore()
		: Command("API_GETSCORE")
	{

	}

	~AAPIGetScore()
	{

	}

	/* Client/Server implementations */
	virtual void Receive(Client *cl, unsigned int boothId) = 0;
	virtual void Send(Client *cl, unsigned int score) = 0;

	void ReceiveAction(Client *cl, sf::Packet &pckOriginal)
	{
		
	}
	void SendAction(Client *cl, sf::Packet &pct)
	{

	}

	void ReceiveAPIAction(Client *cl, std::stringstream &pct)
	{
		int id;

		pct >> id;
		Receive(cl, id);
	}

	void SendAPIAction(Client *cl, std::stringstream &pct)
	{
		int val;

		pct >> val;
		Send(cl, val);
	}

};