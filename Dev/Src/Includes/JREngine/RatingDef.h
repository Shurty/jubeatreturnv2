/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum RATING
{
	RATING_E = 0,
	RATING_F = 699999,
	RATING_C = 799999,
	RATING_B = 849999,
	RATING_A = 899999,
	RATING_S = 949999,
	RATING_SS = 979999,
	RATING_SSS = 999999,
	RATING_EXC = 1000000
};