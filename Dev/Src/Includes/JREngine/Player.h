/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"
#include "json/json.h"
#include <functional>

class Player : public Client
{
private:
	Client m_client;

public:
	struct MusicScoreRecord
	{
	public:
		/*
		- Meilleur score obtenu en 1 run
        - Meilleur nombre de notes non miss/bad en 1 run
        - Meilleur combo en 1 run
        - Meilleur nombre de perfect en 1 run
        - Nombre de fois jou�e
        - Nombre de clear (grade >= C)
        - Score total (tous les runs)
        - Timestamp UNIX dernier highscore
        - Timestamp UNIX derniere fois jou�e
		*/
		typedef struct Details
		{
		public:
			Details();
			Details(const Json::Value&);
			unsigned int bestScore;
			unsigned int maxGoodBeat;
			unsigned int maxCombo;
			unsigned int maxPerfect;
			unsigned int timesPlayed;
			unsigned int timesCleared;
			unsigned int totalScore;
			unsigned int timestampHighscore;
			unsigned int timestampLastPlayed;
			
			Json::Value getJsonValue() const;
		} Details;
		Details m_basic;
		Details m_intermediate;
		Details m_extreme;
		
		std::string m_hashmusicName;
		
		MusicScoreRecord(const std::wstring& musicName)
		{
			std::hash<std::wstring> hashMusicName;
			m_hashmusicName = std::to_string(hashMusicName(musicName));
		}
		MusicScoreRecord(const Json::Value&);
		void set(SONGLEVEL, Details& score);
		Details& get(SONGLEVEL);
		const Details& const_get(SONGLEVEL) const;
		Json::Value getJsonValue() const;
	};
	std::string m_pseudo;
	std::vector<MusicScoreRecord> m_scores;

	Player(const std::string& pseudo, unsigned int id);
	Player(const Player&);
	Player();
	~Player();
	const std::string& getPseudo() const;
	unsigned int getHighScoreFor(const std::wstring& music, SONGLEVEL difficulty) const;

	// returns nil if there's no musicrecord for this music
	MusicScoreRecord* getMusicScoreRecordFor(const std::wstring& musicName);
	// detailsToConsider should be details of last play, with best score being current score
	// the resulting scoreRecord will be stored in memory in the player, then you'll have to save in file.
	const MusicScoreRecord& updateWithDetails(const MusicScoreRecord::Details& detailsToConsider, const std::wstring&, SONGLEVEL difficulty);


	// saves internally (no files written);  returns true if it was highest score for the player
	bool		saveScoreIfHigher(const std::wstring& music, SONGLEVEL songlevel, unsigned int score);

	Player(const Json::Value&);
	Json::Value getJsonValue() const;
};

bool operator==(const Player& lhs, const Player& rhs);
bool operator!=(const Player& lhs, const Player& rhs);
bool operator==(const Player& lhs, const std::string&);