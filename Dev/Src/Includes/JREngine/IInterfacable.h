/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class IInterfacable
{
public:
	virtual ~IInterfacable() { }

	virtual void OnKeyPressed(int key) = 0;
	virtual void OnKeyReleased(int key) = 0;
	/*
	virtual void OnMusicSelectConfirm(Beatmap *bmp) = 0;
	virtual void OnSortButton() = 0;
	virtual void OnBackButton() = 0;
	*/
};
