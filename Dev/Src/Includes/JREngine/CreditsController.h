/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class CreditsController
{
private:
	CREDVTYPE m_credits[CRD_COUNT];

public:
	CreditsController();
	~CreditsController();

	CREDVTYPE Value(CREDSID id = CRD_CREDIT);
	CREDVTYPE Value(CREDVTYPE value, CREDSID id = CRD_CREDIT);
};