/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class Client
{
protected:
	/* Core socket connection verification */
	sf::TcpSocket *m_sock;
	sf::UdpSocket *m_sockUdp;
	bool m_connected;

	/* Account and client data */
	ClientAccount m_account;
	ClientParty m_party;
	ClientParty m_matchmaking;
	bool m_logged;
	int m_id;

	/* Timeout and validity */
	bool m_valid;
	sf::Clock m_tOutClk;

	std::string m_hash;

public:
	Client(int id = -1);
	~Client();

	void SetHashCode(std::string const &);
	std::string const &GetHashcode();

	void OnConnect();
	void OnMatchmakingBegin();

	ClientParty *GetMatchmakingParty();
	void SetMatchmaking(int music);
	bool IsValid() const;
	bool IsConnected() const;
	void MarkAsDisconnected();
	void SetValid();
	int GetId() const;
	sf::TcpSocket &GetSock();
	sf::Clock &GetTimeoutClock();
	ClientAccount &GetAccountData();
};