// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

/*
enum CmdID
{
	PING,
	LOGIN,
	REGISTER,
	PLAY,
	MATCHMAKING,
};
*/

typedef std::string CmdID;
