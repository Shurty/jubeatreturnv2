/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class GameState;

class GameStateManager
{
private:
	std::vector<GameState *>	m_states;
	GameState*					m_activeState;
	GameState*					m_futureState;
	Parameters					&m_pm;

public:
	GameStateManager(Parameters &pm);
	~GameStateManager();

	void ChangeState(GameState *st);
	void ChangeState(GMSTATEID stid);
	GameState *GetActiveState();
	void PushState(GameState *st);

	void UpdateState();
	void RenderState();
};