/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrshared.h>


class AMatchmaking : public Command
{
private:

public:
	AMatchmaking() 
		: Command("MATCHMAKING")
	{

	}

	~AMatchmaking()
	{

	}

	/* Client/Server implementations */
	virtual void Receive(Client *cl, std::string const &playerName, int music) = 0;
	virtual void Send(Client *cl, std::string const &playerName, int music) = 0;

	void ReceiveAction(Client *cl, sf::Packet &pckOriginal)
	{
		sf::Int16 song;
		std::string pl;

		BeforeReceive(cl, pckOriginal);
		pckOriginal >> pl >> song;
		Receive(cl, pl, song);
		AfterReceive(cl, pckOriginal);
	}
	void SendAction(Client *cl, sf::Packet &pct)
	{
		sf::Int16 song;
		std::string pl;

		BeforeReceive(cl, pct);
		pct >> pl >> song;
		Send(cl, pl, song);
		AfterReceive(cl, pct);
	}
};