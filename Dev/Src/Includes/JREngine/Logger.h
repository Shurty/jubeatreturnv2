/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

class Logger
{

protected:
	std::string buffer;
	std::stringstream outBuffer;
	std::string savefile;

	Logger();
public:
	void Bufferize(std::string const &msg);
	void Output();
	void Save();
	void ClearLog();

	~Logger();

	static Logger Log;

	/*Logger &operator<<(std::string const &msg);
	//Logger &operator<<(int msg);
	Logger &operator<<(double msg);
	Logger &operator<<(void *msg);*/


};

// COUT definition
typedef std::basic_ostream<char, std::char_traits<char> > CoutType;
typedef CoutType& (*StandardEndLine)(CoutType&);

template<class T>
Logger& operator << (Logger &, T const &val);

template<>
Logger &operator << (Logger &, std::string const & msg);

Logger &operator << (Logger &, char const res[]);
Logger &operator << (Logger &, double const &msg);
Logger &operator << (Logger &, int const &msg);
Logger &operator << (Logger &, unsigned int const &msg);
Logger &operator << (Logger &, StandardEndLine const &val);
