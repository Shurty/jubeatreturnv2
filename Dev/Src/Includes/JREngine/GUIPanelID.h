/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

/* 
 * Since we are displaying 2 areas, some
 * panels will be divided in 2 subpanels
 */
enum GUIPANELID
{
	PANEL_SPLASH,
	PANEL_GAMEMODE,
	PANEL_INPUT,
	PANEL_MESSAGE,
	PANEL_MUSIC_SELECT,
	PANEL_PLAY,
	PANEL_DEBRIEFING,
	PANEL_END_GAME,
	PANEL_CONFIG,
	PANEL_PRELOAD,
};