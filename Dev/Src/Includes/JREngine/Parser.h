/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

struct Beatmap;

class Parser
{
public:
	Parser() {}
	virtual ~Parser() { }

	virtual Beatmap *ParseBeatmap(std::string const &src, bool full = true) = 0;
};

