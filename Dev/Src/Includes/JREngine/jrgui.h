/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <math.h>
#include <list>
#include <vector>
#include <iostream>
#include <JREngine/jrshared.h>

#include "GUI/CppTweener.h"

#include "GUI/GUIWidget.h"
#include "GUI/GUIKeyboard.h"
#include "GUI/AGUICallback.hpp"
#include "GUI/GUIActionButton.h"
#include "GUI/GUISystemPopup.h"
