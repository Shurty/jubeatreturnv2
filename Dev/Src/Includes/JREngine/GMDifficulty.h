/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class GMDifficulty
{
private:
	sf::Texture*		m_assocTexture;
	std::string			m_name;
	std::string			m_cfgSrc;

public:
	GMDifficulty(sf::Texture* tex, std::string const &name, std::string const &src);
	~GMDifficulty(void);

	std::string GetName();
	sf::Texture* GetTexture();
	void LoadConfig(Parameters* params);
};