#pragma once

#include <SFML/Graphics.hpp>
#include "AGUICallback.hpp"
#include "GUIWidget.h"

struct MarkerEntry;

class GUIMarkerButton : public GUIWidget
{
protected:
	MarkerEntry *m_config;
	sf::Clock m_clk;

	void DrawBackground(GraphicsController *target);
public:
	GUIMarkerButton();
	virtual ~GUIMarkerButton();

	void SetMarkerEntry(MarkerEntry *bmp);
	MarkerEntry *GetMarkerEntry() const;

	virtual void Init();
	virtual void Draw(GraphicsController *target);
	virtual void Update(sf::Clock *time);
	virtual void OnPressed();
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};