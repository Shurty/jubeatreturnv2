/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrgui.h>

class AGUICallback
{
private:

public:
	AGUICallback() { }
	virtual ~AGUICallback() { }

	virtual void OnPrevious(GUIWidget *btn) { }
	virtual void OnNext(GUIWidget *btn) { }
	virtual void OnSettings(GUIWidget *btn) { }
	virtual void OnBack(GUIWidget *btn) { }
	virtual void OnStart(GUIWidget *btn) { }
	virtual void OnEndGame(GUIWidget *btn) { }
	virtual void OnGamemode(GUIWidget *btn) { }
	virtual void OnSelect(GUIWidget *btn) { }
	virtual void OnParamSelect(GUIWidget *btn) { }
};

