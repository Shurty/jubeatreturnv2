#pragma once

#include <SFML/Graphics.hpp>
#include "GUIWidget.h"

class GUISplashWidget : public GUIWidget
{
private:
	sf::Clock m_clk;
	int m_delayIdt;

public:
	GUISplashWidget(int);
	virtual ~GUISplashWidget();

	virtual void Init();
	virtual void Draw(GraphicsController *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};