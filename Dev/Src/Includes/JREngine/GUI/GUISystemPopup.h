/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrgui.h>

class GUISystemPopup : public GUIWidget
{
private:
	enum SYSPOPUP_TYPE
	{
		UI_SYS_ERROR,
		UI_SYS_NOTICE,

	} m_popupType;

	AGUICallback *m_cb;
	void (AGUICallback::*m_cbMethod)(GUIWidget *);

	std::string m_msg;
	sf::Clock m_displayClk;

public:
	GUISystemPopup(Parameters &pm, std::string const &msg);
	virtual ~GUISystemPopup();

	virtual void Init();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);

	bool Expired();
};