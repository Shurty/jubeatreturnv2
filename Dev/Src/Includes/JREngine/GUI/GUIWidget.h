/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrgui.h>

#define TRIGGER_LENGTH		100

class GUIWidget
{
protected:
	sf::Vector2f m_position;
	sf::Vector2f m_size;

	int m_linkedKey;
	GUIWidget *m_parent;
	bool m_clicked;
	bool m_pressed;
	bool m_triggered;
	int m_triggerTime;
	void *m_userData;

	Parameters &m_params;
	void (*m_renderingFunc)(GUIWidget *, RenderingEngine *);

public:
	GUIWidget(Parameters &pm);
	virtual ~GUIWidget();

	// Position and size definition (non sfml related)
	void SetPosition(float x, float y);
	void SetPosition(sf::Vector2f pos);
	sf::Vector2f GetPosition() const;	
	void SetSize(float x, float y);
	void SetSize(sf::Vector2f sz);
	sf::Vector2f GetSize() const;
	
	// Key that triggers the widget
	void SetLinkedKey(int key);
	void SetParent(GUIWidget *);
	GUIWidget *GetParent() const;
	int GetLinkedKey() const;
	void LinkUserData(void *ud);
	void *GetUserData() const;
	bool IsTriggered() const;

	void SetRender(void (*func)(GUIWidget *, RenderingEngine *));
	virtual void Init();
	virtual void RenderAsSelection(RenderingEngine *target, sf::Clock *clk, sf::Vector2f offsets, float alpha);
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);

	// On trigger event (can be any kind of scripting) with a delay being active as triggered
	virtual void OnTriggered();

	// On first press of the key before release
	virtual void OnClick(); // Alias of OnClickFinish

	// If the key is pressed but not released (constantly active)
	virtual void OnPressed(); 

	// If the key is released (active once)
	virtual void OnReleased(); 


};