#pragma once

#include <SFML/Graphics.hpp>
#include "GUIWidget.h"
#include "Game.h"

class GUIGameEndWidget : public GUIWidget
{
private:
	sf::Clock m_clk;
	int m_delayIdt;

public:
	GUIGameEndWidget(int);
	virtual ~GUIGameEndWidget();

	virtual void Init();
	virtual void Draw(GraphicsController *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};