#pragma once

#include <JREngine/jrgui.h>

class GUIActionImageButton : public GUIActionButton
{
private:
	sf::Texture m_bg_texture;
	sf::Texture m_texture;

public:
	GUIActionImageButton(std::string const& backgroundImage);

	void DrawBackground(GraphicsController *target);
	virtual void Draw(GraphicsController *target);
};