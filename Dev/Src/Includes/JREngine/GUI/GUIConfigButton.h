#pragma once

#include <SFML/Graphics.hpp>
#include "AGUICallback.hpp"
#include "GUIWidget.h"

struct ConfigEntry;

class GUIConfigButton : public GUIWidget
{
protected:
	sf::Font m_font;
	ConfigEntry *m_config;
	int m_textSize;

	void DrawBackground(GraphicsController *target);
public:
	GUIConfigButton();
	virtual ~GUIConfigButton();

	void SetConfigEntry(ConfigEntry *bmp);
	ConfigEntry *GetConfigEntry() const;

	virtual void Init();
	virtual void Draw(GraphicsController *target);
	virtual void Update(sf::Clock *time);
	virtual void OnPressed();
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};