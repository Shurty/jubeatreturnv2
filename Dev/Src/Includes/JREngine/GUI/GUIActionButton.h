/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrgui.h>

class GUIActionButton : public GUIWidget
{
private:
	AGUICallback *m_cb;
	void (AGUICallback::*m_cbMethod)(GUIWidget *);

public:
	GUIActionButton(Parameters &pm);
	virtual ~GUIActionButton();

	void LinkAction(AGUICallback *, void (AGUICallback::*clb)(GUIWidget *));

	virtual void RenderAsSelection(RenderingEngine *target, sf::Clock *clk, sf::Vector2f offsets, float alpha);
	virtual void Init();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void OnPressed();
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};