#pragma once

#include <SFML/Graphics.hpp>
#include "AGUICallback.hpp"
#include "GUIActionButton.h"
#include "Game.h"

class GUIActionTextButton : public GUIActionButton
{
private:
	sf::Font	m_font;
	std::string m_title;

public:
	GUIActionTextButton();

	void DrawBackground(GraphicsController *target);
	virtual void Draw(GraphicsController *target);
	void SetTitle(std::string const &);
	std::string GetTitle() const;
};