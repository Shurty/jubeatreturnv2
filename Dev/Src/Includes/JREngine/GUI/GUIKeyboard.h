/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrgui.h>

#define KB_KEYS_COUNT			16

// Step for 540x800 screen
#define KB_DEFAULT_STEP			8
// Step for 1920x1080 screen for 60mm size 10mm seperation
//#define KB_DEFAULT_STEP			36
#define KB_DEFAULT_BTN_SIZE		KB_DEFAULT_STEP * 6

class RenderingEngine;

class GUIKeyboard : public GUIWidget
{
private:
	GUIWidget *m_widgets[KB_KEYS_COUNT];
	float m_step;
	float m_sideStep;
	Parameters &m_pm;

public:
	GUIKeyboard(Parameters &pm);
	~GUIKeyboard();

	void SetWidget(int position, GUIWidget *wdg);
	GUIWidget* GetWidget(int position);

	virtual void Init(bool autoCalc = true);
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};