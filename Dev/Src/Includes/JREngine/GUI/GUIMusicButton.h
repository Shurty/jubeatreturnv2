#pragma once

#include <SFML/Graphics.hpp>
#include "AGUICallback.hpp"
#include "GUIWidget.h"
#include "Game.h"

class GUIMusicButton : public GUIWidget
{
protected:
	sf::Texture m_level_texture;
	sf::Font m_font;
	sf::Clock m_animationTime;
	sf::Texture m_bg;
	Beatmap *m_music;
	AGUICallback *m_cb;
	void (AGUICallback::*m_cbMethod)(GUIWidget *);
	bool m_selected;
	int m_textSize;

	void DrawBG(GraphicsController *target);

public:
	GUIMusicButton();
	virtual ~GUIMusicButton();

	void SetBeatmap(Beatmap *bmp);
	Beatmap *GetBeatmap() const;
	void LinkAction(AGUICallback *, void (AGUICallback::*clb)(GUIWidget *));
	void SetSelected(bool res);
	void SetTextSize(int sz);

	virtual void Init();
	virtual void Draw(GraphicsController *target);
	virtual void DrawLevel(GraphicsController *target);
	virtual void Update(sf::Clock *time);
	virtual void OnPressed();
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};