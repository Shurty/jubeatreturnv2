#pragma once

#include "GUIMusicButton.h"

class GUIMusicInfoButton : public GUIMusicButton
{
public:
	GUIMusicInfoButton();
	virtual ~GUIMusicInfoButton();

	virtual void Draw(GraphicsController *target);
	virtual void DrawLevel(GraphicsController *target);
	virtual void DrawBG(GraphicsController *target);
};