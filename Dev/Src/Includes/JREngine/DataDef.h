/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

// JREngine Version
#define VERSION					"0.3.0"

#define MAX(a, b)				(((a) > (b)) ? (a) : (b))
#define MIN(a, b)				(((a) > (b)) ? (b) : (a))
#define IS_IN(from, a, b, c, d)	(((from) == (a)) || ((from) == (b)) || ((from) == (c)) || ((from) == (d)))

#define SETTINGS_DATA_SRC		"Data/config.ini"
#define CHARLIST_DATA_SRC		"Data/charlist.txt"

#define LOGSSRC					"Data/Logs/"
#define TEXTURESDIR				"Data/Textures/"
#define ACTIVATORSDIR			"Data/Textures/Activators/"
#define MUSICDIR				"Data/Musics/"
#define SOUNDDIR				"Data/Sounds/"
#define BEATMAPSDIR				"Data/Beatmaps/"
#define CBEATMAPSDIR			"Data/BeatmapsCompiled/"
#define FONTSDIR				"Data/Fonts/"

#define MUSICEXT				".ogg"
#define MUSICEXTWAV				".wav"

#define NBGRAPHBEATS			100
#define PLT_OFFSET				2000

#define NOVIDEO					1
#ifdef _DEBUG
#define ISDEBUG					1
#endif

#define SHOW_FPS				1
