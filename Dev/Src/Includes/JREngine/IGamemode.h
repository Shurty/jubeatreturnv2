/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

struct Beatmap;
class GUIKeyboard;
class GUIWidget;

class IGamemode
{
protected:
	std::string m_name;
	std::string m_selectSndName;
	Beatmap *m_selectedBeatmap;
	sf::Clock m_gameClock;

	char m_availableDifficulties;

public:
	
	// Interface methods
	
	IGamemode() { }
	virtual ~IGamemode() { }

	virtual GMMODE_ID GetId() = 0;

	// Generic callbacks

	virtual void Init() = 0;
	virtual void Update() = 0;
	virtual void Render() = 0;
	virtual void OnSetActive() = 0;
	virtual void OnUnload() = 0;
	virtual bool IsActive() const = 0;

	// Init callbacks

	virtual void OnPlayKeyboardButtonInit(GUIKeyboard *kb, GUIWidget *wdg) = 0;

	// Rendering callbacks

	/*
	 *	Called when the icon/image is asked for rendering for a button
	 */
	virtual void OnIconRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2) = 0;

	/*
	*	Called when the gamemode is selected in GamemodePanel and the top render is being done
	*/
	virtual void OnSelectedTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2) = 0;

	/*
	*	Called when the top render is called in Play state
	*/
	virtual void OnPlayTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2) = 0;

	/*
	*	Called when the top render is called in Debriefing state
	*/
	virtual void OnDebriefingTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2) = 0;
	
	// State swap/action events

	virtual void OnBeatAction(BEATRESULT res, GUIWidget *widget) = 0;
	virtual void OnGamemodeSelected() = 0;
	virtual void OnGamemodeConfirmed() = 0;
	virtual void OnGameSet(Beatmap *res) = 0;
	virtual void OnGameStart() = 0;
	virtual void OnGameEnd() = 0;
	virtual void OnMusicSelect(Beatmap *) = 0;
	virtual void OnDebriefingExit() = 0;

	// General gm related data methods

	virtual unsigned int GetCreditPrice() { return (1); }
	virtual ScoreManager& GetScoreManager() = 0;
	virtual Beatmap* GetSelectedBeatmap() const { return (m_selectedBeatmap); }
	virtual int GetPlayTime() { return (m_gameClock.getElapsedTime().asMilliseconds() - PLT_OFFSET); }

	void SetName(std::string const &nm) { m_name = nm; }
	std::string const &GetName() { return (m_name); }
	std::string const &GetSoundName() { return (m_selectSndName); }
};
