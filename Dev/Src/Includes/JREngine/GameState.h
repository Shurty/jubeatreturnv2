/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class GameState
{
protected:
	std::string				m_name;
	GMSTATEID				m_id;

public:
	GameState();
	virtual ~GameState();

	GMSTATEID GetId();

	virtual void OnInit(Parameters &pm) = 0;
	virtual void OnStart(Parameters &pm) = 0;
	virtual void OnRender(Parameters &pm) = 0;
	virtual void OnUpdate(Parameters &pm) = 0;
	virtual void OnEnd(Parameters &pm) = 0;
	virtual void OnUnload(Parameters &pm) = 0;
};