/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrshared.h>

class AHello : public Command
{
private:

public:
	AHello()
		: Command("HELLO")
	{

	}

	~AHello()
	{

	}

	/* Client/Server implementations */
	virtual void Receive(Client *cl, std::string const &hash, bool isapi) = 0;
	virtual void Send(Client *cl, std::string const &hash, bool isapi) = 0;

	void ReceiveAction(Client *cl, sf::Packet &pckOriginal)
	{
		std::string hashcode;

		BeforeReceive(cl, pckOriginal);
		pckOriginal >> hashcode;
		Receive(cl, hashcode, false);
		AfterReceive(cl, pckOriginal);
	}
	void SendAction(Client *cl, sf::Packet &pct)
	{
		std::string hashcode;

		BeforeReceive(cl, pct);
		pct >> hashcode;
		Send(cl, hashcode, false);
		AfterReceive(cl, pct);
	}

	void ReceiveAPIAction(Client *cl, std::stringstream &pct)
	{
		std::string hashcode;

		pct >> hashcode;
		Receive(cl, hashcode, true);
	}

	void SendAPIAction(Client *cl, std::stringstream &pct)
	{
		std::string hashcode;

		pct >> hashcode;
		Send(cl, hashcode, true);
	}

};
