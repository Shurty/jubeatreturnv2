/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

#define BMPCHARS_PRESSABLE	24

#define BMPCHARS_COUNT		28		// Count of beatmap special character codes
#define BMPCHARS_EMPTY_STR	25		// ID of beatmap special character code for EMPTY (structure)
#define BMPCHARS_EMPTY_RYT	24		// ID of beatmap special character code for EMPTY (rythm)
#define BMPCHARS_SEP_RYT	26		// ID of beatmap special character code for SEP1/16 (rythm)
#define BMPCHARS_HOLD_RYT	27		// ID of beatmap special character code for HOLDKEY (rythm) followed by 1/16 times count

/*
 * BeatmapSection
 *
 * Structure used in parsing after extracting the header from the file
 * This is the 1st parsing process structure before translating into
 * correct vector of beats
 * Used to store a raw and pre-parsed version of the beatmap,
 * by converting all ISO characters into int. values equivalents
 */
struct BeatmapSection;

struct BeatmapSection
{
	/*
	 * Activation table
	 * List of buttons to press in a specific order
	 * Values :
	 * -1   : Never pressed in current section
	 * 0    : Not set yet, or was never set (error state)
	 * 1-16 : (included bounds) Order to press the button
	 * The keys are representing a 2D array of buttons
	 * - - - -      0  1  2  3 
	 * - - - -      4  5  6  7
	 * - - - -      8  9  10 11
	 * - - - -      12 13 14 15
	 */
	int activations[16];
	/* 
	 * Rythm string
	 * Indicates the 1/16 times of BPS
	 * (ex: a BPM of 149 results in a 100.6ms delay between each - or press)
	 * So rythm delay is 60/BPM * 250 (result in ms)
	 * Rythm syntax:
	 * - - - -  or extended - - - - - - if needed
	 * - - - -
	 * - - - -
	 * - - - -
	 * Keys syntax:
	 * [1-24 keys like activation]: define a key correspondance in rythm
	 * Special attributes syntax (added after a key or an attribute)
	 * &		Hold key for a defined ammount of 1/16 of time (3 reserved chars) ex: &008 
	 *
	 */
	std::string rythm;
	/* 
	 * Rythm count string
	 * Indicates the elements count of a section line
	 */
	std::string rythmCount;
	/* 
	 * Raw string for the rythm 
	 */
	std::wstring rythmRaw;
	/* 
	 * Raw string for the activations structure, before
	 * translated into the activations table
	 */
	std::wstring activationsRaw;
	/* 
	 * Sub beat map following the previous beatmap.
	 * Used for complex beatmaps definition
	 */
	BeatmapSection *subBeatmap;
};

class MemoParser : public Parser
{
private:
	/* 
		Chars list for the beatmap parsing
	*/
	std::wstring beatmapCharacters[BMPCHARS_COUNT];
	//char beatmapCharacters[BMPCHARS_COUNT][32];

	std::string m_targetDirectory;
	std::wifstream bmpFile;
	std::string m_src;
	Beatmap *resBmp;
	float m_activeTime;
	int m_activeSection;

	// Sigh...
	std::string IntToStr(int x);
	void ParseSectionLine(BeatmapSection &res);
	void Generate(BeatmapSection &active, float bpm);
	int ExtractIntFromStr(std::string const &res);

public:
	MemoParser();
	~MemoParser();

	void ParseHeader();
	void ParseContent();
	BeatmapSection ParseSection();
	void EchoResult();
	virtual Beatmap *ParseBeatmap(std::string const &src, bool full = true);
};