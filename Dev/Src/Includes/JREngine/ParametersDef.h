/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#define SETTINGS_DATA_SRC		"Data/config.ini"

#define FRAMETIME 16.67// ~ (1000 / 60) // in miliseconds

enum Parameter
{
	P_DEBUG,
	P_INPUT_TYPE,
	P_WND_WIDTH,
	P_WND_HEIGHT,
	P_WND_TITLE,
};