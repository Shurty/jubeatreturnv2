/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum GMSTATEID
{
	GMST_BOOT,				// Boot screen
	GMST_SPLASH,			// Splash screen (waiting for user input)
	GMST_WELCOME,			// Splash screen (welcoming user)
	GMST_CREDITS,			// Splash screen (insert credit ?)
	GMST_LOGIN,				// Login screen (login info)
	GMST_REGISTER,			// Register screen
	GMST_REGISTERED,		// Register success screen
	GMST_LOCAL_PARTY,		// Local party create screen
	GMST_GAMEMODE_SELECT,	// Gamemode select screen
	GMST_BETAMSG,			// BETA: Game state to show a "inbeta" message
	GMST_MUSIC_SELECT,		// Music select screen
	GMST_SETTINGS,			//
	GMST_MODS,				//
	GMST_MARKERS,			//
	GMST_CREATE_PARTY,		// Force create party screen
	GMST_KEYBOARD_INPUT,	// ABC. button keyboard screen 
	GMST_MATCHMAKING,		// Matchmaking on music screen
	GMST_PLAYING,			// On playing screen
	GMST_MUSIC_END,			// Music ended result screen
	GMST_DEBRIEFING,		// Stats screen
	GMST_END,				// Game finished/Credits expired screen
};