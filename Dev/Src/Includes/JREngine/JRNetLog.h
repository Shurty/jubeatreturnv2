/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////
//
// JRNetLog is a logger extension providing direct network log upload
// and controls an extended logging system.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class JRNetLog
{
private:
	std::string m_ip;
	std::string m_port;

public:

};