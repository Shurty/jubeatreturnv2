/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class RenderingEngine;

class InputController
{
private:
	sf::Event m_activeEvent;
	void (IInterfacable::*m_btnPressedCallback)(int);
	void (IInterfacable::*m_btnReleasedCallback)(int);

public:
	InputController(Parameters &pm);
	~InputController();

	virtual void SetBtnPressedCallback(void (IInterfacable::*clb)(int));
	virtual void SetBtnReleasedCallback(void (IInterfacable::*clb)(int));

	// Returns the ammount of unhandled events
	virtual int PollEvents(RenderingEngine *wnd, IInterfacable *target);
};