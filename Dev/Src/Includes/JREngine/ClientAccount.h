/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class ClientAccount
{
private:
	/* Generic account data */
	std::string m_accountName;
	std::string m_accountPwd;
	unsigned int m_uid;

	/* Score storage impl */

public:
	ClientAccount();
	~ClientAccount();

};