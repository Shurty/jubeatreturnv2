/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "targetver.h"

// ************************* Libs *************************
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Header Files:
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Network.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <deque>
#include <locale>
#ifdef WIN32
#include <codecvt>
#endif

// ************************* Ext. Libs *************************
#include <json/json.h>
// TODO: reference additional headers your program requires here

class InputController;

// ************************* Cross compile *************************
std::string UTF8ToAscii(std::wstring rstr);

#ifdef WIN32

#else
typedef void* HANDLE;
typedef int COMSTAT;
typedef unsigned long DWORD;
#endif

// ************************* Defines *************************
#include "UnitTestsDef.h"
#include "BeatResultDef.h"
#include "DataDef.h"
#include "GamemodeDef.h"
#include "GameStateDef.h"
#include "GUIEvents.h"
#include "GUIPanelID.h"
#include "ParametersDef.h"
#include "RatingDef.h"
#include "GradeDef.h"
#include "RenderingEngineDef.h"
#include "SoundManagerDef.h"
#include "CommandDef.h"
#include "SongLevel.h"
#include "CreditsDef.h"

// ************************* Parameters *************************
#include "SettingsIO.h"
#include "Parameters.h"

// ************************* Networking modules *************************
#include "Command.h"
#include "AMatchmaking.hpp"
#include "APing.hpp"
#include "AHello.hpp"

#include "AAPIGetScore.hpp"

// ************************* Rendering *************************
#include "AnimatedSprite.h"
#include "RenderingEngine.h"

// ************************* Mapping *************************
#include "Beatmap.hpp"
#include "Parser.h"
#include "JRBParser.h"
#include "MemoParser.h"

// ************************* Scoring *************************
#include "ScoreManager.h"
#include "CreditsController.h"

// ************************* Gamemodes/FSM *************************
#include "IInterfacable.h"
#include "IGamemode.h"
#include "GMDifficulty.h"
#include "GameState.h"
#include "GameModeManager.h"
#include "GameStateManager.h"

// ************************* IO *************************
#include "Logger.h"
#include "JRNetLog.h"
#include "InputController.h"
#include "ArduinoInput.h"
#include "ScoreLogger.h"

// ************************* Players *************************
#include "ClientAccount.h"
#include "ClientParty.h"
#include "Client.h"
#include "PlayersManager.h"
#include "Player.h"


