/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class Client;

class ClientParty
{
private:
	std::vector<Client *> m_clients;
	int m_activeSong;

public:
	ClientParty();
	~ClientParty();

	void Clear();

	void PushClient(Client *);
	void PopClient(Client *);
	int GetCount();
	Client *GetClientByIndex(int idx);

	void SetActiveSong(int song);
	int GetActiveSong() const;
};