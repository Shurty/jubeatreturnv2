/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class Client;

class Command
{
private:
	CmdID m_id;

public:
	Command(CmdID id);
	~Command();

	/* 
		Reception scheme
		-> Client get packet		[main loop]
		-> Extract CMD id			[main loop]
		-> Fetch CMD from id		[main loop]
		--> Call ReceiveAction		[external]
		---> Call BeforeReceive		[internal]
		---> Call Receive			[internal]
		---> Call AfterReceive		[internal]

		Sending scheme
		-> Get CMD from id												[main loop]
		-> Call SendAction with a sf::Packet containing temporary data	[external]
		--> Call BeforeSend												[internal]
		--> Call Send (recreate a new packet from the prev. one)		[internal]
		--> Call AfterSend												[internal]
	*/

	/* Overridable events */
	virtual void BeforeSend(Client *cl, sf::Packet &pct);
	virtual void AfterSend(Client *cl, sf::Packet &pct);
	virtual void BeforeReceive(Client *cl, sf::Packet &pct);
	virtual void AfterReceive(Client *cl, sf::Packet &pct);

	/* Tech. reception */
	virtual void ReceiveAction(Client *cl, sf::Packet &pct);
	virtual void ReceiveAPIAction(Client *cl, std::stringstream &pct);
	virtual void SendAction(Client *cl, sf::Packet &pct);
	virtual void SendAPIAction(Client *cl, std::stringstream &pct);

	sf::String GetID();
};

