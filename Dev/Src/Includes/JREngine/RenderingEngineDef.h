/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#define MAXTEXANONHANDLES			128

enum ACTID
{
	ACTIVATOR_BLUE,
	ACTIVATOR_MECA,
	ACTIVATOR_JBR,
	MAXACTIVATORS
};

enum TEXID
{
	// activators
	TEX_ACTIVATOR_BLUE,
	TEX_ACTIVATOR_BLUE_END,
	TEX_ACTIVATOR_MECA,
	TEX_ACTIVATOR_MECA_END,
	TEX_ACTIVATOR_JBR,
	TEX_ACTIVATOR_JBR_END,

	// msg
	TEX_MSG_HIT_FORBIDDEN,

	// bg
	TEX_BG_CLOUD,
	TEX_BG_BUBBLE,
	TEX_BG_EXTRACT,

	// containers
	TEX_COVER_BG_BIG,
	TEX_COVER_BG_SMALL,
	TEX_DIFFICULTY_BG,
	TEX_INPUT_PAD,
	TEX_PLAYBOX_BG,
	TEX_COIN,

	// details
	TEX_QRCODE_FEEDBACK,
	TEX_ICON_IMPORTANT,
	TEX_LOGO_TITLE,
	TEX_CLEARED_TEXT,
	TEX_CLEARED_LARGE_TEXT,
	TEX_FAILED_TEXT,
	TEX_FAILED_LARGE_TEXT,

	// difficulty
	TEX_DIFFICULTY_GREEN,
	TEX_DIFFICULTY_ORANGE,
	TEX_DIFFICULTY_RED,
	
	TEX_GM_DIFF_NORMAL,
	TEX_GM_DIFF_HARD,
	TEX_GM_DIFF_INSANE,

	// notes
	TEX_GRADE_E,
	TEX_GRADE_D,
	TEX_GRADE_C,
	TEX_GRADE_B,
	TEX_GRADE_A,
	TEX_GRADE_S,
	TEX_GRADE_SS,
	TEX_GRADE_SSS,
	TEX_GRADE_EXC,

	// buttons
	TEX_START_BTN,
	TEX_START_NOLOGO_BTN,
	TEX_PREV_BTN,
	TEX_NEXT_BTN,
	TEX_NEXT_NOLOGO_BTN,
	TEX_CONFIG_BTN,
	TEX_READYBTN_BTN,
	TEX_MENU_ACHIEVEMENTS_BTN,
	TEX_MENU_STATS_BTN,
	TEX_MENU_DEBUG_BTN,
	TEX_MENU_GAMEMODES_BTN,
	TEX_MENU_RESET_BTN,
	TEX_MENU_FILTERS_BTN,
	TEX_MENU_ACTIVATORS_BTN,
	TEX_MENU_SORTING_BTN,
	TEX_MENU_MODS_BTN,

	// keypad buttons
	TEX_KEYPAD_0,
	TEX_KEYPAD_1,
	TEX_KEYPAD_2,
	TEX_KEYPAD_3,
	TEX_KEYPAD_4,
	TEX_KEYPAD_5,
	TEX_KEYPAD_6,
	TEX_KEYPAD_7,
	TEX_KEYPAD_8,
	TEX_KEYPAD_9,
	TEX_KEYPAD_RET,

	// wordpad buttons
	TEX_WORDPAD_0,
	TEX_WORDPAD_1,
	TEX_WORDPAD_2,
	TEX_WORDPAD_3,
	TEX_WORDPAD_4,
	TEX_WORDPAD_5,
	TEX_WORDPAD_6,
	TEX_WORDPAD_7,
	TEX_WORDPAD_8,
	TEX_WORDPAD_RET,

	NBTEX
};

enum FONTID
{
	FONT_BUTTON_TX,			// GUI Buttons labels
	FONT_TITLE_TX,			// Content titles
	FONT_SUBTITLE_TX,		// Subtitles
	FONT_SCORING_TX,		// Scoring font for score, usernames, combo, ranks and usual numbers
	FONT_TOP_TITLE_TX,		// Top title usually in grey on top of a selection menu ex "Select Music"
	FONT_FIXED_SIZE_TX,		// Fixed font size/width for numbers and letters, usable for timers and counters
	NBFONTS
};