/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "BeatResultDef.h"

struct PList
{
	// Input
	std::string			input_arduino_port;
	bool				input_arduino_connected;
	
	// Gamedata
	unsigned int		player_activeScore;
	unsigned int		player_activeCombo;
	unsigned int		player_musicsCount;
	bool				simulation;

	float				press_delays[BR_MAX_RESULTS];
	float				press_durations[16];
	unsigned int		press_range;
	float				press_img_delay;
	float				press_offset;
	float				press_latency;

	std::string			offline_player_name;

	// Rendering
	unsigned int		window_width;
	unsigned int		window_height;
	unsigned int		debug_window_width;
	unsigned int		debug_window_height;
	bool				debug_window_show;

	int					activator_sprite_width;
	int					activator_sprite_height;
	int					activator_sprite_count;

	float				btn_side_step;
	float				btn_step;
	float				btn_size;
	float				btn_top_offset;

	// JRExe
	bool				test;
	bool				jrexe_valid;
	bool				game_debug;
	bool				ui_debug;
	bool				wav_mode;
};

class Parameters
{
public:
	PList m_params;

	void Reload();
	void LoadGameSettings(std::string const &src);

	Parameters();
	~Parameters();
};