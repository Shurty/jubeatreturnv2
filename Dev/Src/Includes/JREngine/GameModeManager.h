/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class IGamemode;

class GamemodeManager
{
private:
	IGamemode*								m_activeMode;
	IGamemode*								m_futureMode;

	std::vector<GMDifficulty>				m_difficulties;
	std::vector<GMDifficulty>::iterator		m_activeDifficulty;

public:
	std::vector<IGamemode* >				m_gamemodes;
	
	GamemodeManager();
	~GamemodeManager(void);
	void OnInit();
	void InitDifficulties(RenderingEngine* re);

	void ResetDifficulty();
	bool UpDifficulty();
	std::vector<GMDifficulty> const& GetDifficulties();
	GMDifficulty* GetDifficulty();

	void SetMode(GMMODE_ID mdid);
	IGamemode*	currentMode();
	IGamemode*	GetGamemodeFromId(int id);
	void Update();
};