/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum BEATRESULT
{
	BR_MISS,
	BR_BAD,
	BR_GOOD,
	BR_GREAT,
	BR_PERFECT,
	BR_GREAT_AFTER,
	BR_GOOD_AFTER,
	BR_BAD_AFTER,
	BR_BONUS,

	BR_MAX_RESULTS,
};

#define BAD_DELAY			m_params.m_params.press_delays[BR_BAD]
#define GOOD_DELAY			m_params.m_params.press_delays[BR_GOOD]
#define GREAT_DELAY			m_params.m_params.press_delays[BR_GREAT]
#define PERFECT_DELAY		m_params.m_params.press_delays[BR_PERFECT]


