/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrshared.h>

class ACommandPing : public Command
{
private:

public:
	ACommandPing() 
		: Command("PING")
	{

	}

	~ACommandPing()
	{

	}

	/* Client/Server implementations */
	virtual void Receive(Client *cl, bool isapi) = 0;
	virtual void Send(Client *cl, bool isapi) = 0;

	void ReceiveAction(Client *cl, sf::Packet &pckOriginal)
	{
		BeforeReceive(cl, pckOriginal);
		Receive(cl, false);
		AfterReceive(cl, pckOriginal);
	}
	void SendAction(Client *cl, sf::Packet &pct)
	{
		BeforeReceive(cl, pct);
		Send(cl, false);
		AfterReceive(cl, pct);
	}

	void ReceiveAPIAction(Client *cl, std::stringstream &pct)
	{
		Receive(cl, true);
	}

	void SendAPIAction(Client *cl, std::stringstream &pct)
	{
		Send(cl, true);
	}

};
