#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

#include "Player.h"
#include "jrshared.h"

class PlayersManager
{
private:
	// TODO: you don't need every players, load one, compare to others, save it, free it. // DONE, lazy to cause errors by deleting it.. still TODO.
	std::vector<Player> m_players;
	Player* currentPlayer;
	int m_indexCurrentPlayer; // if it's -1 it means he's not present in the scores
	const std::string m_pathToFile; // not ref because well, somehow the passed value was destroyed, fuck it.
	std::vector< std::pair<std::string, unsigned int> >* m_currentLoadedScores;

public:
	PlayersManager(const std::string& pathToFile);
	~PlayersManager(void);

	// free the memory, you'll have to reload stuff !
	void unloadPlayers();
	
	// /!\ caution with this : it removes the files of the previously loaded players so you can't load it again (doesn't free memory, so you could save it again)
	// also, not working for tests, so probably not in program either
	void deleteFiles();

	// load a player and its details from the save files
	Player* loadPlayer(const std::string& playerName);
	void savePlayer(const Player* p);
	void saveMusicRecordForPlayer(const Player::MusicScoreRecord&, const Player* p, const std::wstring& musicname, SONGLEVEL lvl);
	void savePlayers();
	
	// this load scores for music and level directly from file and keep the reference (free previous reference if needed)
	std::vector< std::pair<std::string, unsigned int> >* loadScoresForMusic(const std::wstring& name, SONGLEVEL level);

	// sets current score of player to 0 in the currentLoadedScores, but keeps his previous highscore, so next updates to loaded score will make the player rise and see every other players (including himself)
	void setNewScoreSession();

	unsigned int getCurrentPlayerPositionInScoreSession() const;

	// updates the list we have locally
	void updateCurrentLoadedScoresForPlayer(const std::string& pseudo, unsigned int score);

	const std::vector< std::pair<std::string, unsigned int> >* getCurrentLoadedScores() const;
	/*
		this will reload every highscores for this specific music, and erase previous highscore of the player.
		resulting list saved in file is in descending order.
		(useless to reload everything in local, but will be changed when online so not a big deal)
	*/
	void saveScore(unsigned int score, const Player* p, const std::wstring& name, SONGLEVEL level);
	
	/*
		this will create the player if it doesn't exist :
	*/
	Player* connectPlayer(const std::string& username);
	Player* getPlayer(const std::string& username);
	// TODO: Player* getBetterPlayer(Player* p, const std::wstring& musicName, SONGLEVEL difficulty);
	Player* getCurrentPlayer();

	static int getPlayerPosition(std::vector< std::pair<std::string, unsigned int> > scores, const std::string& pseudo);
};

