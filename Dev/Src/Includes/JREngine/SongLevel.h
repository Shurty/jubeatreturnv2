/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum SONGLEVEL
{
	SL_BASIC,
	SL_INTERMEDIATE,
	SL_EXTREME,
	SL_BONUS,
	SL_EDIT,
	SL_DEBUG,
};
