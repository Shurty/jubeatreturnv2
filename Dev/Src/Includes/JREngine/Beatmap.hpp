/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrshared.h>

#define MUSIC_USE_MEMORY			1

enum BTYPE
{
	BTY_SINGLE,
	BTY_DOUBLE,
	BTY_HOLD,
};

struct Beat
{
	float bpm;				// Current BPM
	int time;				// Corresponding timestamp
	int levelSection;		// Corresponding section
	char activate;			// Button to activate
	BTYPE type;				// Type of beat
	unsigned int duration;

	Beat()
		: bpm(-1), time(0), levelSection(0), activate(0),
		type(BTY_SINGLE), duration(0)
	{

	}
};

struct Beatmap
{
	std::wstring		name;
	std::wstring		author;
	std::string			src;
	int					originalBpm;			// Displayed BPM
	SONGLEVEL			songLevel;				// Level of the beatmap over the same song beatmaps
	int					level;					// Overall level of the beatmap
	int					beats;					// Total beats of the song
	int					delay;					// Time delay of the song and beatmap
	float				speedMul;				// Speed multiplier - Used for mapping when song is 0.5x speed of original (used on export)
	unsigned int		graphStep;				// Graph step for beats calculation
	std::deque<Beat>	beatsList;				// List of beats in chrono. order
	size_t				hash;

	Beatmap()
	{
		speedMul = 1;
	}

	void GenerateHash()
	{
		std::hash<std::string> hasher;

		hash = hasher(src + std::to_string(songLevel));
	}

	void Export(std::string const &target)
	{
		std::ofstream file;
		std::string diffName;

		switch (songLevel)
		{
		case SL_BASIC:
			diffName = "BASIC";
			break;
		case SL_INTERMEDIATE:
			diffName = "INTERMEDIATE";
			break;
		default:
			diffName = "EXTREME";
			break;
		}
		delay = 0;
		file.open(target);
		// Pre-header
		file << std::endl
			<< std::string(name.begin(), name.end()) << std::endl
			<< std::string(author.begin(), author.end()) << std::endl
			<< src << std::endl
			<< std::endl
			<< diffName << std::endl
			<< std::endl
			<< "Level: " << level << std::endl
			<< "BPM: " << originalBpm << std::endl
			<< "Notes: " << beats << std::endl
			<< "Delay: " << delay << std::endl
			<< std::endl
			;
		// Content
		std::for_each(beatsList.begin(), beatsList.end(), [=, &file] (Beat &beat)
		{
			file << (int)beat.activate << " " 
				<< beat.time * speedMul << " "
				<< beat.type << " "
				<< beat.duration * speedMul << " "
				<< std::endl;
		});
		file.close();
	}
};