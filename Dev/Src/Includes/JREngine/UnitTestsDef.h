
#pragma once

#include "jrshared.h"

enum TESTLVL
{
	TLVL_NOTEST,
	TLVL_TEST_ONLY,
	TLVL_UNIT_ONLY,
	TLVL_ALL,
};