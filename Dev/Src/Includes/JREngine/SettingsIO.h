/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

class SettingsIO
{
private:
	std::string m_filename;

public:
	SettingsIO(std::string const &filename);
	SettingsIO();
	~SettingsIO();

	void Open(std::string const &filename);

	// In Actions
	bool GetBool(std::string const &section, std::string const &key);
	int GetInt(std::string const &section, std::string const &key);
	float GetFloat(std::string const &section, std::string const &key);
	std::string GetString(std::string const &section, std::string const &key);
	std::pair<std::string, unsigned int> GetHighScore(std::string const& section,
		std::string const& key);

	// Out Actions
	void Write(std::string const &section, std::string const &key, std::string const &val);

	/*
	template<typename T>
	bool Get<bool>(std::wstring const &section, std::wstring const &key);*/
};