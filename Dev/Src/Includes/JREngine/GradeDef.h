/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum GRADE
{
	GRADE_E = 0,
	GRADE_D = 1,
	GRADE_C = 2,
	GRADE_B = 3,
	GRADE_A = 4,
	GRADE_S = 5,
	GRADE_SS = 6,
	GRADE_SSS = 7,
	GRADE_EXC = 8,
	NUMBER_GRADES = 9
};