/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

class JRBParser : public Parser
{
private:
	std::wifstream bmpFile;
	std::string m_src;
	Beatmap *resBmp;
	
	int ExtractIntFromStr(std::string const &res);

public:
	JRBParser();
	~JRBParser();

	void ParseHeader();
	virtual Beatmap *ParseBeatmap(std::string const &src, bool full = true);

};