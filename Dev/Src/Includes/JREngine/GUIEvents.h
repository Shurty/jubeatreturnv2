/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

// Since we are on a controller based game,
// inputs are simplified to click only
enum GUIEVENT {
	CLICK,				// (int) Called after button id was pressed then released
	PRESSED,			// (int) Called when button id was pressed
	RELEASED,			// (int) Called when button id was released
	TRIGGERED,			// (int) Called when an action from user is required on parameter button
	TRIGGERBEAT,		// (Beat*) Called when a button is triggered (beat button version)
	DRAW,				// (GraphicsController *) Called on game rendering/gui rendering
	UPDATE,				// (sf::Clock *) Called on game update/gui update
	BEATMAP_PRESTART,	// (Beatmap *) Called when the beatmap is about to start, show Ready screen 
	BEATMAP_START,		// (Beatmap *) Called when beatmap starts
	BEATMAP_END,		// (Beatmap *) Called when beatmap finishes
	GAME_SET,			// (Game *) Called when game is loaded and set in core
	SONGLIBRARY_SET,	// (SongLibrary *) Called when song library is loaded and set in core
	IINTERFACABLE_SET,	// (IInterfacable *) Called when the interface for panel actions is set
	BEATMAP_SELECTED,	// (Beatmap *) Called when beatmap is selected in song selection panel
	CONFIGURATION_SET,	// (ConfigurationController *) Called when configuration is loaded
	FIRSTBTN_SET,		// () The button is set as a first press button
};