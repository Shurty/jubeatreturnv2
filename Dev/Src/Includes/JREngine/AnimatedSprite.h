
#pragma once

#include "jrshared.h"

class RenderingEngine;

class AnimatedSprite
{
private:
	sf::Texture* m_tex;
	int m_frameW;
	int m_frameH;
	int m_xFrames;
	int m_yFrames;
	int m_totalFrames;

	// Active frame is relative and should be from 0 to 1
	float m_activeFrame;

	void CalcFrames();

public:
	bool m_isSet;

	AnimatedSprite();

	sf::Texture* GetTexture();
	void Set(sf::Texture *tex, int fW, int fH);
	int GetFramesCount() const;
	float GetActiveFrame() const;
	void SetActiveFrame(float frame);
	void Render(RenderingEngine *target, int x, int y, int w, int h);
};