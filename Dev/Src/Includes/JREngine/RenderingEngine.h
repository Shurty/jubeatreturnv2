/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "Parameters.h"
#include "RenderingEngineDef.h"

class MainWindow;

struct TextureAnonHandle
{
	std::string texName;
	sf::Texture texture;
	bool isSet;
};

class RenderingEngine
{
private:
	sf::RenderWindow*	m_sfmlWnd;
	sf::RenderWindow*	m_debugWnd;
	MainWindow*			m_wnd;

	unsigned int		m_window_width;
	unsigned int		m_window_height;
	TextureAnonHandle	m_anonTextures[MAXTEXANONHANDLES];
	sf::Font			m_fonts[NBFONTS];
	sf::Texture			m_texs[NBTEX];
	AnimatedSprite		m_activators[MAXACTIVATORS];
	AnimatedSprite*		m_activatorTex;

public:
	RenderingEngine(Parameters &pm);
	~RenderingEngine();

	void OnInit(Parameters &pm);
	void OnPreRender(Parameters &pm);
	void OnRender(Parameters &pm);
	void OnExit(Parameters &pm);
	void CreateRenderWindow(Parameters &pm);
	void CreateDebugWindow(Parameters &pm);
	float GetRenderingRatio();

	void SetActivatorTexture(AnimatedSprite* tex);
	AnimatedSprite *GetActivatorTexture(unsigned int id);
	AnimatedSprite &GetActiveActivatorTexture();
	sf::RenderWindow *GetRenderWindow();
	sf::RenderWindow *GetDebugWindow();
	sf::Font &GetFont(FONTID id);
	sf::Texture *GetTexture(TEXID id);
	sf::Texture *GetTexture(std::string const &anon);
};