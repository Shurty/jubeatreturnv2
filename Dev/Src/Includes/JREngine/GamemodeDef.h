/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum GMMODE_ID
{
	GMMD_NONE,
	GMMD_BOMB,
	GMMD_UNLIMITED, // unlimited credits
	GMMD_STANDARD1,	// 1 music standard mode
	GMMD_STANDARD3,	// 3 musics standard mode
	GMMD_STANDARD5, // 5 musics standard mode
};