#pragma once

#include "GradeDef.h"
#include "BeatResultDef.h"
#include <vector>
class ScoreLogger
{
public:
	struct Data {
		Data(BEATRESULT, unsigned int, unsigned int, unsigned int, unsigned int);
		Data();
		BEATRESULT beat;
		unsigned int currentScore;
		unsigned int currentBonus;
		unsigned int predictedScore;
		unsigned int predictedBonus;
	};
	ScoreLogger(void);
	~ScoreLogger(void);

	void	addScore(BEATRESULT beatScored, unsigned int currentScore, unsigned int currentBonus, unsigned int predictedScore, unsigned int predictedBonus);

	// writes a file containing scores fed to the object.
	// first line is number of beats
	// next [numberofbeats] lines will be :
	//  beatScored currentScore currentBonus predictedScore predictedBonus
	// current means after the said beat was scored.
	// to plot with gnuplot you can use: plot "filename" using 1:3, "filename" using 1:4, "filename" using 1:5, "filename" using 1:6
	void	writeFile(const std::string&) const;
	static std::vector<Data>	readFile(const std::string& filepath);
private:
	std::vector<Data> scores;
};

