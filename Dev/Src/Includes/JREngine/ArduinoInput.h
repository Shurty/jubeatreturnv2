/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "jrshared.h"

// Update delay
#define ARDUINO_WAIT_TIME		50
#define ARDUINO_RESET_TIME		2000

class RenderingEngine;

class ArduinoInput : public InputController
{
private:
	//Serial comm handler
    HANDLE hSerial;
    //Connection status
    bool connected;
    //Get various information about the connection
    COMSTAT status;
    //Keep track of last error
    DWORD errors;
	//Arduino Port
	std::string m_port;
	int m_waitDelay;
	int m_resetDelay;

	// Active reception buffer
	std::string m_buffer;

	float m_nextUpdate;
	sf::Event m_activeEvent;
	void (IInterfacable::*m_btnPressedCallback)(int);
	void (IInterfacable::*m_btnReleasedCallback)(int);

	void ParseBuffer(IInterfacable *target);

public:
	ArduinoInput(Parameters &pm);
	~ArduinoInput();

	virtual void SetBtnPressedCallback(void (IInterfacable::*clb)(int));
	virtual void SetBtnReleasedCallback(void (IInterfacable::*clb)(int));

	// Returns the ammount of unhandled events
	virtual int PollEvents(RenderingEngine *wnd, IInterfacable *target);

    //Read data in a buffer, if nbChar is greater than the
    //maximum number of bytes available, it will return only the
    //bytes available. The function return -1 when nothing could
    //be read, the number of bytes actually read.
    int ReadData(char *buffer, unsigned int nbChar);
    //Writes data from a buffer through the Serial connection
    //return true on success.
    bool WriteData(char *buffer, unsigned int nbChar);
    //Check if we are actually connected
    bool IsConnected();
};