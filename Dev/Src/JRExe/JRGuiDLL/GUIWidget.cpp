/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrgui.h>

GUIWidget::GUIWidget(Parameters &pm)
	: m_params(pm), m_linkedKey(0), m_parent(NULL), m_clicked(false), m_pressed(false), m_renderingFunc(NULL)
{
	m_triggered = false;
	m_triggerTime = 0;
	m_userData = NULL;
}

GUIWidget::~GUIWidget()
{

}

void GUIWidget::SetPosition(float x, float y) { m_position.x = x; m_position.y = y; }
void GUIWidget::SetSize(float x, float y) { m_size.x = x; m_size.y = y; }
void GUIWidget::SetPosition(sf::Vector2f pos) { m_position = pos; }
void GUIWidget::SetSize(sf::Vector2f sz) { m_size = sz; }

void GUIWidget::LinkUserData(void *ud) { m_userData = ud; }
void *GUIWidget::GetUserData() const { return(m_userData); }
bool GUIWidget::IsTriggered() const { return(m_triggered); }

sf::Vector2f GUIWidget::GetPosition() const { return (m_position); }
sf::Vector2f GUIWidget::GetSize() const { return (m_size); }

void GUIWidget::SetParent(GUIWidget *res) { m_parent = res; }
GUIWidget *GUIWidget::GetParent() const { return (m_parent); }
void GUIWidget::SetLinkedKey(int key) { m_linkedKey = key; }
int GUIWidget::GetLinkedKey() const { return (m_linkedKey); }

void GUIWidget::RenderAsSelection(RenderingEngine *target, sf::Clock *clk, sf::Vector2f offsets, float alpha) { }

void GUIWidget::Draw(RenderingEngine *target)
{
	if (m_renderingFunc)
	{
		m_renderingFunc(this, target);
	}
	/*
	sf::RectangleShape rectangle(m_size);

	if (m_parent)
		rectangle.setPosition(m_position + m_parent->GetPosition());
	else
		rectangle.setPosition(m_position);

	rectangle.setFillColor(sf::Color(100, 250, 50));
	if (m_triggered) {
		rectangle.setFillColor(sf::Color(250, 50, 50));
	}
	if (m_pressed) {
		rectangle.setFillColor(sf::Color(50, 50, 250));
	}
	target->GetRenderWindow()->draw(rectangle);
	*/
}

void GUIWidget::SetRender(void (*func)(GUIWidget *, RenderingEngine *))
{
	m_renderingFunc = func;
}

void GUIWidget::Update(sf::Clock *t)
{
	if (m_triggered && m_triggerTime == -1) {
		m_triggerTime = t->getElapsedTime().asMilliseconds();
	} else if (m_triggerTime + TRIGGER_LENGTH < t->getElapsedTime().asMilliseconds()) {
		m_triggered = false;
	}
}

void GUIWidget::ReceiveEvent(GUIEVENT ev, void *param)
{
	switch (ev)
	{
	case DRAW:
		Draw((RenderingEngine *)param);
		break;
	case UPDATE:
		Update((sf::Clock *)param);
		break;
	case CLICK:
		OnClick();
		break;
	case PRESSED:
		OnPressed();
		break;
	case RELEASED:
		OnReleased();
		break;
	case TRIGGERED:
		OnTriggered();
		break;
	default:
		break;
	}
}

void GUIWidget::Init()
{

}

void GUIWidget::OnTriggered()
{
	m_triggered = true;
	m_triggerTime = -1;
}

void GUIWidget::OnClick()
{
	m_clicked = true;
}

void GUIWidget::OnPressed()
{
	m_pressed = true;
}

void GUIWidget::OnReleased()
{
	m_pressed = false;
	m_clicked = false;
}
