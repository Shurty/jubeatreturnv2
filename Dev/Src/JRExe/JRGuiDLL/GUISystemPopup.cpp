/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrgui.h>

GUISystemPopup::GUISystemPopup(Parameters &pm, std::string const &msg)
: GUIWidget(pm), m_msg(msg)
{
	m_cb = NULL;
}

GUISystemPopup::~GUISystemPopup()
{
}

void GUISystemPopup::Init()
{
	m_displayClk.restart();
}

void GUISystemPopup::Update(sf::Clock *time)
{

}

void GUISystemPopup::Draw(RenderingEngine *re)
{
	float ratio = re->GetRenderingRatio();
	float sw = m_params.m_params.window_width;
	float sh = m_params.m_params.window_height;

	GUIWidget::Draw(re);

	sf::RectangleShape rect;
	sf::Text text;
	sf::FloatRect textRect;

	text.setString(m_msg);
	text.setFont(re->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 0, 0, 255));
	text.setCharacterSize(24 * ratio);
	textRect = text.getLocalBounds();
	text.setPosition(sw - 10, 10);
	text.setOrigin(textRect.width, 0);

	rect.setSize(sf::Vector2f(textRect.width + 10, textRect.height + 10));

	rect.setOutlineThickness(0);
	rect.setPosition(sw - 5 - rect.getSize().x, 5);
	rect.setOutlineColor(sf::Color(0, 0, 0, 0));
	rect.setFillColor(sf::Color(0, 0, 0, 255));
	re->GetRenderWindow()->draw(rect);

	rect.setOutlineThickness(2);
	rect.setPosition(sw - 5 - rect.getSize().x, 5);
	rect.setOutlineColor(sf::Color(255, 0, 0, 255));
	rect.setFillColor(sf::Color(0, 0, 0, 0));
	re->GetRenderWindow()->draw(rect);

	re->GetRenderWindow()->draw(text);

}

void GUISystemPopup::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
}

bool GUISystemPopup::Expired()
{
	return (m_displayClk.getElapsedTime().asMilliseconds() > 7500);
}