/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrgui.h>

GUIKeyboard::GUIKeyboard(Parameters &pm)
	: GUIWidget(pm), m_pm(pm)
{
	m_sideStep = pm.m_params.btn_side_step;
	m_step = pm.m_params.btn_step;
	//m_step = KB_DEFAULT_STEP;
	for (int i = 0; i < KB_KEYS_COUNT; i++) {
		m_widgets[i] = 0;
	}
}

GUIKeyboard::~GUIKeyboard()
{
	for (int i = 0; i < KB_KEYS_COUNT; i++) {
		if (m_widgets[i]) {
			delete m_widgets[i];
		}
	}
}

void GUIKeyboard::Init(bool autoCalc)
{
	if (!autoCalc)
	{
		float py = (int) (m_position.y);
		float step = 5.0;
		float sz = (m_size.x - step * 5.0) / 4.0;
		//int sz = (int) ((m_size.x - m_step * 3 - m_sideStep * 2) / 4);
		for (int i = 0; i < KB_KEYS_COUNT; i++) {
			if (m_widgets[i]) {
				m_widgets[i]->SetParent(this);
				m_widgets[i]->SetPosition((float) (step + (i % 4) * (sz + step)) + m_position.x,
					(float) (step + py + (i / 4) * (sz + step)));
				m_widgets[i]->SetSize((float) sz, (float) sz);
				m_widgets[i]->SetLinkedKey(i + 1);
			}
		}
	}
	else
	{
		float py = (int) (m_position.y + m_sideStep);
		float sz = m_pm.m_params.btn_size;
		//int sz = (int) ((m_size.x - m_step * 3 - m_sideStep * 2) / 4);
		for (int i = 0; i < KB_KEYS_COUNT; i++) {
			if (m_widgets[i]) {
				m_widgets[i]->SetParent(this);
				m_widgets[i]->SetPosition((float) (m_sideStep + (i % 4) * (sz + m_step)),
					(float) (py + (i / 4) * (sz + m_step)));
				m_widgets[i]->SetSize((float) sz, (float) sz);
				m_widgets[i]->SetLinkedKey(i + 1);
			}
		}
	}
}

GUIWidget* GUIKeyboard::GetWidget(int position)
{
	// Position is an expected valid table index
	if (position >= KB_KEYS_COUNT || position < 0)
		return (NULL);

	// Clear existing widget ?
	/*if (m_widgets[position])
		delete m_widgets[position];
	*/
	return (m_widgets[position]);
}

void GUIKeyboard::SetWidget(int position, GUIWidget *wdg)
{
	// Position is an expected valid table index
	if (position >= KB_KEYS_COUNT || position < 0)
		return ;

	// Clear existing widget ?
	/*if (m_widgets[position])
		delete m_widgets[position];
	*/
	m_widgets[position] = wdg;
}

void GUIKeyboard::Update(sf::Clock *time)
{
	return ;
	GUIWidget::Update(time);
	for (int i = 0; i < KB_KEYS_COUNT; i++) {
		if (m_widgets[i]) {
			m_widgets[i]->Update(time);
		}
	}
}

void GUIKeyboard::Draw(RenderingEngine *target)
{
	return ;
	for (int i = 0; i < KB_KEYS_COUNT; i++) {
		if (m_widgets[i]) {
			m_widgets[i]->Draw(target);
		}
	}
}

void GUIKeyboard::ReceiveEvent(GUIEVENT ev, void *param)
{
	int btn;

	GUIWidget::ReceiveEvent(ev, param);
	if (ev == PRESSED) {
		btn = (int)param;
		if (btn > 0 && btn <= 16 && m_widgets[btn - 1]) {
			m_widgets[btn - 1]->ReceiveEvent(ev, param);
		}
	} else if (ev == RELEASED) {
		btn = (int)param;
		if (btn > 0 && btn <= 16 && m_widgets[btn - 1]) {
			m_widgets[btn - 1]->ReceiveEvent(ev, param);
		}
	} else if (ev == TRIGGERED) {
		btn = (int)param;
		if (btn > 0 && btn <= 16 && m_widgets[btn - 1]) {
			m_widgets[btn - 1]->ReceiveEvent(ev, param);
		}
	} else {
		for (int i = 0; i < KB_KEYS_COUNT; i++) {
			if (m_widgets[i]) {
				m_widgets[i]->ReceiveEvent(ev, param);
			}
		}
	}
}