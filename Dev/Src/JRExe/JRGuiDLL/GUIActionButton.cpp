/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" and "JRGui" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrgui.h>

GUIActionButton::GUIActionButton(Parameters &pm)
	: GUIWidget(pm)
{
	m_cb = NULL;
}

GUIActionButton::~GUIActionButton()
{
}

void GUIActionButton::LinkAction(AGUICallback *cblInst, void (AGUICallback::*clb)(GUIWidget *))
{
	m_cb = cblInst;
	m_cbMethod = clb;
}

void GUIActionButton::Init()
{
}

void GUIActionButton::Update(sf::Clock *time)
{

}

void GUIActionButton::RenderAsSelection(RenderingEngine *target, sf::Clock *clk, 
										sf::Vector2f offsets, float alpha)
{
	sf::RectangleShape rect;
	sf::RenderWindow* window = target->GetRenderWindow();
	float modulation = sinf(clk->getElapsedTime().asMilliseconds() / 250.0f);
	sf::Color color = sf::Color(0, 61 + modulation * 25, 87 + modulation * 35, MIN(255, MAX(0, alpha)));
	sf::Vector2f pos(m_position.x + offsets.x, m_position.y + offsets.y);
	sf::View view(sf::FloatRect(pos.x, pos.y, m_size.x, m_size.y));
	int pad = m_size.x * 0.05;

	view.setViewport(sf::FloatRect(static_cast<float>(pos.x)/window->getSize().x, 
		static_cast<float>(pos.y)/window->getSize().y,
		static_cast<float>(view.getSize().x)/window->getSize().x, 
		static_cast<float>(view.getSize().y)/window->getSize().y));
	target->GetRenderWindow()->setView(view);

	rect.setSize(sf::Vector2f(m_size.x, pad));
	rect.setFillColor(color);
	rect.setPosition(pos.x, pos.y);
	target->GetRenderWindow()->draw(rect);

	rect.setSize(sf::Vector2f(m_size.x, pad));
	rect.setFillColor(color);
	rect.setPosition(pos.x, pos.y + m_size.y - pad);
	target->GetRenderWindow()->draw(rect);

	rect.setSize(sf::Vector2f(pad, m_size.y));
	rect.setFillColor(color);
	rect.setPosition(pos.x, pos.y);
	target->GetRenderWindow()->draw(rect);

	rect.setSize(sf::Vector2f(pad, m_size.y));
	rect.setFillColor(color);
	rect.setPosition(pos.x + m_size.x - pad, pos.y);
	//glScissor(rect.getPosition().x, rect.getPosition().y, rect.getSize().x, rect.getSize().y);
	target->GetRenderWindow()->draw(rect);
	target->GetRenderWindow()->setView(target->GetRenderWindow()->getDefaultView());
}

void GUIActionButton::Draw(RenderingEngine *re)
{
	GUIWidget::Draw(re);
	if (m_pressed) {
		sf::RectangleShape rect(sf::Vector2f(m_size.x - 4, m_size.y - 4));

		rect.setOutlineThickness(2);
		rect.setPosition(m_position.x + 2, m_position.y + 2);
		rect.setOutlineColor(sf::Color(255, 0, 0, 255));
		rect.setFillColor(sf::Color(0, 0, 0, 0));
		re->GetRenderWindow()->draw(rect);
	}
}

void GUIActionButton::OnPressed()
{
	GUIWidget::OnPressed();
	if (m_cb) {
		(m_cb->*m_cbMethod)(this);
	}
}

void GUIActionButton::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
}
