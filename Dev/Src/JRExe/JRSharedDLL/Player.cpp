
#include <JREngine/jrshared.h>

void	Player::MusicScoreRecord::set(SONGLEVEL songlevel, Player::MusicScoreRecord::Details& score)
{
	if (songlevel == SONGLEVEL::SL_BASIC)
		this->m_basic = score;
	else if (songlevel == SONGLEVEL::SL_INTERMEDIATE)
		this->m_intermediate = score;
	else if (songlevel == SONGLEVEL::SL_EXTREME)
		this->m_extreme = score;
}

Player::MusicScoreRecord::Details&	Player::MusicScoreRecord::get(SONGLEVEL songlevel)
{
	if (songlevel == SONGLEVEL::SL_BASIC)
		return this->m_basic;
	else if (songlevel == SONGLEVEL::SL_INTERMEDIATE)
		return this->m_intermediate;
	else if (songlevel == SONGLEVEL::SL_EXTREME)
		return this->m_extreme;
	// I don't like warnings, if this happen that's shit.
	Logger::Log << "ERROR: songlevel unhandled: " << (int)songlevel << " (avoiding error and returning basic level)"<< std::endl;
	return this->m_basic;
}

const Player::MusicScoreRecord::Details&	Player::MusicScoreRecord::const_get(SONGLEVEL songlevel) const
{
	if (songlevel == SONGLEVEL::SL_BASIC)
		return this->m_basic;
	else if (songlevel == SONGLEVEL::SL_INTERMEDIATE)
		return this->m_intermediate;
	else if (songlevel == SONGLEVEL::SL_EXTREME)
		return this->m_extreme;
	Logger::Log << "ERROR: songlevel unhandled: " << (int)songlevel << " (avoiding error and returning basic level)"<< std::endl;
	return this->m_basic;
}

Player::MusicScoreRecord::Details::Details() :
	bestScore(0),
	maxGoodBeat(0),
	maxCombo(0),
	maxPerfect(0),
	timesPlayed(0),
	timesCleared(0),
	totalScore(0),
	timestampHighscore(0),
	timestampLastPlayed(0)
{
}

Player::Player(const std::string& pseudo, unsigned int id)
	: m_pseudo(pseudo)//, m_id(id)
{

}

Player::Player(const Player& p) : m_pseudo(p.m_pseudo), m_scores(p.m_scores)
{

}

Player::Player() : m_pseudo("Shurty")
{

}

Player::~Player()
{
}

const std::string& Player::getPseudo() const
{
	return m_pseudo;
}

unsigned int Player::getHighScoreFor(const std::wstring& music, SONGLEVEL difficulty) const
{
	const MusicScoreRecord* musicScores = NULL;

	std::hash<std::wstring> hashMusicName;
	std::string hashMusic = std::to_string(hashMusicName(music));
	for (std::vector<MusicScoreRecord>::const_iterator it = m_scores.begin(); it != m_scores.end(); it++)
	{
		if (it->m_hashmusicName == hashMusic)
		{
			musicScores = &(*it);
		}
	}
	if (musicScores == NULL)
		return 0;
	return musicScores->const_get(difficulty).bestScore;
}

Player::MusicScoreRecord* Player::getMusicScoreRecordFor(const std::wstring& musicName)
{
	Player::MusicScoreRecord* musicScores = nullptr;

	for (std::vector<Player::MusicScoreRecord>::iterator it = this->m_scores.begin(); it != this->m_scores.end(); it++)
	{
		std::hash<std::wstring> hashMusicName;
		if (it->m_hashmusicName == std::to_string(hashMusicName(musicName)))
		{
			musicScores = &(*it);
		}
	}
	return musicScores;
}

const Player::MusicScoreRecord& Player::updateWithDetails(const MusicScoreRecord::Details& detailsToConsider, const std::wstring& music, SONGLEVEL difficulty)
{
	Player::MusicScoreRecord* musicScores = getMusicScoreRecordFor(music);
	if (musicScores == nullptr)
	{
		this->m_scores.push_back(Player::MusicScoreRecord(music));
		musicScores = &this->m_scores.at(this->m_scores.size() - 1);
	}
	MusicScoreRecord::Details& detailsToUpdate = musicScores->get(difficulty);
	
	auto currentTime = std::time(nullptr);
	if (detailsToUpdate.bestScore < detailsToConsider.bestScore)
	{
		detailsToUpdate.bestScore = detailsToConsider.bestScore;
		detailsToUpdate.timestampHighscore = currentTime;
	}
	if (detailsToUpdate.maxCombo < detailsToConsider.maxCombo)
		detailsToUpdate.maxCombo = detailsToConsider.maxCombo;
	if (detailsToUpdate.maxGoodBeat < detailsToConsider.maxGoodBeat)
		detailsToUpdate.maxGoodBeat = detailsToConsider.maxGoodBeat;
	if (detailsToUpdate.maxPerfect < detailsToConsider.maxPerfect)
		detailsToUpdate.maxPerfect = detailsToConsider.maxPerfect;
	detailsToUpdate.timesCleared += detailsToConsider.timesCleared;
	detailsToUpdate.timesPlayed++;
	detailsToUpdate.timestampLastPlayed = currentTime;
	detailsToUpdate.totalScore += detailsToConsider.bestScore;
	return *musicScores;
}

bool		Player::saveScoreIfHigher(const std::wstring& music, SONGLEVEL songlevel, unsigned int score)
{
	Player::MusicScoreRecord* musicScores = getMusicScoreRecordFor(music);
	if (musicScores == nullptr)
	{
		this->m_scores.push_back(Player::MusicScoreRecord(music));
		musicScores = &this->m_scores.at(this->m_scores.size() - 1);
	}

	MusicScoreRecord::Details& musicScore = musicScores->get(songlevel);
	musicScore.totalScore += score;
	musicScore.timesPlayed++;
	if (ScoreManager::getGradeForFinalScore(score) >= GRADE::GRADE_C)
		musicScore.timesCleared++;
	musicScore.timestampLastPlayed = 0;//std::time(nullptr);
	
	// TODO: these are not accessible here, how to ?
	//musicScore.maxPerfect;
	//musicScore.maxGoodBeat;
	//musicScore.maxCombo;

	if (score >= musicScore.bestScore)
		{
			musicScore.bestScore = score;
			musicScore.timestampHighscore = musicScore.timestampLastPlayed;
			return true;
		}
	return false;
}

Player::Player(const Json::Value& v)
{
	m_pseudo = v["pseudo"].asString();
	/*for (int i = 0 ; i < v["scores"].size(); ++i)
	{
		m_scores.push_back(MusicScoreRecord(v["scores"][i]));
	}*/
}


Json::Value Player::getJsonValue() const {
	Json::Value v;
	v["pseudo"] = m_pseudo;
	int i = 0;
	v["scores"] = Json::Value();
	for (MusicScoreRecord m : m_scores) {
		Logger::Log << "Music: " << m_pseudo << " - " << m.m_hashmusicName << std::endl;
		v["scores"][i++] = m_pseudo + "_score_" + m.m_hashmusicName + ".json";//m.getJsonValue();
	}
	return v;
}

Player::MusicScoreRecord::MusicScoreRecord(const Json::Value& v)
{
	this->m_hashmusicName = v["musicName"].asString();
	this->m_basic = Details(v["basic"]);
	this->m_intermediate = Details(v["intermediate"]);
	this->m_extreme = Details(v["extreme"]);
}

Json::Value Player::MusicScoreRecord::getJsonValue() const {
	Json::Value v;
	v["musicName"] = this->m_hashmusicName;
	v["basic"] = this->m_basic.getJsonValue();
	v["intermediate"] = this->m_intermediate.getJsonValue();
	v["extreme"] = this->m_extreme.getJsonValue();
	return v;
}

Player::MusicScoreRecord::Details::Details(const Json::Value& v)
{
	bestScore = v["bestScore"].asUInt();
	maxGoodBeat = v["maxGoodBeat"].asUInt();
	maxCombo = v["maxCombo"].asUInt();
	maxPerfect = v["maxPerfect"].asUInt();
	timesPlayed = v["timesPlayed"].asUInt();
	timesCleared = v["timesCleared"].asUInt();
	totalScore = v["totalScore"].asUInt();
	timestampHighscore = v["timestampHighscore"].asUInt();
	timestampLastPlayed = v["timestampLastPlayed"].asUInt();
}

Json::Value Player::MusicScoreRecord::Details::getJsonValue() const  {
	Json::Value v;
	v["bestScore"] = bestScore;
	v["maxGoodBeat"] = maxGoodBeat;
	v["maxCombo"] = maxCombo;
	v["maxPerfect"] = maxPerfect;
	v["timesPlayed"] = timesPlayed;
	v["timesCleared"] = timesCleared;
	v["totalScore"] = totalScore;
	v["timestampHighscore"] = timestampHighscore;
	v["timestampLastPlayed"] = timestampLastPlayed;
	return v;
}

bool operator==(const Player& lhs, const Player& rhs) {
	return lhs.getPseudo() == rhs.getPseudo();
}

bool operator!=(const Player& lhs, const Player& rhs) {
	return !(lhs == rhs);
}

bool operator==(const Player& lhs, const std::string& rhs) {
	return lhs.getPseudo() == rhs;
}