/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

RenderingEngine::RenderingEngine(Parameters &pm)
	: m_sfmlWnd(NULL), m_debugWnd(NULL)
{
	m_debugWnd = NULL;
	m_activatorTex = NULL;
	m_sfmlWnd = NULL;
	for (unsigned int i = 0; i < MAXTEXANONHANDLES; i++)
	{
		m_anonTextures[i].isSet = false;
	}
	CreateRenderWindow(pm);
	CreateDebugWindow(pm);
	Logger::Log << "Module construct: RenderingEngine OK" << std::endl;
}

RenderingEngine::~RenderingEngine()
{
}

void RenderingEngine::OnInit(Parameters &pm)
{
	m_fonts[FONT_BUTTON_TX].loadFromFile(FONTSDIR + std::string("ARIALN.TTF")); 
	m_fonts[FONT_TITLE_TX].loadFromFile(FONTSDIR + std::string("ARIALN.TTF")); 
	m_fonts[FONT_SUBTITLE_TX].loadFromFile(FONTSDIR + std::string("ARIALN.TTF")); 
	m_fonts[FONT_SCORING_TX].loadFromFile(FONTSDIR + std::string("Square.ttf")); 
	m_fonts[FONT_TOP_TITLE_TX].loadFromFile(FONTSDIR + std::string("Square.ttf")); 
	m_fonts[FONT_FIXED_SIZE_TX].loadFromFile(FONTSDIR + std::string("HyperspaceBold.ttf")); 
	
	m_texs[TEX_ACTIVATOR_BLUE].loadFromFile(ACTIVATORSDIR + std::string("marker_blue.png"));
	m_texs[TEX_ACTIVATOR_BLUE_END].loadFromFile(ACTIVATORSDIR + std::string("marker_sht_end.png"));
	m_texs[TEX_ACTIVATOR_MECA].loadFromFile(ACTIVATORSDIR + std::string("marker_sht.png"));
	m_texs[TEX_ACTIVATOR_MECA_END].loadFromFile(ACTIVATORSDIR + std::string("marker_sht_end.png"));
	m_texs[TEX_ACTIVATOR_JBR].loadFromFile(ACTIVATORSDIR + std::string("markerReturnV2.5.png"));
	m_texs[TEX_ACTIVATOR_JBR_END].loadFromFile(ACTIVATORSDIR + std::string("marker_sht_end.jpg"));

	m_texs[TEX_MSG_HIT_FORBIDDEN].loadFromFile(TEXTURESDIR + std::string("jubeatHitForbidden.jpg"));

	m_texs[TEX_BG_CLOUD].loadFromFile(TEXTURESDIR + std::string("bgFlare.png"));
	m_texs[TEX_BG_BUBBLE].loadFromFile(TEXTURESDIR + std::string("bgBubble.png"));
	m_texs[TEX_BG_EXTRACT].loadFromFile(TEXTURESDIR + std::string("bgExtract.png"));

	m_texs[TEX_CLEARED_TEXT].loadFromFile(TEXTURESDIR + std::string("resultClearedText.png"));
	m_texs[TEX_CLEARED_LARGE_TEXT].loadFromFile(TEXTURESDIR + std::string("resultClearedTextLarge.png"));
	m_texs[TEX_FAILED_TEXT].loadFromFile(TEXTURESDIR + std::string("resultFailedText.png"));
	m_texs[TEX_FAILED_LARGE_TEXT].loadFromFile(TEXTURESDIR + std::string("resultFailedTextLarge.png"));

	m_texs[TEX_QRCODE_FEEDBACK].loadFromFile(TEXTURESDIR + std::string("qrcodeFeedback.png"));
	m_texs[TEX_LOGO_TITLE].loadFromFile(TEXTURESDIR + std::string("titleStart.png"));
	m_texs[TEX_ICON_IMPORTANT].loadFromFile(TEXTURESDIR + std::string("iconImportant.png"));
	m_texs[TEX_INPUT_PAD].loadFromFile(TEXTURESDIR + std::string("smallCoverBack.png"));
	m_texs[TEX_COIN].loadFromFile(TEXTURESDIR + std::string("coin.png"));

	m_texs[TEX_GRADE_EXC].loadFromFile(TEXTURESDIR + std::string("gradeEXC.png"));
	m_texs[TEX_GRADE_SSS].loadFromFile(TEXTURESDIR + std::string("gradeSSS.png"));
	m_texs[TEX_GRADE_SS].loadFromFile(TEXTURESDIR + std::string("gradeSS.png"));
	m_texs[TEX_GRADE_S].loadFromFile(TEXTURESDIR + std::string("gradeS.png"));
	m_texs[TEX_GRADE_A].loadFromFile(TEXTURESDIR + std::string("gradeA.png"));
	m_texs[TEX_GRADE_B].loadFromFile(TEXTURESDIR + std::string("gradeB.png"));
	m_texs[TEX_GRADE_C].loadFromFile(TEXTURESDIR + std::string("gradeC.png"));
	m_texs[TEX_GRADE_D].loadFromFile(TEXTURESDIR + std::string("gradeD.png"));
	m_texs[TEX_GRADE_E].loadFromFile(TEXTURESDIR + std::string("gradeE.png"));

	m_texs[TEX_COVER_BG_BIG].loadFromFile(TEXTURESDIR + std::string("bigCover.png"));
	m_texs[TEX_COVER_BG_SMALL].loadFromFile(TEXTURESDIR + std::string("smallCoverBack.png"));
	m_texs[TEX_PLAYBOX_BG].loadFromFile(TEXTURESDIR + std::string("smallPreview.png"));
	
	m_texs[TEX_DIFFICULTY_BG].loadFromFile(TEXTURESDIR + std::string("rankBack.png"));
	m_texs[TEX_DIFFICULTY_GREEN].loadFromFile(TEXTURESDIR + std::string("rankGreen.png"));
	m_texs[TEX_DIFFICULTY_ORANGE].loadFromFile(TEXTURESDIR + std::string("rankOrange.png"));
	m_texs[TEX_DIFFICULTY_RED].loadFromFile(TEXTURESDIR + std::string("rankRed.png"));

	m_texs[TEX_GM_DIFF_NORMAL].loadFromFile(TEXTURESDIR + std::string("difficultyNormal.png"));
	m_texs[TEX_GM_DIFF_HARD].loadFromFile(TEXTURESDIR + std::string("difficultyHard.png"));
	m_texs[TEX_GM_DIFF_INSANE].loadFromFile(TEXTURESDIR + std::string("difficultyInsane.png"));

	m_texs[TEX_START_BTN].loadFromFile(TEXTURESDIR + std::string("newStart.png"));
	m_texs[TEX_START_NOLOGO_BTN].loadFromFile(TEXTURESDIR + std::string("newStart2_NoLogo.png"));
	m_texs[TEX_PREV_BTN].loadFromFile(TEXTURESDIR + std::string("newBack.png"));
	m_texs[TEX_NEXT_BTN].loadFromFile(TEXTURESDIR + std::string("newNext.png"));
	m_texs[TEX_NEXT_NOLOGO_BTN].loadFromFile(TEXTURESDIR + std::string("newNext2_NoLogo.png"));
	m_texs[TEX_CONFIG_BTN].loadFromFile(TEXTURESDIR + std::string("newMenu.png"));
	m_texs[TEX_READYBTN_BTN].loadFromFile(TEXTURESDIR + std::string("readyPlayBtn.png"));
	
	m_texs[TEX_WORDPAD_0].loadFromFile(TEXTURESDIR + std::string("keypadABC.png"));
	m_texs[TEX_WORDPAD_1].loadFromFile(TEXTURESDIR + std::string("keypadDEF.png"));
	m_texs[TEX_WORDPAD_2].loadFromFile(TEXTURESDIR + std::string("keypadGHI.png"));
	m_texs[TEX_WORDPAD_3].loadFromFile(TEXTURESDIR + std::string("keypadJKL.png"));
	m_texs[TEX_WORDPAD_4].loadFromFile(TEXTURESDIR + std::string("keypadMNO.png"));
	m_texs[TEX_WORDPAD_5].loadFromFile(TEXTURESDIR + std::string("keypadPQR.png"));
	m_texs[TEX_WORDPAD_6].loadFromFile(TEXTURESDIR + std::string("keypadSTU.png"));
	m_texs[TEX_WORDPAD_7].loadFromFile(TEXTURESDIR + std::string("keypadVWX.png"));
	m_texs[TEX_WORDPAD_8].loadFromFile(TEXTURESDIR + std::string("keypadYZ#.png"));
	m_texs[TEX_WORDPAD_RET].loadFromFile(TEXTURESDIR + std::string("keypadDEL.png"));

	m_texs[TEX_MENU_ACHIEVEMENTS_BTN].loadFromFile(TEXTURESDIR + std::string("menuAch.png"));
	m_texs[TEX_MENU_STATS_BTN].loadFromFile(TEXTURESDIR + std::string("menuStats.png"));
	m_texs[TEX_MENU_DEBUG_BTN].loadFromFile(TEXTURESDIR + std::string("menuDebug.png"));
	m_texs[TEX_MENU_GAMEMODES_BTN].loadFromFile(TEXTURESDIR + std::string("menuExitGM.png"));
	m_texs[TEX_MENU_RESET_BTN].loadFromFile(TEXTURESDIR + std::string("menuReset.png"));
	m_texs[TEX_MENU_FILTERS_BTN].loadFromFile(TEXTURESDIR + std::string("menuFilters.png"));
	m_texs[TEX_MENU_ACTIVATORS_BTN].loadFromFile(TEXTURESDIR + std::string("menuActivators.png"));
	m_texs[TEX_MENU_SORTING_BTN].loadFromFile(TEXTURESDIR + std::string("menuSorting.png"));
	m_texs[TEX_MENU_MODS_BTN].loadFromFile(TEXTURESDIR + std::string("menuMods.png"));

	m_activators[ACTIVATOR_BLUE].Set(&m_texs[TEX_ACTIVATOR_BLUE], 100, 100);
	//m_activators[ACTIVATOR_BLUE_END].Set(&m_texs[TEX_ACTIVATOR_BLUE_END], 100, 100);
	m_activators[ACTIVATOR_MECA].Set(&m_texs[TEX_ACTIVATOR_MECA], 100, 100);
	//m_activators[ACTIVATOR_MECA_END].Set(&m_texs[TEX_ACTIVATOR_MECA_END], 100, 100);
	m_activators[ACTIVATOR_JBR].Set(&m_texs[TEX_ACTIVATOR_JBR], 100, 100);
	//m_activators[ACTIVATOR_JBR_END].Set(&m_texs[TEX_ACTIVATOR_JBR_END], 100, 100);

	SetActivatorTexture(&m_activators[ACTIVATOR_MECA]);

	Logger::Log << "Textures loaded" << std::endl;
	Logger::Log << "Module init: RenderingEngine OK" << std::endl;
	//CreateDebugWindow(pm);
}

void RenderingEngine::SetActivatorTexture(AnimatedSprite* tex)
{
	m_activatorTex = tex;
}

AnimatedSprite& RenderingEngine::GetActiveActivatorTexture()
{
	return (*m_activatorTex);
}

AnimatedSprite* RenderingEngine::GetActivatorTexture(unsigned int id)
{
	if (id < 0 || id > MAXACTIVATORS)
		return (NULL);
	return (&m_activators[id]);
}

void RenderingEngine::OnPreRender(Parameters &pm)
{
	m_sfmlWnd->clear(sf::Color(18, 22, 25, 255));
}

void RenderingEngine::OnRender(Parameters &pm)
{
	m_sfmlWnd->display();
	if (m_debugWnd && pm.m_params.debug_window_show) {
		m_debugWnd->display();
	}
}

void RenderingEngine::OnExit(Parameters &pm)
{
}

void RenderingEngine::CreateRenderWindow(Parameters &pm)
{
	SettingsIO graphicSettings(SETTINGS_DATA_SRC);
	sf::Uint32 cxt = sf::Style::None;

	if (graphicSettings.GetBool("Render", "Borders"))
		cxt = sf::Style::Default;
	Logger::Log << "Creating a window from SFML" << std::endl;
	/* We let SFML generate the window and the view */
	m_sfmlWnd = new sf::RenderWindow(sf::VideoMode(
		graphicSettings.GetInt("Render", "Width"), 
		graphicSettings.GetInt("Render", "Height"), 32
		), graphicSettings.GetString("Render", "Title"), cxt);
	m_sfmlWnd->setVerticalSyncEnabled(true);

	/*m_sfmlWnd->setSize(sf::Vector2u(graphicSettings.GetInt("Render", "Width"), 
		graphicSettings.GetInt("Render", "Height")));
	m_sfmlWnd->setTitle(graphicSettings.GetString("Render", "Title"));
	*/

	m_sfmlWnd->setPosition(sf::Vector2i(
		graphicSettings.GetInt("Render", "PosX"), 
		graphicSettings.GetInt("Render", "PosY")));

	m_window_width = m_sfmlWnd->getSize().x;
	m_window_height = m_sfmlWnd->getSize().y;
	pm.m_params.window_width = m_window_width;
	pm.m_params.window_height = m_window_height;
}

void RenderingEngine::CreateDebugWindow(Parameters &pm)
{
	SettingsIO graphicSettings(SETTINGS_DATA_SRC);

	Logger::Log << "Creating a window from SFML" << std::endl;
	/* We let SFML generate the window and the view */
	m_debugWnd = new sf::RenderWindow(sf::VideoMode(
		graphicSettings.GetInt("Debug", "Width"), 
		graphicSettings.GetInt("Debug", "Height")
		), graphicSettings.GetString("Debug", "Title"), sf::Style::Default);

	m_debugWnd->setSize(sf::Vector2u(graphicSettings.GetInt("Debug", "Width"), 
		graphicSettings.GetInt("Debug", "Height")));
	m_debugWnd->setTitle(graphicSettings.GetString("Debug", "Title"));

	m_debugWnd->setPosition(sf::Vector2i(
		graphicSettings.GetInt("Debug", "PosX"), 
		graphicSettings.GetInt("Debug", "PosY")));

	m_debugWnd->setVisible(pm.m_params.debug_window_show);
	pm.m_params.debug_window_width = m_window_width;
	pm.m_params.debug_window_height = m_window_height;

}

sf::Font &RenderingEngine::GetFont(FONTID id)
{
	return (m_fonts[id]);
}

sf::RenderWindow *RenderingEngine::GetRenderWindow() { return (m_sfmlWnd); }
sf::RenderWindow *RenderingEngine::GetDebugWindow() { return (m_debugWnd); }

sf::Texture *RenderingEngine::GetTexture(TEXID id)
{
	return (&m_texs[id]);
}

float RenderingEngine::GetRenderingRatio()
{
	return (m_window_width / 1080.0);
}

sf::Texture *RenderingEngine::GetTexture(std::string const &anon)
{
	for (unsigned int i = 0; i < MAXTEXANONHANDLES; i++)
	{
		TextureAnonHandle& tex = m_anonTextures[i]; 

		if (tex.isSet == false)
		{
			tex.texName = anon;
			tex.isSet = true;
			tex.texture.loadFromFile(anon);
			return (&tex.texture);
		}
		else if (tex.texName == anon)
		{
			return (&tex.texture);
		}
	}
	Logger::Log << "WARNING: Max annonymous texture handles reached. No tex. returned" << std::endl;
	return (NULL);
}