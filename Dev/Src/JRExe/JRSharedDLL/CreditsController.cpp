/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

CreditsController::CreditsController()
{
	for (int i = 0; i < CRD_COUNT; i++)
	{
		m_credits[i] = 0;
	}
}

CreditsController::~CreditsController()
{

}

CREDVTYPE CreditsController::Value(CREDSID id)
{
	return (m_credits[id]);
}

CREDVTYPE CreditsController::Value(CREDVTYPE value, CREDSID id)
{
	m_credits[id] = value;
	return (Value(id));
}