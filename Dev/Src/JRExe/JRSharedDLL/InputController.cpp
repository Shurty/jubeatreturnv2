/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

int basicInputs[32] = {
	sf::Keyboard::Num1, 
	sf::Keyboard::Num2, 
	sf::Keyboard::Num3, 
	sf::Keyboard::Num4, 
	
	sf::Keyboard::Q, 
	sf::Keyboard::W, 
	sf::Keyboard::E, 
	sf::Keyboard::R, 

	sf::Keyboard::A, 
	sf::Keyboard::S, 
	sf::Keyboard::D, 
	sf::Keyboard::F, 

	sf::Keyboard::Z, 
	sf::Keyboard::X, 
	sf::Keyboard::C, 
	sf::Keyboard::V, 

	sf::Keyboard::Num5, 
	sf::Keyboard::Num6,

	-1 // END
};

InputController::InputController(Parameters &pm)
{
	m_btnPressedCallback = NULL;
	m_btnReleasedCallback = NULL;
	Logger::Log << "Submodule construct: InputController OK" << std::endl;
}

InputController::~InputController()
{

}

void InputController::SetBtnPressedCallback(void (IInterfacable::*clb)(int)) {
	m_btnPressedCallback = clb;
}

void InputController::SetBtnReleasedCallback(void (IInterfacable::*clb)(int)) {
	m_btnReleasedCallback = clb;
}

int InputController::PollEvents(RenderingEngine *gc, IInterfacable *target)
{
	int cnt = 0;
	sf::RenderWindow *wnd = gc->GetRenderWindow();

	while (wnd->pollEvent(m_activeEvent))
	{
		switch (m_activeEvent.type)
		{
		case sf::Event::KeyPressed:
			if (m_btnPressedCallback) {
				for (int i = 0; i < 32 && basicInputs[i] != -1; i++) {
					if (m_activeEvent.key.code == basicInputs[i]) {
						Logger::Log << "Pressed: " << (i + 1) << std::endl;
						(target->*m_btnPressedCallback)(i + 1);
					}
				}
			} else {
				cnt++;
			}
			break;
		case sf::Event::KeyReleased:
			if (m_btnPressedCallback) {
				for (int i = 0; i < 32 && basicInputs[i] != -1; i++) {
					if (m_activeEvent.key.code == basicInputs[i]) {
						Logger::Log << "Released: " << (i + 1) << std::endl;
						(target->*m_btnReleasedCallback)(i + 1);
					}
				}
			} else {
				cnt++;
			}
			break;
		default:
			cnt++;
		}
	}
	return (cnt);
}