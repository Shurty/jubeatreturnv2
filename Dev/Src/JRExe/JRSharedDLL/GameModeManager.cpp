/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

GamemodeManager::GamemodeManager()
{
	m_activeMode = NULL;
	m_futureMode = NULL;
	Logger::Log << "Module construct: GamemodeManager OK" << std::endl;
}

GamemodeManager::~GamemodeManager(void)
{
	while (!m_gamemodes.empty())
	{
		m_gamemodes.back()->OnUnload();
		delete m_gamemodes.back();
		m_gamemodes.pop_back();
	}
}

void	GamemodeManager::OnInit()
{
	// TODO: read params ?

	Logger::Log << "Module init: GamemodeManager OK" << std::endl;
}

void GamemodeManager::InitDifficulties(RenderingEngine* re)
{
	m_difficulties.push_back(GMDifficulty(re->GetTexture(TEX_GM_DIFF_NORMAL), "Normal", "Data/config_normal.ini"));
	m_difficulties.push_back(GMDifficulty(re->GetTexture(TEX_GM_DIFF_HARD), "Hard", "Data/config_hard.ini"));
	m_difficulties.push_back(GMDifficulty(re->GetTexture(TEX_GM_DIFF_INSANE), "Insane", "Data/config_insane.ini"));
	m_activeDifficulty = m_difficulties.begin();
}

void GamemodeManager::SetMode(GMMODE_ID mdid)
{
	IGamemode *st = *std::find_if(m_gamemodes.begin(), m_gamemodes.end(),
		[&mdid] (IGamemode *stm) -> bool {
			if (stm->GetId() == mdid)
				return (true);
			return (false);
	}
	);

	if (st == NULL)
		return ;
	if (m_activeMode == NULL) {
		m_activeMode = st;
		m_activeMode->OnSetActive();
	} else {
		m_futureMode = st;
	}
}

IGamemode*	GamemodeManager::currentMode()
{
	return m_activeMode;
}

void GamemodeManager::Update()
{
	if (m_activeMode == NULL)
		return ;
	if (m_activeMode->IsActive())
	{
		m_activeMode->Update();
	}
	if (m_futureMode) {
		m_activeMode = m_futureMode;
		m_futureMode = NULL;
		m_activeMode->OnSetActive();
	}
}

IGamemode* GamemodeManager::GetGamemodeFromId(int id)
{
	if (id < 0 || id >= m_gamemodes.size())
		return (NULL);
	return (m_gamemodes[id]);
}

void GamemodeManager::ResetDifficulty()
{
	m_activeDifficulty = m_difficulties.begin();
}

bool GamemodeManager::UpDifficulty()
{
	if (m_activeDifficulty == (--m_difficulties.end()))
		return (false);
	++m_activeDifficulty;
	return (true);
}

std::vector<GMDifficulty> const& GamemodeManager::GetDifficulties()
{
	return (m_difficulties);
}

GMDifficulty* GamemodeManager::GetDifficulty()
{
	return (&(*m_activeDifficulty));
}