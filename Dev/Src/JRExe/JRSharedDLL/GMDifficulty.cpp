/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

GMDifficulty::GMDifficulty(sf::Texture* tex, std::string const &name, std::string const &src)
	: m_assocTexture(tex), m_name(name), m_cfgSrc(src)
{

}

GMDifficulty::~GMDifficulty()
{

}

std::string GMDifficulty::GetName()
{
	return (m_name);
}

sf::Texture* GMDifficulty::GetTexture()
{
	return (m_assocTexture);
}

void GMDifficulty::LoadConfig(Parameters* params)
{
	params->LoadGameSettings(m_cfgSrc);
}

