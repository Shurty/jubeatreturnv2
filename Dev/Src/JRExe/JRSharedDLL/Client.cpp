/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

Client::Client(int id)
: m_logged(false), m_valid(true), m_id(id)
{
	m_connected = false;
	m_sock = new sf::TcpSocket();
	m_sock->setBlocking(false);
}

Client::~Client()
{
	if (m_sock)
		delete m_sock;
}

void Client::OnConnect()
{
	/* Setup connection state and reset timeout timer */
	m_connected = true;
	m_valid = false;
	m_tOutClk.restart();
	//m_sock->send(sf::Packet() << "1");
}

void Client::OnMatchmakingBegin()
{

}

void Client::SetMatchmaking(int music)
{

}

void Client::SetHashCode(std::string const &str)
{
	m_hash = str;
}
std::string const &Client::GetHashcode()
{
	return (m_hash);
}

ClientParty *Client::GetMatchmakingParty() { return (&m_matchmaking); }
int Client::GetId() const { return (m_id); }
sf::TcpSocket &Client::GetSock() { return (*m_sock); }
sf::Clock &Client::GetTimeoutClock() { return (m_tOutClk); }
void Client::MarkAsDisconnected() { m_valid = false; m_connected = false; }
void Client::SetValid() { m_valid = true; };
bool Client::IsValid() const { return (m_valid); }
bool Client::IsConnected() const { return (m_connected); }
ClientAccount &Client::GetAccountData() { return (m_account); }