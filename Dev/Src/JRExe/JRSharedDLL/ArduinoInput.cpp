/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

#ifdef WIN32
ArduinoInput::ArduinoInput(Parameters &pm)
	: InputController(pm)
{
	SettingsIO sett(SETTINGS_DATA_SRC);

	m_resetDelay = sett.GetInt("Arduino", "ResetDelay");
	m_waitDelay = sett.GetInt("Arduino", "WaitDelay");
	m_port = sett.GetString("Arduino", "Port");
	pm.m_params.input_arduino_port = m_port;
	m_btnPressedCallback = NULL;
	m_btnReleasedCallback = NULL;
	m_nextUpdate = 0;

	//We're not yet connected
    this->connected = false;

    //Try to connect to the given port through CreateFile
    this->hSerial = CreateFile(m_port.c_str(),
            GENERIC_READ | GENERIC_WRITE,
            0,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            NULL);

	//Check if the connection was successful
    if(this->hSerial==INVALID_HANDLE_VALUE)
    {
        //If not success full display an Error
        if(GetLastError()==ERROR_FILE_NOT_FOUND){

            //Print Error if neccessary
			Logger::Log << "ERROR: Handle was not attached. Reason: " << m_port <<
				"not available." << std::endl;

        }
        else
        {
			Logger::Log << "ERROR: Handle was not attached. Reason: Other" << std::endl;
        }
    }
    else
    {
        //If connected we try to set the comm parameters
        DCB dcbSerialParams = {0};

        //Try to get the current
        if (!GetCommState(this->hSerial, &dcbSerialParams))
        {
            //If impossible, show an error
			Logger::Log << "ERROR: Handle was not attached. Reason: Failed to get params" << std::endl;
        }
        else
        {
            //Define serial connection parameters for the arduino board
            dcbSerialParams.BaudRate=CBR_9600;
            dcbSerialParams.ByteSize=8;
            dcbSerialParams.StopBits=ONESTOPBIT;
            dcbSerialParams.Parity=NOPARITY;

             //Set the parameters and check for their proper application
             if(!SetCommState(hSerial, &dcbSerialParams))
             {
				 Logger::Log << "Warning: Could not set Serial Port parameters" << std::endl;
             }
             else
             {
                 //If everything went fine we're connected
                 this->connected = true;
                 //We wait 2s as the arduino board will be reseting
                 Sleep(m_resetDelay);
				 Logger::Log << "Successfully connected to Ardunino board" << std::endl;
			 }
        }
    }
	Logger::Log << "Submodule construct: ArduinoInput OK" << std::endl;
	pm.m_params.input_arduino_connected = this->connected;
}

ArduinoInput::~ArduinoInput()
{
	if(this->connected)
    {
        //We're no longer connected
        this->connected = false;
		//Close the serial handler
        CloseHandle(this->hSerial);
    }
}

void ArduinoInput::SetBtnPressedCallback(void (IInterfacable::*clb)(int)) {
	m_btnPressedCallback = clb;
	InputController::SetBtnPressedCallback(clb);
}

void ArduinoInput::SetBtnReleasedCallback(void (IInterfacable::*clb)(int)) {
	m_btnReleasedCallback = clb;
	InputController::SetBtnReleasedCallback(clb);
}

int ArduinoInput::PollEvents(RenderingEngine *gc, IInterfacable *target)
{
	char actions[255];
	int cnt = 0;
	sf::RenderWindow *wnd = gc->GetRenderWindow();

	if (!connected)
		return (-1);
	if (m_nextUpdate > clock())
		return (0);
	m_nextUpdate = (float) (clock() + m_waitDelay);

	ClearCommError(this->hSerial, &this->errors, &this->status);

	if(this->status.cbInQue>0)
    {
		memset(actions, 0, 255);
		ReadData(actions, 255);
		//Logger::Log << "Arduino Read success: " << actions << std::endl;
		m_buffer += actions;
		ParseBuffer(target);
	}
	InputController::PollEvents(gc, target);
	return (cnt);
}

void ArduinoInput::ParseBuffer(IInterfacable *target)
{
	std::stringstream ss;
	char op;
	std::stringstream result;
	int val = 0;

	Logger::Log << "Buffer content: " << m_buffer << std::endl;
	ss << m_buffer;
	ss >> op >> val;
	//Logger::Log << "State content: " << m_buffer << std::endl;
	if ((op != '+' && op != '-') || val <= 1 || val >= 18) {
		// Invalid or incomplete operation
		return ;
	}
	if (val == 0)
		return;
	// Correcting the 2 pin (reserved 0-1) difference
	val = val - 1;
	
	// Correcting the pin positions matrix
	if (val <= 12) {
		val = 12 - val + 4;
	} else {
		val = 3 - (val - 13);
	}
	val = (val % 4) * 4 + (val / 4) + 1;
	

	if (op == '-') {
		(target->*m_btnReleasedCallback)(val);
		Logger::Log << "Input release action on key " << val << std::endl;
	} else {
		(target->*m_btnPressedCallback)(val);
		Logger::Log << "Input press action on key " << val << std::endl;
	}

	result << ss.rdbuf();
	m_buffer = result.str();
	ParseBuffer(target);
}

int ArduinoInput::ReadData(char *buffer, unsigned int nbChar)
{
    //Number of bytes we'll have read
    DWORD bytesRead;
    //Number of bytes we'll really ask to read
    unsigned int toRead;

    //Use the ClearCommError function to get status info on the Serial port
    

    //Check if there is something to read
    if(this->status.cbInQue>0)
    {
        //If there is we check if there is enough data to read the required number
        //of characters, if not we'll read only the available characters to prevent
        //locking of the application.
        if(this->status.cbInQue>nbChar)
        {
            toRead = nbChar;
        }
        else
        {
            toRead = this->status.cbInQue;
        }

        //Try to read the require number of chars, and return the number of read bytes on success
        if(ReadFile(this->hSerial, buffer, toRead, &bytesRead, NULL) && bytesRead != 0)
        {
            return bytesRead;
        }

    }

    //If nothing has been read, or that an error was detected return -1
    return -1;

}


bool ArduinoInput::WriteData(char *buffer, unsigned int nbChar)
{
    DWORD bytesSend;

    //Try to write the buffer on the Serial port
    if(!WriteFile(this->hSerial, (void *)buffer, nbChar, &bytesSend, 0))
    {
        //In case it don't work get comm error and return false
        ClearCommError(this->hSerial, &this->errors, &this->status);

        return false;
    }
    else
        return true;
}

bool ArduinoInput::IsConnected()
{
    //Simply return the connection status
    return this->connected;
}
#else

ArduinoInput::ArduinoInput(Parameters &pm)
: InputController(pm)
{
	SettingsIO sett(SETTINGS_DATA_SRC);

	m_resetDelay = sett.GetInt("Arduino", "ResetDelay");
	m_waitDelay = sett.GetInt("Arduino", "WaitDelay");
	m_port = sett.GetString("Arduino", "Port");
	pm.m_params.input_arduino_port = m_port;
	m_btnPressedCallback = NULL;
	m_btnReleasedCallback = NULL;
	m_nextUpdate = 0;

	//We're not yet connected
	this->connected = false;

	Logger::Log << "ERROR: Handle was not attached. Reason: Other" << std::endl;
}

ArduinoInput::~ArduinoInput()
{
	if (this->connected)
	{
		//We're no longer connected
		this->connected = false;
	}
}

void ArduinoInput::SetBtnPressedCallback(void (IInterfacable::*clb)(int)) {
	m_btnPressedCallback = clb;
	InputController::SetBtnPressedCallback(clb);
}

void ArduinoInput::SetBtnReleasedCallback(void (IInterfacable::*clb)(int)) {
	m_btnReleasedCallback = clb;
	InputController::SetBtnReleasedCallback(clb);
}

int ArduinoInput::PollEvents(RenderingEngine *gc, IInterfacable *target)
{
	char actions[255];
	int cnt = 0;
	sf::RenderWindow *wnd = gc->GetRenderWindow();

	if (!connected)
		return (-1);
	if (m_nextUpdate > clock())
		return (0);
	m_nextUpdate = (float)(clock() + m_waitDelay);

	return (cnt);
}

void ArduinoInput::ParseBuffer(IInterfacable *target)
{
	
}

int ArduinoInput::ReadData(char *buffer, unsigned int nbChar)
{
	return -1;

}


bool ArduinoInput::WriteData(char *buffer, unsigned int nbChar)
{
	return true;
}

bool ArduinoInput::IsConnected()
{
	//Simply return the connection status
	return this->connected;
}
#endif