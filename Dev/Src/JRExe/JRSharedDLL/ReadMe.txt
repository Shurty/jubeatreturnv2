/////////////////////////////////////////////////////////////////////////////
//							Jubeat Return Engine						   //
/////////////////////////////////////////////////////////////////////////////

I. DESCRIPTION
/////////////////////////////////////////////////////////////////////////////

JREngine is a modular game engine.

II. USAGE
/////////////////////////////////////////////////////////////////////////////

Please check the generated documentation

III. LICENSE
/////////////////////////////////////////////////////////////////////////////

Use, disclosure, or copying of all sources, headers or files 
under this project named "JREngine" is strictly prohibited without written 
consent.

The following list of ressources are also copyrighted with the same
prohibitions for the project "JREngine" :
- All elements included in "Data/Textures/" and not in a sub-folder
- All text files in "Data/Beatmaps" in format: .txt, .bpm, .jrb, .jbn, .jrm
- All source information files related to the project

Ressources, textures, sounds, fonts, images that are under license-free,
GPL, CC or any external copyright action are free to use under their
related ressources. "JREngine" staff cannot be held responsible for the use
of these ressources.

The following rights are given for complete project use, modification,
copyrights modifications, licensing, copying, disclosure, publication of any
file related to the project or its ressources :		 
	Copyright Jeremy DUBUC 2014-2015. All rights reserved.

The following copyrights are given for complete project use and modification,
with the prohibition of copying, distributing or releasing any file related
to the project or its ressources :
	Copyright Thierry BERGER 2014-2015. Some rights reserved.
	Copyright Quentin GUAY 2014-2015. Some rights reserved.
	Copyright Timothee MAURIN 2014-2015. Some rights reserved.

Please check LICENSE.TXT for additional license informations.

IV. NOTES
/////////////////////////////////////////////////////////////////////////////

No notes available for now.
