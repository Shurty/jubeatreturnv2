#include <JREngine/jrshared.h>
#include <tuple>
#include <string>
#include <functional>

#ifdef WIN32
#include <windows.h>
#else
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif
PlayersManager::PlayersManager(const std::string& pathToFile) : m_pathToFile(pathToFile)
{
	Logger::Log << "Module construct: PlayersManager OK" << std::endl;
	m_players.clear();
	m_currentLoadedScores = nullptr;
	m_indexCurrentPlayer = -1;
}


PlayersManager::~PlayersManager(void)
{
}

void PlayersManager::unloadPlayers()
{
	this->currentPlayer = nullptr;
	m_players.clear();
}

/* Returns a list of files in a directory (except the ones that begin with a dot) */

void GetFilesInDirectory(std::vector<std::string> &out, const std::string &directory)
{
#ifdef WIN32
    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/*").c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return; /* No files found */

    do {
        const std::string file_name = file_data.cFileName;
        const std::string full_file_name = directory + "/" + file_name;
        const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

        if (file_name[0] == '.')
            continue;

        if (is_directory)
            continue;

        out.push_back(full_file_name);
    } while (FindNextFile(dir, &file_data));

    FindClose(dir);
#else
    DIR *dir;
	struct dirent *ent;
	struct stat st;

    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != NULL) {
        const std::string file_name = ent->d_name;
		const std::string full_file_name = directory + "/" + file_name;

        if (file_name[0] == '.')
            continue;

        if (stat(full_file_name.c_str(), &st) == -1)
            continue;

        const bool is_directory = (st.st_mode & S_IFDIR) != 0;

        if (is_directory)
            continue;

        out.push_back(full_file_name);
    }
    closedir(dir);
#endif
} // GetFilesInDirectory

void PlayersManager::deleteFiles()
{
	
}

Json::Value readJson(const std::string& f) {
	Logger::Log << "loadjson file: " << f << std::endl;
	std::ifstream file(f);

	if (!file)
	{
		std::ofstream file_(f);
		file_ << "";
		file_.close();
		file.close();
		Logger::Log << "failed opening file '" + f + "': creating and returning empty." << std::endl;
		return Json::Value();
	}
	Json::Value root;   // will contains the root value after parsing
	Json::Reader reader;
	bool parsingSuccessful = reader.parse(file, root );
	file.close();
	if ( !parsingSuccessful )
	{
	 // report to the user the failure and their locations in the document.
		Logger::Log  << "Failed to parse configuration\n"
		           << reader.getFormattedErrorMessages() << std::endl;
		 return Json::Value();
	}

	return root;
}

Player* PlayersManager::loadPlayer(const std::string& playerName)
{
	std::string player_path(m_pathToFile + "" + playerName + ".json");
	Json::Value playerJson = readJson(player_path);
	Player* p = nullptr;

	if (playerJson.empty() == false) {
		p = new Player(playerJson);
		Json::ValueIterator itr = playerJson["scores"].begin();
		while (itr != playerJson["scores"].end())
		{
			std::string playerScore_path(m_pathToFile + "" + playerName + "_" + itr.key().asString() + ".json");

			Json::Value musicRecordJson = readJson(playerScore_path);
			p->m_scores.push_back(Player::MusicScoreRecord(musicRecordJson));
			++itr;
		}
	}
	return p;
}

void writeJson(Json::Value& v, const std::string& f)
{
	Json::StyledWriter writer;
	std::ofstream file(f);

	if (!file)
	{
		std::ofstream file_(f);
		file_ << "";
		file_.close();
		file.open(f);
		Logger::Log << "failed opening file '" << f << "': creating..." << std::endl;
	}
	/*
		std::ios_base::iostate st = file.rdstate();
		st;
		Logger::Log << "failed opening file '" << f << "': json not saved." << std::endl;
		return;
		
	}
	*/
	writer.write( v );
	file << v;
	file.close();
	Logger::Log << f << std::endl;
//	Logger::Log << v.asString() << std::endl;
}

void PlayersManager::savePlayer(const Player* p)
{
	Json::Value playerJson = p->getJsonValue();
	playerJson["scores"] = Json::Value();
	int i = 0;
	for (Player::MusicScoreRecord m : p->m_scores) {
		Json::Value musicRecordJson = m.getJsonValue();
		std::hash<std::wstring> hashMusicName;
		std::string playerScore_path(m_pathToFile + p->m_pseudo + "_" + m.m_hashmusicName + ".json");
		playerJson["scores"][m.m_hashmusicName] = playerScore_path;
		writeJson(musicRecordJson, playerScore_path);
	}
	std::string player_path(m_pathToFile + p->m_pseudo + ".json");
	writeJson(playerJson, player_path);
}

void PlayersManager::saveMusicRecordForPlayer(const Player::MusicScoreRecord& m, const Player* p, const std::wstring& musicname, SONGLEVEL lvl)
{
	Json::Value playerJson = p->getJsonValue();
	playerJson["scores"] = Json::Value();
	for (Player::MusicScoreRecord m : p->m_scores) {
		Json::Value musicRecordJson = m.getJsonValue();
		std::string playerScore_path(m_pathToFile + p->m_pseudo + "_" + m.m_hashmusicName + ".json");
		playerJson["scores"][m.m_hashmusicName] = playerScore_path;
	}
	Json::Value musicRecordJson = m.getJsonValue();
	std::hash<std::wstring> hashMusicName;
	std::string playerScore_path(m_pathToFile + p->m_pseudo + "_" + m.m_hashmusicName + ".json");
	playerJson["scores"][m.m_hashmusicName] = playerScore_path;
	writeJson(musicRecordJson, playerScore_path);
	std::string player_path(m_pathToFile + p->m_pseudo + ".json");
	writeJson(playerJson, player_path);

	const Player::MusicScoreRecord::Details& details = m.const_get(lvl);
	bool shouldSaveHighScore = details.timestampHighscore == details.timestampLastPlayed;
	if (shouldSaveHighScore)
	{
		saveScore(details.bestScore, p, musicname, lvl);
	}
}

void PlayersManager::savePlayers()
{
	Json::Value root;   // will contains the root value after parsing

	for (Player p : m_players)
	{
		Json::Value playerJson = p.getJsonValue();
		for (Player::MusicScoreRecord m : p.m_scores) {
			Json::Value musicRecordJson = m.getJsonValue();
			 // TODO: hash for musicname !
			std::string playerScore_path(m_pathToFile + p.m_pseudo + "_" + m.m_hashmusicName + ".json");
			playerJson["scores"] = Json::Value();
			playerJson["scores"][m.m_hashmusicName] = playerScore_path;
			writeJson(musicRecordJson, playerScore_path);
		}
		std::string player_path(m_pathToFile + p.m_pseudo + ".json");
		root["players"][p.m_pseudo] = player_path;
		writeJson(playerJson, player_path);
	}
	std::string root_path(m_pathToFile + "scores.json");

	writeJson(root, root_path);
}

std::vector<std::pair<std::string, unsigned int>>* PlayersManager::loadScoresForMusic(const std::wstring& name, SONGLEVEL level)
{
	if (m_currentLoadedScores != nullptr)
	{
		m_currentLoadedScores->clear();
		delete(m_currentLoadedScores);
		m_indexCurrentPlayer = -1;
	}
	std::hash<std::wstring> hashMusicName;
	std::string path(m_pathToFile + "/score_" + std::to_string(hashMusicName(name)) + "_" + std::to_string(level) + ".json");

	Json::Value root = readJson(path);

	std::vector<std::pair<std::string, unsigned int>>* v = new std::vector<std::pair<std::string, unsigned int>>();
	int i = 0;
	for (Json::Value score : root["scores"]) {
		std::cout << score << std::endl;
//		Logger::Log << score.asString() << std::endl;
		Json::Value::UInt t = score["score"].asUInt();
		std::string pseudo = score["pseudo"].asString();
		Logger::Log << pseudo << ": " << (int)t << std::endl;
		v->push_back(std::make_pair<const std::string&, unsigned int>(pseudo, score["score"].asUInt()));
		if (currentPlayer->getPseudo() == pseudo)
		{
			m_indexCurrentPlayer = i;
		}
		i++;
	}
	m_currentLoadedScores = v;
	return v;
}

void PlayersManager::setNewScoreSession()
{
	m_indexCurrentPlayer = m_currentLoadedScores->size();
	m_currentLoadedScores->push_back(std::make_pair<std::string, unsigned int>(std::string(currentPlayer->getPseudo()), 0));
}

unsigned int PlayersManager::getCurrentPlayerPositionInScoreSession() const
{
	return m_indexCurrentPlayer;
}

void PlayersManager::updateCurrentLoadedScoresForPlayer(const std::string& pseudo, unsigned int score)
{
	if (m_indexCurrentPlayer == -1)
	{
		m_indexCurrentPlayer = m_currentLoadedScores->size();
		m_currentLoadedScores->push_back(std::make_pair<std::string, unsigned int>(std::string(pseudo), 0));
	}
	(*m_currentLoadedScores)[m_indexCurrentPlayer].second = score;
	while (m_indexCurrentPlayer > 0 && // currentPlayer is not the best
		(*m_currentLoadedScores)[m_indexCurrentPlayer].second > (*m_currentLoadedScores)[m_indexCurrentPlayer -1].second) // currentPlayer is better than the one stored before him.
	{
		const std::string& pseudoOtherPlayer = (*m_currentLoadedScores)[m_indexCurrentPlayer -1].first;
		unsigned int scoreOtherPlayer = (*m_currentLoadedScores)[m_indexCurrentPlayer -1].second;
		(*m_currentLoadedScores)[m_indexCurrentPlayer - 1].first = pseudo;
		(*m_currentLoadedScores)[m_indexCurrentPlayer - 1].second = score;
		(*m_currentLoadedScores)[m_indexCurrentPlayer].first = pseudoOtherPlayer;
		(*m_currentLoadedScores)[m_indexCurrentPlayer].second = scoreOtherPlayer;
		m_indexCurrentPlayer--;
	}
}

const std::vector< std::pair<std::string, unsigned int> >* PlayersManager::getCurrentLoadedScores() const
{
	return m_currentLoadedScores;
}

void PlayersManager::saveScore(unsigned int score, const Player* p, const std::wstring& name, SONGLEVEL level)
{
	// Reload everything in case something has changed (network?)
	auto scores = this->loadScoresForMusic(name, level);
	Json::Value scoresForMusic;

	bool saved = false;
	unsigned int i = 0;
	unsigned int j = 0;
	while (i < scores->size()) {
		std::pair<std::string, unsigned int>& s = (*scores)[i++];
		if (std::get<0>(s) == p->getPseudo())
		{
			continue;
		}
		if (std::get<1>(s) < score && !saved)
		{
			scoresForMusic["scores"][j]["pseudo"] = p->getPseudo();
			scoresForMusic["scores"][j++]["score"] = score;
			saved = true;
		}
		scoresForMusic["scores"][j]["pseudo"] = std::get<0>(s);
		scoresForMusic["scores"][j++]["score"] = std::get<1>(s);
	}
	if (!saved) {
		scoresForMusic["scores"][j]["pseudo"] = p->getPseudo();
		scoresForMusic["scores"][j++]["score"] = score;
	}
	std::hash<std::wstring> hashMusicName;
	std::string path(m_pathToFile + "/score_" + std::to_string(hashMusicName(name)) + "_" + std::to_string(level) + ".json");
	writeJson(scoresForMusic, path);
}

Player* PlayersManager::connectPlayer(const std::string& username)
{
	Player* p = loadPlayer(username);
	if (p != nullptr) {
		currentPlayer = p;
		return p;
	}
	else
	{
		m_players.push_back(Player(username, m_players.size()));
		currentPlayer = &(m_players.back());
		// FIXME: for networking we should notify database that we created a new player
	}
	// not worth to save now (probably later tell the server if it's a new user..?)
	// savePlayers();
	return currentPlayer;
}
Player* PlayersManager::getPlayer(const std::string& username)
{
	auto p = std::find(m_players.begin(), m_players.end(), username);
	if (p == m_players.end())
		return nullptr;
	return &(*p);	
}

Player* PlayersManager::getCurrentPlayer()
{
	return currentPlayer;
}

int PlayersManager::getPlayerPosition(std::vector< std::pair<std::string, unsigned int> > scores, const std::string& pseudo)
{
	int i = 0;
	for (auto score : scores)
	{
		if (score.first == pseudo)
			return i;
		++i;
	}
	return -1;
}