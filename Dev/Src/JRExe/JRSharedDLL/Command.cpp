/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

Command::Command(CmdID id)
	: m_id(id)
{

}

Command::~Command()
{

}

void Command::ReceiveAction(Client *cl, sf::Packet &pct)
{
	BeforeReceive(cl, pct);
}

void Command::SendAction(Client *cl, sf::Packet &pct)
{
	BeforeSend(cl, pct);
}

void Command::ReceiveAPIAction(Client *cl, std::stringstream &pct)
{

}

void Command::SendAPIAction(Client *cl, std::stringstream &pct)
{

}

void Command::BeforeSend(Client *cl, sf::Packet &pct) { }
void Command::AfterSend(Client *cl, sf::Packet &pct) { }
void Command::BeforeReceive(Client *cl, sf::Packet &pct) { }
void Command::AfterReceive(Client *cl, sf::Packet &pct) { }

sf::String Command::GetID() { return (m_id); }