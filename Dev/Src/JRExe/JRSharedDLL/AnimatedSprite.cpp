
#include <JREngine/jrshared.h>

void AnimatedSprite::CalcFrames()
{
	int texW = m_tex->getSize().x;
	int texH = m_tex->getSize().y;

	m_xFrames = MAX(1, floor((float)texW / (float)m_frameW));
	m_yFrames = MAX(1, floor((float)texH / (float)m_frameH));
	m_totalFrames = m_xFrames * m_yFrames;
	//m_divisionX = 5;
	//m_divisionY = 5;
}

AnimatedSprite::AnimatedSprite()
{
	m_isSet = false;
}

sf::Texture* AnimatedSprite::GetTexture()
{
	return (m_tex);
}

void AnimatedSprite::Set(sf::Texture *tex, int fW, int fH)
{
	m_tex = tex;
	m_frameW = fW;
	m_frameH = fH;
	CalcFrames();
}

float AnimatedSprite::GetActiveFrame() const
{
	return (m_activeFrame);
}

void AnimatedSprite::SetActiveFrame(float frame)
{
	m_activeFrame = frame;
	if (m_activeFrame > 1.0)
		m_activeFrame = 1.0;
}

//
//void AnimatedSprite::SetFrameDivision(int x, int y)
//{
//	m_divisionX = x;
//	m_divisionY = y;
//}

int AnimatedSprite::GetFramesCount() const
{
	return (m_totalFrames);
}

void AnimatedSprite::Render(RenderingEngine *target, int x, int y, int w, int h)
{
	sf::RectangleShape rectangle(sf::Vector2f(w, h));
	int activeCol = (int)(m_activeFrame * m_totalFrames) % m_xFrames;
	int activeRow = (int)(m_activeFrame * m_totalFrames) / m_yFrames;

	rectangle.setPosition(x, y);
	rectangle.setFillColor(sf::Color(255, 255, 255, 255));
	rectangle.setTexture(m_tex);
	rectangle.setTextureRect(sf::IntRect(activeCol * m_frameW, activeRow * m_frameH, m_frameW, m_frameH));
	target->GetRenderWindow()->draw(rectangle);
}

