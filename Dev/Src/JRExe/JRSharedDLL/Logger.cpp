/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

Logger Logger::Log;

Logger::Logger()
{
	*this << "Module construct: Logger OK" << std::endl;
	buffer = "";
	savefile = LOGSSRC + std::string("log_" + std::to_string(time(NULL)) + ".txt");
	ClearLog();
	outBuffer.str("");
	outBuffer.clear();
}

void Logger::ClearLog()
{
	std::ofstream file;
	file.open(savefile, std::ofstream::trunc);
	file.close();
}

Logger::~Logger()
{
	//Save();
}

void Logger::Bufferize(std::string const &msg)
{
	buffer += msg;
}

void Logger::Output()
{
	std::string res = "[LOG] " + buffer;
	std::cout << res << std::endl;
	outBuffer << res << std::endl;
	buffer = "";
}

void Logger::Save()
{
	std::ofstream file;

	file.open(savefile, std::ofstream::app);
	file << outBuffer.str();
	file.close();
	outBuffer.str("");
	outBuffer.clear();
}

/*
Logger &Logger::operator << (int msg)
{
Bufferize(std::to_string(msg));
return (*this);
}*/

/*
template<class T>
Logger &Logger::operator << (void *msg)
{
	Bufferize(std::to_string((unsigned long int)msg));
	return (*this);
}*/

Logger &operator << (Logger &res, double const &msg)
{
	res.Bufferize(std::to_string(msg)); 
	return (res);
}

Logger &operator << (Logger &res, int const &msg)
{
	res.Bufferize(std::to_string(msg));
	return (res);
}

Logger &operator << (Logger &res, unsigned int const &msg)
{
	res.Bufferize(std::to_string(msg));
	return (res);
}

template<>
Logger &operator << (Logger &res, std::string const & msg)
{
	res.Bufferize(msg);
	return (res);
}

Logger &operator << (Logger &res, char const msg[])
{
	res.Bufferize(msg);
	return (res);
}

Logger &operator << (Logger &res, StandardEndLine const& manip)
{
	res.Output();
	return (res);
}

template<class T>
Logger &operator << (Logger &res, T const& val)
{
	//Bufferize(val.to_string());
	return (res);
}