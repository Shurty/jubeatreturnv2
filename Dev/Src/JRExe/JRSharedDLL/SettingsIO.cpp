/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

SettingsIO::SettingsIO(std::string const &filename)
{
	Open(filename);
}

SettingsIO::SettingsIO()
{

}

SettingsIO::~SettingsIO()
{

}

void SettingsIO::Open(std::string const &filename)
{
	// In fact this is done at "get" call, so this is somehow useless :/
	// Blame ms for doing things the crap way, no need to check if file exists or stuff
	m_filename = filename;
}

bool SettingsIO::GetBool(std::string const &section, std::string const &key)
{
#ifdef WIN32
	int result = GetPrivateProfileInt(section.c_str(), key.c_str(), 0,
		m_filename.c_str());
#else
	int result = 0;
#endif
	return (result != 0);
}

int SettingsIO::GetInt(std::string const &section, std::string const &key)
{
#ifdef WIN32
	int result = GetPrivateProfileInt(section.c_str(), key.c_str(), 0,
		m_filename.c_str());
#else
	int result = 0;
#endif 
	return result;
}

std::string SettingsIO::GetString(std::string const &section, std::string const &key)
{
#ifdef WIN32
	std::string result;
	char resultRaw[255];

	GetPrivateProfileString(section.c_str(), key.c_str(), "", resultRaw, 255,
		m_filename.c_str());
	result = resultRaw;
#else
	std::string result = "";
#endif
	return result;
}

float SettingsIO::GetFloat(std::string const &section, std::string const &key)
{
#ifdef WIN32
	float result = 0.0f;
	char resultRaw[255];
	std::stringstream resultStr;

	GetPrivateProfileString(section.c_str(), key.c_str(), "", resultRaw, 255,
		m_filename.c_str());
	resultStr.str(resultRaw);
	resultStr >> result;
#else
	float result = 0.0f;
#endif
	return result;
}

std::pair<std::string, unsigned int>	SettingsIO::GetHighScore(std::string const& section, std::string const& key)
{
#ifdef WIN32
	char	resultRaw[255];
	std::string		name;
	unsigned int	score;
	std::stringstream	resultStr;

	GetPrivateProfileString(section.c_str(), key.c_str(), "", resultRaw, 255,
		m_filename.c_str());
	resultStr.str(resultRaw);
	resultStr >> name;
	resultStr >> score;

	std::pair<std::string, unsigned int> rtrn;

	rtrn.first = name;
	rtrn.second = score;
#else
	std::pair<std::string, unsigned int> rtrn;
	rtrn.first = "";
	rtrn.second = 0;
#endif
	return (rtrn);
}

void SettingsIO::Write(std::string const &section, std::string const &key, std::string const &val)
{
#ifdef WIN32
	WritePrivateProfileString(section.c_str(), key.c_str(), val.c_str(), m_filename.c_str());
#else
#endif
}
/*
bool CIniReader::ReadBoolean(char *section, char *key, bool DefaultValue)
{
  char Result[255];
  char Default[255];
  bool bolResult;

  sprintf_s(Default, 255, "%s", DefaultValue ? "True" : "False");
  GetPrivateProfileString(section, key, Default, Result, 255, m_FileName);

  bolResult = ( strcmp(Result, "True") == 0 ||
    strcmp(Result, "true") == 0 ) ? true : false;

  return bolResult;
}*/