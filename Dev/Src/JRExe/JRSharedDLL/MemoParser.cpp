/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>
#ifdef WIN32
#else
#include <string.h>
#endif
/* 
 * Char table definition 
 * DO NOT change anything in this
 * If you want to support another version of Cosmos Memo, create
 * another table for retro-compatibility issues in another class
 * inheriting this one
 */
/*
char beatmapCharacters[][BMPCHARS_COUNT] = {
	"??, "??, "??, "??, 
	"??, "??, "??, "??, 
	"??, "??, "??, "??,
	"??, "??, "??, "??,
	"�?, // Equivalent of -  in rythm (means empty)
	"??, // Equivalent of [] in structure (means empty)
	"|",  // Separator for rythm control
};*/

MemoParser::MemoParser()
{
	std::wfstream file(CHARLIST_DATA_SRC, std::fstream::in);
	std::wstring res;

#ifdef WIN32
	const std::locale empty_locale = std::locale::empty();
	typedef std::codecvt_utf8<wchar_t> converter_type;
	const converter_type* converter = new converter_type;
	const std::locale utf8_locale = std::locale(empty_locale, converter);
#endif

	int i = 0;

#ifdef WIN32
	file.imbue(utf8_locale);
#endif
	getline(file, res);
	while (getline(file, res) && i < BMPCHARS_COUNT)
	{
		int k = 0;
		beatmapCharacters[i] = res;
		/*while (k < res.length()) {
			beatmapCharacters[i][k] = res.c_str()[k];
			k++;
		}*/
		//beatmapCharacters[i][k] = 0;
		i++;
	}
	file.close();
	//delete converter;
	Logger::Log << "Submodule construct: MemoParser OK" << std::endl;
}

MemoParser::~MemoParser()
{

}

std::string MemoParser::IntToStr(int x)
{
    // cr?r un flux de sortie 
    std::ostringstream oss; 
    // ?rire un nombre dans le flux 
    oss << x; 
    // r?up?er une cha?e de caract?es 
    std::string result = oss.str();
	return (result);
}

void MemoParser::ParseSectionLine(BeatmapSection &res)
{
	std::wstring readStr;
	std::wstringstream ss;
	std::wstring activationPart;
	std::wstring rythmPart;
	int prevChar = BMPCHARS_SEP_RYT;
	int btCount = 0;
#ifdef WIN32
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
#endif

	getline(bmpFile, readStr);
	ss.clear();
	ss.str(readStr);
	if (readStr.length() == 0)
		return;
	if (readStr.length() >= 4)
		activationPart = readStr.substr(0, 4);
	if (readStr.length() > 4)
		rythmPart = readStr.substr(5, readStr.length() - 5);
	//ss >> activationPart >> rythmPart;
	res.activationsRaw += activationPart;
	res.rythmRaw += rythmPart;

	/* Reparsing the rythm list */
	std::wstring substr;
	for (unsigned int i = 0; i < res.rythmRaw.length(); i++)
	{
		substr += res.rythmRaw.at(i);
		for (unsigned int k = 0; k < BMPCHARS_COUNT; k++)
		{
			if (beatmapCharacters[k] == substr) 
			{
				if (k < BMPCHARS_PRESSABLE) {
					/* if it's a pressed character, we save the time */
					res.rythm += IntToStr(k + 1) + " ";
					btCount++;
				} else {
					/* Else we use specific case */
					if (k == BMPCHARS_EMPTY_RYT) {
						res.rythm += "0 ";
						btCount++;
					} else if (k == BMPCHARS_HOLD_RYT) {
						res.rythm += "100 ";
						std::string intstr = "";
						for (unsigned int j = i + 1; j <= i + 3; j++)
						{
							intstr += res.rythmRaw.at(j);
						}
						res.rythm += intstr + " ";
						Logger::Log << res.rythm << std::endl;
						i += 3;
					} else if (k == BMPCHARS_SEP_RYT) {
						if (prevChar != BMPCHARS_SEP_RYT) {
							res.rythm += "200 ";
							res.rythmCount += IntToStr(btCount) + " ";
							btCount = 0;
						}
						res.rythm += "";
					} else {
						Logger::Log << "Unexpected statement in rythm parsing" << std::endl;
					}
				}
				prevChar = k;
				substr.clear();
			}
		}
	}
	res.rythmRaw.clear();
}

BeatmapSection MemoParser::ParseSection()
{
	BeatmapSection res;

	res.rythmCount = "";
	res.subBeatmap = NULL;
	memset(res.activations, 0, sizeof(int) * 16);
	ParseSectionLine(res);
	ParseSectionLine(res);
	ParseSectionLine(res);
	ParseSectionLine(res);
	res.rythm += "-1 ";

	/* Reparsing the activation list */
	std::wstring substr;
	int findingCount = 0;
	for (unsigned int i = 0; i < res.activationsRaw.length(); i++)
	{
		substr += res.activationsRaw.at(i);
		for (unsigned int k = 0; k < BMPCHARS_COUNT; k++)
		{
			if (beatmapCharacters[k] == substr) 
			{
				if (k < BMPCHARS_PRESSABLE) {
					/* if it's a pressed character, we save the time */
					res.activations[findingCount] = k + 1;
				} else {
					/* Else we use specific case */
					if (k == BMPCHARS_EMPTY_STR) {
						res.activations[findingCount] = 0;
					} else {
						Logger::Log << "Unexpected statement in structure parsing" << std::endl;
						// Pre-decrease to nullify next statement (whatever)
						findingCount--;
					}
				}
				substr.clear();
				// We increase the target location index
				findingCount++;
			}
		}
	}

	return (res);

	/* Testing ! */
	for (int i = 0; i < BMPCHARS_PRESSABLE; i++)
	{
		if (i % 4 == 0 && i != 0) {
			Logger::Log << std::endl;
		}
		if (res.activations[i] == -1) {
			Logger::Log << "- ";
		} else if (res.activations[i] == 0) {
			Logger::Log << "? ";
		} else {
			Logger::Log << res.activations[i] << " ";
		}
	}
	Logger::Log << std::endl << res.rythm << std::endl;

	return (res);
}

int MemoParser::ExtractIntFromStr(std::string const &res)
{
	std::stringstream ss;
	std::string header;
	int val;
	MemoParser *parser = this;

	ss.str(res);
	ss >> val;
	return (val);
}

void MemoParser::ParseHeader()
{
	std::wstring res;
	std::string colHeader;
#ifdef WIN32
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
#endif

	Logger::Log << "Parse begin for " << m_src << std::endl;

	 // UTF8 header
#ifdef WIN32
	while (getline(bmpFile, res) && converter.to_bytes(res) != "###BODY###")
#else
	while (getline(bmpFile, res) && UTF8ToAscii(res) != "###BODY###")
#endif
	{
		std::wstringstream ss;
		std::wstring wheader;
		std::wstring value;
		std::string header;

		ss.str(res);
		ss >> wheader;
		std::getline(ss, value);
#ifdef WIN32
		header = converter.to_bytes(wheader);
#else
		header = UTF8ToAscii(wheader);
#endif
		if (value.length() > 0)
			value = value.substr(1);
		if (header == "Name:") {
			resBmp->name = value;
		} else if (header == "Author:") {
			resBmp->author = value;
		} else if (header == "Src:") {
#ifdef WIN32
			resBmp->src = converter.to_bytes(value);
#else
			resBmp->src = UTF8ToAscii(value);
#endif
		} else if (header == "Difficulty:") {
#ifdef WIN32
			if (converter.to_bytes(value) == "EXTREME") {
				resBmp->songLevel = SL_EXTREME;
			}
			else if (converter.to_bytes(value) == "INTERMEDIATE") {
				resBmp->songLevel = SL_INTERMEDIATE;
			} 
#else
			if (UTF8ToAscii(value) == "EXTREME") {
				resBmp->songLevel = SL_EXTREME;
			}
			else if (UTF8ToAscii(value) == "INTERMEDIATE") {
				resBmp->songLevel = SL_INTERMEDIATE;
			}
#endif
			else {
				resBmp->songLevel = SL_BASIC;
			}
#ifdef WIN32
		} else if (header == "Level:") {
			resBmp->level = ExtractIntFromStr(converter.to_bytes(value));
		} else if (header == "BPM:") {
			resBmp->originalBpm = ExtractIntFromStr(converter.to_bytes(value));
		} else if (header == "Notes:") {
			resBmp->beats = ExtractIntFromStr(converter.to_bytes(value));
		} else if (header == "Delay:") {
			resBmp->delay = ExtractIntFromStr(converter.to_bytes(value));
		}
#else
		}
		else if (header == "Level:") {
			resBmp->level = ExtractIntFromStr(UTF8ToAscii(value));
		}
		else if (header == "BPM:") {
			resBmp->originalBpm = ExtractIntFromStr(UTF8ToAscii(value));
		}
		else if (header == "Notes:") {
			resBmp->beats = ExtractIntFromStr(UTF8ToAscii(value));
		}
		else if (header == "Delay:") {
			resBmp->delay = ExtractIntFromStr(UTF8ToAscii(value));
		}
#endif
	}
	m_activeTime = (float) resBmp->delay;
	//DEBUG: TODO: Disable delay mode for debug detecting orig beat position
	//m_activeTime = 0;
	return ;
}

void MemoParser::ParseContent()
{
	std::wstring WreadStr;
	std::string readStr;
	std::string headerStr;
	int activeBPM = 0;
	int activeDt = 0;
	std::stringstream ss;
	BeatmapSection activeSectionCnt;
	BeatmapSection activeSubsectionCnt;
#ifdef WIN32
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
#endif

	activeBPM = resBmp->originalBpm;
	//getline(bmpFile, WreadStr); // Empty first line 
	//m_activeTime = 0;
	m_activeSection = -1;
	while (getline(bmpFile, WreadStr))
	{
#ifdef WIN32
		readStr = converter.to_bytes(WreadStr);
#else
		readStr = UTF8ToAscii(WreadStr);
#endif
		if (readStr != "")
		{
			/*
			 * If the header of the beat section is not a BPM update, we create a new section
			 * Else we load the new bmp and try again
			 */
			if (1) 
			{
				// Swap the active section into a correct beat list
				if (m_activeSection != -1) {
					Generate(activeSectionCnt, (float) activeBPM);
				}
				// We create a new beat section
				ss.clear();
				ss.str(readStr);
				ss >> m_activeSection;

				//Logger::Log << "Receiving section " << m_activeSection << std::endl;
				// We read 4 lines of beats structure and ryhtm
				activeSectionCnt = ParseSection();

				// We process these into a valid beat structure element
				;

				// We get back into process
			}
			else
			{
				// Parse the new BPM and update the active one, then go back into process
				// We create a new beat section
				ss.clear();
				ss.str(readStr);
				ss >> m_activeSection;
			}
		}
		else
		{
			// We are receiving a 2nd row of beats for the active section
			// Parse 4 lines of beats structure and rythm
			//Logger::Log << "Receiving subsection for " << m_activeSection << std::endl;
				
			activeSubsectionCnt = ParseSection();
			activeSectionCnt.subBeatmap = &activeSubsectionCnt;

			// We process these into a valid beat structure element
			;

			// We get back into process
		}
	}
	/* If we have a pending section */
	if (m_activeSection != 0) {
		Generate(activeSectionCnt, (float) activeBPM);
	}
}

/*
 * Generate the Beatmap beats of a section
 */
void MemoParser::Generate(BeatmapSection &active, float bpm)
{
	std::stringstream rythmStream(active.rythm);
	std::stringstream rythmCountStream(active.rythmCount);
	int selectedBtn;
	float activeCount = 0;
	float dtPerLine = 60000.0f / bpm;
	float dtPerSegment = 0;
	Beat newBt;
 
	rythmCountStream >> activeCount;
	dtPerSegment = dtPerLine / activeCount;

	selectedBtn = 0;
	rythmStream >> selectedBtn;

	//Logger::Log << (int)bpm << ":" << (int)activeCount << ":" << (int)dtPerSegment << std::endl;
	//Logger::Log << rythmStream.str() << std::endl;

	while (selectedBtn != -1)
	{
		if (selectedBtn == 0) {
			m_activeTime += dtPerSegment;
			rythmStream >> selectedBtn;
		}
		else if (selectedBtn == 200) {
			rythmCountStream >> activeCount;
			dtPerSegment = dtPerLine / activeCount;
			rythmStream >> selectedBtn;
		}
		else if (selectedBtn != -1) {
			/* Check for modifiers */
			int selectedBtnNext;
			BTYPE btnType = BTY_SINGLE;
			int btnDuration = 0;
			while (true)
			{
				rythmStream >> selectedBtnNext;
				if (selectedBtnNext == 100)
				{
					btnType = BTY_HOLD;
					rythmStream >> btnDuration;
					btnDuration *= dtPerSegment;
					continue;
				}
				break;
			}

			for (int i = 0; i < 16; i++)
			{
				if (active.activations[i] == selectedBtn) {
					newBt.activate = i + 1;
					newBt.bpm = bpm;
					newBt.levelSection = m_activeSection;
					newBt.time = (int) m_activeTime;
					newBt.duration = btnDuration;
					newBt.type = btnType;
					resBmp->beatsList.push_back(newBt);
				}
			}
			//rythmStream >> selectedBtn;
			selectedBtn = selectedBtnNext;
			m_activeTime += dtPerSegment;
		}
	}
	//Logger::Log << "Active time: " << m_activeTime << std::endl;
	/* If subbeatmap exists, go and generate ! */
	if (active.subBeatmap)
		Generate(*active.subBeatmap, bpm);
}

void MemoParser::EchoResult()
{
	//std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

	/* Disabled */
	return ;
	/*Logger::Log 
		<< "Parsing complete: " << m_src 
		<< " - " << converter.to_bytes(resBmp->name)
		<< " (lvl" << resBmp->level << ")"
		<< std::endl;
		*/
	int cnt = 0;
	/*
	while (!resBmp->beatsList.empty())
	{
		if (cnt < 50)
		{
			Logger::Log << "#" << cnt << "-" << (int)(resBmp->beatsList.front().levelSection) 
				<< " Beat: " << (int)(resBmp->beatsList.front().activate) << " at " 
				<< resBmp->beatsList.front().time << std::endl;
		}
		cnt++;
		resBmp->beatsList.pop_front();
	}*/
}

Beatmap *MemoParser::ParseBeatmap(std::string const &src, bool full)
{
#ifdef WIN32
	const std::locale empty_locale = std::locale::empty();
	typedef std::codecvt_utf8<wchar_t> converter_type;
	const converter_type* converter = new converter_type;
	const std::locale utf8_locale = std::locale(empty_locale, converter);

	bmpFile.imbue(utf8_locale);
#endif
	bmpFile.open(src, std::ios::in);
	if (bmpFile.bad())
		return (NULL);
	resBmp = new Beatmap();
	m_src = src;
	ParseHeader();
	if (full) {
		ParseContent();
		resBmp->beats = resBmp->beatsList.size();
	}
	bmpFile.close();
#if _DEBUG
	EchoResult();
#endif
	if (full) {
		resBmp->graphStep = resBmp->beatsList.back().time / NBGRAPHBEATS;
	}
	//delete converter;
	return (resBmp);
}