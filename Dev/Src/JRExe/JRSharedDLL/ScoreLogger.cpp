#include <JREngine/jrshared.h>

ScoreLogger::Data::Data(BEATRESULT b , unsigned int cs, unsigned int cb, unsigned int ps, unsigned int pb) : 
	beat(b),
	currentScore(cs),
	currentBonus(cb),
	predictedScore(ps),
	predictedBonus(pb)
{

}
ScoreLogger::Data::Data()
{

}

ScoreLogger::ScoreLogger(void)
{

}


ScoreLogger::~ScoreLogger(void)
{
	scores.clear();
}

void	ScoreLogger::addScore(BEATRESULT bs, unsigned int cs, unsigned int cb, unsigned int ps, unsigned int pb)
{
	scores.push_back(Data(bs, cs, cb, ps, pb));
}

//  beatScored currentScore currentBonus predictedScore predictedBonus
void	ScoreLogger::writeFile(const std::string& filepath) const
{
	Json::StyledWriter writer;
	std::ofstream file(filepath);

	if (!file)
	{
		std::ofstream file_(filepath);
		file_ << "";
		file_.close();
		file.open(filepath);
		Logger::Log << "failed opening file '" << filepath << "': creating..." << std::endl;
	}
	file << scores.size() << std::endl;
	int i = 0;
	for ( auto score : scores)
	{
		file << i++ << " " // 'i' should be the time
			 << score.beat				<< " "
			 << score.currentScore		<< " "
			 << score.currentBonus		<< " "
			 << score.predictedScore	<< " "
			 << score.predictedBonus	<< std::endl;
	}
	file.close();
	Logger::Log << filepath << std::endl;
}

std::vector<ScoreLogger::Data>	ScoreLogger::readFile(const std::string& f) {
//	Logger::Log << "load scores file: " << f << std::endl;
	std::ifstream file(f);

	if (!file)
	{
		std::ofstream file_(f);
		file_ << "";
		file_.close();
		file.close();
		Logger::Log << "failed opening file '" + f + "': creating and returning empty." << std::endl;
		return std::vector<ScoreLogger::Data>();
	}
	// TODO: get the data in vector ; maybe modify way of writing ?
	int nbBeats;

	std::vector<ScoreLogger::Data> scores = std::vector<ScoreLogger::Data>();

	file >> nbBeats;
	for (int i = 0; i < nbBeats; i++) {
		int time;
		file >> time;
		int beatScored;
		file >> beatScored;
		unsigned int currentScore;
		file >> currentScore;
		unsigned int currentBonus;
		file >> currentBonus;
		unsigned int predictedScore;
		file >> predictedScore;
		unsigned int predictedBonus;
		file >> predictedBonus;
		scores.push_back(Data((BEATRESULT)beatScored, currentScore, currentBonus, predictedScore, predictedBonus));
    }

	file.close();
	//if ( !parsingSuccessful )
	//{
	// // report to the user the failure and their locations in the document.
	//	Logger::Log  << "Failed to parse configuration\n"
	//	           << reader.getFormattedErrorMessages() << std::endl;
	//	 return Json::Value();
	//}

	return scores;
}