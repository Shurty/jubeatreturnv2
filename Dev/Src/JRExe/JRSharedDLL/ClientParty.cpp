/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

ClientParty::ClientParty()
{

}

ClientParty::~ClientParty()
{
	Clear();
}

void ClientParty::Clear()
{
	while (m_clients.empty() == false)
		m_clients.pop_back();
}

void ClientParty::PushClient(Client *cl)
{
	m_clients.push_back(cl);
}

void ClientParty::PopClient(Client *cl)
{
	std::vector<Client *>::iterator res =
		std::find_if(m_clients.begin(), m_clients.end(), [&cl](Client *_cl) -> bool {
		if (cl == _cl)
			return (true);
		return (false);
	});
	if (res != m_clients.end()) {
		m_clients.erase(res);
	}
}

int ClientParty::GetCount() { return (m_clients.size()); }
Client *ClientParty::GetClientByIndex(int idx) { return (idx < (int)m_clients.size() ? m_clients[idx] : NULL); }

void ClientParty::SetActiveSong(int song) { m_activeSong = song; }
int ClientParty::GetActiveSong() const { return (m_activeSong); }