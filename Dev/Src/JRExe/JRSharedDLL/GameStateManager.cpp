/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

GameStateManager::GameStateManager(Parameters &pm)
	: m_activeState(NULL), m_futureState(NULL), m_pm(pm)
{
	Logger::Log << "Module construct: GameStateManager OK" << std::endl;
}

GameStateManager::~GameStateManager()
{
	while (!m_states.empty())
	{
		m_states.back()->OnUnload(m_pm);
		delete m_states.back();
		m_states.pop_back();
	}
}

void GameStateManager::ChangeState(GameState *st)
{
	if (m_activeState == NULL) {
		m_activeState = st;
		m_activeState->OnStart(m_pm);
	} else {
		m_futureState = st;
	}
}

void GameStateManager::ChangeState(GMSTATEID stid)
{
	Logger::Log << "Change state attempt to " << (int)stid << std::endl;
	GameState *st = *std::find_if(m_states.begin(), m_states.end(),
		[&stid] (GameState *stm) -> bool {
			if (stm->GetId() == stid)
				return (true);
			return (false);
		}
	);
	
	if (st == NULL)
		return ;
	Logger::Log << "State found: " << (int)st->GetId() << std::endl;
	if (m_activeState == NULL) {
		m_activeState = st;
		m_activeState->OnStart(m_pm);
	} else {
		m_futureState = st;
	}
}

void GameStateManager::PushState(GameState *st)
{
	m_states.push_back(st);
	st->OnInit(m_pm);
}

GameState *GameStateManager::GetActiveState()
{
	return (m_activeState);
}

void GameStateManager::UpdateState()
{
	if (m_activeState == NULL)
		return ;
	m_activeState->OnUpdate(m_pm);
	if (m_futureState) {
		m_activeState->OnEnd(m_pm);
		m_activeState = m_futureState;
		m_futureState = NULL;
		m_activeState->OnStart(m_pm);
		// Save every state change
		Logger::Log.Save();
	}
}

void GameStateManager::RenderState()
{
	if (m_activeState == NULL)
		return ;
	m_activeState->OnRender(m_pm);
}