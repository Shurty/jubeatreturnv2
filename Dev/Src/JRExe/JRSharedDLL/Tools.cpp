
#include <JREngine/jrshared.h>

std::string UTF8ToAscii(std::wstring rstr)
{
	std::string res;

	std::for_each(rstr.begin(), rstr.end(), [&res](wchar_t ch)
	{
		if ((int)ch > 127) {
			res += "";
		}
		else {
			res += (char)ch;
		}

	});
	return (res);
}