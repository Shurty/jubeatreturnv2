/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

JRBParser::JRBParser()
{
	Logger::Log << "Submodule construct: JRBParser OK" << std::endl;
}

JRBParser::~JRBParser()
{
}

int JRBParser::ExtractIntFromStr(std::string const &res)
{
	std::stringstream ss;
	std::string header;
	int val;

	ss.str(res);
	ss >> header >> val;
	return (val);
}

Beatmap *JRBParser::ParseBeatmap(std::string const &src, bool full)
{
	std::stringstream ss;
#ifdef WIN32
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
#endif

	bmpFile.open(src, std::ios::in);
	if (bmpFile.bad())
		return (NULL);
	resBmp = new Beatmap();
	m_src = src;
	ParseHeader();
	
	if (full)
	{
		/* Parsing beats */
		std::wstring res;
		getline(bmpFile, res); // Header separator
		while (getline(bmpFile, res))
		{
			ss.str("");
			ss.clear();
#ifdef WIN32
			ss.str(converter.to_bytes(res));
#else
			ss.str(UTF8ToAscii(res));
#endif
			if (res.length() == 0)
				break ;
			resBmp->beatsList.push_back(Beat());
			resBmp->beatsList.back().bpm = resBmp->originalBpm;
			resBmp->beatsList.back().levelSection = 1;
			int resAct;
			ss >> resAct;
			resBmp->beatsList.back().activate = resAct;
			ss >> resBmp->beatsList.back().time;
			resBmp->beatsList.back().time += resBmp->delay;
			int btype;
			ss >> btype;
			resBmp->beatsList.back().type = (BTYPE)btype;
			ss >> resBmp->beatsList.back().duration;
		}
		resBmp->graphStep = resBmp->beatsList.back().time / NBGRAPHBEATS;
	}

	bmpFile.close();
	return (resBmp);
}

void JRBParser::ParseHeader()
{
	std::wstring res;
	std::string colHeader;
#ifdef WIN32
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
#endif

	Logger::Log << "Parse begin for " << m_src << std::endl;

	getline(bmpFile, res); // UTF8 header
	getline(bmpFile, res); // Song name
	resBmp->name = res;
	getline(bmpFile, res); // Song Author
	resBmp->author = res;
	getline(bmpFile, res); // Song SRC
#ifdef WIN32
	resBmp->src = converter.to_bytes(res);
#else
	resBmp->src = UTF8ToAscii(res);
#endif
	getline(bmpFile, res); // empty
	getline(bmpFile, res); // Song Difficulty
#ifdef WIN32
	if (converter.to_bytes(res) == "EXTREME") {
		resBmp->songLevel = SL_EXTREME;
	}
	else if (converter.to_bytes(res) == "INTERMEDIATE") {
		resBmp->songLevel = SL_INTERMEDIATE;
	} 
#else
	if (UTF8ToAscii(res) == "EXTREME") {
		resBmp->songLevel = SL_EXTREME;
	}
	else if (UTF8ToAscii(res) == "INTERMEDIATE") {
		resBmp->songLevel = SL_INTERMEDIATE;
	}
#endif
	else {
		resBmp->songLevel = SL_BASIC;
	}
	getline(bmpFile, res); // empty

	getline(bmpFile, res); // Global Level
#ifdef WIN32
	resBmp->level = ExtractIntFromStr(converter.to_bytes(res));
#else
	resBmp->level = ExtractIntFromStr(UTF8ToAscii(res));
#endif
	getline(bmpFile, res); // Original BPM
#ifdef WIN32
	resBmp->originalBpm = ExtractIntFromStr(converter.to_bytes(res));
#else
	resBmp->originalBpm = ExtractIntFromStr(UTF8ToAscii(res));
#endif

	getline(bmpFile, res); // Beats count
#ifdef WIN32
	resBmp->beats = ExtractIntFromStr(converter.to_bytes(res));
#else
	resBmp->beats = ExtractIntFromStr(UTF8ToAscii(res));
#endif

	getline(bmpFile, res); // Delay
#ifdef WIN32
	resBmp->delay = ExtractIntFromStr(converter.to_bytes(res));
#else
	resBmp->delay = ExtractIntFromStr(UTF8ToAscii(res));
#endif
}
