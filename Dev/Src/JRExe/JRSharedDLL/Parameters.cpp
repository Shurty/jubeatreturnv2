/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

Parameters::Parameters()
	: m_params()
{
	Reload();
	Logger::Log << "Module construct: Parameters OK" << std::endl;

}

void Parameters::LoadGameSettings(std::string const &src)
{
	SettingsIO sett(src);

	m_params.press_delays[BR_BAD] = sett.GetInt("PressDelay", "BR_BAD") / 1000.0;
	m_params.press_delays[BR_GOOD] = sett.GetInt("PressDelay", "BR_GOOD") / 1000.0;
	m_params.press_delays[BR_GREAT] = sett.GetInt("PressDelay", "BR_GREAT") / 1000.0;
	m_params.press_delays[BR_PERFECT] = sett.GetInt("PressDelay", "BR_PERFECT") / 1000.0;
	m_params.press_img_delay = sett.GetInt("PressDelay", "BR_DELAY") / 1000.0;
	m_params.press_offset = sett.GetInt("PressDelay", "BR_OFFSET");
	m_params.press_latency = sett.GetInt("PressDelay", "BR_LATENCY");

	for (int i = 0; i < 11; i++)
	{
		m_params.press_durations[i] = sett.GetInt("PressDelay", "BD_LEN_" + std::to_string(i));
	}

	m_params.press_range = 900;
}

void Parameters::Reload()
{
	SettingsIO sett(SETTINGS_DATA_SRC);

	m_params.debug_window_show = sett.GetBool("Debug", "Show");

	m_params.offline_player_name = sett.GetString("Game", "PlayerName");	
	m_params.simulation = sett.GetBool("Game", "Simulation");
	m_params.game_debug = sett.GetBool("Game", "Debug");
	m_params.ui_debug = sett.GetBool("UI", "Debug");
	m_params.wav_mode = sett.GetBool("Music", "ForceWAV");

	m_params.activator_sprite_width = 100;
	m_params.activator_sprite_height = 100;
	m_params.activator_sprite_count = 25;

	LoadGameSettings("Data/config_normal.ini");
	Logger::Log << "Parameters configuration file loaded " << std::endl;
}

Parameters::~Parameters()
{
}