/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC - Thierry BERGER 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine/jrshared.h>

#pragma region member functions

ScoreManager::ScoreManager()
{
	m_scoreForBeat[BEATRESULT::BR_MISS] = 0.0;
	m_scoreForBeat[BEATRESULT::BR_BAD] = 0.1;
	m_scoreForBeat[BEATRESULT::BR_BAD_AFTER] = 0.1;
	m_scoreForBeat[BEATRESULT::BR_GOOD] = 0.4;
	m_scoreForBeat[BEATRESULT::BR_GOOD_AFTER] = 0.4;
	m_scoreForBeat[BEATRESULT::BR_GREAT] = 0.7;
	m_scoreForBeat[BEATRESULT::BR_GREAT_AFTER] = 0.7;
	m_scoreForBeat[BEATRESULT::BR_PERFECT] = 1.0;

	m_scoreForBeat[BEATRESULT::BR_BONUS] = 0.0; // wut ?
	//m_scoreForBeat[BEATRESULT::BR_MAX_RESULTS] = 0.0; // wut ?
	m_calculatedFinalGrade = GRADE_EXC;
	//m_results = NULL;
	scoreLogger = nullptr;
}


ScoreManager::~ScoreManager(void)
{
	delete scoreLogger;
	scoreLogger = nullptr;
}

void	ScoreManager::reset(unsigned int numberOfNotes, bool logThisSession)
{
	if (m_logThisSession || true) // FIXME: fuck it
		scoreLogger = new ScoreLogger();
	else
	{
		delete scoreLogger;
		scoreLogger = nullptr;
	}

	m_numberOfNotes = numberOfNotes;
	m_calculatedBonusScore = 0;
	m_calculatedFinalScore = 0;
	m_numberOfBeatsDone = 0;
	m_maxCombo = 0;
	m_bonusCounter = 0.0;
	m_currentScore = 0.0;
	m_bonusNotesCounter = 0;
	m_beatValue = 900000.0 / (double)numberOfNotes;
	m_calculatedFinalGrade = GRADE_EXC;
	for (unsigned int i = 0; i < BR_MAX_RESULTS; i++)
	{
		m_beatResults[i] = 0;
	}
}

std::string	ScoreManager::logScores() const
{
	Logger::Log << "number of beats done: " << m_numberOfBeatsDone << " should be equal to number Of beats of the music: " << m_numberOfNotes << std::endl;
	return this->logScores(LOGSSRC + std::string("log_scores_") + std::to_string(time(NULL)));
}

const std::string&	ScoreManager::logScores(const std::string& filepath) const
{
	if (scoreLogger)
	{
		 scoreLogger->writeFile(filepath);
		return filepath;
	}
	return filepath;
}

std::vector<ScoreLogger::Data>	ScoreManager::readScores(const std::string& filepath) const
{
	return ScoreLogger::readFile(filepath);
}

void ScoreManager::scoreBeat(BEATRESULT res)
{
	m_currentScore += m_beatValue * m_scoreForBeat[res];
	m_beatResults[res]++;

	if (res == BEATRESULT::BR_MISS || res == BEATRESULT::BR_BAD) {
		m_bonusCounter -= 8.0 / (double)m_numberOfNotes;
		m_bonusNotesCounter = 0;
	} else if (res == BEATRESULT::BR_PERFECT || res == BEATRESULT::BR_GREAT) {
		m_bonusCounter += 2.0 / (double)m_numberOfNotes;
		m_bonusNotesCounter++;
	} else if (res == BEATRESULT::BR_GOOD) {
		m_bonusCounter += 1.0 / (double)m_numberOfNotes;
		m_bonusNotesCounter++;
	}
	if (m_bonusNotesCounter > m_maxCombo)
		m_maxCombo = m_bonusNotesCounter;

	if (m_bonusCounter > 1.0)
		m_bonusCounter = 1.0;
	else if (m_bonusCounter < 0.0)
		m_bonusCounter = 0.0;

	//unsigned int nbBeat = 0;
	//for (int i = 0; i < BEATRESULT::BR_MAX_RESULTS; ++i)
	//{
	//	nbBeat += m_beatResults[i];
	//}
	// Logger::Log << "beats: " << nbBeat << "/" << m_numberOfNotes << std::endl;

	// Logger::Log << "Max beat value: " << m_beatValue << " || Bonus score: " << 100000.0 * m_bonusCounter << " || Score: " << m_currentScore << std::endl;
	m_numberOfBeatsDone++;

	m_calculatedBonusScore = floor(100000.0 * m_bonusCounter + 0.5);
	int theoryBonusScore = computePotentialBonusScore();
	m_calculatedFinalScore = floor(((m_currentScore / (double)m_numberOfBeatsDone) * (double)m_numberOfNotes) + theoryBonusScore + 0.5);
	m_calculatedFinalGrade = getGradeForFinalScore((unsigned int)m_calculatedFinalScore);
	//if (m_currentScore == 899999)
	//	m_currentScore = 900000;
	// m_calculatedFinalScore = theoryBonusScore + m_currentScore;
	if (scoreLogger)
		scoreLogger->addScore(res, getScoreWithoutCombo(), (unsigned int)m_calculatedBonusScore, getPotentialScore() - theoryBonusScore, theoryBonusScore);
	//Logger::Log << "Combo: " << m_bonusNotesCounter << " Bonus: " << m_bonusCounter << " Score: " << m_currentScore << std::endl;
}

unsigned int ScoreManager::getScoreWithoutCombo() const
{
	// FIXME: put all these rounding in the initial initialization of these variables !
	return (unsigned int)floor(m_currentScore + 0.5);
}

unsigned int ScoreManager::getBonusScore() const
{
	return (unsigned int)m_calculatedBonusScore;
}

unsigned int ScoreManager::getScoreWithCombo() const
{
	//return getScoreWithoutCombo() + getBonusScore();
	return (unsigned int)m_calculatedFinalScore;
}

unsigned int ScoreManager::computePotentialBonusScore() const {
	int theoryBonusScore = (int)m_calculatedBonusScore;
	//if (m_numberOfBeatsDone <= m_numberOfNotes / 2) // FIXME: tested fo	
		theoryBonusScore = floor(((m_bonusCounter * (double)m_numberOfNotes) / (double)m_numberOfBeatsDone) * 100000 + 0.5);
	if (theoryBonusScore > 100000.0)
		theoryBonusScore = 100000.0;
	return theoryBonusScore;
}

double ScoreManager::getBonusScoreMultiplier() const
{
	return (m_bonusCounter);
}

unsigned int ScoreManager::getBonusNotesCount() const
{
	return m_bonusNotesCounter;
}

unsigned int ScoreManager::getPotentialScore() const
{
	return floor(m_calculatedFinalScore + 0.5);
}
unsigned int ScoreManager::getPotentialGrade() const
{
	return m_calculatedFinalGrade;
}

unsigned int ScoreManager::getMaxCombo() const
{
	return m_maxCombo;
}

unsigned int ScoreManager::getNbBeatResult(BEATRESULT br) const
{
	return m_beatResults[br];
}

#pragma endregion

#pragma region static class functions

unsigned int ScoreManager::testPerfectNotes(unsigned int numberOfNotes = 0)
{
	ScoreManager m = ScoreManager();
	while (numberOfNotes == 0)
	{
		Logger::Log << "enter number of notes to test (higher than 0) : ";
		std::cin >> numberOfNotes;
	}
	m.reset(numberOfNotes);
	for (int i = 0; i < numberOfNotes; i++)
	{
		m.scoreBeat(BEATRESULT::BR_PERFECT);
	}
	unsigned int score = m.getScoreWithCombo();
	// Logger::Log << "score for " << numberOfNotes << " notes : " << score << std::endl;
	return score;
}

const unsigned int ScoreManager::minimumScoreForGrade[GRADE::NUMBER_GRADES] = {
			// E
	500000, // D
	700000, // C
	800000, // B
	850000, // A
	900000, // S
	950000, // SS
	980000, // SSS
	999999, // EXC
	1000000 // EXC
};

GRADE ScoreManager::getGradeForFinalScore(unsigned int finalScore)
{
	for (int i = 0; i < GRADE::NUMBER_GRADES; ++i)
	{
		if (finalScore < minimumScoreForGrade[i])
			return (GRADE)i;
	}
	return GRADE_EXC;
}

#pragma endregion