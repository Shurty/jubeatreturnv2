
#include <JREngine/jrshared.h>
#include "StateGameSelect.h"
#include "Background.h"

StateGameSelect::StateGameSelect(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_GAMEMODE_SELECT;
	m_name = "Gamemode selection";
}

void StateGameSelect::OnInit(Parameters &pm)
{
	Logger::Log << "StateGameSelect: OnInit" << std::endl;
}

void StateGameSelect::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
	Logger::Log << "StateGameSelect: Begin" << std::endl;
	m_core.GetGUI()->HideAll();
	m_core.GetGUI()->Show(PANEL_GAMEMODE);
}

void StateGameSelect::OnRender(Parameters &pm)
{
	m_core.m_render.OnPreRender(pm);
	m_core.m_gui.m_mainBg->Render(&m_core);
	m_core.m_gui.Render();
	m_core.m_gui.m_mainBg->RenderGridOverlay(&m_core);
	m_core.m_gui.m_mainBg->RenderCredits(&m_core);
	m_core.m_render.OnRender(pm);
}

void StateGameSelect::OnUpdate(Parameters &pm)
{
	m_core.m_input->PollEvents(&m_core.m_render, &m_core);
	m_core.m_network.Update();
	m_core.m_songs.Update();
	m_core.m_sounds.Update();
	m_core.m_gui.m_mainBg->Update(&m_core);
	m_core.m_gui.Update();
	m_core.m_tweener.step(m_core.GetMainClock()->getElapsedTime().asMilliseconds());
}

void StateGameSelect::OnEnd(Parameters &pm)
{
}

void StateGameSelect::OnUnload(Parameters &pm)
{
}

void StateGameSelect::selectGameMode(GMMODE_ID id)
{
	// TODO: select the mode, preview, validate it
	m_core.m_gmm->SetMode(id);
}