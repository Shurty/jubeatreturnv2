#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrgui.h>

#include "GUIPanel.h"
#include "GUIJubeatIcon.hpp"

class JRExe;
class RenderingEngine;

class SplashPanel : public GUIPanel 
{
private:
	GUIKeyboard m_keyboard;
	GUIJubeatIcon m_jicon;
	int m_step;
	sf::Music m_bgMusic;
	JRExe *m_gm;
	float m_posX;
	float m_alpha;
	bool m_started;

	// Autoplay system
	sf::Clock m_songPlayTime;
	int m_activeMusicIdx;
	Beatmap* m_activeBeatmap;
	enum PLAYSTATE {
		PLAY,
		OFF
	} m_previewState;
	bool(*m_listFilter)(const SongLibrary&, Beatmap *);

	// Top header autoplay info

public:
	SplashPanel(JRExe *gm);
	~SplashPanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
	virtual void OnStart(GUIWidget *btn);
};