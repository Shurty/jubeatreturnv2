
#pragma once

#include "UnitTest.hpp"
#include "JRExe.h"

namespace Unit
{
	class SFML : public UnitTest
	{
	private:
		virtual void ExecTest()
		{
		}
		JRExe *m_jre;

	public:
		SFML(JRExe *jre)
			: UnitTest("SFMLEngine"), m_jre(jre)
		{
		}
		~SFML()
		{
			// Begin.

			// Create SFML window
			sf::RenderWindow wnd(sf::VideoMode(100, 100, 32), "UnitWindow");
			wnd.setPosition(sf::Vector2i(50, 50));
			wnd.clear(sf::Color(18, 22, 25, 255));
			wnd.display();

			// Create SFML primitives
			wnd.clear(sf::Color(18, 22, 25, 255));
			sf::RectangleShape rect(sf::Vector2f(100, 100));
			rect.setPosition(0, 0);
			rect.setFillColor(sf::Color(0, 255, 0, 255));
			wnd.draw(rect);
			sf::CircleShape circ(100);
			circ.setPosition(0, 0);
			circ.setFillColor(sf::Color(255, 0, 0, 255));
			wnd.draw(circ);
			wnd.display();

			// Load texture
			wnd.clear(sf::Color(18, 22, 25, 255));
			sf::Texture tex;
			if (tex.loadFromFile(TEXTURESDIR + std::string("bgBubble.png")) == false)
				throw Unit::Exception("Failed loading SFML texture");
			rect.setTexture(&tex);
			wnd.draw(rect);
			wnd.display();

			// Load font
			wnd.clear(sf::Color(18, 22, 25, 255));
			sf::Font font;
			if (font.loadFromFile(FONTSDIR + std::string("ARIALN.TTF")) == false)
				throw Unit::Exception("Failed loading SFML font");

			// SFML text
			sf::Text tx;
			tx.setPosition(0, 0);
			tx.setFont(font);
			tx.setString("ABCDEF");
			tx.setCharacterSize(33);
			wnd.draw(tx);
			wnd.display();

			// Unloading
			wnd.close();

			// Pass.
		}
	};
}