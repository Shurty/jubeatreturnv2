
#include <JREngine/jrshared.h>
#include "StateSplash.h"
#include "Background.h"

StateSplash::StateSplash(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_SPLASH;
	m_name = "Splash";
	Logger::Log << "State construct: " << m_name << std::endl;
}

void StateSplash::OnInit(Parameters &pm)
{
	Logger::Log << "State init: " << m_name << std::endl;
}

void StateSplash::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
	Logger::Log << "StateSplash: Begin" << std::endl;
	m_core.GetGUI()->HideAll();
	m_core.GetGUI()->Show(PANEL_SPLASH);
	m_core.m_songs.FilterAvailable([](Beatmap* bt) { return (true); });
	m_core.m_songs.RefreshFilteringSorting();
}

void StateSplash::OnRender(Parameters &pm)
{
	m_core.m_render.OnPreRender(pm);
	m_core.m_gui.m_mainBg->Render(&m_core);
	m_core.m_gui.Render();
	m_core.m_gui.m_mainBg->RenderGridOverlay(&m_core);
	m_core.m_gui.m_mainBg->RenderCredits(&m_core);

	m_core.m_render.OnRender(pm);
}

void StateSplash::OnUpdate(Parameters &pm)
{
	m_core.m_input->PollEvents(&m_core.m_render, &m_core);
	m_core.m_network.Update();
	m_core.m_songs.Update();
	m_core.m_sounds.Update();
	m_core.m_gui.m_mainBg->Update(&m_core);
	m_core.m_gui.Update();
	m_core.m_tweener.step(m_core.GetMainClock()->getElapsedTime().asMilliseconds());
}

void StateSplash::OnEnd(Parameters &pm)
{
	m_core.m_songs.FilterAvailable([](Beatmap* bt) { return (false); });
	m_core.m_songs.RefreshFilteringSorting();
}

void StateSplash::OnUnload(Parameters &pm)
{
}

/* This is definitly not about JubeatReturn project */