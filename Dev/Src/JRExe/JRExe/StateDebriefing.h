
#pragma once

#include <JREngine/jrshared.h>
#include "JRExe.h"

class StateDebriefing : public GameState
{
private:
	JRExe&	m_core;
public:
	StateDebriefing(JRExe&);

	void OnInit(Parameters &pm);
	void OnStart(Parameters &pm);
	void OnRender(Parameters &pm);
	void OnUpdate(Parameters &pm);
	void OnEnd(Parameters &pm);
	void OnUnload(Parameters &pm);

};