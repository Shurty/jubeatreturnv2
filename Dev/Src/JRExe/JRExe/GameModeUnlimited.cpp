#include <JREngine/jrshared.h>
#include "GameModeUnlimited.h"
#include "Background.h"

GamemodeUnlimited::GamemodeUnlimited(JRExe* core) 
	: IGamemode(), m_core(core)
{
	m_name = "Unlimited Mode";
	m_selectSndName = "gm_select_unlimited.ogg";
	m_isActive = false;
	m_isGameRunning = false;
	m_isGameEnding = false;
	m_gameEndTimestamp = 0;
	m_selectLogo = core->m_render.GetTexture(TEXTURESDIR + std::string("Gamemodes/gmUnlimited.png"));
}

GamemodeUnlimited::~GamemodeUnlimited(void)
{

}

void 	GamemodeUnlimited::saveScore()
{
	Player* p = m_core->m_playersManager.getCurrentPlayer();
	Logger::Log << "current score: " << m_scoreManager.getScoreWithCombo() << " || previous Score: " << p->getHighScoreFor(m_selectedBeatmap->name, m_selectedBeatmap->songLevel) << std::endl;
	
	// fill these details like it's first time we do it.
	Player::MusicScoreRecord::Details details;
	details.bestScore = m_scoreManager.getScoreWithCombo();
	details.maxCombo = m_scoreManager.getMaxCombo();
	details.maxGoodBeat =	m_scoreManager.getNbBeatResult(BEATRESULT::BR_GOOD) +
							m_scoreManager.getNbBeatResult(BEATRESULT::BR_GREAT) +
							m_scoreManager.getNbBeatResult(BEATRESULT::BR_PERFECT);
	details.maxPerfect = m_scoreManager.getNbBeatResult(BEATRESULT::BR_PERFECT);
	details.timesCleared = ((m_scoreManager.getPotentialGrade() >= GRADE::GRADE_C ) ? 1 : 0);
	details.timesPlayed = 1;
	// details.timestampHighscore = std::time(nullptr); // these 2 lines are not checked by function p->updateWithDetails
	// details.timestampLastPlayed = std::time(nullptr); 
	details.totalScore = m_scoreManager.getPotentialScore();

	// then pass these details to player, he will save or ignore everything depending of its past
	const Player::MusicScoreRecord& musicRecordUpdated = p->updateWithDetails(details, m_selectedBeatmap->name, m_selectedBeatmap->songLevel);
	
	m_core->m_playersManager.saveMusicRecordForPlayer(musicRecordUpdated, p, m_selectedBeatmap->name, m_selectedBeatmap->songLevel);

	//if (p->saveScoreIfHigher(m_selectedBeatmap->name, m_selectedBeatmap->songLevel, m_scoreManager.getScoreWithCombo()))
	//{
	//	m_core->m_playersManager.saveScore(m_scoreManager.getScoreWithCombo(), p, m_selectedBeatmap->name, m_selectedBeatmap->songLevel);
	//}
}

void 	GamemodeUnlimited::loadScore()
{
	// FIXME: this function erase current player (we should load then we connect the player, but the player is already connected at this point)
	// m_core->m_playersManager.loadPlayers();
}

GMMODE_ID GamemodeUnlimited::GetId()
{
	return GMMD_UNLIMITED;
}

// FIXME: this is not called ?
void GamemodeUnlimited::Init()
{
	// Here is the init of the gamemode, called on creation. For game init, go to gamestart callback
	m_isGameRunning = false;
	m_isGameEnding = false;
}

void GamemodeUnlimited::Update()
{
	static int graphTimestamp = 0;
	static int graphCount = 0;
	int timestamp = m_gameClock.getElapsedTime().asMilliseconds();

	if (m_isGameRunning)
	{
		float pressTime = m_core->m_params.m_params.press_range / 2;
		while (m_beatit != m_selectedBeatmap->beatsList.end() && 
			(*m_beatit).time <= timestamp + m_pressStartOffset)
		{
			//Logger::Log << "Press #" << (int)((*m_beatit).activate) << " at " << (*m_beatit).time << std::endl;
			m_core->m_gui.BroadcastEvent(TRIGGERED, (void *)(m_beatit)->activate);
			m_core->m_gui.BroadcastEvent(TRIGGERBEAT, (void *)&(*m_beatit));
			++m_beatit;
			graphCount++;
		}
		if (timestamp > graphTimestamp)
		{
			//Logger::Log << "Graph value = " << graphCount << " at " << graphTimestamp << std::endl;
			graphTimestamp += m_selectedBeatmap->graphStep;
			graphCount = 0;
		}
		if (m_beatit == m_selectedBeatmap->beatsList.end())
		{
			m_gameEndTimestamp = timestamp + 4000;
			m_isGameRunning = false;
			m_isGameEnding = true;
			m_core->m_sounds.FadeOutMusic();
		}
	}
	else if (m_isGameEnding && timestamp > m_gameEndTimestamp)
	{
		m_gameEndTimestamp = 0;
		graphTimestamp = 0;
		graphCount = 0;
		m_core->m_sounds.GetMusic().stop();
		OnGameEnd();
	}
}

void GamemodeUnlimited::Render()
{

}

void GamemodeUnlimited::OnIconRender(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
{
	sf::RectangleShape rect(sf::Vector2f(x2 - x1, y2 - y1));

	rect.setPosition(x1, y1);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	rect.setTexture(m_selectLogo);
	target->GetRenderWindow()->draw(rect);
}

void GamemodeUnlimited::OnSelectedTopRender(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
{
	float ratio = target->GetRenderingRatio();

	sf::RectangleShape rect;
	sf::Text text;
	sf::FloatRect textRect;

	/* Panel title */
	text.setPosition(x1, y1);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(60 * ratio);
	text.setString("Unlimited Mode");
	target->GetRenderWindow()->draw(text);

	text.setPosition(x1, y1 + 80 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(40 * ratio);
	text.setString("Play all musics without any limitation");
	target->GetRenderWindow()->draw(text);
}


void GamemodeUnlimited::OnSetActive()
{

}

void GamemodeUnlimited::OnUnload()
{

}

bool GamemodeUnlimited::IsActive() const
{
	return m_isActive;
}

void GamemodeUnlimited::OnBeatAction(BEATRESULT res, GUIWidget *widget)
{
	if (!isGameRunning() && !m_isGameEnding)
		return ;


	if (m_core->m_params.m_params.simulation)
	{
		if (res != BEATRESULT::BR_PERFECT) {
			if (m_beatit != m_selectedBeatmap->beatsList.end()) {
				Logger::Log << "WARNING: Simulation resulted on a non perfect result on beat press:" << (int)m_beatit->activate <<
					"-time:" << m_beatit->time << "-#combo:" << m_scoreManager.getBonusNotesCount() << std::endl;
			}
		}
	}

	m_scoreManager.scoreBeat(res);
	m_rewardsManager.hasScoredBeat(res);

	double scM = m_scoreManager.getBonusScoreMultiplier();
	if (scM == 1) {
		m_core->m_gui.m_mainBg->m_generateCloudsCount = MAX(0, MIN(BG_CLOUDS_NB, BG_CLOUDS_NB * scM));
	} else {
		m_core->m_gui.m_mainBg->m_generateCloudsCount = MAX(0, MIN(BG_CLOUDS_NB, BG_CLOUDS_NB * scM * 0.7));
	}
	m_core->m_gui.m_mainBg->m_generateBubblesCount = MAX(0, MIN(BG_BUBBLES_NB, BG_BUBBLES_NB * scM));
}

void GamemodeUnlimited::OnGamemodeSelected()
{
	m_core->m_sounds.PlaySnd(SOUNDDIR + GetSoundName());
	loadScore();
}

void GamemodeUnlimited::OnGamemodeConfirmed()
{
	m_isActive = true;
	m_rewardsManager.m_rewardCombo.m_player = m_core->m_playersManager.getCurrentPlayer();
}

void GamemodeUnlimited::OnGameSet(Beatmap *res)
{
	// Panel will swap to gamestate "Playing", you don't need to
	//m_scoreManager.reset(res->beats);
	m_selectedBeatmap = res;
	m_core->m_sounds.StopPreviewMusic();
	m_beatit = m_selectedBeatmap->beatsList.begin();
	//m_core->m_params.m_params.press_range = MAX(MIN(120000.0 / (float)m_selectedBeatmap->originalBpm, 900), 900);
	m_core->m_params.m_params.press_range = m_core->m_params.m_params.press_durations[m_selectedBeatmap->level];
	m_scoreManager.reset(m_selectedBeatmap->beats);
	m_core->m_gui.m_mainBg->m_generateCloudsCount = 0;
	m_core->m_gui.m_mainBg->m_generateBubblesCount = 0;
	//m_core->m_gui.m_mainBg->m_minBubbleGenTime = 1000 / m_selectedBeatmap->originalBpm * 1000;

	int targetDt = m_core->m_params.m_params.press_range / 2;
	m_pressStartOffset = targetDt +
		m_core->m_params.m_params.press_offset +
		targetDt * m_core->m_params.m_params.press_img_delay;
}

void GamemodeUnlimited::OnMusicSelect(Beatmap *res)
{
	if (m_core->m_params.m_params.wav_mode) {
		m_core->m_sounds.PlayPreviewMusic(MUSICDIR + res->src + MUSICEXTWAV);
	} else {
		m_core->m_sounds.PlayPreviewMusic(MUSICDIR + res->src + MUSICEXT);
	}
	m_selectedBeatmap = res;
}

void GamemodeUnlimited::OnGameStart()
{
	if (m_core->m_params.m_params.wav_mode) {
		m_core->m_sounds.PlayMusic(MUSICDIR + m_selectedBeatmap->src + MUSICEXTWAV);
	}
	else {
		m_core->m_sounds.PlayMusic(MUSICDIR + m_selectedBeatmap->src + MUSICEXT);
	}
	m_gameClock.restart();
	m_isGameRunning = true;
	m_isGameEnding = false;
}

void GamemodeUnlimited::OnGameEnd()
{
	// now save the new highscore !
	this->saveScore();
	this->m_scoreManager.logScores();
	// Not panel controlled - Switch to debriefing here
	m_isGameRunning = false;
	m_isGameEnding = false;
	m_core->m_gsm.ChangeState(GMST_DEBRIEFING);
	m_core->m_gui.m_mainBg->m_generateCloudsCount = BG_CLOUDS_NB;
	m_core->m_gui.m_mainBg->m_generateBubblesCount = BG_BUBBLES_NB/2;

}

void GamemodeUnlimited::OnDebriefingExit()
{
	m_core->m_gsm.ChangeState(GMST_MUSIC_SELECT);
}

bool GamemodeUnlimited::isGameRunning()
{
	return m_isGameRunning;
}

ScoreManager& GamemodeUnlimited::GetScoreManager()
{
	return (m_scoreManager);
}

void GamemodeUnlimited::OnPlayKeyboardButtonInit(GUIKeyboard *kb, GUIWidget *wdg)
{
	Beat *bt = &m_selectedBeatmap->beatsList.front();
	std::deque<Beat>::iterator it;

	it = m_selectedBeatmap->beatsList.begin();
	while (it->time == bt->time)
	{
		if (it->activate == wdg->GetLinkedKey())
		{
			wdg->ReceiveEvent(FIRSTBTN_SET, NULL);
		}
		++it;
	}
	// Example of use for this callback
	/*
	wdg->LinkUserData(this);
	wdg->SetRender([] (GUIWidget *wdg, RenderingEngine *tgt) {
		GamemodeUnlimited *gm = (GamemodeUnlimited*)wdg->GetUserData();
		sf::Vector2f pos, size;
		sf::Text tx;
		sf::FloatRect sz;
		int pt = (gm->GetPlayTime() / 100) % 20;

		pos = wdg->GetPosition();
		size = wdg->GetSize();

		sf::RectangleShape rect(sf::Vector2f(size.x - 20, size.y - 20));

		rect.setOutlineThickness(pt + 5);
		rect.setOutlineColor(sf::Color(0, 50, 0, 255));
		rect.setPosition(pos.x, pos.y);
		rect.setFillColor(sf::Color(0, 50, 0, 0));
		tgt->GetRenderWindow()->draw(rect);
	});
	*/
}

void GamemodeUnlimited::OnDebriefingTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2)
{

}

void GamemodeUnlimited::OnPlayTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2)
{

}
