#pragma once

#include <sstream>
#include "JRExe.h"

#include "GameModeStandard.h"

class GamemodeStandard3 :
	public GamemodeStandard
{
private:

public:
	GamemodeStandard3(JRExe *core) :
		GamemodeStandard(core)
	{
		m_selectLogo = core->m_render.GetTexture(TEXTURESDIR + std::string("Gamemodes/gmStandard3.png"));
		m_maxMusics = 3;
	}

	~GamemodeStandard3()
	{

	}

	virtual GMMODE_ID GetId() { return GMMD_STANDARD3; }
};
