
#include <iostream>
#include <JREngine/jrgui.h>
#include "PlayPanel.h"
#include "GUIPanel.h"
#include "GUIBeatButton.h"
#include "JRExe.h"

PlayPanel::PlayPanel(JRExe *gm) 
	: GUIPanel(PANEL_PLAY, gm), m_keyboard(*gm->GetParams()), m_keyboardTop(*gm->GetParams()), m_graph(gm), m_gm(gm)
{
	m_step = 10;
	m_graph.MarkAsPlaying(true);

	for (int i = 0; i < 16; i++) {
		GUIBeatButton *btn = new GUIBeatButton(gm);
		btn->LinkUserData(this);
		m_keyboard.SetWidget(i, btn);
	}

	for (int i = 0; i < 16; i++) {
		GUIBeatButton *btn = new GUIBeatButton(gm);
		btn->LinkUserData(this);
		btn->SetDummy(true);
		m_keyboardTop.SetWidget(i, btn);
	}
}

PlayPanel::~PlayPanel()
{

}

void PlayPanel::Init()
{
	int py = (int)(m_size.y - m_size.x);
	int sz = (int)m_size.x;

	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	float ratio = m_gm->m_render.GetRenderingRatio();
	float internalPadding = 21 * ratio;
	float posY = 100 * ratio + internalPadding * 2;
	float svh = sh - posY * 2;

	m_keyboard.SetPosition(0, (float)py);
	m_keyboard.SetSize((float)sz, (float)sz);
	m_keyboard.Init();

	m_keyboardTop.SetPosition(internalPadding, posY + internalPadding);
	m_keyboardTop.SetSize(svh - internalPadding * 2, svh - internalPadding * 2);
	m_keyboardTop.Init(false);

	m_graph.SetMaxX(80);
	m_graph.SetMaxY(12);
}

void PlayPanel::Start()
{
	m_posX = 300;
	m_headerX = 50;
	m_headerAlpha = 0;
	m_alpha = 0;

	m_targetScore = 0;
	m_displayScore = 0;
	m_displayCombo = 0;
	m_comboAnim = 0;

	m_startState = STARTSTATE_READY;

	tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 0);
	param.addProperty(&m_headerX, 0);
	param.addProperty(&m_headerAlpha, 255);
	param.forceEnd = true;
	m_gm->m_tweener.addTween(param);

	tween::TweenerParam param2(1300, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param2.addProperty(&m_alpha, 255);
	param2.forceEnd = true;
	m_gm->m_tweener.addTween(param2);

	for (int i = 0; i < 16; i++) {
		m_gm->m_gmm->currentMode()->OnPlayKeyboardButtonInit(&m_keyboard, m_keyboard.GetWidget(i));
		m_keyboard.GetWidget(i)->OnReleased();
	}
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/ready.wav"));
}

void PlayPanel::Stop()
{
}

void PlayPanel::RenderHeader(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;
	float internalPadding = 21 * ratio;
	Beatmap *bmp = m_gm->m_gmm->currentMode()->GetSelectedBeatmap();

	sf::RectangleShape rect(sf::Vector2f(sw, 100 * ratio + internalPadding * 2));
	sf::Text text;
	sf::FloatRect textRect;

	rect.setFillColor(sf::Color(0, 0, 0, m_headerAlpha * 0.4));
	rect.setPosition(0, 0);
	target->GetRenderWindow()->draw(rect);
	rect.setPosition(0, sh - (internalPadding * 2 + 100 * ratio));
	target->GetRenderWindow()->draw(rect);


	rect.setSize(sf::Vector2f(100 * ratio, 100 * ratio));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	rect.setPosition(m_headerX + internalPadding, internalPadding);
	rect.setTexture(target->GetTexture(TEX_COVER_BG_SMALL));
	target->GetRenderWindow()->draw(rect);

	rect.setSize(sf::Vector2f(90 * ratio, 90 * ratio));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	rect.setTexture(target->GetTexture(TEXTURESDIR + std::string("Musics/" + bmp->src + ".jpg")));
	rect.setTextureRect(sf::IntRect(0, 0, rect.getTexture()->getSize().x, rect.getTexture()->getSize().y));
	rect.setPosition(m_headerX + internalPadding + 5 * ratio, internalPadding + 5 * ratio);
	target->GetRenderWindow()->draw(rect);

	/* Texts*/
	float textX = internalPadding * 2 + 100 * ratio + m_headerX;

	text.setPosition(textX, internalPadding);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setOrigin(0, 0);
	text.setCharacterSize(35 * ratio);
	text.setString(bmp->name);
	target->GetRenderWindow()->draw(text);

	text.setPosition(textX, internalPadding + 40 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setOrigin(0, 0);
	text.setCharacterSize(24 * ratio);
	text.setString(bmp->author);
	target->GetRenderWindow()->draw(text);

	/* Gamemode and difficulty */
	rect.setSize(sf::Vector2f(100 * ratio, 100 * ratio));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	rect.setTexture(target->GetTexture(TEX_DIFFICULTY_BG));
	rect.setPosition(sw - m_headerX - internalPadding - 100 * ratio, internalPadding);
	rect.setTextureRect(sf::IntRect(0, 0, 101, 102));
	target->GetRenderWindow()->draw(rect);

	rect.setSize(sf::Vector2f(90 * ratio, 90 * ratio));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	if (bmp->songLevel == SL_BASIC)
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_GREEN));
	else if (bmp->songLevel == SL_INTERMEDIATE)
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_ORANGE));
	else	
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_RED));
	rect.setPosition(sw - m_headerX - internalPadding + 5 * ratio - 100 * ratio, internalPadding + 5 * ratio);
	rect.setTextureRect(sf::IntRect(0, 0, 87, 87));
	target->GetRenderWindow()->draw(rect);

	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(35 * ratio);
	text.setString(std::to_string(bmp->level));
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width/2.0f,
				   textRect.top  + textRect.height/2.0f);
	text.setPosition(sw - m_headerX - internalPadding - 100/2 * ratio, internalPadding + 100/2 * ratio);
	target->GetRenderWindow()->draw(text);

	text.setPosition(sw - m_headerX - internalPadding * 2 - 100 * ratio, internalPadding);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(35 * ratio);
	text.setString(m_gm->m_gmm->currentMode()->GetName());
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width,
				   0);
	target->GetRenderWindow()->draw(text);

	text.setPosition(sw - m_headerX - internalPadding * 2 - 100 * ratio, internalPadding + 40 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setCharacterSize(24 * ratio);
	if (bmp->songLevel == SL_EXTREME) {
		text.setString("EXTREME");
		text.setColor(sf::Color(255, 0, 0, m_headerAlpha));
	} else if (bmp->songLevel == SL_INTERMEDIATE) {
		text.setString("NORMAL");
		text.setColor(sf::Color(255, 255, 0, m_headerAlpha));
	} else {
		text.setString("BASIC");
		text.setColor(sf::Color(0, 255, 0, m_headerAlpha));
	}
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width,
				   0);
	target->GetRenderWindow()->draw(text);

	/* Render playbox bg */
	float posY = 100 * ratio + internalPadding * 2;
	float svh = sh - posY * 2;

	rect.setSize(sf::Vector2f(svh - internalPadding * 2, svh - internalPadding * 2));
	rect.setFillColor(sf::Color(80, 80, 80, m_headerAlpha));
	rect.setPosition(internalPadding + m_headerX, posY + internalPadding);
	rect.setTexture(target->GetTexture(TEX_PLAYBOX_BG));
	rect.setTextureRect(sf::IntRect(0, 0, 340, 346));
	target->GetRenderWindow()->draw(rect);

	/* Render active scoring */
	float ptextX = svh + m_headerX;
	float ptextY = posY + internalPadding;
	/*
	for (int i = 0; i < 4; i++)
	{
		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
		text.setCharacterSize(20 * ratio);
		text.setString("Player");
		text.setOrigin(0, 0);
		text.setPosition(ptextX, ptextY + i * 25 * ratio);
		target->GetRenderWindow()->draw(text);

		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
		text.setCharacterSize(20 * ratio);
		text.setString("100 000");
		text.setPosition(ptextX + 100 * ratio, ptextY + i * 25 * ratio);
		target->GetRenderWindow()->draw(text);
	}*/

	ptextY = ptextY + svh - internalPadding * 2;

	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(60 * ratio);
	text.setString(std::to_string((int)m_displayScore));
	text.setPosition(ptextX, ptextY);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(32 * ratio);
	text.setString("(+0)");
	text.setPosition(ptextX + textRect.width + 20 * ratio, ptextY);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(60 * ratio);
	text.setString(m_gm->m_playersManager.getCurrentPlayer()->getPseudo());
	text.setPosition(ptextX, ptextY - 70 * ratio);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(32 * ratio);
	text.setString("RANK 1");
	text.setPosition(ptextX + textRect.width + 20 * ratio, ptextY - 70 * ratio);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	/* The ranking! */
	float rankX = sw - internalPadding + m_headerX;
	float rankY = posY + internalPadding;

	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(30 * ratio);
	text.setString("ACTIVE RANKING");
	text.setPosition(rankX, rankY);
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width, 0);
	target->GetRenderWindow()->draw(text);

	rect.setSize(sf::Vector2f(textRect.width, textRect.width));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	rect.setPosition(rankX - textRect.width, rankY + 40 * ratio);
	rect.setTexture(target->GetTexture((TEXID)(TEX_GRADE_E + m_gm->m_gmm->currentMode()->GetScoreManager().getPotentialGrade())));
	rect.setTextureRect(sf::IntRect(0, 0, 143, 135));
	target->GetRenderWindow()->draw(rect);

	/* Graph */
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(150, 150, 150, m_headerAlpha));
	text.setCharacterSize(20 * ratio);
	text.setString("START");
	text.setPosition(internalPadding - 5 * ratio + 75 * ratio - m_headerX, sh - 5 * ratio);
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(150, 150, 150, m_headerAlpha));
	text.setCharacterSize(20 * ratio);
	text.setString("END");
	text.setPosition(sw - internalPadding + 5 * ratio - 75 * ratio + m_headerX, sh - 5 * ratio);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	m_graph.SetPosition(internalPadding + 75 * ratio, sh - (internalPadding + 70 * ratio));
	m_graph.SetSize(sw - internalPadding * 2 - 150 * ratio, 70 * ratio + internalPadding);
	m_graph.Draw(target);

	/* Stats/achivements/challenges */
	/* TODO? */

	//tx.setFont(target->GetFont(FONT_TITLE_TX));
	//tx.setString(std::to_string((int)m_displayScore));
	//tx.setCharacterSize(50);
	//tx.setColor(sf::Color(255, 255, 255, 255));
	//sz = tx.getLocalBounds();
	//tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 - 150);
	//target->GetRenderWindow()->draw(tx);

	m_gm->m_gmm->currentMode()->OnPlayTopRender(target, m_headerAlpha, 0, 0, sw, sh);
}

void PlayPanel::Draw(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();

	if (m_startState != STARTSTATE_PLAYING)
	{
		sf::Text tx;
		sf::Vector2f pos, size;
		sf::FloatRect sz;

		pos = GetPosition();
		size = GetSize();
		pos.y += size.y - size.x;
		size.y = size.x;

		tx.setFont(target->GetFont(FONT_TITLE_TX));
		if (m_startState == STARTSTATE_WAIT) {
			tx.setString("Please wait.");
			tx.setCharacterSize(150 * ratio);
			pos.x += m_posX;
		}
		if (m_startState == STARTSTATE_READY) {
			tx.setString("Ready?");
			tx.setCharacterSize(300 * ratio);
			pos.x += m_posX;
		}
		if (m_startState == STARTSTATE_GO) {
			tx.setString("GO!");
			tx.setCharacterSize(400 * ratio);
			pos.y -= m_posX;
		}
		tx.setColor(sf::Color(255, 255, 255, m_alpha));
		sz = tx.getLocalBounds();
		tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
		target->GetRenderWindow()->draw(tx);
	}
	else
	{
		sf::Text tx;
		sf::Vector2f pos, size;
		sf::FloatRect sz;

		pos = GetPosition();
		size = GetSize();
		pos.y += size.y - size.x;
		size.y = size.x;

		if (m_displayCombo > 4)
		{
			tx.setFont(target->GetFont(FONT_SCORING_TX));
			tx.setString(std::to_string((int)m_displayCombo));
			tx.setCharacterSize(480 * ratio);
			pos.y -= m_comboAnim;
			tx.setColor(sf::Color(80, 80, 80, 150));
			sz = tx.getLocalBounds();
			tx.setOrigin(sz.left + sz.width / 2, sz.top + sz.height / 2);
			tx.setPosition(pos.x + size.x / 2, pos.y + size.y / 2 - 95 * ratio);
			target->GetRenderWindow()->draw(tx);
			pos.y += m_comboAnim;

			tx.setFont(target->GetFont(FONT_SCORING_TX));
			tx.setString("COMBO");
			tx.setCharacterSize(70 * ratio);
			tx.setColor(sf::Color(80, 80, 80, 150));
			tx.setPosition(pos.x + size.x / 2 + m_params.m_params.btn_size + m_params.m_params.btn_step / 2, 
				pos.y + size.y / 2 + sz.height / 2 - 55 * ratio);
			sz = tx.getLocalBounds();
			tx.setOrigin(sz.left + sz.width, sz.top + sz.height / 2);
			target->GetRenderWindow()->draw(tx);
		}

		/* Display the ranking difference and player name */	
		/*tx.setFont(target->GetFont(FONT_SCORING_TX));
		tx.setString("+54120");
		tx.setCharacterSize(40 * ratio);
		tx.setColor(sf::Color(0, 255, 0, 150));
		sz = tx.getLocalBounds();
		tx.setOrigin(sz.left + sz.width, sz.top + sz.height / 2);
		tx.setPosition(pos.x + size.x / 2 + m_params.m_params.btn_size + m_params.m_params.btn_step / 2 - 10 * ratio, 
			pos.y + size.y / 2 + m_params.m_params.btn_size + m_params.m_params.btn_step / 2 - 50 * ratio);
		target->GetRenderWindow()->draw(tx);
		*/
		tx.setFont(target->GetFont(FONT_SCORING_TX));
		tx.setString(m_gm->m_playersManager.getCurrentPlayer()->getPseudo());
		tx.setCharacterSize(25 * ratio);
		tx.setColor(sf::Color(255, 255, 255, 150));
		sz = tx.getLocalBounds();
		tx.setOrigin(sz.left + sz.width, sz.top + sz.height / 2);
		tx.setPosition(pos.x + size.x / 2 + m_params.m_params.btn_size + m_params.m_params.btn_step / 2 - 10 * ratio, 
			pos.y + size.y / 2 + m_params.m_params.btn_size + m_params.m_params.btn_step / 2 - 80 * ratio);
		target->GetRenderWindow()->draw(tx);

		/* Rank state */
		tx.setFont(target->GetFont(FONT_SCORING_TX));
		tx.setString("RANK");
		tx.setCharacterSize(25 * ratio);
		tx.setColor(sf::Color(255, 255, 255, 150));
		sz = tx.getLocalBounds();
		tx.setOrigin(sz.left, sz.top + sz.height / 2);
		tx.setPosition(pos.x + size.x / 2 - m_params.m_params.btn_size - m_params.m_params.btn_step / 2 + 10 * ratio, 
			pos.y + size.y / 2 + m_params.m_params.btn_size + m_params.m_params.btn_step / 2 - 50 * ratio);
		target->GetRenderWindow()->draw(tx);

		tx.setFont(target->GetFont(FONT_TITLE_TX));
		tx.setString("1st");
		tx.setCharacterSize(60 * ratio);
		tx.setColor(sf::Color(255, 255, 0, 150));
		sz = tx.getLocalBounds();
		tx.setOrigin(sz.left, sz.top + sz.height / 2);
		tx.setPosition(pos.x + size.x / 2 - m_params.m_params.btn_size - m_params.m_params.btn_step / 2 + 80 * ratio, 
			pos.y + size.y / 2 + m_params.m_params.btn_size + m_params.m_params.btn_step / 2 - 50 * ratio);
		target->GetRenderWindow()->draw(tx);
	}

	RenderHeader(target);

	return;
}

void PlayPanel::Update(sf::Clock *time)
{
	GUIPanel::Update(time);
	if (m_startState == STARTSTATE_READY && (int)m_alpha == 255)
	{
		m_startState = STARTSTATE_GO;
		m_alpha = 255;
		m_posX = 100;
		tween::TweenerParam param(1000, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.forceEnd = true;
		param.addProperty(&m_alpha, 0);
		param.addProperty(&m_posX, 0);
		m_gm->m_tweener.addTween(param);
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/go.wav"));
	}
	else if (m_startState == STARTSTATE_GO && (int)m_alpha == 0)
	{
		m_startState = STARTSTATE_PLAYING;
		m_gm->m_gmm->currentMode()->OnGameStart();
	}
	else if (m_startState == STARTSTATE_PLAYING)
	{
		unsigned int curScore = m_gm->m_gmm->currentMode()->GetScoreManager().getScoreWithoutCombo();
		if (m_targetScore != curScore)
		{
			tween::TweenerParam param(600, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
			param.addProperty(&m_displayScore, curScore);
			m_gm->m_tweener.addTween(param);

			m_targetScore = curScore;
		}

		unsigned int curCombo = m_gm->m_gmm->currentMode()->GetScoreManager().getBonusNotesCount();
		if (m_displayCombo != curCombo)
		{
			m_comboAnim = 35;

			tween::TweenerParam param(350, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
			param.addProperty(&m_comboAnim, 0);
			m_gm->m_tweener.addTween(param);

			m_displayCombo = curCombo;
		}
	}
}

void PlayPanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIPanel::ReceiveEvent(ev, param);
	m_keyboard.ReceiveEvent(ev, param);
	m_keyboardTop.ReceiveEvent(ev, param);
}