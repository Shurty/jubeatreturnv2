
#pragma once

#include <iostream>
#include <JREngine/jrshared.h>

class CommandPing : public ACommandPing
{
private:

public:
	CommandPing() 
		: ACommandPing()
	{

	}

	CommandPing::~CommandPing()
	{

	}

	virtual void Receive(Client *cl, bool isapi)
	{
		
	}

	virtual void Send(Client *cl, bool isapi)
	{
		sf::Packet pck;

		pck << GetID() << "PING";
		cl->GetSock().send(pck);
	}
};
