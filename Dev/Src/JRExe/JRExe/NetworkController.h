
#pragma once

#include <JREngine/jrshared.h>

class JRExe;

class NetworkController : public Client
{
private:
	JRExe* m_jre;
	std::string m_ip;
	int m_port;
	std::vector<Command *> m_commands;

public:
	NetworkController(JRExe *jre);
	~NetworkController();

	Command* GetCommand(std::string const &id);
	void Update();
	bool Connect();

};