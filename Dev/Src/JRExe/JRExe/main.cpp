
#include <iostream>
#include <string>
#include <sstream>
#include "JRExe.h"

int main(int ac, char **av)
{
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	Logger::Log << "|:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|" << std::endl;
	Logger::Log << "|:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|" << std::endl;
	Logger::Log << "|:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|" << std::endl;
	Logger::Log << "|:::::::'##:'##::::'##:'########::'########::::'###::::'########::|" << std::endl;
	Logger::Log << "|::::::: ##: ##:::: ##: ##.... ##: ##.....::::'## ##:::... ##..:::|" << std::endl;
	Logger::Log << "|::::::: ##: ##:::: ##: ##:::: ##: ##::::::::'##:. ##::::: ##:::::|" << std::endl;
	Logger::Log << "|::::::: ##: ##:::: ##: ########:: ######:::'##:::. ##:::: ##:::::|" << std::endl;
	Logger::Log << "|:'##::: ##: ##:::: ##: ##.... ##: ##...:::: #########:::: ##:::::|" << std::endl;
	Logger::Log << "|: ##::: ##: ##:::: ##: ##:::: ##: ##::::::: ##.... ##:::: ##:::::|" << std::endl;
	Logger::Log << "|:. ######::. #######:: ########:: ########: ##:::: ##:::: ##:::::|" << std::endl;
	Logger::Log << "|::......::::.......:::........:::........::..:::::..:::::..::::::|" << std::endl;
	Logger::Log << "|::::::::::::::::::::::::::::::: ____ ____ ___ _  _ ____ _  _   ::|" << std::endl;
	Logger::Log << "|::::::::::::::::::::::::::::::: |__/ |___  |  |  | |__/ |\\ |   ::|" << std::endl;
	Logger::Log << "|::::::::::::::::::::::::::::::: |  \\ |___  |  |__| |  \\ | \\|   ::|" << std::endl;
	Logger::Log << "|:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|" << std::endl;
	Logger::Log << "|:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|" << std::endl;
	Logger::Log << "|:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|" << std::endl;
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	Logger::Log << "                " << std::endl;
	Logger::Log << "  VERSION:      " << VERSION << std::endl;
	Logger::Log << "  Authoring:    " << "Thierry BERGER" << std::endl;
	Logger::Log << "                " << "Jeremy DUBUC" << std::endl;
	Logger::Log << "                " << "Quentin GUAY" << std::endl;
	Logger::Log << "                " << "Timothee MAURIN" << std::endl;
	Logger::Log << "  Libs:         " << "Arduino IO" << std::endl;
	Logger::Log << "                " << "SFML" << std::endl;
	Logger::Log << "                " << "SFVideo" << std::endl;
	Logger::Log << "                " << "MessagePack" << std::endl;
	Logger::Log << "                " << std::endl;
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	Logger::Log << "		JRExe Initialization" << std::endl;
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;

	JRExe jre;

	if (ac > 1 && std::string(av[1]) == "-t")
	{
		jre.SetTestLevel(TLVL_TEST_ONLY);
	}

	if (jre.GetTestLevel() == TLVL_ALL || jre.GetTestLevel() == TLVL_TEST_ONLY)
	{
		Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
		Logger::Log << "		JRExe Unit tests set 1 begin" << std::endl;
		Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	
		jre.RunTests();
	}

	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	Logger::Log << "		JRExe Starting up" << std::endl;
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;

	sf::Time updateTime = sf::seconds(0);
	sf::Time renderTime = sf::seconds(0);

	sf::Time timeToFps = sf::seconds(1);
	unsigned int timeDrawn = 0;
	
	jre.m_currentFps = 60;

	jre.OnStart();
	
	if (jre.GetTestLevel() == TLVL_ALL || jre.GetTestLevel() == TLVL_TEST_ONLY)
	{
		Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
		Logger::Log << "		JRExe Unit tests set 2 begin" << std::endl;
		Logger::Log << "+-----------------------------------------------------------------+" << std::endl;

		jre.RunTests();
	}

	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	Logger::Log << "		JRExe Started" << std::endl;
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;

	sf::Time updateStartTime = jre.m_globClock.getElapsedTime();

	if (jre.GetTestLevel() == TLVL_TEST_ONLY)
	{
		Logger::Log << "Test mode - Exiting" << std::endl;
		jre.SetValid(false);
	}

	while (jre.IsValid()) {

		sf::Time totalUpdateStartTime = jre.m_globClock.getElapsedTime();
		jre.OnUpdate();
		// compute update time (without render)
		updateTime = jre.m_globClock.getElapsedTime() - updateStartTime;
		//Logger::Log << "frametime: " << FRAMETIME << " || updatetime: " << updateTime.asMilliseconds() << std::endl;
		// if total update time is greater than 1 frametime, update without rendering to gain time.
		while (updateTime.asMilliseconds() + renderTime.asMilliseconds() > FRAMETIME)
        {
			//Sleep(1000);
			jre.OnUpdate();
            updateTime = sf::milliseconds(updateTime.asMilliseconds() - FRAMETIME);
        }
		// update time might be negative at this point, this should lead to sleep because we updated a bit in advance to anticipate lag
		updateStartTime = jre.m_globClock.getElapsedTime();

		jre.OnRender();
		jre.OnPostRender();

		// compute render time
		renderTime = jre.m_globClock.getElapsedTime() - updateStartTime;
		// Logger::Log << "render time: " << (jre.m_globClock.getElapsedTime() - updateStartTime).asMilliseconds() << std::endl;

		sf::Time timeSleeping = sf::milliseconds(FRAMETIME - (updateTime + renderTime).asMilliseconds());
		// Logger::Log << "timesleeping: " << timeSleeping.asMilliseconds() << std::endl;
		
		/*if (timeSleeping.asMilliseconds() > 0)
			Sleep(timeSleeping.asMilliseconds());
		*/

		updateStartTime = jre.m_globClock.getElapsedTime();

#if SHOW_FPS
		++timeDrawn;
		if (timeToFps.asMilliseconds() <= 0)
		{
			timeToFps = sf::seconds(1);
			jre.setCurentFps(timeDrawn);
			timeDrawn = 0;
			// Logger::Log << "fps: " << jre.m_currentFps << std::endl;
		}
		sf::Time totalTimeLoop = jre.m_globClock.getElapsedTime() - totalUpdateStartTime;
		timeToFps -= totalTimeLoop;
#endif
	}
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	Logger::Log << "		JRExe Stopping" << std::endl;
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;

	jre.OnExit();

	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	Logger::Log << "		JRExe Stopped" << std::endl;
	Logger::Log << "+-----------------------------------------------------------------+" << std::endl;
	return (0);
}