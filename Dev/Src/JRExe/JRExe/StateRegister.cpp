
#include <JREngine/jrshared.h>
#include "StateRegister.h"
#include "Background.h"

StateRegister::StateRegister(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_REGISTER;
	m_name = "Register";
}

void StateRegister::OnInit(Parameters &pm)
{
	Logger::Log << "StateRegister: OnInit" << std::endl;

}

void StateRegister::OnStart(Parameters &pm)
{
	Logger::Log << "StateRegister: Begin" << std::endl;
	m_core.GetGUI()->HideAll();
	m_core.GetGUI()->Show(PANEL_INPUT);
}

void StateRegister::OnRender(Parameters &pm)
{
	m_core.m_render.OnPreRender(pm);
	m_core.m_gui.m_mainBg->Render(&m_core);
	m_core.m_gui.Render();
	m_core.m_gui.m_mainBg->RenderGridOverlay(&m_core);
	m_core.m_gui.m_mainBg->RenderCredits(&m_core);
	m_core.m_render.OnRender(pm);
}

void StateRegister::OnUpdate(Parameters &pm)
{
	m_core.m_input->PollEvents(&m_core.m_render, &m_core);
	m_core.m_network.Update();
	m_core.m_songs.Update();
	m_core.m_sounds.Update();
	m_core.m_gui.m_mainBg->Update(&m_core);
	m_core.m_gui.Update();
	m_core.m_tweener.step(m_core.GetMainClock()->getElapsedTime().asMilliseconds());
}

void StateRegister::OnEnd(Parameters &pm)
{
}

void StateRegister::OnUnload(Parameters &pm)
{
}
