
#include <JREngine/jrshared.h>
#include "StateMods.h"

StateMods::StateMods(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_MODS;
	m_name = "Mods";
}

void StateMods::OnInit(Parameters &pm)
{
	Logger::Log << "StateMods: OnInit" << std::endl;
}

void StateMods::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateMods::OnRender(Parameters &pm)
{
}

void StateMods::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateMods: update" << std::endl;
}

void StateMods::OnEnd(Parameters &pm)
{
}

void StateMods::OnUnload(Parameters &pm)
{
}
