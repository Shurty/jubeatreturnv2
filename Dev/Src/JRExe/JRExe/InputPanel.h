
#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrgui.h>

#include "GUIPanel.h"

class JRExe;
class RenderingEngine;

class InputPanel : public GUIPanel 
{
private:


	// Position tweens
	float m_posX;
	float m_alpha;
	void RenderHeader(RenderingEngine *target);

	sf::Clock m_startClock;
	int m_startMaxTime;
	float m_startTickSec;
	bool m_pressedOnce;
	std::string m_title;
	int m_timer;

protected:
	GUIKeyboard m_keyboard;
	JRExe *m_gm;
	bool m_started;

	// Input system
	static char m_inputChars[9][3];
	std::string m_result;
	int m_activeIdx;
	int m_activeOffset;
	unsigned int m_maxChars;

public:
	
	InputPanel(JRExe *gm);
	~InputPanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
	virtual void OnBack(GUIWidget *widget);
	virtual void OnStart(GUIWidget *btn);
	virtual void OnSelect(GUIWidget *widget);
};