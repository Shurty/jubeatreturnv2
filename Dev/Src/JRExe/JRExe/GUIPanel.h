#pragma once

#include <JREngine/jrshared.h>
#include <JREngine/jrgui.h>

class RenderingEngine;
class GUIController;
class JRExe;

enum GUISTATE
{
	VISIBLE,
	MAKE_VISIBLE,
	HIDDEN,
	MAKE_HIDDEN,
};

class GUIPanel : public GUIWidget, public AGUICallback
{
private:
	GUIPANELID m_uid;
	GUISTATE m_state;
	std::vector<GUIWidget *> m_elements;
	JRExe *m_gm;
	
public:
	GUIPanel(GUIPANELID id, JRExe *gm);
	virtual ~GUIPanel();

	// Internal calls from the GUI manager
	void SetState(GUISTATE st);
	GUISTATE GetState() const;

	GUIPANELID GetID() const;
	GUIController *GetGUIController() const;

	// Called after creation and parameters setup
	virtual void Init();
	// Called on setActive
	virtual void Start(); 
	// Called on hide
	virtual void Stop();

	virtual void ReceiveEvent(GUIEVENT ev, void *param);

	void BroadcastEvent(GUIEVENT ev, void *param);
	void ButtonPressed(int keyid);
	void ButtonReleased(int keyid);

	void AddElement(GUIWidget *);
	void RemoveElement(GUIWidget *);
	std::vector<GUIWidget *>::iterator *FindElement(GUIWidget *);
};