
#pragma once

#include <iostream>
#include <exception>
#include <stdexcept>
#include <string>
#include <sstream>

namespace Unit
{

	class Exception : public std::exception
	{
	private:
		std::string _what;

	public:
		Exception(std::string denom) {
			_what = "Unit::Exception occurred: " + denom;
		}

		virtual const char* what() const throw()
		{
			return _what.c_str();
		}
	};

}