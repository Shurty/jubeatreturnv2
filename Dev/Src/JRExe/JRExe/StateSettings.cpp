
#include "StateSettings.h"

StateSettings::StateSettings(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_SETTINGS;
	m_name = "Settings";
}

void StateSettings::OnInit(Parameters &pm)
{
	Logger::Log << "StateSettings: OnInit" << std::endl;

}

void StateSettings::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateSettings::OnRender(Parameters &pm)
{
}

void StateSettings::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateSettings: update" << std::endl;
}

void StateSettings::OnEnd(Parameters &pm)
{
}

void StateSettings::OnUnload(Parameters &pm)
{
}
