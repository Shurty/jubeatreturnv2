
#pragma once

#include <JREngine/jrshared.h>
#include <JREngine/jrgui.h>

#define	BG_CLOUDS_NB		64
#define	BG_BUBBLES_NB		128

#if NOVIDEO == 0
#include "BgVideo.hpp"
#endif

class JRExe;

enum BGELEMSTATE
{
	BGEST_STARTED,
	BGEST_COMPLETE,
};

struct BGCloud
{
	float x;
	float y;
	float alpha;
	float scale;
	BGELEMSTATE state;
};

struct BGBubble
{
	float x;
	float y;
	float scale;
	float alpha;
	BGELEMSTATE state;
};

class Background : public GUIWidget
{
private:
	sf::Texture	m_overlay;
	sf::Clock clock;
	JRExe *m_core;
	BGCloud m_clouds[BG_CLOUDS_NB];
	BGBubble m_bubbles[BG_BUBBLES_NB];

#if NOVIDEO == 0
	BgVideo m_movieBot;
	BgVideo m_movieTop;
#endif

	void GenerateCloud();
	void GenerateBubble();

public:
	unsigned int m_generateCloudsCount;
	unsigned int m_generateBubblesCount;

	int m_minCloudGenTime;
	int m_minBubbleGenTime;
	int m_rndCloudGenTime;
	int m_rndBubbleGenTime;

	Background(JRExe *core);
	~Background();

	void Init(JRExe *);
	void Render(JRExe *);
	void RenderCredits(JRExe *core);
	void RenderGridOverlay(JRExe *core);
	void Update(JRExe *);
};

