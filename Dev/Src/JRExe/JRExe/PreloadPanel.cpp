
#include <iostream>
#include <JREngine/jrgui.h>
#include "PreloadPanel.h"
#include "GUIPanel.h"
#include "JRExe.h"

PreloadPanel::PreloadPanel(JRExe *gm)
	: GUIPanel(PANEL_PRELOAD, gm), m_keyboard(*gm->GetParams()), m_gm(gm)
{
	for (int i = 0; i < 16; i++) 
	{
		GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
		btn->LinkUserData(this);
		btn->SetRender([](GUIWidget* widget, RenderingEngine *render) {
			sf::Vector2f pos, size;
			sf::Text tx;
			sf::FloatRect sz;

			PreloadPanel *pane = (PreloadPanel *)widget->GetUserData();
			float ratio = render->GetRenderingRatio();

			pos = widget->GetPosition();
			pos.x += pane->m_posX;
			size = widget->GetSize();

			tx.setFont(render->GetFont(FONT_BUTTON_TX));
			tx.setString("Please wait");
			tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
			tx.setCharacterSize(14);
			sz = tx.getLocalBounds();
			tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
			render->GetRenderWindow()->draw(tx);
		});
		m_keyboard.SetWidget(i, btn);
	}
}

PreloadPanel::~PreloadPanel()
{

}

void PreloadPanel::Init()
{
	int py = (int)(m_size.y - m_size.x);
	int sz = (int)m_size.x;

	m_keyboard.SetPosition(0, (float)py);
	m_keyboard.SetSize((float)sz, (float)sz);
	m_keyboard.Init();
}

void PreloadPanel::Start()
{
	m_posX = 100;
	m_alpha = 0;
	m_state = PRELOAD_FADEIN;

	tween::TweenerParam param(600, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 0);
	param.addProperty(&m_alpha, 255);
	param.forceEnd = true;
	m_gm->m_tweener.addTween(param);

	// Reset the state of all buttons
	for (int i = 0; i < 16; i++) {
		m_keyboard.GetWidget(i)->OnReleased();
	}
}

void PreloadPanel::Stop()
{
	m_gm->m_sounds.m_bgMusic.stop();
}

void PreloadPanel::Draw(RenderingEngine *target)
{
	return;
}

void PreloadPanel::Update(sf::Clock *time)
{
	GUIPanel::Update(time);
	if (m_state == PRELOAD_FADEIN && m_alpha >= 254.0)
	{
		m_state = PRELOAD_IN;
	}
	if (m_state == PRELOAD_IN)
	{
		bool finished = false;
		if (m_gm->m_network.IsConnected()) {
			finished = false;
		}
		else {
			m_gm->m_songs.FilterAvailable([](Beatmap*) { return (true); });
			m_gm->m_songs.RefreshFilteringSorting();
			finished = true;
		}

		if (finished)
		{
			m_state = PRELOAD_FADEOUT;
			tween::TweenerParam param(600, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
			param.addProperty(&m_posX, -100);
			param.addProperty(&m_alpha, 0);
			param.forceEnd = true;
			m_gm->m_tweener.addTween(param);
		}
	}
	if (m_state == PRELOAD_FADEOUT && (int)m_alpha == 0)
	{
		Logger::Log << "Switch to select music" << std::endl;
		m_gm->m_gsm.ChangeState(GMST_MUSIC_SELECT);
		m_state = PRELOAD_OUT;
	}
}

void PreloadPanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	m_keyboard.ReceiveEvent(ev, param);
	GUIPanel::ReceiveEvent(ev, param);
}
