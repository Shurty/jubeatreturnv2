#pragma once

#include <sstream>
#include "JRExe.h"

#include "GameModeStandard.h"

class GamemodeStandard5 :
	public GamemodeStandard
{
private:

public:
	GamemodeStandard5(JRExe *core) :
		GamemodeStandard(core)
	{
		m_selectLogo = core->m_render.GetTexture(TEXTURESDIR + std::string("Gamemodes/gmStandard5.png"));
		m_maxMusics = 5;
	}

	~GamemodeStandard5()
	{

	}

	virtual GMMODE_ID GetId() { return GMMD_STANDARD5; }

	virtual void OnSelectedTopRender(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
	{
		float ratio = target->GetRenderingRatio();

		sf::RectangleShape rect;
		sf::Text text;
		sf::FloatRect textRect;

		/* Panel title */
		text.setPosition(x1, y1);
		text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
		text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
		text.setOrigin(0, 0);
		text.setCharacterSize(60 * ratio);
		text.setString("Standard Mode - 5 Musics");
		target->GetRenderWindow()->draw(text);

		text.setPosition(x1, y1 + 80 * ratio);
		text.setFont(target->GetFont(FONT_TITLE_TX));
		text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
		text.setOrigin(0, 0);
		text.setCharacterSize(40 * ratio);
		text.setString("Play a set of 3 musics of your choice\
				   		\nDouble bonus points !\
						\nUnlock new musics, score points and achievements\
						\nCompete against other players online");
		target->GetRenderWindow()->draw(text);
	}
};
