
#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrshared.h>

enum SOUNDFADING
{
	FADE_IN,
	FADE_NOPE,
	FADE_OUT
};

struct SNDBUFFER
{
	std::string path;
	sf::SoundBuffer buff;
};

#define FADEDLVL 70

class JRExe;

class SoundManager
{
private:
	SNDBUFFER buffers[MAXSNDHANDLES];
	sf::Sound sounds[MAXSNDHANDLES];
	sf::Music previewMusic;
	sf::Music music;
	unsigned int allocatedBuffs;

	bool playPreview;
	SOUNDFADING previewMusicFadingState;
	int previewMusicStartOffset;
	int previewMusicEndOffset;
	float previewMusicFading;
	float previewMusicSoundMax;

	float musicFading;

	JRExe *m_core;

	void AssignAndPlay(unsigned int bid);

public:
	sf::Music m_bgMusic;

	SoundManager(Parameters &p);
	~SoundManager();

	void Init(JRExe *);
	void Update();
	void PlaySnd(std::string const &path);
	void PlayPreviewMusic(std::string const &path, int offset = 55, int endOffset = 65);
	void PlayMusic(std::string const &path);
	void FadeOutMusic();
	void SetPreviewMusicMaxSound(float sound);

	void ReplayPreviewMusic();
	void StopPreviewMusic();
	sf::Music& GetMusic();
};