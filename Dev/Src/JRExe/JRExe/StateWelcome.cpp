
#include "StateWelcome.h"

StateWelcome::StateWelcome(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_WELCOME;
	m_name = "Welcome screen";
}

void StateWelcome::OnInit(Parameters &pm)
{
	Logger::Log << "StateWelcome: OnInit" << std::endl;

}

void StateWelcome::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateWelcome::OnRender(Parameters &pm)
{
}

void StateWelcome::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateWelcome: update" << std::endl;
}

void StateWelcome::OnEnd(Parameters &pm)
{
}

void StateWelcome::OnUnload(Parameters &pm)
{
}
