
#include <JREngine/jrshared.h>
#include "StateLogin.h"

StateLogin::StateLogin(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_LOGIN;
	m_name = "Login";
}

void StateLogin::OnInit(Parameters &pm)
{
	Logger::Log << "StateLogin: OnInit" << std::endl;

}

void StateLogin::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateLogin::OnRender(Parameters &pm)
{
}

void StateLogin::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateLogin: update" << std::endl;
}

void StateLogin::OnEnd(Parameters &pm)
{
}

void StateLogin::OnUnload(Parameters &pm)
{
}
