#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrgui.h>

#include "GUIPanel.h"
#include "GUIBeatGraph.h"

class JRExe;
class RenderingEngine;

enum DEBRIEFSTATE
{
	DST_ANNOUNCE_IN,
	DST_ANNOUNCE,
	DST_ANNOUNCE_OUT,
	DST_RESULT,
	DST_END,
	DST_ENDED
};

class DebriefingPanel : public GUIPanel 
{
private:
	GUIKeyboard m_keyboard;
	int m_step;
	JRExe *m_gm;
	float m_posX;
	float m_posXEnd;
	float m_alpha;
	float m_alphaEnd;
	float m_othersAnim;
	DEBRIEFSTATE m_state;
	GUIBeatGraph m_graph;

	float m_headerAlpha;
	float m_headerX;
	bool m_cleared;

	// Scoring tweens
	float m_displayScore;
	sf::Clock m_startClock;
	float m_startTickSec;
	float m_startMaxTime;

	void RenderHeader(RenderingEngine *target);

public:
	DebriefingPanel(JRExe *gm);
	~DebriefingPanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
	virtual void OnEndGame(GUIWidget *btn);
};