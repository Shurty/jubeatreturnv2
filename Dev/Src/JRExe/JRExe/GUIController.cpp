#include <iostream>
#include <JREngine/jrgui.h>
#include "GUIController.h"
#include "GUIPanel.h"
#include "JRExe.h"
#include "Background.h"

GUIController::GUIController()
	: m_gcPtr(NULL), m_clkPtr(NULL)
{
	m_mainBg = NULL;
	Logger::Log << "Module construct: GUIController OK" << std::endl;
}

GUIController::~GUIController()
{
	while (!m_activePanels.empty())
		m_activePanels.pop_back();
	while (!m_panels.empty())
	{
		delete m_panels.back();
		m_panels.pop_back();
	}
	if (m_mainBg)
		delete m_mainBg;
}

void GUIController::Init(JRExe *jre)
{
	m_jre = jre;
	m_gcPtr = jre->GetRenderer();
	m_clkPtr = jre->GetMainClock();
	m_mainBg = new Background(jre);
	Logger::Log << "Module init: GUIController OK" << std::endl;
}

void GUIController::PushPanel(GUIPanel *pane)
{
	m_panels.push_back(pane);
}

void GUIController::PushPopup(GUISystemPopup* pop)
{
	pop->Init();
	m_popups.push_back(pop);
}

void GUIController::Render()
{
	std::vector<GUIPanel *>::iterator it;

	it = m_activePanels.begin();
	while (it != m_activePanels.end())
	{
		(*it)->ReceiveEvent(DRAW, m_gcPtr);
		++it;
	}
	if (m_popups.size() > 0)
	{
		std::for_each(m_popups.begin(), m_popups.end(), [this](GUISystemPopup* pt) {
			if (!pt->Expired())
			{
				pt->Draw(m_gcPtr);
			}
		});
	}
}

void GUIController::Update()
{
	std::vector<GUIPanel *>::iterator it;

	it = m_activePanels.begin();
	while (it != m_activePanels.end())
	{
		(*it)->ReceiveEvent(UPDATE, m_clkPtr);
		++it;
	}
	if (m_popups.size() > 0)
	{
		std::for_each(m_popups.begin(), m_popups.end(), [this](GUISystemPopup* pt) {
			pt->Update(m_clkPtr);
		});
	}

	// Update the list from corresponding states
	UpdateListStates();

}

void GUIController::Show(GUIPANELID id)
{
	std::vector<GUIPanel *>::iterator it;

	#if 1
		Logger::Log << "GUIController: Show " << (int)id << std::endl;
	#endif
	if (FindActive(id))
		return ;

	//Logger::Log << (unsigned int)(*m_panels.begin()) << std::endl;

	it = std::find_if(m_panels.begin(), m_panels.end(), [&id] (GUIPanel *panel) {
		Logger::Log << "Fetching" << std::endl;
		if (panel->GetID() == id)
			return (true);
		return (false);
	});
	if (it != m_panels.end()) {
		(*it)->SetState(MAKE_VISIBLE);
		#if 1
			Logger::Log << "GUIController: Show " << (int)id << " Found and added to actives" << std::endl;
		#endif
		return ;
	}
	/*
	it = m_panels.begin();
	while (it != m_panels.end())
	{
		if ((*it)->GetID() == id) {
			(*it)->SetState(MAKE_VISIBLE);
			#if DEBUG
				Logger::Log << "[OK] GUIController: Show " << id << " Found and added to actives" << std::endl;
			#endif
			return ;
		}
		++it;
	}*/
	#if 1
		Logger::Log << "GUIController: Show " << (int)id << " Not Found!" << std::endl;
	#endif
}

void GUIController::UpdateListStates()
{
	std::vector<GUIPanel *>::iterator it;

	/* Update the inactive panels to go into active */
	it = m_panels.begin();
	while (it != m_panels.end())
	{
		if ((*it)->GetState() == MAKE_VISIBLE) {
			(*it)->Start();
			(*it)->SetState(VISIBLE);
			m_activePanels.push_back((*it));
		}
		++it;
	}

	/* Update the active panels to go into inactive */
	it = m_activePanels.begin();
	while (it != m_activePanels.end())
	{
		if ((*it)->GetState() == MAKE_HIDDEN) {
			(*it)->Stop();
			(*it)->SetState(HIDDEN);
			m_activePanels.erase(it);
			it = m_activePanels.begin();
		} else {
			++it;
		}
	}

	if (m_popups.size() > 0)
	{
		std::vector<GUISystemPopup*>::iterator it;

		it = m_popups.begin();
		while (it != m_popups.end())
		{
			if ((*it)->Expired()) {
				delete *it;
				m_popups.erase(it);
				it = m_popups.begin();
			}
			else {
				++it;
			}
		}
	}
}

void GUIController::Hide(GUIPANELID id)
{
	std::vector<GUIPanel *>::iterator it;

	it = m_activePanels.begin();
	while (it != m_activePanels.end())
	{
		if ((*it)->GetID() == id) {
			(*it)->SetState(MAKE_HIDDEN);
			return ;
		}
		++it;
	}
}

void GUIController::HideAll()
{
	std::vector<GUIPanel *>::iterator it;

	it = m_activePanels.begin();
	while (it != m_activePanels.end()) {
		(*it)->SetState(MAKE_HIDDEN);
		++it;
	}
}

GUIPanel *GUIController::Find(GUIPANELID id)
{
	std::vector<GUIPanel *>::iterator it;

	it = m_panels.begin();
	while (it != m_panels.end())
	{
		if ((*it)->GetID() == id) {
			return (*it);
		}
		++it;
	}
	return (NULL);
}

GUIPanel *GUIController::FindActive(GUIPANELID id)
{
	std::vector<GUIPanel *>::iterator it;

	it = m_activePanels.begin();
	while (it != m_activePanels.end())
	{
		if ((*it)->GetID() == id) {
			return (*it);
		}
		++it;
	}
	return (NULL);
}

void GUIController::BroadcastEvent(GUIEVENT ev, void *param)
{
	std::vector<GUIPanel *>::iterator it;

	it = m_activePanels.begin();
	while (it != m_activePanels.end())
	{
		(*it)->ReceiveEvent(ev, param);
		++it;
	}
}
