#include <iostream>
#include "GUIPanel.h"
#include "JRExe.h"

GUIPanel::GUIPanel(GUIPANELID id, JRExe *gm)
	: GUIWidget(*gm->GetParams()), m_uid(id), m_state(HIDDEN), m_gm(gm)
{
	#if _DEBUG
		Logger::Log << "Generated panel " << (int)id << std::endl;
	#endif
}

GUIPanel::~GUIPanel()
{

}

void GUIPanel::SetState(GUISTATE st) { m_state = st; }
GUISTATE GUIPanel::GetState() const { return (m_state); }
GUIPANELID GUIPanel::GetID() const { return (m_uid); }
GUIController *GUIPanel::GetGUIController() const { return (m_gm->GetGUI()); }

void GUIPanel::BroadcastEvent(GUIEVENT ev, void *param)
{

}

void GUIPanel::ButtonPressed(int keyid)
{

}

void GUIPanel::ButtonReleased(int keyid)
{

}

void GUIPanel::AddElement(GUIWidget *elem)
{

}

void GUIPanel::RemoveElement(GUIWidget *elem)
{

}

void GUIPanel::Init()
{

}

void GUIPanel::Stop()
{

}

void GUIPanel::Start()
{

}

std::vector<GUIWidget *>::iterator *GUIPanel::FindElement(GUIWidget *elem)
{
	return (NULL);
}

void GUIPanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
}
