#pragma once

//#include <functional>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <functional>
#include <JREngine/jrshared.h>

#define PREVIEW_MAX_LENGTH			20

class SongLibrary
{
private:
	std::map<std::string, Parser *>										m_parsers;

	// This is the source beatmaps list
	std::vector<Beatmap *>												m_beatmaps;
	// This is the list of beatmaps available to the user (server query)
	std::vector<Beatmap *>												m_availableBeatmaps;
	// This is the result filtered then sorted beatmaps list for display
	std::vector<Beatmap *>												m_filteredBeatmaps;

	std::function<bool(const SongLibrary&, Beatmap*bt)>					m_activeFilter;
	std::function<bool(const SongLibrary&, Beatmap*bt1, Beatmap*bt2)>	m_activeSorter;

public:

	SongLibrary();
	~SongLibrary();

	/* Filtering systems */
	int difficultyFilterValue;
	bool(*noFilter)(const SongLibrary&, Beatmap *);
	bool(*difficultyFilter)(const SongLibrary&, Beatmap *);
	void SetFilter(bool(*filter)(const SongLibrary&, Beatmap *));

	// Apply a filter on available selection system
	void FilterAvailable(bool(*sorter)(Beatmap *));

	/* Sorting systems */
	bool sortAscending;
	void SetSorter(bool(*sorter)(const SongLibrary&, Beatmap *, Beatmap *));
	bool(*difficultySorter)(const SongLibrary&, Beatmap *, Beatmap *);
	bool(*nameSorter)(const SongLibrary&, Beatmap *, Beatmap *);
	bool(*authorSorter)(const SongLibrary&, Beatmap *, Beatmap *);

	void ParseAll(std::string const &dir);
	void RefreshFilteringSorting();
	void Update();
	void Clear();
	
	// Set a beatmap available from the active list
	void SetAvailable(size_t hash);

	Beatmap *GetBeatmap(int idx);
	Beatmap *GetBeatmapCopy(int idx);
	//void SortBeatmaps(std::function<bool (Beatmap* a, Beatmap* b)> func);
};
