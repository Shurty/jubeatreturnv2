#pragma once

#include <vector>
#include <SFML/Graphics.hpp>
#include <JREngine/jrshared.h>

class RenderingEngine;
class GUIPanel;
class JRExe;
class Background;
class GUISystemPopup;

class GUIController
{
private:
	std::vector<GUIPanel *> m_panels;
	std::vector<GUIPanel *> m_activePanels;
	std::vector<GUISystemPopup*> m_popups;
	RenderingEngine *m_gcPtr;

	sf::Clock *m_clkPtr;
	JRExe* m_jre;
	
	void UpdateListStates();

public:
	Background *m_mainBg;

	GUIController();
	~GUIController();

	void Init(JRExe *jre);
	void PushPanel(GUIPanel *pane);
	void PushPopup(GUISystemPopup* pop);

	void BroadcastEvent(GUIEVENT ev, void *param);
	void Render();
	void Update();

	/* Helper functions (setstate hidden) */
	void Show(GUIPANELID id);
	void Hide(GUIPANELID id);
	void HideAll();
	
	GUIPanel *Find(GUIPANELID id);
	GUIPanel *FindActive(GUIPANELID id);
};