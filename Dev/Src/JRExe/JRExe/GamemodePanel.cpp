
#include <iostream>
#include <JREngine/jrgui.h>
#include "GamemodePanel.h"
#include "GUIPanel.h"
#include "JRExe.h"

GamemodePanel::GamemodePanel(JRExe *gm) 
	: GUIPanel(PANEL_GAMEMODE, gm), m_keyboard(*gm->GetParams()), m_gm(gm)
{
	m_step = 10;
	m_started = false;

	for (int i = 0; i < 16; i++) {
		if (i == 15) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnStart);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				float ratio = render->GetRenderingRatio();

				GamemodePanel *pane = (GamemodePanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.x += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_START_NOLOGO_BTN));
				render->GetRenderWindow()->draw(rect);

				/* Time calculation for the start timer */
				float time = pane->m_startMaxTime - pane->m_startClock.getElapsedTime().asSeconds();
				if (time < 0)
					time = 0;
				std::string timeStr = std::to_string(time);
				std::string resStr = timeStr;
				if (time < 10)
					resStr = "0" + resStr;
				if (time == 0)
					resStr = "00:00";
				resStr = resStr.substr(0, 5);

				tx.setFont(render->GetFont(FONT_FIXED_SIZE_TX));
				tx.setString(resStr);
				if (time < 10)
					tx.setColor(sf::Color(255, 0, 0, pane->m_alpha));
				else
					tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(50 * ratio);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y + size.y * 0.15);
				render->GetRenderWindow()->draw(tx);
			});
			m_keyboard.SetWidget(i, btn);
		}
		else if (i == 14)
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnSettings);
			btn->LinkUserData(this);
			btn->SetRender([](GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				float ratio = render->GetRenderingRatio();

				GamemodePanel *pane = (GamemodePanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.x += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(pane->m_gm->m_gmm->GetDifficulty()->GetTexture());
				render->GetRenderWindow()->draw(rect);
			});
			m_keyboard.SetWidget(i, btn);
		}
		else if (i < 8)
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnGamemode);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				GamemodePanel *pane = (GamemodePanel *)widget->GetUserData();
				IGamemode *gm = pane->m_gm->m_gmm->GetGamemodeFromId(-1 + widget->GetLinkedKey());
				float ratio = render->GetRenderingRatio();

				if (gm == NULL)
					return ;
				pos = widget->GetPosition();
				pos.x += pane->m_posX;
				size = widget->GetSize();

				/*
				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				if (pane->m_selectedGamemode == gm)
					rect.setFillColor(sf::Color(30, 80, 30, pane->m_alpha));
				else
					rect.setFillColor(sf::Color(50, 50, 50, pane->m_alpha));
				render->GetRenderWindow()->draw(rect);

				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString(gm->GetName());
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/
				gm->OnIconRender(render, pane->m_alpha, pos.x, pos.y, pos.x + size.x, pos.y + size.y);

				if (pane->m_selectedGamemode == gm) {
					widget->RenderAsSelection(render, pane->m_gm->GetMainClock(), sf::Vector2f(pane->m_posX, 0), pane->m_alpha);
				}

				// Render the credits costs box
				sf::RectangleShape rect(sf::Vector2f(size.x * 0.7, size.y * 0.2));
				rect.setPosition(pos.x + size.x * 0.3, pos.y + size.y * 0.1);
				rect.setFillColor(sf::Color(0, 0, 0, pane->m_alpha));
				render->GetRenderWindow()->draw(rect);

				rect.setSize(sf::Vector2f(size.y * 0.2 - 10 * ratio, size.y * 0.2 - 10 * ratio));
				rect.setPosition(pos.x + size.x * 0.3 + 5 * ratio, pos.y + size.y * 0.1 + 5 * ratio);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_COIN));
				render->GetRenderWindow()->draw(rect);
				
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString(std::to_string(gm->GetCreditPrice()) + " CREDIT");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(size.y * 0.2 - 10 * ratio);
				tx.setPosition(pos.x + size.x * 0.3 + 35 * ratio,
					pos.y + size.y * 0.1 + 3 * ratio);
				render->GetRenderWindow()->draw(tx);


			});
			m_keyboard.SetWidget(i, btn);
		}
		else
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			m_keyboard.SetWidget(i, btn);
		}
	}
}

GamemodePanel::~GamemodePanel()
{

}

void GamemodePanel::Init()
{
	int py = (int) (m_size.y - m_size.x);
	int sz = (int) m_size.x;

	m_keyboard.SetPosition(0, (float) py);
	m_keyboard.SetSize((float) sz, (float) sz);
	m_keyboard.Init();
}

void GamemodePanel::Start()
{
	m_posX = 100;
	m_alpha = 0;
	m_started = false;
	m_selectedGamemode = NULL;

	tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 0);
	param.addProperty(&m_alpha, 255);
	m_gm->m_tweener.addTween(param);

	// Reset the state of all buttons
	for (int i = 0; i < 16; i++) {
		m_keyboard.GetWidget(i)->OnReleased();
	}
	m_pressedOnce = false;
	m_startMaxTime = 20.00;
	m_startTickSec = 10.00;
	m_startClock.restart();
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/select_gm.wav"));
}

void GamemodePanel::Stop()
{
	m_gm->m_sounds.m_bgMusic.stop();
}

void GamemodePanel::Draw(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	sf::RectangleShape rect(sf::Vector2f(sw, sh * 0.7));
	sf::Text text;
	sf::FloatRect textRect;

	/* Panel title */
	text.setPosition(0, 0);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(sh * 0.15);
	text.setString("CHOOSE GAMEMODE");
	target->GetRenderWindow()->draw(text);

	rect.setPosition(0, sh * 0.15);
	rect.setFillColor(sf::Color(0, 0, 0, m_alpha * 0.4));
	target->GetRenderWindow()->draw(rect);

	// padding between content and padding inside black bg : 24px

	float internalPadding = 24 * ratio;
	if (m_selectedGamemode) {
		m_selectedGamemode->OnSelectedTopRender(target, m_alpha, internalPadding, sh * 0.15 + internalPadding,
			sw - internalPadding * 2, sh * 0.7 - internalPadding * 2);
	} else {

	}
	return;
}

void GamemodePanel::Update(sf::Clock *time)
{
	GUIPanel::Update(time);
	if (m_started == false && m_startTickSec < m_startClock.getElapsedTime().asSeconds())
	{
		m_startTickSec += 1;
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("tim_tum.wav"));
	}
	if (m_started == false && m_startClock.getElapsedTime().asSeconds() > m_startMaxTime)
	{
		m_gm->m_gsm.ChangeState(GMST_SPLASH);
	}
	if (m_started == true && (int)m_alpha == 0)
	{
		m_gm->m_credits.Value(m_gm->m_credits.Value() - m_selectedGamemode->GetCreditPrice());
		m_gm->m_gmm->SetMode(m_selectedGamemode->GetId());
		m_gm->m_gui.Hide(PANEL_GAMEMODE);
		m_gm->m_gui.Show(PANEL_PRELOAD);
		//m_gm->m_gsm.ChangeState(GMST_BETAMSG);
		m_started = false;
	}
}

void GamemodePanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	m_keyboard.ReceiveEvent(ev, param);
	GUIPanel::ReceiveEvent(ev, param);
	if (ev == PRESSED && m_pressedOnce == false)
	{
		m_pressedOnce = true;
		m_startMaxTime = 60.00;
		m_startTickSec = 50.00;
		m_startClock.restart();
	}
}

void GamemodePanel::OnGamemode(GUIWidget *btn)
{
	IGamemode *gm = m_gm->m_gmm->GetGamemodeFromId(-1 + btn->GetLinkedKey());

	if (!gm)
		return ;
	m_selectedGamemode = gm;
	m_selectedGamemode->OnGamemodeSelected();
}

void GamemodePanel::OnSettings(GUIWidget *btn)
{
	if (m_gm->m_gmm->UpDifficulty() == false)
		m_gm->m_gmm->ResetDifficulty();
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("beatclap.wav"));
}

void GamemodePanel::OnStart(GUIWidget *btn)
{
	if (m_started == false && m_selectedGamemode &&
		m_gm->m_credits.Value() >= m_selectedGamemode->GetCreditPrice())
	{
		m_started = true;
		m_alpha = 255;

		tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_alpha, 0);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));
		m_selectedGamemode->OnGamemodeConfirmed();
	}
}