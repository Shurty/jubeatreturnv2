
#include <JREngine/jrshared.h>
#include "StateLocalParty.h"

StateLocalParty::StateLocalParty(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_LOCAL_PARTY;
	m_name = "Local party";
}

void StateLocalParty::OnInit(Parameters &pm)
{
	Logger::Log << "StateLocalParty: OnInit" << std::endl;

}

void StateLocalParty::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateLocalParty::OnRender(Parameters &pm)
{
}

void StateLocalParty::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateLocalParty: update" << std::endl;
}

void StateLocalParty::OnEnd(Parameters &pm)
{
}

void StateLocalParty::OnUnload(Parameters &pm)
{
}
