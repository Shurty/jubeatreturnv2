
#include <JREngine/jrshared.h>
#include "StateCredits.h"

StateCredits::StateCredits(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_CREDITS;
	m_name = "Credits";
}

void StateCredits::OnInit(Parameters &pm)
{
	Logger::Log << "StateCredits: OnInit" << std::endl;

}

void StateCredits::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateCredits::OnRender(Parameters &pm)
{
}

void StateCredits::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateCredits: update" << std::endl;
}

void StateCredits::OnEnd(Parameters &pm)
{
}

void StateCredits::OnUnload(Parameters &pm)
{
}
