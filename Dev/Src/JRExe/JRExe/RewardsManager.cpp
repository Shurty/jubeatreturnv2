
#include "JRExe.h"
#include "RewardsManager.h"

RewardsManager::RewardsManager(void)
{
}


RewardsManager::~RewardsManager(void)
{
}

void RewardsManager::selectedMusic(const std::string& musicName, SONGLEVEL level)
{
	m_rewardCombo.selectedMusic(musicName, level);
}

void RewardsManager::hasScoredBeat(BEATRESULT res)//, unsigned int pointsForBeat, unsigned int currentCombo)
{
	m_rewardCombo.hasScoredBeat(res);
}

void RewardsManager::hasRegisteredFinalScore(unsigned int score)
{
	m_rewardCombo.hasRegisteredFinalScore(score);
}