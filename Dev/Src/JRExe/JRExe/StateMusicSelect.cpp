
#include <JREngine/jrshared.h>
#include "StateMusicSelect.h"
#include "Background.h"

StateMusicSelect::StateMusicSelect(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_MUSIC_SELECT;
	m_name = "Music selection";
}

void StateMusicSelect::OnInit(Parameters &pm)
{
	Logger::Log << "StateMusicSelect: OnInit" << std::endl;

}

void StateMusicSelect::OnStart(Parameters &pm)
{
	Logger::Log << "StateMusicSelect: Begin" << std::endl;
	m_core.GetGUI()->HideAll();
	m_core.GetGUI()->Show(PANEL_MUSIC_SELECT);
}

void StateMusicSelect::OnRender(Parameters &pm)
{
	m_core.m_render.OnPreRender(pm);
	m_core.m_gui.m_mainBg->Render(&m_core);
	m_core.m_gui.Render();
	m_core.m_gui.m_mainBg->	RenderGridOverlay(&m_core);
	m_core.m_render.OnRender(pm);
}

void StateMusicSelect::OnUpdate(Parameters &pm)
{
	m_core.m_input->PollEvents(&m_core.m_render, &m_core);
	m_core.m_network.Update();
	m_core.m_songs.Update();
	m_core.m_sounds.Update();
	m_core.m_gui.m_mainBg->Update(&m_core);
	m_core.m_gui.Update();
	m_core.m_tweener.step(m_core.GetMainClock()->getElapsedTime().asMilliseconds());
}

void StateMusicSelect::OnEnd(Parameters &pm)
{
}

void StateMusicSelect::OnUnload(Parameters &pm)
{
}
