#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrgui.h>
#include "GUIPanel.h"
#include "GUIBeatGraph.h"

class JRExe;
class RenderingEngine;
struct Beatmap;

class MusicSelectPanel : public GUIPanel 
{
private:
	GUIKeyboard m_keyboard;
	GUIBeatGraph m_graph;
	int m_step;
	JRExe *m_gm;
	float m_posX;
	float m_posXMusic;
	float m_posXSelect;
	float m_alpha;
	float m_alphaMusic;
	float m_alphaSelect;
	bool m_started;
	bool m_toSettings;
	int m_musicOffset;

	sf::Clock m_startClock;
	float m_startTickSec;
	float m_startMaxTime;

	unsigned int m_curScore;

public:
	Beatmap *m_selectedBeatmap;

	MusicSelectPanel(JRExe *gm);
	~MusicSelectPanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
	virtual void OnStart(GUIWidget *btn);
	virtual void OnSelect(GUIWidget *btn);
	virtual void OnSettings(GUIWidget *btn);

	virtual void OnNext(GUIWidget *btn);
	virtual void OnPrevious(GUIWidget *btn);
};