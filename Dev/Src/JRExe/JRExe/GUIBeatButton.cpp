#include <string>
#include <JREngine/jrgui.h>
#include "GUIBeatButton.h"
#include "JRExe.h"

GUIBeatButton::GUIBeatButton(JRExe *core)
	: GUIWidget(core->m_params), m_core(core)
{
	m_isDummy = false;
	m_animateEnd = false;
	m_endFrameId = 0;
	m_isFirst = false;
	m_animOutAlpha = 0;
	m_holding = false;
}

GUIBeatButton::~GUIBeatButton()
{

}

void GUIBeatButton::Init()
{
	m_isFirst = false;
	m_animateEnd = false;
	m_animOutAlpha = 0;
	m_activeBeat = NULL;
}

void GUIBeatButton::Draw(RenderingEngine *target)
{
	if (m_triggered || m_holding) 
	{
		AnimatedSprite *actTex = &target->GetActiveActivatorTexture();
		int dt = m_triggerTime.getElapsedTime().asMilliseconds();
		int totalLength = m_params.m_params.press_range;
		int spW = m_params.m_params.activator_sprite_width;
		int spH = m_params.m_params.activator_sprite_height;
		//int spCount = m_params.m_params.activator_sprite_count;
		int spCount = actTex->GetFramesCount();
		int activeFrame = (int)(dt * (float)spCount / totalLength);
		
		int targetDt = totalLength / 2 + totalLength / 2 * m_params.m_params.press_img_delay;

		if (m_holding) {
			activeFrame = m_holdingFrame;
		}

		if (!m_holding)
		{
			actTex->SetActiveFrame((float)activeFrame / (float)spCount);
			actTex->Render(target, m_position.x, m_position.y,
				m_size.x, m_size.y);
		}
		if (!m_holding && m_activeBeat && m_activeBeat->type == BTY_HOLD)
		{	
			int endTime = m_triggerTime.getElapsedTime().asMilliseconds()
				+ targetDt;
			float scale = (float)endTime / (float)dt;

			sf::RectangleShape rectangle(m_size);

			rectangle.setSize(sf::Vector2f(m_size.x - 25, m_size.y - 25));
			rectangle.setOrigin(sf::Vector2f(rectangle.getSize().x / 2, rectangle.getSize().y / 2));
			rectangle.setPosition(m_position + sf::Vector2f(m_size.x / 2, m_size.y / 2));
			rectangle.setScale(sf::Vector2f(scale, scale));
			rectangle.setFillColor(sf::Color(255, 255, 255, 0));
			rectangle.setOutlineColor(sf::Color(255, 255, 255, 255));
			rectangle.setOutlineThickness(3);
			rectangle.setTexture(NULL);
			target->GetRenderWindow()->draw(rectangle);
		}
		else if (m_holding)
		{
			int endTime = targetDt + m_activeBeat->duration;
			float scale = 1.0 - MAX(1.0, (float)endTime / (float)dt);
			// Quick and dirty but it does the work
			m_scaleExt++;
			if (m_scaleExt > 25)
				m_scaleExt = 0;
			float scaleExt = (float)(25 - m_scaleExt) / 25.0;

			float sizeX = m_size.x * scale;

			sf::RectangleShape rectangle(m_size);
			rectangle.setSize(sf::Vector2f(m_size.x, m_size.y));
			rectangle.setOrigin(sf::Vector2f(rectangle.getSize().x / 2, rectangle.getSize().y / 2));
			rectangle.setPosition(m_position + sf::Vector2f(m_size.x / 2, m_size.y / 2));
			rectangle.setScale(sf::Vector2f(scaleExt, scaleExt));
			rectangle.setFillColor(sf::Color(255, 255, 255, 0));
			rectangle.setOutlineColor(sf::Color(255, 255, 255, 255));
			rectangle.setOutlineThickness(3);
			rectangle.setTexture(NULL);
			target->GetRenderWindow()->draw(rectangle);

			actTex->SetActiveFrame((float)activeFrame / (float)spCount);
			actTex->Render(target, m_position.x + m_size.x / 2 - sizeX / 2, m_position.y + m_size.x / 2 - sizeX / 2,
				sizeX, sizeX);

		}
		/*sf::Texture *actTex = &target->GetActiveActivatorTexture();
		sf::RectangleShape rectangle(m_size);
		
		rectangle.setPosition(m_position);
		rectangle.setFillColor(sf::Color(255, 255, 255, 255));
		rectangle.setTexture(actTex);
		rectangle.setTextureRect(sf::IntRect(activeCol * spW, activeRow * spH, spW, spH));
		target->GetRenderWindow()->draw(rectangle);*/

		
	}
	else if (m_animateEnd) 
	{
		sf::Texture *actTex = target->GetTexture(TEX_ACTIVATOR_MECA_END);
		sf::RectangleShape rectangle(m_size);
		
		int activeCol = (int)m_endFrameId % 5;
		int activeRow = m_endFrameId / 5;
		int spW = m_params.m_params.activator_sprite_width;
		int spH = m_params.m_params.activator_sprite_height;

		rectangle.setPosition(m_position);
		rectangle.setFillColor(sf::Color(255, 255, 255, 255));
		rectangle.setTexture(actTex);
		rectangle.setTextureRect(sf::IntRect(activeCol * spW, activeRow * spH, spW, spH));
		target->GetRenderWindow()->draw(rectangle);
	}
	if (m_isFirst)
	{
		sf::RectangleShape rect(sf::Vector2f(m_size.x, m_size.y));

		rect.setPosition(m_position.x, m_position.y);
		rect.setFillColor(sf::Color(255, 255, 255, 130));
		rect.setTexture(target->GetTexture(TEX_READYBTN_BTN));
		target->GetRenderWindow()->draw(rect);
	}
	if (m_pressed /*|| IS_IN(m_linkedKey, 1, 2, 5, 6)*/)
	{
		sf::RectangleShape rect(sf::Vector2f(m_size.x - 4, m_size.y - 4));

		rect.setOutlineThickness(2);
		rect.setPosition(m_position.x + 2, m_position.y + 2);
		/*if (IS_IN(m_linkedKey, 1, 2, 5, 6))
			rect.setOutlineColor(sf::Color(255, 255, 255, 255));
		else*/
			rect.setOutlineColor(sf::Color(255, 0, 0, 255));
		rect.setFillColor(sf::Color(0, 0, 0, 0));
		target->GetRenderWindow()->draw(rect);
	}

	sf::RectangleShape rect(sf::Vector2f(m_size.x, m_size.y));

	rect.setPosition(m_position.x, m_position.y);
	rect.setFillColor(sf::Color(animOutColor.r, animOutColor.g, animOutColor.b, m_animOutAlpha));
	target->GetRenderWindow()->draw(rect);

	// If debug is on, we display the local pression qualitymap
	if (m_params.m_params.game_debug && m_isDummy == false)
	{
		sf::RectangleShape rect(sf::Vector2f(m_size.x / 8, m_size.y));
		sf::Text tx;

		// Score update, untrigger etc
		int dt = m_triggerTime.getElapsedTime().asMilliseconds();
		dt = dt - m_params.m_params.press_latency;
		int startTime = 0;
		int endTime = m_params.m_params.press_range;

		int targetDt = m_params.m_params.press_range / 2;
		targetDt += targetDt * m_params.m_params.press_img_delay;
		int res = targetDt - dt;

		int perfectDt = targetDt * m_core->m_params.m_params.press_delays[BR_PERFECT];
		int perfectMin = targetDt - perfectDt;
		int perfectMax = targetDt + perfectDt;
		int greatDt = perfectDt + targetDt * m_core->m_params.m_params.press_delays[BR_GREAT];
		int greatMin = targetDt - greatDt;
		int greatMax = targetDt + greatDt;
		int goodDt = greatDt + targetDt * m_core->m_params.m_params.press_delays[BR_GOOD];
		int goodMin = targetDt - goodDt;
		int goodMax = greatMin;

		tx.setFont(target->GetFont(FONT_TITLE_TX));
		tx.setCharacterSize(12);
		tx.setColor(sf::Color(255, 255, 255, 255));
		tx.setString("Dt: " + std::to_string(dt));
		tx.setPosition(m_position.x + 35, m_position.y + 5);
		target->GetRenderWindow()->draw(tx);
		tx.setString("End: " + std::to_string(endTime));
		tx.setPosition(m_position.x + 35, m_position.y + 20);
		target->GetRenderWindow()->draw(tx);
		tx.setString("PE: " + std::to_string(perfectDt));
		tx.setPosition(m_position.x + 35, m_position.y + 35);
		target->GetRenderWindow()->draw(tx);
		tx.setString("GR: " + std::to_string(greatDt));
		tx.setPosition(m_position.x + 35, m_position.y + 50);
		target->GetRenderWindow()->draw(tx);
		tx.setString("GO: " + std::to_string(goodDt));
		tx.setPosition(m_position.x + 35, m_position.y + 65);
		target->GetRenderWindow()->draw(tx);

		switch (m_lastpress)
		{
		case BR_BAD:
			tx.setString("BR_BAD");
			tx.setPosition(m_position.x + 35, m_position.y + 80);
			tx.setColor(sf::Color(255, 0, 0, 255));
			target->GetRenderWindow()->draw(tx);
			break;
		case BR_MISS:
			tx.setString("BR_MISS");
			tx.setPosition(m_position.x + 35, m_position.y + 80);
			tx.setColor(sf::Color(100, 100, 100, 255));
			target->GetRenderWindow()->draw(tx);
			break;
		case BR_GOOD:
			tx.setString("BR_GOOD");
			tx.setPosition(m_position.x + 35, m_position.y + 80);
			tx.setColor(sf::Color(200, 50, 0, 255));
			target->GetRenderWindow()->draw(tx);
			break;
		case BR_GREAT:
			tx.setString("BR_GREAT");
			tx.setPosition(m_position.x + 35, m_position.y + 80);
			tx.setColor(sf::Color(100, 150, 0, 255));
			target->GetRenderWindow()->draw(tx);
			break;
		case BR_PERFECT:
			tx.setString("BR_PERFECT");
			tx.setPosition(m_position.x + 35, m_position.y + 80);
			tx.setColor(sf::Color(0, 255, 0, 255));
			target->GetRenderWindow()->draw(tx);
			break;
		default:
			tx.setString("??");
			tx.setPosition(m_position.x + 35, m_position.y + 80);
			tx.setColor(sf::Color(100, 100, 100, 255));
			target->GetRenderWindow()->draw(tx);
			break;
		}

		// Render the background (miss/bad)
		rect.setSize(sf::Vector2f(m_size.x / 8, m_size.y));
		rect.setFillColor(sf::Color(255, 0, 0, 255));
		rect.setPosition(m_position.x, m_position.y);
		target->GetRenderWindow()->draw(rect);

		// Render the good
		rect.setSize(sf::Vector2f(m_size.x / 8, (goodMax - goodMin) * m_size.y / endTime));
		rect.setFillColor(sf::Color(200, 50, 0, 255));
		rect.setPosition(m_position.x, m_position.y + goodMin * m_size.y / endTime);
		target->GetRenderWindow()->draw(rect);

		// Render the great
		rect.setSize(sf::Vector2f(m_size.x / 8, (greatMax - greatMin) * m_size.y / endTime));
		rect.setFillColor(sf::Color(100, 150, 0, 255));
		rect.setPosition(m_position.x, m_position.y + greatMin * m_size.y / endTime);
		target->GetRenderWindow()->draw(rect);

		// Render the perfect
		rect.setSize(sf::Vector2f(m_size.x / 8, (perfectMax - perfectMin) * m_size.y / endTime));
		rect.setFillColor(sf::Color(0, 255, 0, 255));
		rect.setPosition(m_position.x, m_position.y + perfectMin * m_size.y / endTime);
		target->GetRenderWindow()->draw(rect);

		// Render the active marker
		int currentPosOffset = dt * m_size.y / endTime;
		if (currentPosOffset < m_size.y)
		{
			rect.setSize(sf::Vector2f(m_size.x / 4, 2));
			rect.setPosition(m_position.x, m_position.y + currentPosOffset);
			rect.setFillColor(sf::Color(255, 255, 255, 255));
			target->GetRenderWindow()->draw(rect);
		}
	}
    GUIWidget::Draw(target);
}

void GUIBeatButton::OnReleased()
{
	GUIWidget::OnReleased();
	if (m_holding) {
		m_lastpress = BR_BAD;
		m_core->m_gmm->currentMode()->OnBeatAction(m_lastpress, this);
		m_holding = false;
		m_triggered = false;
		AnimateResult(m_lastpress);
	}
}

void GUIBeatButton::Update(sf::Clock *time)
{
	/*int inLength = (int) ((BAD_DELAY + GOOD_DELAY + GREAT_DELAY + PERFECT_DELAY) / m_activeBpm);
	int outLength = (int) ((GREAT_DELAY + GOOD_DELAY + BAD_DELAY) / 2.0f / m_activeBpm);
	int totalLength = inLength + outLength;*/

	int totalLength = m_params.m_params.press_range;
	int targetDt = totalLength / 2 + totalLength / 2 * m_params.m_params.press_img_delay;

	if (m_animateEnd == true && (int)m_endFrameId == 24)
	{
		m_animateEnd = false;
	}
	if (m_triggered) 
	{
		int dt = m_triggerTime.getElapsedTime().asMilliseconds();

		if (!m_isDummy)
		{
			if (!m_holding && (!m_core->m_gmm->currentMode() || m_params.m_params.simulation) /*|| IS_IN(m_linkedKey, 1, 2, 5, 6)*/)
			{
				if (dt + 0 > targetDt)
				{
					OnPressed();
					if (!m_holding)
						OnReleased();
					if (m_core->m_gmm->currentMode()) {
						m_core->m_sounds.PlaySnd(SOUNDDIR + std::string("handclap.wav"));
					}
					return ;
				}
			}

			if (m_holding && dt > targetDt + m_activeBeat->duration)
			{
				m_holding = false;
				m_triggered = false;
				if (m_core->m_gmm->currentMode()) {
					m_core->m_gmm->currentMode()->OnBeatAction(m_lastpress, this);
				}
				if (m_params.m_params.simulation || !m_core->m_gmm->currentMode())
					OnReleased();
				AnimateResult(m_lastpress);
				return;
			}

			if (dt > totalLength / 2 && m_clickSimulateDone == false)
			{
				//m_core->m_sounds.PlaySnd(SOUNDDIR + std::string("handclap.wav"));
				m_clickSimulateDone = true;
			}
		}
		if (m_holding == false && dt > totalLength)
		{
			if (!m_isDummy)
			{
				m_lastpress = BR_MISS;
				if (m_core->m_gmm->currentMode()) {
					m_core->m_gmm->currentMode()->OnBeatAction(BR_MISS, this);
				}
			}
			//(m_linkedGame->*m_pressActionClb)(MISS);
			m_triggered = false;
		}
		
	}
}

void GUIBeatButton::OnTriggered()
{
	// Calculate the event times
	m_triggered = true;
	m_clickSimulateDone = false;
	m_isFirst = false;
	if (m_pressed /*&& !IS_IN(m_linkedKey, 1, 2, 5, 6)*/)
	{
		float x;
		float y;

		if (m_parent) {
			x = (m_position + m_parent->GetPosition()).x;
			y = (m_position + m_parent->GetPosition()).y;
		}
		else {
			x = m_position.x;
			y = m_position.y;
		}
		m_lastpress = BR_BAD;
		if (m_core->m_gmm->currentMode()) {
			m_core->m_gmm->currentMode()->OnBeatAction(BR_BAD, this);
		}
		m_triggered = false;

		AnimateResult(m_lastpress);
	}
	else
	{

	}
	m_triggerTime.restart();
}

void GUIBeatButton::OnBeatSet(Beat *beat)
{
	if (beat->activate != m_linkedKey)
		return;
	m_activeBeat = beat;
	m_holding = false;
	//if (beat->type == BTY_HOLD)
	//	m_holding = true;
}

void GUIBeatButton::OnPressed()
{
	GUIWidget::OnPressed();
	if (m_isDummy)
	{
		m_triggered = false;
		return ;
	}
	if (m_triggered)
	{
		// Score update, untrigger etc
		int dt = m_triggerTime.getElapsedTime().asMilliseconds();
		dt = dt - m_params.m_params.press_latency;
		int targetDt = m_params.m_params.press_range / 2;
		targetDt += targetDt * m_params.m_params.press_img_delay;
		int res = targetDt - dt;

		int perfectDt = targetDt * m_core->m_params.m_params.press_delays[BR_PERFECT];
		int greatDt = perfectDt + targetDt * m_core->m_params.m_params.press_delays[BR_GREAT];
		int goodDt = greatDt + targetDt * m_core->m_params.m_params.press_delays[BR_GOOD];

		
		//DEBUG: Assist mode forcing to perfect
		/*if (IS_IN(m_linkedKey, 1, 2, 5, 6))
		{
			res = 0;
		}
		*/

		if (res >= -perfectDt && res <= perfectDt)
		{
			m_lastpress = BR_PERFECT;
		}
		else if (res >= -greatDt && res <= greatDt)
		{
			m_lastpress = BR_GREAT;
		}
		else if (res > 0 && res <= goodDt)
		{
			m_lastpress = BR_GOOD;
		}
		else
		{
			m_lastpress = BR_BAD;
		}

		if (m_triggered && m_activeBeat && m_activeBeat->type == BTY_HOLD)
		{
			if (m_lastpress != BR_BAD) {
				m_holdingFrame = 15;
				m_holding = true;
				m_triggered = true;
				return;
			}
		}

		AnimateResult(m_lastpress);
		if (m_core->m_gmm->currentMode()) {
			m_core->m_gmm->currentMode()->OnBeatAction(m_lastpress, this);
		}
		m_triggered = false;
	}
}

void GUIBeatButton::AnimateResult(BEATRESULT res)
{
	m_animateEnd = true;
	tween::TweenerParam param2(600, tween::LINEAR, tween::EASE_OUT); //2 seconds tween

	if (res == BR_PERFECT)
	{
		animOutColor = sf::Color(0, 255, 0, 0);
		m_endFrameId = 1;
	}
	else if (res == BR_GREAT)
	{
		animOutColor = sf::Color(255, 255, 0, 0);
		m_endFrameId = 6;
	}
	else if (res == BR_GOOD)
	{
		animOutColor = sf::Color(255, 125, 0, 0);
		m_endFrameId = 8;
	}
	else
	{
		animOutColor = sf::Color(255, 0, 0, 0);
		m_endFrameId = 11;
	}

	param2.addProperty(&m_endFrameId, 24);
	param2.forceEnd = true;
	m_core->m_tweener.addTween(param2);

	m_animOutAlpha = 80;
	tween::TweenerParam param(400, tween::LINEAR, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_animOutAlpha, 0);
	param.forceEnd = true;
	m_core->m_tweener.addTween(param);
}

void GUIBeatButton::SetDummy(bool dum) { m_isDummy = dum; }
void GUIBeatButton::SetFirst(bool bl) { m_isFirst = bl; }

void GUIBeatButton::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
	if (ev == FIRSTBTN_SET) {
		SetFirst(true);
	}
	if (ev == TRIGGERBEAT) {
		OnBeatSet((Beat*)param);
	}
}