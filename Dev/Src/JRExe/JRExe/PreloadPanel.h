#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrgui.h>

#include "GUIPanel.h"

class JRExe;
class RenderingEngine;

class PreloadPanel : public GUIPanel
{
private:
	GUIKeyboard m_keyboard;
	JRExe *m_gm;
	float m_posX;
	float m_alpha;

	enum {
		PRELOAD_FADEIN,
		PRELOAD_IN,
		PRELOAD_FADEOUT,
		PRELOAD_OUT
	} m_state;

public:
	PreloadPanel(JRExe *gm);
	~PreloadPanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};