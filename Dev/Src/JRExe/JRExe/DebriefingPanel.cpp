
#include <iostream>
#include <JREngine/jrgui.h>
#include "DebriefingPanel.h"
#include "Background.h"
#include "GUIPanel.h"
#include "JRExe.h"

DebriefingPanel::DebriefingPanel(JRExe *gm) 
	: GUIPanel(PANEL_DEBRIEFING, gm), m_keyboard(*gm->GetParams()), m_gm(gm), m_graph(gm)
{
	m_step = 10;

	for (int i = 0; i < 16; i++) {
		if (i == 15)
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkUserData(this);
			btn->LinkAction(this, &AGUICallback::OnEndGame);
			btn->SetRender([] (GUIWidget *widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				float ratio = render->GetRenderingRatio();

				DebriefingPanel *pane = (DebriefingPanel *)widget->GetUserData();
				if (pane->m_state != DST_RESULT)
					return ;

				pos = widget->GetPosition();
				pos.x += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_NEXT_NOLOGO_BTN));
				render->GetRenderWindow()->draw(rect);
				
				float time = pane->m_startMaxTime - pane->m_startClock.getElapsedTime().asSeconds();
				if (time < 0)
					time = 0;
				std::string timeStr = std::to_string(time);
				std::string resStr = timeStr;
				if (time < 10)
					resStr = "0" + resStr;
				if (time == 0)
					resStr = "00:00";
				resStr = resStr.substr(0, 5);

				tx.setFont(render->GetFont(FONT_FIXED_SIZE_TX));
				tx.setString(resStr);
				if (time < 10)
					tx.setColor(sf::Color(255, 0, 0, pane->m_alpha));
				else
					tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(50 * ratio);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y + size.y * 0.15);
				render->GetRenderWindow()->draw(tx);
			});
			m_keyboard.SetWidget(i, btn);
		}
		else
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget *widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::FloatRect sz;
				float ratio = render->GetRenderingRatio();

				pos = widget->GetPosition();
				size = widget->GetSize();

				DebriefingPanel *pane = (DebriefingPanel *)widget->GetUserData();
				if (pane->m_state == DST_ANNOUNCE_IN)
				{
					sf::Text tx;

					pos.x += pane->m_posX;
					tx.setFont(render->GetFont(FONT_BUTTON_TX));
					tx.setString("Result...");
					tx.setColor(sf::Color(255, 255, 255, MAX(pane->m_alpha - 100, 0)));
					tx.setCharacterSize(14 * ratio);
					sz = tx.getLocalBounds();
					tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
					render->GetRenderWindow()->draw(tx);
				}
				else if (pane->m_state == DST_ANNOUNCE)
				{
					sf::RectangleShape rect(size);

					pos.y -= pane->m_posX; 
					rect.setPosition(pos.x, pos.y);
					rect.setFillColor(sf::Color(255, 255, 255, MIN(pane->m_alpha, 255)));
					if (pane->m_cleared == true)
						rect.setTexture(render->GetTexture(TEX_CLEARED_TEXT));
					else
						rect.setTexture(render->GetTexture(TEX_FAILED_TEXT));
					render->GetRenderWindow()->draw(rect);
				}

			});
			m_keyboard.SetWidget(i, btn);
		}
	}
}

DebriefingPanel::~DebriefingPanel()
{

}

void DebriefingPanel::Init()
{
	int py = (int) (m_size.y - m_size.x);
	int sz = (int) m_size.x;

	m_keyboard.SetPosition(0, (float) py);
	m_keyboard.SetSize((float) sz, (float) sz);
	m_keyboard.Init();

	m_graph.SetMaxX(80);
	m_graph.SetMaxY(12);
}

void DebriefingPanel::Start()
{
	m_posX = 300;
	m_posXEnd = 0;
	m_alpha = 0;
	m_alphaEnd = 255;
	m_headerAlpha = 255;
	m_headerX = 0;
	m_cleared = false;
	m_othersAnim = 1.0f;
	m_displayScore = m_gm->m_gmm->currentMode()->GetScoreManager().getScoreWithoutCombo();

	m_state = DST_ANNOUNCE_IN;

	tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 0);
	param.addProperty(&m_displayScore, m_gm->m_gmm->currentMode()->GetScoreManager().getScoreWithCombo());
	param.forceEnd = true;

	m_gm->m_tweener.addTween(param);

	tween::TweenerParam param2(1300, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param2.addProperty(&m_alpha, 255);
	param2.forceEnd = true;
	m_gm->m_tweener.addTween(param2);

	// Reset the state of all buttons
	for (int i = 0; i < 16; i++) {
		m_keyboard.GetWidget(i)->OnReleased();
	}
	m_startClock.restart();
	m_startMaxTime = 40.00;
	m_startTickSec = 30.00;
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/result.wav"));
}

void DebriefingPanel::Stop()
{
	m_gm->m_sounds.m_bgMusic.stop();
}

void DebriefingPanel::RenderHeader(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;
	float internalPadding = 21 * ratio;
	Beatmap *bmp = m_gm->m_gmm->currentMode()->GetSelectedBeatmap();

	sf::RectangleShape rect(sf::Vector2f(sw, 100 * ratio + internalPadding * 2));
	sf::Text text;
	sf::FloatRect textRect;

	rect.setFillColor(sf::Color(0, 0, 0, m_headerAlpha * 0.4));
	rect.setPosition(0, 0);
	target->GetRenderWindow()->draw(rect);
	rect.setPosition(0, sh - (internalPadding * 2 + 100 * ratio));
	target->GetRenderWindow()->draw(rect);


	rect.setSize(sf::Vector2f(100 * ratio, 100 * ratio));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	rect.setPosition(m_headerX + internalPadding, internalPadding);
	rect.setTexture(target->GetTexture(TEX_COVER_BG_SMALL));
	target->GetRenderWindow()->draw(rect);

	rect.setSize(sf::Vector2f(90 * ratio, 90 * ratio));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	rect.setTexture(target->GetTexture(TEXTURESDIR + std::string("Musics/" + bmp->src + ".jpg")));
	rect.setTextureRect(sf::IntRect(0, 0, rect.getTexture()->getSize().x, rect.getTexture()->getSize().y));
	rect.setPosition(m_headerX + internalPadding + 5 * ratio, internalPadding + 5 * ratio);
	target->GetRenderWindow()->draw(rect);

	/* Texts*/
	float textX = internalPadding * 2 + 100 * ratio + m_headerX;

	text.setPosition(textX, internalPadding);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setOrigin(0, 0);
	text.setCharacterSize(35 * ratio);
	text.setString(bmp->name);
	target->GetRenderWindow()->draw(text);

	text.setPosition(textX, internalPadding + 40 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setOrigin(0, 0);
	text.setCharacterSize(24 * ratio);
	text.setString(bmp->author);
	target->GetRenderWindow()->draw(text);

	/* Gamemode and difficulty */
	rect.setSize(sf::Vector2f(100 * ratio, 100 * ratio));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	rect.setTexture(target->GetTexture(TEX_DIFFICULTY_BG));
	rect.setPosition(sw - m_headerX - internalPadding - 100 * ratio, internalPadding);
	rect.setTextureRect(sf::IntRect(0, 0, 101, 102));
	target->GetRenderWindow()->draw(rect);

	rect.setSize(sf::Vector2f(90 * ratio, 90 * ratio));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	if (bmp->songLevel == SL_BASIC)
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_GREEN));
	else if (bmp->songLevel == SL_INTERMEDIATE)
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_ORANGE));
	else	
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_RED));
	rect.setPosition(sw - m_headerX - internalPadding + 5 * ratio - 100 * ratio, internalPadding + 5 * ratio);
	rect.setTextureRect(sf::IntRect(0, 0, 87, 87));
	target->GetRenderWindow()->draw(rect);

	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(35 * ratio);
	text.setString(std::to_string(bmp->level));
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width/2.0f,
				   textRect.top  + textRect.height/2.0f);
	text.setPosition(sw - m_headerX - internalPadding - 100/2 * ratio, internalPadding + 100/2 * ratio);
	target->GetRenderWindow()->draw(text);

	text.setPosition(sw - m_headerX - internalPadding * 2 - 100 * ratio, internalPadding);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(35 * ratio);
	text.setString(m_gm->m_gmm->currentMode()->GetName());
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width,
				   0);
	target->GetRenderWindow()->draw(text);

	text.setPosition(sw - m_headerX - internalPadding * 2 - 100 * ratio, internalPadding + 40 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setCharacterSize(24 * ratio);
	if (bmp->songLevel == SL_EXTREME) {
		text.setString("EXTREME");
		text.setColor(sf::Color(255, 0, 0, m_headerAlpha));
	} else if (bmp->songLevel == SL_INTERMEDIATE) {
		text.setString("NORMAL");
		text.setColor(sf::Color(255, 255, 0, m_headerAlpha));
	} else {
		text.setString("BASIC");
		text.setColor(sf::Color(0, 255, 0, m_headerAlpha));
	}
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width,
				   0);
	target->GetRenderWindow()->draw(text);

	/* Render playbox bg */
	float posY = 100 * ratio + internalPadding * 2;
	float svh = sh - posY * 2;

	rect.setSize(sf::Vector2f(svh - internalPadding * 2, svh - internalPadding * 2));
	rect.setFillColor(sf::Color(80, 80, 80, m_headerAlpha));
	rect.setPosition(internalPadding, posY + internalPadding);
	rect.setTexture(target->GetTexture(TEX_PLAYBOX_BG));
	rect.setTextureRect(sf::IntRect(0, 0, 340, 346));
	target->GetRenderWindow()->draw(rect);

	/* Render active scoring */
	float ptextX = svh;
	float ptextY = posY + internalPadding;
	/*
	for (int i = 0; i < 4; i++)
	{
		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
		text.setCharacterSize(20 * ratio);
		text.setString("Player");
		text.setOrigin(0, 0);
		text.setPosition(ptextX, ptextY + i * 25 * ratio);
		target->GetRenderWindow()->draw(text);

		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
		text.setCharacterSize(20 * ratio);
		text.setString("100 000");
		text.setPosition(ptextX + 100 * ratio, ptextY + i * 25 * ratio);
		target->GetRenderWindow()->draw(text);
	}*/

	ptextY = ptextY + svh - internalPadding * 2;

	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(60 * ratio);
	text.setString(std::to_string((int)m_displayScore));
	text.setPosition(ptextX, ptextY);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(32 * ratio);
	text.setString("(+0)");
	text.setPosition(ptextX + textRect.width + 20 * ratio, ptextY);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(60 * ratio);
	text.setString(m_gm->m_playersManager.getCurrentPlayer()->getPseudo());
	text.setPosition(ptextX, ptextY - 70 * ratio);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(32 * ratio);
	text.setString("RANK 1");
	text.setPosition(ptextX + textRect.width + 20 * ratio, ptextY - 70 * ratio);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	/* The ranking! */
	float rankX = sw - internalPadding;
	float rankY = posY + internalPadding;

	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_headerAlpha));
	text.setCharacterSize(30 * ratio);
	text.setString("ACTIVE RANKING");
	text.setPosition(rankX, rankY);
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width, 0);
	target->GetRenderWindow()->draw(text);

	rect.setSize(sf::Vector2f(textRect.width, textRect.width));
	rect.setFillColor(sf::Color(255, 255, 255, m_headerAlpha));
	rect.setPosition(rankX - textRect.width, rankY + 40 * ratio);
	rect.setTexture(target->GetTexture((TEXID)(TEX_GRADE_E + m_gm->m_gmm->currentMode()->GetScoreManager().getPotentialGrade())));
	rect.setTextureRect(sf::IntRect(0, 0, 143, 135));
	target->GetRenderWindow()->draw(rect);

	/* Graph */
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(150, 150, 150, m_headerAlpha));
	text.setCharacterSize(20 * ratio);
	text.setString("START");
	text.setPosition(internalPadding - 5 * ratio + 75 * ratio, sh - 5 * ratio);
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(150, 150, 150, m_headerAlpha));
	text.setCharacterSize(20 * ratio);
	text.setString("END");
	text.setPosition(sw - internalPadding + 5 * ratio - 75 * ratio, sh - 5 * ratio);
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.top + textRect.height);
	target->GetRenderWindow()->draw(text);

	m_graph.SetPosition(internalPadding + 75 * ratio, sh - (internalPadding + 70 * ratio));
	m_graph.SetSize(sw - internalPadding * 2 - 150 * ratio, 70 * ratio + internalPadding);
	m_graph.Draw(target);

	/* Stats/achivements/challenges */
	/* TODO? */

	//tx.setFont(target->GetFont(FONT_TITLE_TX));
	//tx.setString(std::to_string((int)m_displayScore));
	//tx.setCharacterSize(50);
	//tx.setColor(sf::Color(255, 255, 255, 255));
	//sz = tx.getLocalBounds();
	//tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 - 150);
	//target->GetRenderWindow()->draw(tx);

	m_gm->m_gmm->currentMode()->OnDebriefingTopRender(target, m_headerAlpha, 0, 0, sw, sh);
}


void DebriefingPanel::Draw(RenderingEngine *target)
{
	sf::Text tx;
	sf::Vector2f pos, size;
	sf::FloatRect sz;
	float ratio = target->GetRenderingRatio();

	pos = GetPosition();
	size = GetSize();
	pos.y += size.y - size.x;
	size.y = size.x;

	/* Top panel */
	RenderHeader(target);

	// Render color background
	if (m_state != DST_ANNOUNCE_IN)
	{
		sf::RectangleShape rect(sf::Vector2f(size.x, size.y));
		float cAlpha = 0.0;
		if (m_state == DST_ANNOUNCE)
			cAlpha = m_alpha / 555.0;
		float modulation = sinf(m_gm->GetMainClock()->getElapsedTime().asMilliseconds() / 450.0f);
		float tAlpha = -MIN(40, 255 - m_alphaEnd) / 4 + 30 + 15 * modulation;

		rect.setPosition(pos.x, pos.y);
		if (m_cleared == true)
			rect.setFillColor(sf::Color(0, 255, 0, MAX(0, cAlpha * 60.0 + tAlpha)));
		else
			rect.setFillColor(sf::Color(255, 0, 0, MAX(0, cAlpha * 60.0 + tAlpha)));
		rect.setOrigin(0, 0);
		rect.setTexture(NULL);
		target->GetRenderWindow()->draw(rect);
	}

	/* Bottom panel */
	if (m_state == DST_ANNOUNCE_IN)
	{
		tx.setFont(target->GetFont(FONT_TITLE_TX));
		tx.setString("Result");
		tx.setCharacterSize(160 * ratio);
		pos.x += m_posX;
		tx.setColor(sf::Color(255, 255, 255, m_alpha));
		sz = tx.getLocalBounds();
		tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
		target->GetRenderWindow()->draw(tx);
	}
	else if (m_state == DST_ANNOUNCE)
	{
		sf::RectangleShape rect(sf::Vector2f(1024 * ratio, m_params.m_params.btn_size));

		pos.y -= m_posX; 
		for (int i = 0; i < 3; i++)
		{
			float cAlpha = MAX(0, MIN(655 - m_alpha, 255) - i * 100);
			float yDiff = i * 140 - (m_posX * 2) + m_params.m_params.btn_size / 2;
			float posY = pos.y + size.y / 2 - rect.getSize().y / 2 - m_params.m_params.btn_size + yDiff * ratio;

			rect.setPosition(pos.x + size.x / 2 - rect.getSize().x / 2, posY);
			rect.setFillColor(sf::Color(255, 255, 255, cAlpha));
			if (m_cleared == true)
				rect.setTexture(target->GetTexture(TEX_CLEARED_LARGE_TEXT));
			else
				rect.setTexture(target->GetTexture(TEX_FAILED_LARGE_TEXT));
			target->GetRenderWindow()->draw(rect);
		}
	}
	else
	{
		sf::RectangleShape rect(sf::Vector2f(1024 * ratio, m_params.m_params.btn_size));

		pos.x += m_posXEnd; 
		for (int i = 0; i < 3; i++)
		{
			float cAlpha = MAX(0, m_alphaEnd - i * 100);
			float yDiff = i * 140 + m_params.m_params.btn_size / 2;
			float posY = pos.y + size.y / 2 - rect.getSize().y / 2 - m_params.m_params.btn_size + yDiff * ratio;

			rect.setPosition(pos.x + size.x / 2 - rect.getSize().x / 2, posY);
			rect.setFillColor(sf::Color(255, 255, 255, cAlpha));
			if (m_cleared == true)
				rect.setTexture(target->GetTexture(TEX_CLEARED_LARGE_TEXT));
			else
				rect.setTexture(target->GetTexture(TEX_FAILED_LARGE_TEXT));
			target->GetRenderWindow()->draw(rect);
		}

		float posY = pos.y + size.y / 2 + m_params.m_params.btn_step / 2 + m_params.m_params.btn_size / 2;
		float posX = pos.x + size.x / 2 + m_params.m_params.btn_step / 2 + m_params.m_params.btn_size / 2;

		sf::Texture *gradeTex = target->GetTexture((TEXID)(TEX_GRADE_E + m_gm->m_gmm->currentMode()->GetScoreManager().getPotentialGrade()));
		rect.setPosition(posX, posY);
		rect.setSize(sf::Vector2f(m_params.m_params.btn_size, m_params.m_params.btn_size));
		rect.setFillColor(sf::Color(255, 255, 255, m_alphaEnd - m_othersAnim * 255.0));
		rect.setOrigin(rect.getSize().x / 2, rect.getSize().y / 2);
		rect.setTextureRect(sf::IntRect(0, 0, gradeTex->getSize().x, gradeTex->getSize().y));
		rect.setScale(sf::Vector2f(m_othersAnim * 1.5 + 0.85, m_othersAnim * 1.5 + 0.85));
		rect.setTexture(gradeTex);
		target->GetRenderWindow()->draw(rect);
		
		posY = pos.y + size.y / 2 - m_params.m_params.btn_step / 2 + m_params.m_params.btn_size * 0.85;
		posX = pos.x + size.x / 2 - m_params.m_params.btn_step / 2 - m_params.m_params.btn_size * 0.65;
		
		tx.setFont(target->GetFont(FONT_SCORING_TX));
		tx.setString("RATING");
		tx.setCharacterSize(30 * ratio);
		pos.x += m_posX;
		tx.setColor(sf::Color(255, 255, 255, m_alphaEnd - m_othersAnim * 255.0));
		sz = tx.getLocalBounds();
		tx.setPosition(posX, posY);
		target->GetRenderWindow()->draw(tx);
	}

	/*
	sf::RectangleShape rect(sf::Vector2f(200, 100));

	rect.setPosition(m_size.x / 2 - 100 + m_posX, (m_size.y - m_size.x) / 2 - 50);
	rect.setFillColor(sf::Color(255, 0, 0, m_alpha));
	target->GetRenderWindow()->draw(rect);

	if (m_started)
	{
		rect.setSize(sf::Vector2f(m_size.x, m_size.y));
		rect.setPosition(0, 0);
		rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
		target->GetRenderWindow()->draw(rect);
	}
	*/
	return;
}

void DebriefingPanel::Update(sf::Clock *time)
{
	GUIPanel::Update(time);
	if (m_state == DST_RESULT && m_startTickSec < m_startClock.getElapsedTime().asSeconds())
	{
		m_startTickSec += 1;
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("tim_tum.wav"));
	}
	if (m_state == DST_RESULT && m_startClock.getElapsedTime().asSeconds() > m_startMaxTime)
	{
		OnEndGame(NULL);
	}
	if (m_state == DST_ANNOUNCE_IN && (int)m_alpha == 255)
	{
		m_gm->m_sounds.m_bgMusic.openFromFile(MUSICDIR + std::string("Menu/InfinitePower_MusicEnd.ogg"));
		m_gm->m_sounds.m_bgMusic.play();
		m_gm->m_sounds.m_bgMusic.setVolume(40);
		m_gm->m_sounds.m_bgMusic.setLoop(true);

		m_cleared = false;
		m_gm->m_gui.m_mainBg->m_generateBubblesCount = 0;
		if (m_gm->m_gmm->currentMode()->GetScoreManager().getPotentialGrade() > 1) {
			m_gm->m_gui.m_mainBg->m_generateCloudsCount = BG_CLOUDS_NB;
			m_gm->m_gui.m_mainBg->m_generateBubblesCount = BG_BUBBLES_NB;
			m_cleared = true;
			m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/cleared.wav"));
		}
		else
		{
			m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/failed.wav"));
		}
		m_state = DST_ANNOUNCE;
		m_alpha = 555;
		m_posX = 100;

		tween::TweenerParam param(1000, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_posX, 0);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);

		tween::TweenerParam param2(2000, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param2.addProperty(&m_alpha, 0);
		param2.forceEnd = true;
		m_gm->m_tweener.addTween(param2);
	}
	if (m_state == DST_ANNOUNCE && (int)m_alpha == 0)
	{
		m_othersAnim = 1.0;
		m_state = DST_ANNOUNCE_OUT;

		tween::TweenerParam param(500, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_othersAnim, 0);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);
	}
	if (m_state == DST_ANNOUNCE_OUT)
	{
		m_state = DST_RESULT;
		m_posX = 100;
		m_alpha = 0;
		m_posXEnd = 0;
		m_alphaEnd = 255;

		tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_posX, 0);
		param.addProperty(&m_alpha, 255);
		m_gm->m_tweener.addTween(param);
	}
	if (m_state == DST_END && (int)m_alphaEnd == 0)
	{
		m_state = DST_ENDED;
		m_gm->m_gmm->currentMode()->OnDebriefingExit();
	}
}

void DebriefingPanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	m_keyboard.ReceiveEvent(ev, param);
	GUIPanel::ReceiveEvent(ev, param);
}

void DebriefingPanel::OnEndGame(GUIWidget *btn)
{
	if (m_state != DST_RESULT)
		return ;
	m_state = DST_END;

	m_posX = 0;
	m_alpha = 255;
	m_posXEnd = 0;
	m_alphaEnd = 255;

	tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 100);
	param.addProperty(&m_alpha, 0);
	param.addProperty(&m_headerAlpha, 0);
	param.addProperty(&m_posXEnd, 100);
	param.addProperty(&m_alphaEnd, 0);
	param.forceEnd = true;
	m_gm->m_tweener.addTween(param);

	m_gm->m_gui.m_mainBg->m_generateCloudsCount = BG_CLOUDS_NB;
	m_gm->m_gui.m_mainBg->m_generateBubblesCount = BG_BUBBLES_NB;
}