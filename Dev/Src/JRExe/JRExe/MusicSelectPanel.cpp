
#include <iostream>
#include <JREngine/jrgui.h>
#include "MusicSelectPanel.h"
#include "GUIPanel.h"
#include "JRExe.h"
#include "SongLibrary.h"

MusicSelectPanel::MusicSelectPanel(JRExe *gm) 
	: GUIPanel(PANEL_MUSIC_SELECT, gm), m_keyboard(*gm->GetParams()), m_graph(gm), m_gm(gm)
{
	m_step = 10;
	m_started = false;
	m_musicOffset = 0;
	m_selectedBeatmap = NULL;

	m_graph.SetMaxX(80);
	m_graph.SetMaxY(12);

	for (int i = 0; i < 16; i++) {
		/* Previous button */
		if (i == 12) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnPrevious);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				MusicSelectPanel *pane = (MusicSelectPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_PREV_BTN));
				render->GetRenderWindow()->draw(rect);
				/*
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString("<");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/
			});
			m_keyboard.SetWidget(i, btn);
		}
		/* Next button */
		else if (i == 13) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnNext);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				MusicSelectPanel *pane = (MusicSelectPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_NEXT_BTN));
				render->GetRenderWindow()->draw(rect);
				/*
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString(">");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/
			});
			m_keyboard.SetWidget(i, btn);
		}
		/* Menu button */
		else if (i == 14) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnSettings);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				MusicSelectPanel *pane = (MusicSelectPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_CONFIG_BTN));
				render->GetRenderWindow()->draw(rect);
				/*
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString(">");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/
			});
			m_keyboard.SetWidget(i, btn);
		}
		/* Start button */
		else if (i == 15) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnStart);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				float ratio = render->GetRenderingRatio();

				MusicSelectPanel *pane = (MusicSelectPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_START_NOLOGO_BTN));
				render->GetRenderWindow()->draw(rect);
				/*
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString("Play");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/

				/* Time calculation for the start timer */
				float time = pane->m_startMaxTime - pane->m_startClock.getElapsedTime().asSeconds();
				if (time < 0)
					time = 0;
				std::string timeStr = std::to_string(time);
				std::string resStr = timeStr;
				if (time < 10)
					resStr = "0" + resStr;
				if (time == 0)
					resStr = "00:00";
				resStr = resStr.substr(0, 5);

				tx.setFont(render->GetFont(FONT_FIXED_SIZE_TX));
				tx.setString(resStr);
				if (time < 10)
					tx.setColor(sf::Color(255, 0, 0, pane->m_alpha));
				else
					tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(50 * ratio);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y + size.y * 0.15);
				render->GetRenderWindow()->draw(tx);

			});
			m_keyboard.SetWidget(i, btn);
		}
		/* Music buttons */
		else if (i < 12)
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnSelect);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				float ratio = render->GetRenderingRatio();

				MusicSelectPanel *pane = (MusicSelectPanel *)widget->GetUserData();
				int musicId = (widget->GetLinkedKey() - 1) / 4 + ((widget->GetLinkedKey() - 1) % 4) * 3 + pane->m_musicOffset;
				Beatmap *bmp = pane->m_gm->m_songs.GetBeatmap(musicId);

				if (bmp == NULL)
					return ;

				pos = widget->GetPosition();
				pos.x += pane->m_posXMusic;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				if (pane->m_selectedBeatmap == bmp)
					rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaMusic));
				else
					rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaMusic));
				rect.setTexture(render->GetTexture(TEX_COVER_BG_SMALL));
				render->GetRenderWindow()->draw(rect);

				// Rendering song image
				rect.setSize(sf::Vector2f(size.x - 30 * ratio, size.y - 30 * ratio));
				rect.setPosition(pos.x + 15 * ratio, pos.y + 15 * ratio);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaMusic));
				rect.setTexture(render->GetTexture(TEXTURESDIR + std::string("Musics/" + bmp->src + ".jpg")));
				rect.setTextureRect(sf::IntRect(0, 0, rect.getTexture()->getSize().x, rect.getTexture()->getSize().y));
				render->GetRenderWindow()->draw(rect);

				if (pane->m_selectedBeatmap == bmp) {
					widget->RenderAsSelection(render, pane->m_gm->GetMainClock(), sf::Vector2f(pane->m_posXMusic, 0), pane->m_alpha - pane->m_alphaMusic);
				}

				// Rendering song level
				float levelBoxW = 40 * ratio;
				float contentPosX = pos.x + size.x - levelBoxW;
				float contentPosY = pos.y + size.y - levelBoxW;

				rect.setOrigin(0, 0);
				rect.setRotation(0);
				rect.setSize(sf::Vector2f(levelBoxW, levelBoxW));
				rect.setPosition(contentPosX, contentPosY);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaMusic));
				rect.setTexture(render->GetTexture(TEX_DIFFICULTY_BG));
				rect.setTextureRect(sf::IntRect(0, 0, 101, 102));
				render->GetRenderWindow()->draw(rect);

				rect.setSize(sf::Vector2f(levelBoxW - 12 * ratio, levelBoxW - 12 * ratio));
				rect.setPosition(contentPosX + 6 * ratio, contentPosY + 6 * ratio);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaMusic));
				if (bmp->songLevel == SL_BASIC)
					rect.setTexture(render->GetTexture(TEX_DIFFICULTY_GREEN));
				else if (bmp->songLevel == SL_INTERMEDIATE)
					rect.setTexture(render->GetTexture(TEX_DIFFICULTY_ORANGE));
				else	
					rect.setTexture(render->GetTexture(TEX_DIFFICULTY_RED));
				rect.setTextureRect(sf::IntRect(0, 0, 87, 87));
				render->GetRenderWindow()->draw(rect);

				tx.setFont(render->GetFont(FONT_TITLE_TX));
				tx.setString(std::to_string(bmp->level));
				tx.setCharacterSize(30 * ratio);
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaMusic));
				sz = tx.getLocalBounds();
				tx.setOrigin(sz.left + sz.width/2.0f,
							   sz.top  + sz.height/2.0f);
				tx.setPosition(contentPosX + levelBoxW / 2, contentPosY + levelBoxW / 2);
				render->GetRenderWindow()->draw(tx);

				// Rendering song title box
				rect.setPosition(pos.x, pos.y);
				rect.setSize(sf::Vector2f(size.x, 20 * ratio));
				rect.setFillColor(sf::Color(0, 0, 0, (pane->m_alpha - pane->m_alphaMusic) * 0.5));
				rect.setTexture(NULL);
				render->GetRenderWindow()->draw(rect);
				
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString(bmp->name);
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaMusic));
				tx.setCharacterSize(14 * ratio);
				sz = tx.getLocalBounds();
				tx.setOrigin(sz.left + sz.width / 2,
					sz.top  + sz.height/2.0f);
				tx.setPosition(pos.x + size.x / 2, pos.y + 10 * ratio);
				render->GetRenderWindow()->draw(tx);

			});
			m_keyboard.SetWidget(i, btn);
		}
		else
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			m_keyboard.SetWidget(i, btn);
		}
	}
}

MusicSelectPanel::~MusicSelectPanel()
{

}

void MusicSelectPanel::Init()
{
	int py = (int) (m_size.y - m_size.x);
	int sz = (int) m_size.x;

	m_selectedBeatmap = NULL;
	m_keyboard.SetPosition(0, (float) py);
	m_keyboard.SetSize((float) sz, (float) sz);
	m_keyboard.Init();
	m_musicOffset = 0;
}

void MusicSelectPanel::Start()
{
	m_posX = 100;
	m_posXMusic = 0;
	m_alpha = 0;
	m_alphaMusic = 0;
	m_alphaSelect = 0;
	m_posXSelect = 0;
	m_started = false;
	//m_selectedBeatmap = NULL;

	tween::TweenerParam param(200, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 0);
	param.addProperty(&m_alpha, 255);
	param.forceEnd = true;
	m_gm->m_tweener.addTween(param);

	// Reset the state of all buttons
	for (int i = 0; i < 16; i++) {
		m_keyboard.GetWidget(i)->OnReleased();
	}
	if (m_selectedBeatmap) {
		m_gm->m_gmm->currentMode()->OnMusicSelect(m_selectedBeatmap);
	}
	m_startClock.restart();
	m_startMaxTime = 60.00;
	m_startTickSec = 50.00;

	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/select_music.wav"));
}

void MusicSelectPanel::Stop()
{
	m_gm->m_sounds.StopPreviewMusic();
}

void MusicSelectPanel::Draw(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	sf::RectangleShape rect(sf::Vector2f(sw, sh * 0.7));
	sf::Text text;
	sf::FloatRect textRect;

	/* Panel title */
	text.setPosition(0, 0);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(sh * 0.15);
	text.setString("SELECT MUSIC");
	target->GetRenderWindow()->draw(text);

	rect.setPosition(0, sh * 0.15);
	rect.setFillColor(sf::Color(0, 0, 0, m_alpha * 0.4));
	target->GetRenderWindow()->draw(rect);

	if (!m_selectedBeatmap)
	{
		return ;
	}

	// padding between content and padding inside black bg : 24px
	// padding between black bg and window : 90px
	// padding between black jacket holder and jacket : 10px
	// padding between level box and jacket : 15px

	float internalPadding = 24 * ratio;
	float jacketPadding = 10 * ratio;
	float postJacketPadding = 10 * ratio;

	// animations calculations
	float contentAlpha = MAX(0, MIN(255, m_alpha - m_alphaSelect));

	/* Rendering the jacket and its box */
	rect.setSize(sf::Vector2f(sh * 0.7 - internalPadding * 2 + 20 * ratio, sh * 0.7 - internalPadding * 2 + 10 * ratio));
	rect.setTexture(target->GetTexture(TEX_COVER_BG_BIG));
	rect.setPosition(internalPadding + m_posX - 10 * ratio, sh * 0.15 + internalPadding);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	target->GetRenderWindow()->draw(rect);

	float jacSize = (sh * 0.7 - internalPadding * 2 - jacketPadding * 2);

	rect.setSize(sf::Vector2f(jacSize, jacSize));
	rect.setOrigin(jacSize / 2, jacSize / 2);
	rect.setPosition(internalPadding + m_posX + jacketPadding + jacSize / 2, 
		sh * 0.15 + internalPadding + jacketPadding + jacSize / 2);
	rect.setFillColor(sf::Color(255, 255, 255, contentAlpha));
	rect.setRotation(m_posXSelect);
	rect.setTexture(target->GetTexture(TEXTURESDIR + std::string("Musics/" + m_selectedBeatmap->src + ".jpg")));
	rect.setTextureRect(sf::IntRect(0, 0, rect.getTexture()->getSize().x, rect.getTexture()->getSize().y));
	target->GetRenderWindow()->draw(rect);

	float contentPosX = m_posX + internalPadding + jacketPadding * 2 + jacSize + postJacketPadding;
	float contentPosY = sh * 0.15 + internalPadding;

	/* Rendering the level box on top right content */
	float levelBoxW = 80 * ratio;

	rect.setOrigin(0, 0);
	rect.setRotation(0);
	rect.setSize(sf::Vector2f(levelBoxW, levelBoxW));
	rect.setPosition(contentPosX, contentPosY);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	rect.setTexture(target->GetTexture(TEX_DIFFICULTY_BG));
	rect.setTextureRect(sf::IntRect(0, 0, 101, 102));
	target->GetRenderWindow()->draw(rect);

	rect.setSize(sf::Vector2f(levelBoxW - 12 * ratio, levelBoxW - 12 * ratio));
	rect.setPosition(contentPosX + 6 * ratio, contentPosY + 6 * ratio);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	if (m_selectedBeatmap->songLevel == SL_BASIC)
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_GREEN));
	else if (m_selectedBeatmap->songLevel == SL_INTERMEDIATE)
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_ORANGE));
	else	
		rect.setTexture(target->GetTexture(TEX_DIFFICULTY_RED));
	rect.setTextureRect(sf::IntRect(0, 0, 87, 87));
	target->GetRenderWindow()->draw(rect);

	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setString(std::to_string(m_selectedBeatmap->level));
	text.setCharacterSize(40 * ratio);
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width/2.0f,
				   textRect.top  + textRect.height/2.0f);
	text.setPosition(contentPosX + levelBoxW / 2, contentPosY + levelBoxW / 2);
	target->GetRenderWindow()->draw(text);

	float textPosX = contentPosX + levelBoxW + postJacketPadding + m_posXSelect;

	/* Rendering the title, author, difficulty, bpm on top content */
	text.setPosition(textPosX, contentPosY);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	text.setOrigin(0, 0);
	text.setCharacterSize(35 * ratio);
	text.setString(m_selectedBeatmap->name);
	target->GetRenderWindow()->draw(text);

	text.setPosition(textPosX, contentPosY + 45 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	text.setOrigin(0, 0);
	text.setCharacterSize(22 * ratio);
	text.setString(m_selectedBeatmap->author);
	target->GetRenderWindow()->draw(text);

	text.setPosition(sw - internalPadding, contentPosY);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setCharacterSize(18 * ratio);
	if (m_selectedBeatmap->songLevel == SL_EXTREME) {
		text.setString("EXTREME");
		text.setColor(sf::Color(255, 0, 0, contentAlpha));
	} else if (m_selectedBeatmap->songLevel == SL_INTERMEDIATE) {
		text.setString("NORMAL");
		text.setColor(sf::Color(255, 255, 0, contentAlpha));
	} else {
		text.setString("BASIC");
		text.setColor(sf::Color(0, 255, 0, contentAlpha));
	}
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width,
				   0);
	target->GetRenderWindow()->draw(text);

	text.setPosition(sw - internalPadding, contentPosY + 30 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	text.setCharacterSize(16 * ratio);
	text.setString(std::to_string(m_selectedBeatmap->originalBpm) + "BPM");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width,
				   0);
	target->GetRenderWindow()->draw(text);

	float highscoreX = contentPosX + 150 * ratio;
	float highscoreY = contentPosY + 110 * ratio;

	/* Rendering the high scores */
	unsigned int curScore = m_curScore;
	/*for (int i = 0; i < 3; i++)
	{
		text.setPosition(highscoreX - 15 * ratio - m_posXSelect, highscoreY + i * 35 * ratio);
		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, contentAlpha));
		text.setCharacterSize(24 * ratio);
		text.setString("160 000");
		textRect = text.getLocalBounds();
		text.setOrigin(textRect.left + textRect.width, 0);
		target->GetRenderWindow()->draw(text);

		text.setPosition(highscoreX + 15 * ratio + m_posXSelect, highscoreY + i * 35 * ratio);
		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, contentAlpha));
		text.setCharacterSize(24 * ratio);
		text.setString("Player");
		textRect = text.getLocalBounds();
		text.setOrigin(0, 0);
		target->GetRenderWindow()->draw(text);
	}*/


	/* Rendering the best rank */
	if (curScore)
	{
		text.setPosition(highscoreX - 15 * ratio - m_posXSelect, highscoreY + 50 * ratio);
		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, contentAlpha));
		text.setCharacterSize(24 * ratio);
		text.setString(std::to_string(curScore));
		textRect = text.getLocalBounds();
		text.setOrigin(textRect.left + textRect.width, 0);
		target->GetRenderWindow()->draw(text);

		text.setPosition(highscoreX + 15 * ratio + m_posXSelect, highscoreY + 50 * ratio);
		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, contentAlpha));
		text.setCharacterSize(24 * ratio);
		text.setString("Top score");
		textRect = text.getLocalBounds();
		text.setOrigin(0, 0);
		target->GetRenderWindow()->draw(text);
	}
	else
	{
		text.setPosition(highscoreX - 35 * ratio + m_posXSelect, highscoreY + 85 * ratio);
		text.setFont(target->GetFont(FONT_SCORING_TX));
		text.setColor(sf::Color(255, 255, 255, contentAlpha));
		text.setCharacterSize(24 * ratio);
		text.setString("No score available");
		textRect = text.getLocalBounds();
		text.setOrigin(0, 0);
		target->GetRenderWindow()->draw(text);
	}

	text.setPosition(highscoreX - 35 * ratio + m_posXSelect, highscoreY + 20 * ratio);
	text.setFont(target->GetFont(FONT_SCORING_TX));
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	text.setCharacterSize(24 * ratio);
	text.setString("S.Delay: " + std::to_string(m_selectedBeatmap->beatsList.front().time));
	textRect = text.getLocalBounds();
	text.setOrigin(0, 0);
	target->GetRenderWindow()->draw(text);

	/* Rendering the best rank */
	if (curScore)
	{
		unsigned int tex;

		rect.setSize(sf::Vector2f(140 * ratio, 140 * ratio));
		rect.setPosition(sw - highscoreX + contentPosX + m_posX + internalPadding - 140 * ratio + m_posXSelect, highscoreY);
		rect.setFillColor(sf::Color(255, 255, 255, contentAlpha));
		//if (curScore == 1000000) tex = target->GetTexture(TEX_GRADE_EXC);
		//else if (curScore >= 980000) tex = target->GetTexture(TEX_GRADE_SSS);
		//else if (curScore >= 950000) tex = target->GetTexture(TEX_GRADE_SS);
		//else if (curScore >= 900000) tex = target->GetTexture(TEX_GRADE_S);
		//else if (curScore >= 850000) tex = target->GetTexture(TEX_GRADE_A);
		//else if (curScore >= 800000) tex = target->GetTexture(TEX_GRADE_B);
		//else if (curScore >= 700000) tex = target->GetTexture(TEX_GRADE_C);
		//else if (curScore >= 400000) tex = target->GetTexture(TEX_GRADE_D);
		//else tex = target->GetTexture(TEX_GRADE_E);
		tex = m_gm->m_gmm->currentMode()->GetScoreManager().getGradeForFinalScore(curScore);
		rect.setTexture(target->GetTexture((TEXID)(TEX_GRADE_E + tex)));
		rect.setTextureRect(sf::IntRect(0, 0, 143, 135));
		target->GetRenderWindow()->draw(rect);
	}

	/* Rendering the stats */
	/*
	float statsX = contentPosX + (sw - contentPosX - internalPadding) / 2 + m_posXSelect;
	float statsY = highscoreY + 35 * ratio * 3 + 45 * ratio;
	float statsPad = (sw - contentPosX - internalPadding) / 4;

	text.setPosition(statsX - statsPad * 1.5, statsY);
	text.setFont(target->GetFont(FONT_SUBTITLE_TX));
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	text.setCharacterSize(20 * ratio);
	text.setString("COMBO RATE");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2,
		textRect.top  + textRect.height/2.0f);
	target->GetRenderWindow()->draw(text);
	text.setPosition(statsX - statsPad * 1.5, statsY + 25 * ratio);
	text.setString("88%");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2,
		textRect.top  + textRect.height/2.0f);
	target->GetRenderWindow()->draw(text);

	text.setPosition(statsX - statsPad * 0.5, statsY);
	text.setFont(target->GetFont(FONT_SUBTITLE_TX));
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	text.setCharacterSize(20 * ratio);
	text.setString("PERFECTION");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2,
		textRect.top  + textRect.height/2.0f);
	target->GetRenderWindow()->draw(text);
	text.setPosition(statsX - statsPad * 0.5, statsY + 25 * ratio);
	text.setString("88%");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2,
		textRect.top  + textRect.height/2.0f);
	target->GetRenderWindow()->draw(text);

	text.setPosition(statsX + statsPad * 0.5, statsY);
	text.setFont(target->GetFont(FONT_SUBTITLE_TX));
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	text.setCharacterSize(20 * ratio);
	text.setString("WIN RATE");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2,
		textRect.top  + textRect.height/2.0f);
	target->GetRenderWindow()->draw(text);
	text.setPosition(statsX + statsPad * 0.5, statsY + 25 * ratio);
	text.setString("88%");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2,
		textRect.top  + textRect.height/2.0f);
	target->GetRenderWindow()->draw(text);

	text.setPosition(statsX + statsPad * 1.5, statsY);
	text.setFont(target->GetFont(FONT_SUBTITLE_TX));
	text.setColor(sf::Color(255, 255, 255, contentAlpha));
	text.setCharacterSize(20 * ratio);
	text.setString("POPULARITY");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2,
		textRect.top  + textRect.height/2.0f);
	target->GetRenderWindow()->draw(text);
	text.setPosition(statsX + statsPad * 1.5, statsY + 25 * ratio);
	text.setString("88%");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2,
		textRect.top  + textRect.height/2.0f);
	target->GetRenderWindow()->draw(text);
	*/

	/* Rendering the graph */
	m_graph.SetPosition(contentPosX, contentPosY + sh * 0.7 - internalPadding * 2 - 70 * ratio);
	m_graph.SetSize(sw - internalPadding - contentPosX, 70 * ratio);
	m_graph.Draw(target);

	return;
}

void MusicSelectPanel::Update(sf::Clock *time)
{
	GUIPanel::Update(time);
	if (m_started == false && m_startTickSec < m_startClock.getElapsedTime().asSeconds())
	{
		m_startTickSec += 1;
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("tim_tum.wav"));
	}
	if (m_started == false && m_startClock.getElapsedTime().asSeconds() > 60.0)
	{
		if (m_selectedBeatmap == NULL)
		{
			m_selectedBeatmap = m_gm->m_songs.GetBeatmap(0);
		}
		OnStart(NULL);
	}
	if (m_started == true && (int)m_alpha == 0)
	{
		/* TODO: SWAP TO MATCHMAKING INSTEAD OF DIRECT PLAY */

		if (m_toSettings)
		{
			m_gm->GetGUI()->HideAll();
			m_gm->GetGUI()->Show(PANEL_CONFIG);
		}
		else
		{
			m_gm->m_gmm->currentMode()->OnGameSet(m_selectedBeatmap);
			m_gm->m_gsm.ChangeState(GMST_PLAYING);
		}
		m_started = false;
	}
}

void MusicSelectPanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	m_keyboard.ReceiveEvent(ev, param);
	GUIPanel::ReceiveEvent(ev, param);
}

void MusicSelectPanel::OnSelect(GUIWidget *btn)
{
	Beatmap *bmp = m_gm->m_songs.GetBeatmap((btn->GetLinkedKey() - 1) / 4 + ((btn->GetLinkedKey() - 1) % 4) * 3 + m_musicOffset);
	
	if (!bmp || m_started)
		return ;
	
	m_selectedBeatmap = bmp;
	m_gm->m_gmm->currentMode()->OnMusicSelect(m_selectedBeatmap);
	m_curScore = m_gm->m_playersManager.getCurrentPlayer()->getHighScoreFor(m_selectedBeatmap->name, m_selectedBeatmap->songLevel);

	m_posXSelect = 50;
	m_alphaSelect = 255;
	tween::TweenerParam param(400, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posXSelect, 0);
	param.addProperty(&m_alphaSelect, 0);
	param.forceEnd = true;
	m_gm->m_tweener.addTween(param);
	/*IGamemode *gm = m_gm->m_gmm->GetGamemodeFromId(-1 + btn->GetLinkedKey());

	if (!gm)
		return ;
	m_selectedGamemode = gm;
	m_gm->m_sounds.PlaySnd(SOUNDDIR + m_selectedGamemode->GetSoundName());*/
}

void MusicSelectPanel::OnPrevious(GUIWidget *btn)
{
	if (m_started)
		return ;
	m_musicOffset -= 3;
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));

	m_posXMusic = -m_gm->m_params.m_params.btn_size;
	m_alphaMusic = 255;

	tween::TweenerParam param(400, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posXMusic, 0);
	param.addProperty(&m_alphaMusic, 0);
	m_gm->m_tweener.addTween(param);
}

void MusicSelectPanel::OnNext(GUIWidget *btn)
{
	if (m_started)
		return ;
	m_musicOffset += 3;
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));

	m_posXMusic = m_gm->m_params.m_params.btn_size;
	m_alphaMusic = 255;

	tween::TweenerParam param(400, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posXMusic, 0);
	param.addProperty(&m_alphaMusic, 0);
	m_gm->m_tweener.addTween(param);
}

void MusicSelectPanel::OnStart(GUIWidget *btn)
{
	if (m_started == false && m_selectedBeatmap)
	{
		m_started = true;
		m_toSettings = false;
		m_alpha = 255;

		tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_alpha, 0);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));
	}
}

void MusicSelectPanel::OnSettings(GUIWidget *btn)
{
	if (m_started == false)
	{
		m_started = true;
		m_toSettings = true;
		m_alpha = 255;

		tween::TweenerParam param(200, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_alpha, 0);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));
	}
}