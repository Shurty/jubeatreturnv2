#include <string>
#include "GUIBeatGraph.h"
#include "JRExe.h"

GUIBeatGraph::GUIBeatGraph(JRExe *core)
	: GUIWidget(core->m_params), m_core(core)
{
	m_playmode = false;
}

GUIBeatGraph::~GUIBeatGraph()
{

}

/*
void GUIBeatButton::LinkToPlayPanel(PlayPanel* pp,
	void(PlayPanel::*clb)(float x, float y, float size))
{
	_playPanel = pp;
	_pressActionClb = clb;
}
*/

void GUIBeatGraph::SetMaxY(float i) { m_maxY = i; }
void GUIBeatGraph::SetMaxX(float i) { m_maxX = i; }
void GUIBeatGraph::MarkAsPlaying(bool mk) { m_playmode = mk; }

void GUIBeatGraph::Init()
{

}

void GUIBeatGraph::Draw(RenderingEngine *target)
{
	GUIWidget::Draw(target);

	// we want to render a total of X notes with a defined size
	// the notes will have a fixed size

	float graphX = m_position.x;
	float graphMaxX = m_position.x + m_size.x;
	float graphY = m_position.y + m_size.y;
	int graphDotSize = (m_size.y - m_maxY) / m_maxY;
	float graphStep = (m_size.x - graphDotSize * m_maxX) / m_maxX;
	sf::RectangleShape rect;

	if (!m_core->m_gmm->currentMode() || !m_core->m_gmm->currentMode()->GetSelectedBeatmap())
		return ;

	Beatmap *bmp = m_core->m_gmm->currentMode()->GetSelectedBeatmap();
	// now go trough all the notes at a defined time step, the added maxtime is the end boundary for fadeout effect
	int maxTime = (--bmp->beatsList.end())->time + 2500;
	float playmodeTime = (float)m_core->m_gmm->currentMode()->GetPlayTime();
	float curTime = 0;
	float timestep = (float)maxTime / (float)m_maxX;
	std::deque<Beat>::iterator it = bmp->beatsList.begin();

	for (int i = 0; i < m_maxX; i++)
	{
		int cnt = 0;
		int prevTm = -1;
		while (it != bmp->beatsList.end() && it->time <= curTime)
		{
			//if (it->time != prevTm)
				cnt++;
			//prevTm = it->time;
			++it;
		}
		for (int count = 0; count < m_maxY; count++)
		{
			int posX = graphX + i * graphDotSize + i * graphStep;

			rect.setSize(sf::Vector2f(graphDotSize, graphDotSize));
			rect.setPosition(posX, graphY - ((count + 1) * graphDotSize) - (1 * count));
			if (count < cnt) {
				if (!m_playmode || playmodeTime > curTime)
					rect.setFillColor(sf::Color(255, 255, 255, 255));
				else
					rect.setFillColor(sf::Color(80, 80, 80, 255));
			} else {
				rect.setFillColor(sf::Color(0, 0, 0, 100));
			}
			rect.setTexture(NULL);
			target->GetRenderWindow()->draw(rect);
		}
		curTime += timestep;
	}
	if (m_playmode)
	{
		float cursorX = m_size.x * (playmodeTime / (float)maxTime);

		rect.setFillColor(sf::Color(255, 255, 255, 255));
		rect.setPosition(cursorX + m_position.x, m_position.y);
		rect.setSize(sf::Vector2f(graphDotSize, m_size.y));
		rect.setTexture(NULL);
		target->GetRenderWindow()->draw(rect);
	}

}

void GUIBeatGraph::Update(sf::Clock *time)
{
	/*int inLength = (int) ((BAD_DELAY + GOOD_DELAY + GREAT_DELAY + PERFECT_DELAY) / m_activeBpm);
	int outLength = (int) ((GREAT_DELAY + GOOD_DELAY + BAD_DELAY) / 2.0f / m_activeBpm);
	int totalLength = inLength + outLength;*/
}

void GUIBeatGraph::OnTriggered()
{
}

void GUIBeatGraph::OnPressed()
{
}

void GUIBeatGraph::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
	if (ev == BEATMAP_START) {
		m_bmp = (Beatmap *)param;
	}
}