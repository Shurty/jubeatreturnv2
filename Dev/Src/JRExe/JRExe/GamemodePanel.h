#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrgui.h>

#include "GUIPanel.h"

class JRExe;
class RenderingEngine;
class IGamemode;

class GamemodePanel : public GUIPanel 
{
private:
	GUIKeyboard m_keyboard;
	int m_step;
	JRExe *m_gm;
	float m_posX;
	float m_alpha;
	bool m_started;

	sf::Clock m_startClock;
	int m_startMaxTime;
	float m_startTickSec;
	bool m_pressedOnce;
	IGamemode *m_selectedGamemode;

public:
	GamemodePanel(JRExe *gm);
	~GamemodePanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
	virtual void OnStart(GUIWidget *btn);
	virtual void OnSettings(GUIWidget *btn);
	virtual void OnGamemode(GUIWidget *btn);
};