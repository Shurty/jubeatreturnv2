
#pragma once

#include <vector>
#include <JREngine/jrshared.h>
#include <JREngine/jrgui.h>

#include "SongLibrary.h"
#include "GUIController.h"
#include "SoundManager.h"
#include "NetworkController.h"

class InputController;

class JRExe : public IInterfacable
{
public: // FIXME: Sorry I had to put that in public
	Parameters			m_params;
	GameStateManager	m_gsm;
	GamemodeManager*	m_gmm;
	RenderingEngine		m_render;
	GUIController		m_gui;
	InputController*	m_input;
	CreditsController	m_credits;
	// Player*				m_player;
	SoundManager		m_sounds;
	SongLibrary			m_songs;
	bool				&m_valid;
	sf::Clock			m_globClock;
	int					m_currentFps;
	tween::Tweener		m_tweener;

	PlayersManager		m_playersManager;
	NetworkController	m_network;
	TESTLVL				m_testLvl;
	// std::vector<Player> m_players;
public:
	JRExe();
	virtual ~JRExe();

	void OnStart();
	void OnUpdate();
	void OnRender();
	void OnPostRender();
	void OnExit();
	void RunTests();

	void SetValid(bool st);
	bool IsValid();
	void SetTestLevel(TESTLVL);
	TESTLVL GetTestLevel() const;

	RenderingEngine* GetRenderer();
	GUIController* GetGUI();
	Parameters* GetParams();
	sf::Clock* GetMainClock();

	virtual void OnKeyPressed(int key);
	virtual void OnKeyReleased(int key);

	void setCurentFps(int framesPerSecond);
};
