
#pragma once

#include <JREngine/jrshared.h>
#include "JRExe.h"

class StatePlaying : public GameState
{
private:
	JRExe&	m_core;
public:
	StatePlaying(JRExe&);

	void OnInit(Parameters &pm);
	void OnStart(Parameters &pm);
	void OnRender(Parameters &pm);
	void OnUpdate(Parameters &pm);
	void OnEnd(Parameters &pm);
	void OnUnload(Parameters &pm);

};