
#pragma once

#include <JREngine/jrshared.h>

class CommandMatchmaking : public AMatchmaking
{
private:

public:
	CommandMatchmaking() 
		: AMatchmaking()
	{

	}

	CommandMatchmaking::~CommandMatchmaking()
	{

	}

	virtual void Receive(Client *cl, std::string const &pl, int song)
	{
		std::cout << "New player in active matchmaking: " << pl << std::endl;
		cl->GetMatchmakingParty()->PushClient(NULL);
	}

	virtual void Send(Client *cl, std::string const &pl, int song)
	{
		sf::Packet pck;

		std::cout << "Start matchmaking for song" << song << std::endl;
		pck << GetID() << pl << song;
		cl->GetSock().send(pck);
	}
};