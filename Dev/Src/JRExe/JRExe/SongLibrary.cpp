#ifdef WIN32
#include <windows.h>
#else

#endif
#include <JREngine/jrshared.h>
#include "SongLibrary.h"

SongLibrary::SongLibrary()
{
	m_parsers["txt"] = new MemoParser(); 
	m_parsers["jcm"] = m_parsers["txt"]; 
	m_parsers["jrb"] = new JRBParser(); 
	Logger::Log << "Module construct: SongLibrary OK" << std::endl;
	/*m_activeFilter = NULL;
	m_activeSorter = NULL;*/
	difficultyFilterValue = 0;
	noFilter = [](const SongLibrary&me, Beatmap*bt) -> bool {
		return (true);
	};
	difficultyFilter = [](const SongLibrary&me, Beatmap*bt) -> bool {
		if (bt->level == me.difficultyFilterValue)
			return true;
		return false;
	};

	// Sorting
	sortAscending = true;
	difficultySorter = [](const SongLibrary&me, Beatmap*bt1, Beatmap*bt2) -> bool {
		if ((me.sortAscending == true && bt1->level < bt2->level) ||
			(me.sortAscending == false && bt1->level > bt2->level))
			return true;
		return false;
	};
	nameSorter = [](const SongLibrary&me, Beatmap*bt1, Beatmap*bt2) -> bool {
		if ((me.sortAscending == true && bt1->name.compare(bt2->name) < 0) ||
			(me.sortAscending == false && bt1->name.compare(bt2->name) > 0))
			return true;
		return false;
	};
	authorSorter = [](const SongLibrary&me, Beatmap*bt1, Beatmap*bt2) -> bool {
		if ((me.sortAscending == true && bt1->author.compare(bt2->author) < 0) ||
			(me.sortAscending == false && bt1->author.compare(bt2->author) > 0))
			return true;
		return false;
	};

	m_activeFilter = noFilter;
	m_activeSorter = difficultySorter;
}

SongLibrary::~SongLibrary()
{
}

void SongLibrary::ParseAll(std::string const &dirsrc)
{
#ifdef WIN32
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;
	Beatmap *bmp;
	std::string activeSrc;
	
	Logger::Log << "SongLibrary: Loading handle " << dirsrc << std::endl;
	hFind = FindFirstFile((dirsrc + "*").c_str(), &ffd);
	if (INVALID_HANDLE_VALUE == hFind) 
	{
		Logger::Log << "SongLibrary Error: Invalid handle " << dirsrc << std::endl;
		// No files
		return ;
	}
	else 
	{
		do {
			activeSrc = ffd.cFileName;
			if (activeSrc[0] != '.') {
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					ParseAll(dirsrc + "" + activeSrc + "/");
				} else {
					std::string ext = activeSrc.substr(activeSrc.length() - 3, 3);

					if (!m_parsers[ext])
						ext = "txt";
					bmp = m_parsers[ext]->ParseBeatmap(dirsrc + "" + activeSrc);
					bmp->GenerateHash();
					Logger::Log << "SongLibrary: Added " << std::string(bmp->name.begin(), bmp->name.end()) << " " << bmp->level << std::endl;
					m_beatmaps.push_back(bmp);
				}
			}
		} while (FindNextFile(hFind, &ffd) != 0);
	}
#else
#endif
	m_filteredBeatmaps = m_availableBeatmaps;
	RefreshFilteringSorting();
}

void SongLibrary::Update()
{
	
}

void SongLibrary::Clear()
{
	while (!m_beatmaps.empty())
	{
		delete m_beatmaps.back();
		m_beatmaps.pop_back();
	}
}

/*
void SongLibrary::SortBeatmaps(std::function<bool (Beatmap* a, Beatmap* b)> func)
{
	std::sort(m_beatmaps.begin(), m_beatmaps.end(), func);
}
*/

void SongLibrary::RefreshFilteringSorting()
{
	m_filteredBeatmaps.clear();
	if (m_activeFilter)
	{
		std::for_each(m_availableBeatmaps.begin(), m_availableBeatmaps.end(), [this](Beatmap *bt) {
			if (this->m_activeFilter(*this, bt)) {
				this->m_filteredBeatmaps.push_back(bt);
			}
		});
	}
	if (m_activeSorter)
	{
		std::sort(m_filteredBeatmaps.begin(), m_filteredBeatmaps.end(), [this](Beatmap *bt1, Beatmap *bt2) {
			return (this->m_activeSorter(*this, bt1, bt2));
		});
	}
}

void SongLibrary::FilterAvailable(bool(*sorter)(Beatmap *))
{
	m_availableBeatmaps.clear();
	std::for_each(m_beatmaps.begin(), m_beatmaps.end(), [&sorter, this](Beatmap *bt) {
		if (sorter(bt)) {
			m_availableBeatmaps.push_back(bt);
		}
	});
}

void SongLibrary::SetAvailable(size_t hash)
{
	auto bt_it = std::find_if(m_beatmaps.begin(), m_beatmaps.end(), [&hash](Beatmap *bt) {
		if (bt->hash == hash)
			return (true);
		return (false);
	});
	if (bt_it != m_beatmaps.end()) {
		m_availableBeatmaps.push_back(*bt_it);
	}
}

void SongLibrary::SetFilter(bool(*filter)(const SongLibrary&, Beatmap *))
{
	m_activeFilter = filter;
}

void SongLibrary::SetSorter(bool(*sorter)(const SongLibrary&, Beatmap *, Beatmap *))
{
	m_activeSorter = sorter;
}

Beatmap *SongLibrary::GetBeatmap(int idx)
{ 
	/*
	idx = idx % m_beatmaps.size();
	if (idx < 0)
		idx += m_beatmaps.size();
	*/
	if (idx < 0 || idx >= m_filteredBeatmaps.size())
		return (NULL);
	return (m_filteredBeatmaps[idx]);
}

Beatmap *SongLibrary::GetBeatmapCopy(int idx) 
{ 
	/*
	idx = idx % m_beatmaps.size();
	if (idx < 0)
		idx += m_beatmaps.size();
	*/
	if (idx < 0 || idx >= m_filteredBeatmaps.size())
		return (NULL);
	return (new Beatmap(*m_filteredBeatmaps[idx]));
}