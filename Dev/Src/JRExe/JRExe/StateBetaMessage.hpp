
#pragma once

#include <JREngine/jrshared.h>
#include "JRExe.h"
#include "Background.h"

class StateBetaMessage : public GameState
{
private:
	JRExe&	m_core;
public:
	StateBetaMessage(JRExe& core)
		: GameState(), m_core(core)
	{
		m_id = GMST_BETAMSG;
		m_name = "BETA message";
		Logger::Log << "State construct: " << m_name << std::endl;
	}

	void OnInit(Parameters &pm)
	{
		Logger::Log << "State init: " << m_name << std::endl;
	}

	void OnStart(Parameters &pm)
	{
		// FIXME: check if init has been correctly done ?
		Logger::Log << "State start: " << m_name << std::endl;
		m_core.GetGUI()->HideAll();
		m_core.GetGUI()->Show(PANEL_MESSAGE);
	}

	void OnRender(Parameters &pm)
	{
		m_core.m_render.OnPreRender(pm);
		m_core.m_gui.m_mainBg->Render(&m_core);
		m_core.m_gui.Render();
		m_core.m_gui.m_mainBg->RenderGridOverlay(&m_core);

		m_core.m_render.OnRender(pm);
	}

	void OnUpdate(Parameters &pm)
	{
		m_core.m_input->PollEvents(&m_core.m_render, &m_core);
		m_core.m_songs.Update();
		m_core.m_sounds.Update();
		m_core.m_gui.m_mainBg->Update(&m_core);
		m_core.m_gui.Update();
		m_core.m_tweener.step(m_core.GetMainClock()->getElapsedTime().asMilliseconds());
	}

	void OnEnd(Parameters &pm)
	{
	}

	void OnUnload(Parameters &pm)
	{
	}


};