#pragma once

#include <SFML/Graphics.hpp>
#include <JREngine/jrshared.h>
#include <JREngine/jrgui.h>
//#include "Game.h"

enum DETECT
{
	MISS = 0,
	BAD,
	GOOD,
	GREAT,
	PERFECT
};

class JRExe;
class RenderingEngine;

class GUIBeatButton : public GUIWidget
{
private:
	sf::Clock m_triggerTime;
	JRExe *m_core;
	bool m_isDummy;
	bool m_isFirst;

	// Click simulation
	bool m_clickSimulateDone;

	// Post click animation
	bool m_animateEnd;
	float m_endFrameId;
	BEATRESULT m_lastpress;

	// Holding animation (for special button case)
	bool m_holding;
	int m_holdingFrame;
	float m_scaleExt;

	sf::Color animOutColor;
	float m_animOutAlpha;

	Beat* m_activeBeat;

public:
	GUIBeatButton(JRExe *core);
	virtual ~GUIBeatButton();

	void SetDummy(bool dum);
	void SetFirst(bool bl);
	void SetBeat(Beat *bt);

	virtual void Init();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void OnTriggered();
	virtual void OnBeatSet(Beat *beat);
	virtual void OnPressed();
	virtual void OnReleased();
	virtual void ReceiveEvent(GUIEVENT ev, void *param);

	void AnimateResult(BEATRESULT);
};