#pragma once

#include "RewardCombo.h"

class RewardsManager
{
private:
	
public:
	RewardCombo m_rewardCombo;
	
	RewardsManager(void);
	~RewardsManager(void);

	void selectedMusic(const std::string& musicName, SONGLEVEL level);
	void hasScoredBeat(BEATRESULT res);//, unsigned int pointsForBeat, unsigned int currentCombo);
	void hasRegisteredFinalScore(unsigned int score);
};

