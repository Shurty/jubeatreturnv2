
#pragma once

#include <iostream>
#include <string>
#include "UnitException.hpp"

namespace Unit 
{

	class UnitTest
	{
	private:
		virtual void ExecTest() = 0;
		bool _success;
		std::string _testName;

	protected:

	public:
		UnitTest(std::string testName) 
			: _success(false), _testName(testName)
		{
		}
		~UnitTest() 
		{
		}

		void RunTest()
		{
			if (_success) {
				Logger::Log << "Test already run. Aborting." << std::endl;
			}
			try {
				ExecTest();
				Logger::Log << _testName << "Test result : \t\t\tOK" << std::endl;
			} catch (Unit::Exception &ex) {
				Logger::Log << _testName << ex.what() << std::endl;
				Logger::Log << _testName << "Test result : \t\t\tKO" << std::endl;
				_success = false;
			}
		}
	};

}