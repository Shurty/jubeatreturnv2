#pragma once

#include <iostream>
#include <JREngine/jrgui.h>
#include "ConfigPanel.h"
#include "GUIPanel.h"
#include "JRExe.h"
#include "SongLibrary.h"
#include "MusicSelectPanel.h"

ConfigPanel::ConfigPanel(JRExe *gm) 
	: GUIPanel(PANEL_CONFIG, gm), m_keyboard(*gm->GetParams()), m_gm(gm), 
	m_main(gm), 
	m_filters(gm), m_sorting(gm), m_activators(gm), m_mods(gm),
	m_achievements(gm), m_stats(gm), m_debug(gm)
{
	m_activeLayout = &m_main;
	m_selectOffset = 0;

	SettingsIO sett(SETTINGS_DATA_SRC);
	m_showBackBtn = sett.GetBool("UI", "BackButton");

	if (!m_showBackBtn) {
		m_main.items[m_main.entriesCnt - 1].set = false;
		m_main.items[m_main.entriesCnt - 2].set = false;
	}

	for (int i = 0; i < 16; i++) {
		/* Previous button */
		if (i == 12) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnPrevious);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				ConfigPanel *pane = (ConfigPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_PREV_BTN));
				render->GetRenderWindow()->draw(rect);
				/*
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString("<");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/
			});
			m_keyboard.SetWidget(i, btn);
		}
		/* Next button */
		else if (i == 13) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnNext);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				ConfigPanel *pane = (ConfigPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_NEXT_BTN));
				render->GetRenderWindow()->draw(rect);
				/*
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString(">");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/
			});
			m_keyboard.SetWidget(i, btn);
		}
		/* Menu button */
		else if (i == 14) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnStart);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				ConfigPanel *pane = (ConfigPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(0, 0, 0, pane->m_alpha));
				//rect.setTexture(render->GetTexture(TEX_CONFIG_BTN));
				render->GetRenderWindow()->draw(rect);
				
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString("Back");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);
			});
			m_keyboard.SetWidget(i, btn);
		}
		/* Start button */
		else if (i == 15) 
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnStart);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				ConfigPanel *pane = (ConfigPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				//rect.setTexture(render->GetTexture(TEX_START_BTN));
				render->GetRenderWindow()->draw(rect);
				
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString("Ok");
				tx.setColor(sf::Color(0, 0, 0, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);
			});
			m_keyboard.SetWidget(i, btn);
		}
		else
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnSelect);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				ConfigPanel *pane = (ConfigPanel *)widget->GetUserData();
				int btnId = (widget->GetLinkedKey() - 1) / 4 + ((widget->GetLinkedKey() - 1) % 4) * 3 + pane->m_selectOffset;
				if (btnId < 0 || btnId >= pane->m_activeLayout->entriesCnt) {
					return ;
				}
				ConfigLayoutItem itm = pane->m_activeLayout->items[btnId];
				if (itm.set == false) {
					return ;
				}

				pos = widget->GetPosition();
				pos.x += pane->m_posXItm;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaItm));
				rect.setTexture(itm.tex);
				render->GetRenderWindow()->draw(rect);
				
				if (itm.hideLabel)
					return;
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString(itm.text);
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha - pane->m_alphaItm));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);
			});
			m_keyboard.SetWidget(i, btn);
		}
	}
}

ConfigPanel::~ConfigPanel()
{

}

void ConfigPanel::Init()
{
	int py = (int) (m_size.y - m_size.x);
	int sz = (int) m_size.x;

	m_goBack = false;
	m_selectAction = false;
	m_keyboard.SetPosition(0, (float) py);
	m_keyboard.SetSize((float) sz, (float) sz);
	m_keyboard.Init();
}

void ConfigPanel::Start()
{
	m_posX = 100;
	m_posXItm = 0;
	m_alpha = 0;
	m_alphaItm = 0;
	m_goBack = false;
	m_selectAction = false;

	tween::TweenerParam param(200, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 0);
	param.addProperty(&m_alpha, 255);
	param.forceEnd = true;
	m_gm->m_tweener.addTween(param);

	// Reset the state of all buttons
	for (int i = 0; i < 16; i++) {
		m_keyboard.GetWidget(i)->OnReleased();
	}
}

void ConfigPanel::Stop()
{
}

void ConfigPanel::Draw(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	sf::RectangleShape rect(sf::Vector2f(sw, sh * 0.7));
	sf::Text text;
	sf::FloatRect textRect;

	/* Panel title */
	text.setPosition(0, 0);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(sh * 0.15);
	text.setString("MAIN MENU");
	target->GetRenderWindow()->draw(text);

	rect.setPosition(0, sh * 0.15);
	rect.setFillColor(sf::Color(0, 0, 0, m_alpha * 0.4));
	target->GetRenderWindow()->draw(rect);

	// padding between content and padding inside black bg : 24px

	float internalPadding = 24 * ratio;
	m_activeLayout->Render(target, m_alpha, internalPadding, sh * 0.15 + internalPadding,
		sw - internalPadding * 2, sh * 0.7 - internalPadding * 2);
}

void ConfigPanel::Update(sf::Clock *time)
{
	GUIPanel::Update(time);
	if (m_goBack == true && (int)m_alpha == 0)
	{
		/* TODO: SWAP TO MATCHMAKING INSTEAD OF DIRECT PLAY */
		m_selectOffset = 0;
		switch (m_activeLayout->id)
		{
		case CFLAY_FILTER:
		case CFLAY_SORTING:
		case CFLAY_ACTIVATORS:
		case CFLAY_MODS:
		case CFLAY_ACHIEVEMENTS:
		case CFLAY_STATS:
		case CFLAY_DEBUG:
			m_activeLayout = &m_main;
			m_goBack = false;
			Start();
			break;
		default:
			m_goBack = false;
			m_gm->GetGUI()->HideAll();
			m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
			break;
		}

	}
	if (m_selectAction == true && (int)m_alpha == 0)
	{
		m_selectOffset = 0;

		switch (m_activeLayout->id)
		{
		case CFLAY_CONFIG:
			switch (m_selectActionIdx)
			{
			case 0:
				m_activeLayout = &m_filters;
				break;
			case 1:
				m_activeLayout = &m_sorting;
				break;
			case 2:
				m_activeLayout = &m_activators;
				break;
			case 3:
				m_activeLayout = &m_mods;
				break;
			case 4:
				m_activeLayout = &m_achievements;
				break;
			case 5:
				m_activeLayout = &m_stats;
				break;
			case 6:
				m_gm->m_gsm.ChangeState(GMST_GAMEMODE_SELECT);
				break;
			case 7:
				m_gm->m_gsm.ChangeState(GMST_SPLASH);
				break;
			case 8:
				m_activeLayout = &m_debug;
				break;
			default:
				break;
			}
			break;
		case CFLAY_FILTER:
			switch (m_selectActionIdx)
			{
			case 0:
				m_gm->m_songs.SetFilter(m_gm->m_songs.noFilter);
				m_gm->m_songs.RefreshFilteringSorting();
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
				m_gm->m_songs.difficultyFilterValue = m_selectActionIdx;
				m_gm->m_songs.SetFilter(m_gm->m_songs.difficultyFilter);
				m_gm->m_songs.RefreshFilteringSorting();
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Find(PANEL_MUSIC_SELECT)->Init();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			default:
				break;
			}
			break;
		case CFLAY_SORTING:
			switch (m_selectActionIdx)
			{
			case 0:
				m_gm->m_songs.sortAscending = !m_gm->m_songs.sortAscending;
				m_gm->m_songs.RefreshFilteringSorting();
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			case 1:
				m_gm->m_songs.SetSorter(m_gm->m_songs.difficultySorter);
				m_gm->m_songs.RefreshFilteringSorting();
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Find(PANEL_MUSIC_SELECT)->Init();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			case 2:
				m_gm->m_songs.SetSorter(m_gm->m_songs.nameSorter);
				m_gm->m_songs.RefreshFilteringSorting();
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Find(PANEL_MUSIC_SELECT)->Init();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			case 3:
				m_gm->m_songs.SetSorter(m_gm->m_songs.authorSorter);
				m_gm->m_songs.RefreshFilteringSorting();
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Find(PANEL_MUSIC_SELECT)->Init();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			default:
				break;
			}
			break;
		case CFLAY_DEBUG:
			switch (m_selectActionIdx)
			{
			case 0:
				m_params.Reload();
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			case 1:
				m_gm->m_songs.Clear();
				m_gm->m_songs.ParseAll(CBEATMAPSDIR);
				((MusicSelectPanel *)m_gm->GetGUI()->Find(PANEL_MUSIC_SELECT))->m_selectedBeatmap = NULL;
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			case 2:
				m_gm->m_songs.Clear();
				m_gm->m_songs.ParseAll(BEATMAPSDIR);
				((MusicSelectPanel *)m_gm->GetGUI()->Find(PANEL_MUSIC_SELECT))->m_selectedBeatmap = NULL;
				m_gm->GetGUI()->HideAll();
				m_gm->GetGUI()->Show(PANEL_MUSIC_SELECT);
				break;
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			default:
				break;
			}
			break;
		case CFLAY_ACTIVATORS:
			m_gm->GetRenderer()->SetActivatorTexture(
				m_gm->GetRenderer()->GetActivatorTexture(m_selectActionIdx)
			);
			m_activeLayout = &m_main;
			m_goBack = false;
			Start();
			break;
		default:
			break;
		}
		Start();
		m_selectAction = false;
	}
}

void ConfigPanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	m_keyboard.ReceiveEvent(ev, param);
	GUIPanel::ReceiveEvent(ev, param);
}

void ConfigPanel::OnSelect(GUIWidget *btn)
{
	if (m_goBack || m_selectAction)
		return ;

	int btnId = (btn->GetLinkedKey() - 1) / 4 + ((btn->GetLinkedKey() - 1) % 4) * 3 + m_selectOffset;
	m_selectActionIdx = btnId;
	if (m_selectActionIdx < 0 || m_selectActionIdx >= MAXLAYOUT_ITEMS || 
		m_activeLayout->items[m_selectActionIdx].set == false)
		return;

	m_selectAction = true;
	m_alpha = 255;

	tween::TweenerParam param(200, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_alpha, 0);
	m_gm->m_tweener.addTween(param);
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));
}

void ConfigPanel::OnPrevious(GUIWidget *btn)
{
	if (m_goBack || m_selectAction)
		return ;

	m_selectOffset -= 3;
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));

	m_posXItm = -m_gm->m_params.m_params.btn_size;
	m_alphaItm = 255;

	tween::TweenerParam param(400, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posXItm, 0);
	param.addProperty(&m_alphaItm, 0);
	m_gm->m_tweener.addTween(param);
}

void ConfigPanel::OnNext(GUIWidget *btn)
{
	if (m_goBack || m_selectAction)
		return ;
	
	m_selectOffset += 3;
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));

	m_posXItm = m_gm->m_params.m_params.btn_size;
	m_alphaItm = 255;

	tween::TweenerParam param(400, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posXItm, 0);
	param.addProperty(&m_alphaItm, 0);
	m_gm->m_tweener.addTween(param);
}

void ConfigPanel::OnStart(GUIWidget *btn)
{
	if (m_goBack || m_selectAction)
		return ;

	m_goBack = true;
	m_alpha = 255;

	tween::TweenerParam param(200, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_alpha, 0);
	m_gm->m_tweener.addTween(param);
	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));
}