#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrgui.h>

#include "GUIPanel.h"
#include "GUIBeatGraph.h"

class JRExe;
class RenderingEngine;

enum STARTSTATE
{
	STARTSTATE_WAIT,
	STARTSTATE_READY,
	STARTSTATE_GO,
	STARTSTATE_PLAYING
};

class PlayPanel : public GUIPanel 
{
private:
	GUIKeyboard m_keyboard;
	GUIKeyboard m_keyboardTop;
	GUIBeatGraph m_graph;
	int m_step;
	JRExe *m_gm;
	STARTSTATE m_startState;

	// Position tweens
	float m_posX;
	float m_alpha;
	float m_headerAlpha;
	float m_headerX;

	// Scoring tweens
	float m_targetScore;
	float m_displayScore;
	float m_displayCombo;
	float m_comboAnim;

	void RenderHeader(RenderingEngine *target);

public:
	PlayPanel(JRExe *gm);
	~PlayPanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};