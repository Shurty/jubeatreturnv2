/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <JREngine/jrshared.h>

class CommandHello : public AHello
{
//private:
//	JRExe* m_jre;

public:
	CommandHello()
		: AHello()
	{

	}

	CommandHello::~CommandHello()
	{

	}

	virtual void Receive(Client *cl, std::string const &hashcode, bool isapi)
	{
		cl->SetValid();
		Logger::Log << "CommandHello: Recieved validity confirmation from master" << std::endl;
	}

	virtual void Send(Client *cl, std::string const &hashcode, bool isapi)
	{
		sf::Packet pck;

		pck << GetID() << hashcode;
		Logger::Log << "CommandHello: Sending identity informations to master" << std::endl;
		cl->GetSock().send(pck);
	}
};
