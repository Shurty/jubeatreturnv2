
#include <JREngine/jrshared.h>
#include "StateCreateParty.h"

StateCreateParty::StateCreateParty(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_CREATE_PARTY;
	m_name = "Create party";
}

void StateCreateParty::OnInit(Parameters &pm)
{
	Logger::Log << "StateCreateParty: OnInit" << std::endl;

}

void StateCreateParty::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateCreateParty::OnRender(Parameters &pm)
{
}

void StateCreateParty::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateCreateParty: update" << std::endl;
}

void StateCreateParty::OnEnd(Parameters &pm)
{
}

void StateCreateParty::OnUnload(Parameters &pm)
{
}
