#pragma once

#include <JREngine/jrgui.h>
#include "JRExe.h"
//#include "Game.h"

#define JICON_CNT_DELAY		5

struct Beatmap;
class RenderingEngine;
class PlayPanel;
class JRExe;

struct JIcon
{
	float x;
	float y;
	float w;
	float h;
	bool visible;
};

class GUIJubeatIcon : public GUIWidget
{
private:
	JRExe *m_core;
	JIcon m_squares[4];
	
	int m_cntdown;
	int m_visoffset;
	float m_pad;

public:
	GUIJubeatIcon(JRExe *core)
		: GUIWidget(core->m_params), m_core(core)
	{
		m_pad = 2;
		m_cntdown = JICON_CNT_DELAY;
		m_visoffset = 2;
	}
	virtual ~GUIJubeatIcon()
	{

	}

	void ResetPositions()
	{
		m_squares[2].x = 0;
		m_squares[2].y = 0;
		m_squares[3].x = m_squares[2].x + m_squares[2].w + m_pad;
		m_squares[3].y = 0;
		m_squares[0].x = m_squares[2].x + m_squares[2].w + m_pad;
		m_squares[0].y = m_squares[2].y + m_squares[2].h + m_pad;
		m_squares[1].x = 0;
		m_squares[1].y = m_squares[2].y + m_squares[2].h + m_pad;
	}

	virtual void Init()
	{
		for (int i = 0; i < 4; i++) {
			m_squares[i].w = m_size.x / 2 + m_pad / 2;
			m_squares[i].h = m_size.y / 2 + m_pad / 2;
			m_squares[i].visible = true;
		}

		ResetPositions();

		m_squares[3].visible = false;
		m_visoffset = 2;
	}
	virtual void Draw(RenderingEngine *target)
	{
		for (int i = 0; i < 4; i++)
		{
			if (m_squares[i].visible)
			{
				sf::RectangleShape rect(sf::Vector2f(m_squares[i].w, m_squares[i].h));

				rect.setFillColor(sf::Color(200, 200, 200, 255));
				if (i == 0)
					rect.setFillColor(sf::Color(255, 255, 255, 255));
				if (i == 1)
					rect.setFillColor(sf::Color(150, 150, 150, 255));
				if (i == 2)
					rect.setFillColor(sf::Color(220, 220, 220, 255));
				rect.setPosition(m_squares[i].x + m_position.x, m_squares[i].y + m_position.y);
				target->GetRenderWindow()->draw(rect);
			}
		}
	}
	virtual void Update(sf::Clock *time)
	{
		GUIWidget::Update(time);
		int tgtOffset = (m_visoffset + 1) % 4;

		m_cntdown--;

		if (m_cntdown == 0)
		{
			m_squares[tgtOffset].visible = true;
			m_visoffset = (m_visoffset + 3) % 4;
			tgtOffset = (m_visoffset + 1) % 4;
			m_squares[tgtOffset].visible = false;
			m_cntdown = JICON_CNT_DELAY;

			ResetPositions();

			tween::TweenerParam param(JICON_CNT_DELAY * 15, tween::CUBIC, tween::EASE_OUT);
			param.addProperty(&(m_squares[m_visoffset].x), m_squares[tgtOffset].x);
			param.addProperty(&(m_squares[m_visoffset].y), m_squares[tgtOffset].y);
			m_core->m_tweener.addTween(param);
		}
	}
	virtual void OnTriggered() { }
	virtual void OnPressed() { }
	virtual void ReceiveEvent(GUIEVENT ev, void *param)
	{
		GUIWidget::ReceiveEvent(ev, param);
	}
};