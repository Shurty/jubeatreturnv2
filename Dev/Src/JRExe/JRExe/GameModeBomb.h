#pragma once

#include <sstream>
#include <map>
#include "JRExe.h"

#include "RewardsManager.h"
#include "BombScoreManager.h"

class GamemodeBomb :
	public IGamemode
{
private:
	JRExe*							m_core;
	bool							m_isActive;
	bool							m_isGameRunning;
	bool							m_isGameEnding;
	BombScoreManager				m_scoreManager;
	RewardsManager					m_rewardsManager;
	int								m_gameEndTimestamp;
	std::map<int, bool>				m_activatedBomb;
	bool							isGameRunning();
	std::deque<Beat>::iterator		m_beatit;
	float							m_pressStartOffset;

	void saveScore();
	void loadScore();

	sf::Texture						*m_selectLogo;
	sf::Texture						*m_bombTexture;

public:
	GamemodeBomb(JRExe *);
	~GamemodeBomb(void);

	virtual GMMODE_ID GetId();

	virtual void OnPlayKeyboardButtonInit(GUIKeyboard *kb, GUIWidget *wdg);
	virtual void OnDebriefingTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2);
	virtual void OnPlayTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2);

	virtual void Init();
	virtual void Update();
	virtual void Render();
	virtual void OnSetActive();
	virtual void OnUnload();
	virtual bool IsActive() const;
	virtual ScoreManager& GetScoreManager();

	virtual void OnIconRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2);
	virtual void OnSelectedTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2);
	virtual void OnBeatAction(BEATRESULT res, GUIWidget *widget);
	virtual void OnGamemodeSelected();
	virtual void OnGamemodeConfirmed();
	virtual void OnGameSet(Beatmap *res);
	virtual void OnMusicSelect(Beatmap *);
	virtual void OnGameStart();
	virtual void OnGameEnd();
	virtual void OnDebriefingExit();

	virtual unsigned int GetCreditPrice();
};

