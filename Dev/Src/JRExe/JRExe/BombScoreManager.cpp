#include "BombScoreManager.h"


BombScoreManager::BombScoreManager(void)
	: ScoreManager()
{
}


BombScoreManager::~BombScoreManager(void)
{
}

void BombScoreManager::scoreBeat(BEATRESULT res, bool isBomb)
{
	if (isBomb == false) {
		m_currentScore += m_beatValue * m_scoreForBeat[res];
		m_beatResults[res]++;

		m_bonusNotesCounter++;
		if (res == BEATRESULT::BR_MISS || res == BEATRESULT::BR_BAD) {
			m_bonusCounter -= 8.0 / (double)m_numberOfNotes;
			m_bonusNotesCounter = 0.0;
		} else if (res == BEATRESULT::BR_PERFECT || res == BEATRESULT::BR_GREAT) {
			m_bonusCounter += 2.0 / (double)m_numberOfNotes;
		} else if (res == BEATRESULT::BR_GOOD) {
			m_bonusCounter += 1.0 / (double)m_numberOfNotes;
		}
	} else {
		m_bonusNotesCounter++;
		if (res == BEATRESULT::BR_MISS) {
			m_currentScore += m_beatValue;
			m_bonusCounter += 1.0 / (double)m_numberOfNotes;
			m_beatResults[BR_PERFECT]++;
		}
		else if (res == BEATRESULT::BR_BAD) {
			m_currentScore -= m_beatValue * 0.5;
			m_bonusCounter -= 4.0 / (double)m_numberOfNotes;
			m_bonusNotesCounter = 0.0;
			m_beatResults[BR_BAD]++;
		}
		else {
			m_currentScore -= m_beatValue * 2;
			m_bonusCounter -= 16.0 / (double)m_numberOfNotes;
			m_bonusNotesCounter = 0.0;
			m_beatResults[BR_BAD]++;
		}
	}
	if (m_currentScore < 0)
		m_currentScore = 0;
	if (m_bonusCounter > 1.0)
		m_bonusCounter = 1.0;
	else if (m_bonusCounter < 0.0)
		m_bonusCounter = 0.0;
	//unsigned int nbBeat = 0;
	//for (int i = 0; i < BEATRESULT::BR_MAX_RESULTS; ++i)
	//{
	//	nbBeat += m_beatResults[i];
	//}
	// Logger::Log << "beats: " << nbBeat << "/" << m_numberOfNotes << std::endl;

	// Logger::Log << "Max beat value: " << m_beatValue << " || Bonus score: " << 100000.0 * m_bonusCounter << " || Score: " << m_currentScore << std::endl;
	m_numberOfBeatsDone++;

	m_calculatedBonusScore = floor(100000.0 * m_bonusCounter + 0.5);
	int theoryBonusScore = computePotentialBonusScore();
	m_calculatedFinalScore = floor(((m_currentScore / (double)m_numberOfBeatsDone) * (double)m_numberOfNotes) + theoryBonusScore + 0.5);
	m_calculatedFinalGrade = getGradeForFinalScore(m_calculatedFinalScore);
	//if (m_currentScore == 899999)
	//	m_currentScore = 900000;
	// m_calculatedFinalScore = theoryBonusScore + m_currentScore;

	Logger::Log << "Combo: " << m_bonusNotesCounter << " Bonus: " << m_bonusCounter << " Score: " << m_currentScore << std::endl;
}