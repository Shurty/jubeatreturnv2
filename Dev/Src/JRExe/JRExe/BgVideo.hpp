
#pragma once

#include <sfeMovie/Movie.hpp>

class BgVideo : public sfe::Movie
{
private:

public:
	BgVideo()
	{

	}

	~BgVideo()
	{

	}

	void LoadFromFile(std::string const &file)
	{
		openFromFile(file);
	}
};