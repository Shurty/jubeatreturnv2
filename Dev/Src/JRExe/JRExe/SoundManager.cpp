
#include <JREngine/jrgui.h>
#include "SoundManager.h"
#include "JRExe.h"

SoundManager::SoundManager(Parameters &p)
{
	allocatedBuffs = 0;
	previewMusicSoundMax = FADEDLVL;
	playPreview = false;
	musicFading = 100;
	previewMusicEndOffset = 0;
	previewMusicStartOffset = 0;
	previewMusicFading = 0;
	Logger::Log << "Module construct: SoundManager OK" << std::endl;
}

SoundManager::~SoundManager()
{

}

void SoundManager::Init(JRExe *core)
{
	m_core = core;
	playPreview = false;
	musicFading = 100;
	Logger::Log << "Module init: SoundManager OK" << std::endl;
}

void SoundManager::Update()
{
	if (playPreview)
	{
		previewMusic.setVolume(previewMusicFading);
		if (previewMusicFadingState == FADE_NOPE)
		{
			if (previewMusic.getPlayingOffset().asSeconds() >= previewMusicEndOffset)
			{
				previewMusicFadingState = FADE_OUT;
				previewMusicFading = previewMusicSoundMax;
				tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
				param.addProperty(&previewMusicFading, 0);
				m_core->m_tweener.addTween(param);
			}
		}
		else
		{
			if ((int)previewMusic.getVolume() <= 1 && previewMusicFadingState == FADE_OUT)
			{
				previewMusicFadingState = FADE_NOPE;
				ReplayPreviewMusic();
			}
			else if ((int)previewMusic.getVolume() >= previewMusicSoundMax - 1 && previewMusicFadingState == FADE_IN)
			{
				previewMusicFadingState = FADE_NOPE;
			}
		}
	}
	if (music.getStatus() == sf::Music::Playing && music.getVolume() <= 1)
	{
		music.stop();
	}
	music.setVolume(musicFading);
}

void SoundManager::AssignAndPlay(unsigned int bid)
{
	for (int i = 0; i < MAXSNDHANDLES; i++)
	{
		if (sounds[i].getStatus() == sf::Sound::Stopped)
		{
			sounds[i].setBuffer(buffers[bid].buff);
			sounds[i].setLoop(false);
			sounds[i].play();
			return ;
		}
	}
}

void SoundManager::PlaySnd(std::string const &path)
{
	int i = 0;

	for (i = 0; i < allocatedBuffs; i++)
	{
		if (buffers[i].path == path)
		{
			AssignAndPlay(i);
			return ;
		}
	}
	if (i > MAXSNDHANDLES)
	{
		Logger::Log << "Max sound buffers handles reached. Cannot allocate for " << path << std::endl;
		return ;
	}
	buffers[i].path = path;
	buffers[i].buff.loadFromFile(path);
	AssignAndPlay(i);
	allocatedBuffs++;
}

void SoundManager::StopPreviewMusic() { playPreview = false; previewMusic.stop(); }
sf::Music& SoundManager::GetMusic() { return (music); }

void SoundManager::FadeOutMusic() 
{ 
	musicFading = 70;
	tween::TweenerParam param(4000, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&musicFading, 0);
	param.forceEnd = true;
	m_core->m_tweener.addTween(param);
}

void SoundManager::ReplayPreviewMusic()
{
	previewMusic.setLoop(false);
	previewMusic.setPlayingOffset(sf::seconds(previewMusicStartOffset));
	previewMusic.play();
	playPreview = true;
	previewMusicFadingState = FADE_IN;
	previewMusicFading = 0;
	previewMusic.setVolume(previewMusicFading);

	previewMusicFading = 0;
	tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&previewMusicFading, previewMusicSoundMax);
	m_core->m_tweener.addTween(param);
}

void SoundManager::SetPreviewMusicMaxSound(float sound)
{
	previewMusicSoundMax = sound;
}

void SoundManager::PlayPreviewMusic(std::string const &path, int offset, int endOffset)
{
	previewMusic.openFromFile(path);
	previewMusic.setVolume(40);
	previewMusicStartOffset = offset;
	previewMusicEndOffset = endOffset;
	ReplayPreviewMusic();
}

void SoundManager::PlayMusic(std::string const &path)
{
	musicFading = 100;
	music.openFromFile(path);
	music.setVolume(musicFading);
	music.play();
}