#pragma once

#include <JREngine/jrgui.h>
//#include "Game.h"

struct Beatmap;
class RenderingEngine;
class PlayPanel;
class JRExe;

class GUIBeatGraph : public GUIWidget
{
private:
	Beatmap *m_bmp;
	JRExe *m_core;

	/* graph specific */
	float m_maxY;
	float m_maxX;
	bool m_playmode;

public:
	GUIBeatGraph(JRExe *core);
	virtual ~GUIBeatGraph();

	//void LinkToPlayPanel(PlayPanel*, void(PlayPanel::*clb)(float x, float y, float size));
	
	void SetMaxX(float i);
	void SetMaxY(float i);
	void MarkAsPlaying(bool mk);

	virtual void Init();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void OnTriggered();
	virtual void OnPressed();
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};