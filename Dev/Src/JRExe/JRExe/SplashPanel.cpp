#pragma once

#include <iostream>
#include <JREngine/jrgui.h>
#include "SplashPanel.h"
#include "GUIPanel.h"
#include "GUIBeatButton.h"
#include "JRExe.h"

SplashPanel::SplashPanel(JRExe *gm) 
	: GUIPanel(PANEL_SPLASH, gm), m_keyboard(*gm->GetParams()), m_jicon(gm), m_gm(gm)
{
	m_step = 10;
	m_started = false;

	m_listFilter = [](const SongLibrary&me, Beatmap*bt) -> bool {
		if (bt->songLevel == SL_INTERMEDIATE)
			return true;
		return false;
	};

	for (int i = 0; i < 16; i++) {
		if (i == 15) {
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnStart);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;

				SplashPanel *pane = (SplashPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.x += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_START_BTN));
				render->GetRenderWindow()->draw(rect);
			});
			m_keyboard.SetWidget(i, btn);
		}
		else {
			GUIBeatButton *btn = new GUIBeatButton(gm);
			m_keyboard.SetWidget(i, btn);
		}
	}
}

SplashPanel::~SplashPanel()
{

}

void SplashPanel::Init()
{
	int py = (int) (m_size.y - m_size.x);
	int sz = (int) m_size.x;

	m_keyboard.SetPosition(0, (float) py);
	m_keyboard.SetSize((float) sz, (float) sz);
	m_keyboard.Init();
	m_bgMusic.openFromFile("Data/Musics/Menu/DoBeDoBeDo_Networking.ogg");
	m_bgMusic.setLoop(true);
	m_jicon.SetPosition(50, 50);
	m_jicon.SetSize(60, 60);
	m_jicon.Init();
}

void SplashPanel::Start()
{
	//m_bgMusic.play();

	m_posX = 100;
	m_alpha = 0;
	m_started = false;

	tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 0);
	param.addProperty(&m_alpha, 255);
	m_gm->m_tweener.addTween(param);

	for (int i = 0; i < 16; i++) {
		m_keyboard.GetWidget(i)->OnReleased();
	}

	m_previewState = OFF;
	m_activeMusicIdx = -1;
	m_activeBeatmap = NULL;
	m_songPlayTime.restart();
	m_gm->m_songs.SetFilter(m_listFilter);
	m_gm->m_sounds.SetPreviewMusicMaxSound(40);
	m_gm->m_songs.RefreshFilteringSorting();
}

void SplashPanel::Stop()
{
	m_gm->m_sounds.SetPreviewMusicMaxSound(FADEDLVL);
	m_bgMusic.stop();
}

void SplashPanel::Draw(RenderingEngine *target)
{
	//m_jicon.Draw(target);
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	sf::RectangleShape rect(sf::Vector2f(611 * ratio, 324 * ratio));

	rect.setPosition(sw / 2 - rect.getSize().x / 2 - m_posX, 
		sh / 2 - rect.getSize().y / 2);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	rect.setTexture(target->GetTexture(TEX_LOGO_TITLE));
	target->GetRenderWindow()->draw(rect);

	if (m_activeBeatmap)
	{
		rect.setSize(sf::Vector2f(200.0 * ratio, 100.0 * ratio));
		rect.setPosition(sw - rect.getSize().x - 5 * ratio,
			sh - rect.getSize().y - 5 *ratio);
		rect.setFillColor(sf::Color(0, 0, 0, m_alpha * 0.4));
		rect.setTexture(NULL);
		target->GetRenderWindow()->draw(rect);

		sf::Text text;
		text.setString("NOW PLAYING");
		text.setFont(target->GetFont(FONT_BUTTON_TX));
		text.setCharacterSize(30 * ratio);
		text.setColor(sf::Color(255, 255, 255, m_alpha));
		text.setPosition(sw - 200.0 * ratio, sh - rect.getSize().y);
		target->GetRenderWindow()->draw(text);

		text.setString(m_activeBeatmap->name);
		text.setFont(target->GetFont(FONT_BUTTON_TX));
		text.setCharacterSize(30 * ratio);
		text.setColor(sf::Color(255, 255, 255, m_alpha));
		text.setPosition(sw - 200.0 * ratio, sh - rect.getSize().y + 35 * ratio);
		target->GetRenderWindow()->draw(text);
	}
	return;
}

void SplashPanel::Update(sf::Clock *time)
{
	float previewDt = m_songPlayTime.getElapsedTime().asSeconds();

	GUIPanel::Update(time);

	if (m_activeBeatmap && m_previewState == PLAY)
	{
		int ptime = (previewDt * 1000);
		float pressTime = m_gm->m_params.m_params.press_range / 2;

		while (!m_activeBeatmap->beatsList.empty() &&
			m_activeBeatmap->beatsList.front().time <= ptime + m_gm->m_params.m_params.press_offset)
		{
			ReceiveEvent(TRIGGERED, (void*)m_activeBeatmap->beatsList.front().activate);
			ReceiveEvent(TRIGGERBEAT, (void*)(&m_activeBeatmap->beatsList.front()));
			m_activeBeatmap->beatsList.pop_front();
		}
	}

	if (m_activeMusicIdx == -1 || previewDt > 44) {
		if (m_previewState == PLAY)
		{
			//m_gm->m_sounds.StopPreviewMusic();
			m_previewState = OFF;
		}
		else if (m_activeMusicIdx == -1 || previewDt > 44.5) {
			if (m_activeBeatmap)
				delete m_activeBeatmap;
			m_activeMusicIdx++;
			Beatmap *bt = m_gm->m_songs.GetBeatmapCopy(m_activeMusicIdx);
			if (bt == NULL) {
				m_activeMusicIdx = 0;
				bt = m_gm->m_songs.GetBeatmapCopy(m_activeMusicIdx);
			}
			if (bt == NULL)
				return;
			m_activeBeatmap = bt;
			if (m_gm->m_params.m_params.wav_mode) {
				m_gm->m_sounds.PlayPreviewMusic(MUSICDIR + bt->src + MUSICEXTWAV, 0, 44);
			}
			else {
				m_gm->m_sounds.PlayPreviewMusic(MUSICDIR + bt->src + MUSICEXT, 0, 44);
			}
			m_songPlayTime.restart();
			m_gm->m_params.m_params.press_range = m_gm->m_params.m_params.press_durations[m_activeBeatmap->level];
			m_previewState = PLAY;
		}
	}

	


	if (m_started == true && (int)m_alpha == 0)
	{
		if (m_gm->m_network.IsConnected()) {
			m_gm->m_gsm.ChangeState(GMST_REGISTER);
		}
		else {
			Player* p = m_gm->m_playersManager.connectPlayer(m_gm->m_params.m_params.offline_player_name);
			m_gm->m_gsm.ChangeState(GMST_BETAMSG);
		}
		m_gm->m_songs.SetFilter(m_gm->m_songs.noFilter);
		m_gm->m_songs.RefreshFilteringSorting();
		m_activeBeatmap = NULL;
		m_gm->m_sounds.StopPreviewMusic();
		m_previewState = OFF;
		if (m_activeBeatmap)
			delete m_activeBeatmap;
		m_started = false;
	}
	m_jicon.Update(time);
}

void SplashPanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	m_keyboard.ReceiveEvent(ev, param);
	GUIPanel::ReceiveEvent(ev, param);
}

void SplashPanel::OnStart(GUIWidget *btn)
{
	if (m_started == false)
	{
		m_started = true;
		m_alpha = 255;

		tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_alpha, 0);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);
		m_bgMusic.stop();
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));

		//Player* p = m_gm->m_playersManager.connectPlayer("default");
	}
}