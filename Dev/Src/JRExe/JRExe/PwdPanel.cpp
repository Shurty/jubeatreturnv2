
#include <iostream>
#include <JREngine/jrgui.h>
#include "PwdPanel.h"
#include "GUIPanel.h"
#include "JRExe.h"

char PwdPanel::m_inputChars[10][1] = {
	{ '1' },
	{ '2' },
	{ '3' },
	{ '4' },
	{ '5' },
	{ '6' },
	{ '7' },
	{ '8' },
	{ '9' },
	{ '0' },
};

PwdPanel::PwdPanel(JRExe *gm)
: InputPanel(gm), m_keyboard(*gm->GetParams()), m_gm(gm)
{
	m_started = false;
	m_maxChars = 10;
	m_title = "Enter PIN";

	for (int i = 0; i < 16; i++) {
		if ((i % 4 < 3 && i < 14) && i != 12)
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnSelect);
			btn->LinkUserData(this);
			btn->SetRender([](GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				int key = widget->GetLinkedKey() - 1;
				int tid = 1 + (TEX_KEYPAD_0 + key % 4 + 3 * (key / 4));
				if (key == 12) {
					tid = TEX_KEYPAD_0;
				}
				PwdPanel *pane = (PwdPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture((TEXID)tid));
				render->GetRenderWindow()->draw(rect);
				/*
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString("Play");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/
			});
			m_keyboard.SetWidget(i, btn);
		}
		else if (i == 11)
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnBack);
			btn->LinkUserData(this);
			btn->SetRender([](GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				int key = widget->GetLinkedKey() - 1;

				PwdPanel *pane = (PwdPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_WORDPAD_RET));
				render->GetRenderWindow()->draw(rect);
				/*
				tx.setFont(render->GetFont(FONT_BUTTON_TX));
				tx.setString("Play");
				tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(14);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y - sz.height / 2 + size.y / 2);
				render->GetRenderWindow()->draw(tx);*/
			});
			m_keyboard.SetWidget(i, btn);
		}
		else if (i == 15)
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnStart);
			btn->LinkUserData(this);
			btn->SetRender([](GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				float ratio = render->GetRenderingRatio();

				PwdPanel *pane = (PwdPanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.y += pane->m_posX;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alpha));
				rect.setTexture(render->GetTexture(TEX_START_NOLOGO_BTN));
				render->GetRenderWindow()->draw(rect);

				/* Time calculation for the start timer */
				float time = pane->m_startMaxTime - pane->m_startClock.getElapsedTime().asSeconds();
				if (time < 0)
					time = 0;
				std::string timeStr = std::to_string(time);
				std::string resStr = timeStr;
				if (time < 10)
					resStr = "0" + resStr;
				if (time == 0)
					resStr = "00:00";
				resStr = resStr.substr(0, 5);

				tx.setFont(render->GetFont(FONT_FIXED_SIZE_TX));
				tx.setString(resStr);
				if (time < 10)
					tx.setColor(sf::Color(255, 0, 0, pane->m_alpha));
				else
					tx.setColor(sf::Color(255, 255, 255, pane->m_alpha));
				tx.setCharacterSize(50 * ratio);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y + size.y * 0.15);
				render->GetRenderWindow()->draw(tx);
			});
			m_keyboard.SetWidget(i, btn);
		}
		else
		{
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkUserData(this);
			/*btn->SetRender([] (GUIWidget *wdg, RenderingEngine *res) {
			PlayPanel *pane = (PlayPanel *)wdg->GetUserData();
			});*/
			m_keyboard.SetWidget(i, btn);
		}
	}
}

PwdPanel::~PwdPanel()
{

}

void PwdPanel::Init()
{
	int py = (int)(m_size.y - m_size.x);
	int sz = (int)m_size.x;

	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	float ratio = m_gm->m_render.GetRenderingRatio();
	float internalPadding = 21 * ratio;
	float posY = 100 * ratio + internalPadding * 2;
	float svh = sh - posY * 2;

	m_result = "";

	m_keyboard.SetPosition(0, (float)py);
	m_keyboard.SetSize((float)sz, (float)sz);
	m_keyboard.Init();
}

void PwdPanel::Start()
{
	m_posX = 300;
	m_alpha = 0;
	m_started = false;
	m_timer = 0;
	m_activeIdx = -1;
	m_activeOffset = 0;

	tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param.addProperty(&m_posX, 0);
	param.forceEnd = true;
	m_gm->m_tweener.addTween(param);

	tween::TweenerParam param2(1300, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param2.addProperty(&m_alpha, 255);
	param2.forceEnd = true;
	m_gm->m_tweener.addTween(param2);

	//m_gm->m_sounds.m_bgMusic.openFromFile(MUSICDIR + std::string("Menu/DoBeDoBeDo_Info.ogg"));
	//m_gm->m_sounds.m_bgMusic.setVolume(40);
	//m_gm->m_sounds.m_bgMusic.play();
	//m_gm->m_sounds.m_bgMusic.setLoop(true);

	m_result = "";
	// Reset the state of all buttons
	for (int i = 0; i < 16; i++) {
		m_keyboard.GetWidget(i)->OnReleased();
	}
	m_pressedOnce = false;
	m_startMaxTime = 20.00;
	m_startTickSec = 10.00;
	m_startClock.restart();
}

void PwdPanel::Stop()
{
}

void PwdPanel::RenderHeader(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;
	float internalPadding = 21 * ratio;

	sf::RectangleShape rect(sf::Vector2f(sw, sh * 0.7));
	sf::Text text;
	sf::FloatRect textRect;

	rect.setFillColor(sf::Color(0, 0, 0, m_alpha * 0.4));
	rect.setPosition(0, sh * 0.15);
	target->GetRenderWindow()->draw(rect);

	text.setPosition(internalPadding, sh * 0.15 + internalPadding);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setCharacterSize(70 * ratio);
	text.setString(m_title);
	text.setColor(sf::Color(255, 255, 255, m_alpha));
	text.setOrigin(0, 0);
	target->GetRenderWindow()->draw(text);

	float totalSize = 90 * ratio * m_maxChars;
	float origPos = sw / 2 - totalSize / 2;

	for (int i = 0; i < m_maxChars; i++)
	{
		rect.setFillColor(sf::Color(255, 255, 255, m_alpha * 0.7));
		rect.setTexture(target->GetTexture(TEX_INPUT_PAD));
		rect.setSize(sf::Vector2f(90 * ratio, 150 * ratio));
		rect.setPosition(origPos + i * 90 * ratio, sh * 0.7 / 2);
		target->GetRenderWindow()->draw(rect);

		if (m_activeIdx != -1 && m_result.length() - 1 == i)
		{
			rect.setTexture(NULL);
			rect.setFillColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			rect.setSize(sf::Vector2f(90 * ratio, 150 * ratio));
			rect.setPosition(origPos + i * 90 * ratio, sh * 0.7 / 2);
			target->GetRenderWindow()->draw(rect);
		}//nyx!
		if (i < m_result.length())
		{
			text.setPosition(origPos + i * 90 * ratio + 45 * ratio, sh * 0.7 / 2);
			text.setColor(sf::Color(255, 255, 255, m_alpha));
			text.setFont(target->GetFont(FONT_TITLE_TX));
			text.setCharacterSize(120 * ratio);
			text.setString(m_result.at(i));
			textRect = text.getLocalBounds();
			text.setOrigin(textRect.left + textRect.width / 2, 0);
			target->GetRenderWindow()->draw(text);
		}
	}
}

void PwdPanel::Draw(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();

	RenderHeader(target);

	return;
}

void PwdPanel::Update(sf::Clock *time)
{
	GUIPanel::Update(time);
	if (m_started == false && m_startTickSec < m_startClock.getElapsedTime().asSeconds())
	{
		m_startTickSec += 1;
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("tim_tum.wav"));
	}
	if (m_started == false && m_startClock.getElapsedTime().asSeconds() > m_startMaxTime)
	{
		OnStart(NULL);
	}
	if (m_started == true && (int)m_alpha == 0)
	{
		m_gm->m_gsm.ChangeState(GMST_GAMEMODE_SELECT);
		m_started = false;
	}
	if (m_timer > 0)
	{
		m_timer--;
		if (m_timer == 0)
		{
			m_activeIdx = -1;
		}
	}
}

void PwdPanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIPanel::ReceiveEvent(ev, param);
	m_keyboard.ReceiveEvent(ev, param);
	if (ev == PRESSED && m_pressedOnce == false)
	{
		m_pressedOnce = true;
		m_startMaxTime = 60.00;
		m_startTickSec = 50.00;
		m_startClock.restart();
	}
}

void PwdPanel::OnBack(GUIWidget *widget)
{
	if (m_result.length() > 0)
	{
		m_activeIdx = -1;
		m_activeOffset = 0;
		m_result.erase(--m_result.end());
	}
}

void PwdPanel::OnSelect(GUIWidget *widget)
{
	int idx = (widget->GetLinkedKey() - 1) % 4 + ((widget->GetLinkedKey() - 1) / 4) * 3;
	char res = ' ';

	if (m_activeIdx != idx)
	{
		if (m_result.length() == m_maxChars)
			return;
		m_activeIdx = idx;
		m_activeOffset = 0;
		res = m_inputChars[m_activeIdx][m_activeOffset];
		m_result += res;
		m_timer = 120;
	}
	else
	{
		m_activeOffset++;
		if (m_activeOffset >= 3)
			m_activeOffset = 0;
		res = m_inputChars[m_activeIdx][m_activeOffset];
		m_result[m_result.length() - 1] = res;
		m_timer = 120;
	}
}

void PwdPanel::OnStart(GUIWidget *widget)
{
	if (m_started == false)
	{
		m_started = true;
		m_alpha = 255;

		tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_alpha, 0);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));

		Player* p = m_gm->m_playersManager.connectPlayer(m_result);
	}
}