
#include <JREngine/jrgui.h>
#include "JRExe.h"
#include "NetworkController.h"
#include "Ping.hpp"
#include "Matchmaking.hpp"
#include "Hello.hpp"

NetworkController::NetworkController(JRExe *jre)
: m_ip("127.0.0.1"), m_port(52000), m_jre(jre)
{
	m_commands.push_back(new CommandPing());
	m_commands.push_back(new CommandMatchmaking());
	m_commands.push_back(new CommandHello());
	//Connect();
	Logger::Log << "Module construct: NetworkController OK" << std::endl;
}

NetworkController::~NetworkController()
{

}

Command* NetworkController::GetCommand(std::string const &id)
{
	auto it = std::find_if(m_commands.begin(), m_commands.end(), [&id](Command* cmd) {
		if (cmd->GetID() == id) {
			return (true);
		}
		return (false);
	});
	if (it != m_commands.end()) {
		return (*it);
	}
	return (NULL);
}

bool NetworkController::Connect()
{
	// Reset the socket as locking 
	m_sock->setBlocking(true);
	sf::Socket::Status status = m_sock->connect(m_ip, m_port, sf::milliseconds(250));
	if (status != sf::Socket::Done) {
		Logger::Log << "Network status: off" << std::endl;
		m_jre->GetGUI()->PushPopup(new GUISystemPopup(m_jre->m_params, "MS_SRV Connection Failed"));
		return (false);
	}
	OnConnect();

	// Reset as non blocking for upcoming ops.
	m_sock->setBlocking(false);
	Logger::Log << "Network status: ok" << std::endl;
	
	CommandHello *cmd = (CommandHello*)GetCommand("HELLO");
	cmd->Send(this, "default", false);

	return (true);
}

void NetworkController::Update()
{
	sf::SocketSelector selector;
	sf::Socket::Status st;

	if (m_connected == false && m_jre->m_gsm.GetActiveState()->GetId() == GMST_SPLASH)
	{
		if (m_tOutClk.getElapsedTime().asSeconds() > 10) 
		{
			Logger::Log << "Reconnecting..." << std::endl;
			Connect();
			m_tOutClk.restart();
		}
		return;
	}
	selector.add(*m_sock);
	selector.wait(sf::milliseconds(10));
	if (selector.isReady(*m_sock))
	{
		sf::Packet pkt;
		sf::String cmdId = "";

		if (selector.isReady(GetSock())) 
		{
			st = m_sock->receive(pkt);
			if (st == sf::Socket::Disconnected) {
				Logger::Log << "Disconnected from server" << std::endl;
				m_sock->disconnect();
				MarkAsDisconnected();
				m_tOutClk.restart();
				m_jre->GetGUI()->PushPopup(new GUISystemPopup(m_jre->m_params, "MS_SRV Connection Lost"));
				return;
			}

			pkt >> cmdId;

			std::for_each(m_commands.begin(), m_commands.end(), [&pkt, &cmdId, this](Command *cmd)
			{
				if (cmd->GetID() == cmdId) {
					Logger::Log << cmd->GetID().toAnsiString() << " received from server " << std::endl;
					cmd->ReceiveAction(this, pkt);
				}
			});
		}
	}
	if (m_tOutClk.getElapsedTime().asSeconds() > 5.0f && IsValid()) {
		sf::Packet pct;

		m_commands.front()->SendAction(this, pct);
		m_tOutClk.restart();
	}
}