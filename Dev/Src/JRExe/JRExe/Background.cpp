#include "Background.h"
#include "JRExe.h"

Background::Background(JRExe *core)
	: GUIWidget(*core->GetParams())
{
	m_core = core;
	m_generateCloudsCount = BG_CLOUDS_NB;
	m_generateBubblesCount = BG_BUBBLES_NB;
	m_minCloudGenTime = 0;
	m_minBubbleGenTime = 0;
	m_rndCloudGenTime = 1000;
	m_rndBubbleGenTime = 1000;
	for (unsigned int id = 0; id < m_generateCloudsCount; id++)
	{
		m_clouds[id].alpha = 0;
		m_clouds[id].x = 0;
		m_clouds[id].scale = 0;
		m_clouds[id].y = 0;
		m_clouds[id].state = BGEST_COMPLETE;
	}
	for (unsigned int id = 0; id < m_generateBubblesCount; id++)
	{
		m_bubbles[id].alpha = 0;
		m_bubbles[id].x = 0;
		m_bubbles[id].scale = 0;
		m_bubbles[id].y = 0;
		m_bubbles[id].state = BGEST_COMPLETE;
	}
	Logger::Log << "Submodule construct: GUI Background" << std::endl;
}

Background::~Background()
{

}

void Background::Init(JRExe *core)
{
	float sw = core->GetParams()->m_params.window_width;
	float sh = core->GetParams()->m_params.window_height;

	m_minCloudGenTime = 0;
	m_minBubbleGenTime = 0;
	m_rndCloudGenTime = 13000;
	m_rndBubbleGenTime = 2600;
	m_generateCloudsCount = BG_CLOUDS_NB;
	m_generateBubblesCount = BG_BUBBLES_NB;

#if NOVIDEO == 0
	float scaleX = sw / 1920.0;
	float scaleY = sw / 1920.0 * 1.15;
	m_movieTop.LoadFromFile("Data/Video/drops_flares.ogv");
	m_movieTop.setPosition(0, 0);
	m_movieTop.setScale(scaleX, scaleY);
	
	scaleX = sw / 1920.0;
	scaleY = sw / 1080.0;
	m_movieBot.LoadFromFile("Data/Video/drops_flares.ogv");
	m_movieBot.setRotation(180);
	m_movieBot.setPosition(sw, sh);
	m_movieBot.setScale(scaleX, scaleY);
#endif

	/*
	for (unsigned int id = 0; id < BG_CLOUDS_NB; id++)
	{
		m_clouds[id].alpha = rand() % 255;
		m_clouds[id].x = rand() % (int)sw;
		m_clouds[id].y = rand() % (int)sh;
		m_clouds[id].state = BGEST_STARTED;
	}*/
	for (unsigned int id = 0; id < m_generateCloudsCount; id++)
	{
		GenerateCloud();
	}
	for (unsigned int id = 0; id < m_generateBubblesCount; id++)
	{
		GenerateBubble();
	}

	m_minCloudGenTime = 10000;
	m_minBubbleGenTime = 1600;
	m_rndCloudGenTime = 3000;
	m_rndBubbleGenTime = 1000;
	Logger::Log << "Submodule init: GUI Background" << std::endl;
}

void Background::GenerateCloud()
{
	float sw = m_core->GetParams()->m_params.window_width;
	float sh = m_core->GetParams()->m_params.window_height;

	for (unsigned int i = 0; i < m_generateCloudsCount; i++)
	{
		BGCloud& res = m_clouds[i];
		if (res.state == BGEST_COMPLETE)
		{
			float dir = ((rand() % 2) * 2 - 1) * ((rand() % (int)(sh * 0.25)) + 100);
			res.state = BGEST_STARTED;
			res.x = rand() % (int)sw;
			res.y = rand() % (int)sh;
			res.scale = 2 - (rand() % 280) / 100 + 0.2;
			res.alpha = 0;

			tween::TweenerParam param(m_minCloudGenTime + rand() % m_rndCloudGenTime, tween::CUBIC, tween::EASE_OUT);
			param.addProperty(&res.x, res.x + dir);
			param.addProperty(&res.y, res.y + dir);
			param.addProperty(&res.alpha, 600);
			param.forceEnd = true;
			m_core->m_tweener.addTween(param);
		}
	}
}

void Background::GenerateBubble()
{
	float sw = m_core->GetParams()->m_params.window_width;
	float sh = m_core->GetParams()->m_params.window_height;

	for (unsigned int i = 0; i < m_generateBubblesCount; i++)
	{
		BGBubble& res = m_bubbles[i];
		if (res.state == BGEST_COMPLETE)
		{
			float dir = ((rand() % 2) * 2 - 1) * ((rand() % (int)sh * 0.5) + 100);
			res.state = BGEST_STARTED;
			res.x = rand() % (int)sw;
			if (dir > 0)
				res.y = rand() % (int)(sh * 0.25) - 100;
			else
				res.y = rand() % (int)(sh * 0.25) + sh * 0.75 + 100;
			res.scale = 2 - (rand() % 180) / 100 + 0.2;
			res.alpha = 0;

			tween::TweenerParam param(m_minBubbleGenTime + rand() % m_rndBubbleGenTime, tween::CUBIC, tween::EASE_OUT);
			param.addProperty(&res.y, res.y + dir);
			param.addProperty(&res.alpha, 600);
			param.forceEnd = true;
			m_core->m_tweener.addTween(param);
		}
	}
}

void Background::Render(JRExe *core)
{
	int sw = core->GetParams()->m_params.window_width;
	int sh = core->GetParams()->m_params.window_height;
	sf::RenderWindow *wnd = core->GetRenderer()->GetRenderWindow();

	// TODO: animation for bg with combo/beats
	/*
	float ratio = (float)m_generateBubblesCount / (float)BG_BUBBLES_NB;
	int clt = m_core->GetMainClock()->getElapsedTime().asMilliseconds() / 50;

	sf::RectangleShape rect(sf::Vector2f(64, 64));
	rect.setOrigin(32, 32);
	rect.setTexture(core->GetRenderer()->GetTexture(TEX_BG_EXTRACT));
	rect.setScale(ratio * (25.0 + (clt % 10) * 0.4), ratio * 25.0);
	rect.setPosition(sw / 2, sh - sw / 2);
	rect.setFillColor(sf::Color(255, 255, 255, 255 * ratio));
	wnd->draw(rect);
	*/

	/*
	int i = clock() % 749;
	std::string str = "";
		
	if (i >= 100) {
		str = std::to_string(i);
	} else if (i >= 10) {
		str = "0" + std::to_string(i);
	} else {
		str = "00" + std::to_string(i);			
	}
	sf::Texture m_rtex;
	m_rtex.loadFromFile("Data/Background/main_bg-0000" + str + ".jpg");
	*/

	/* Render background video */
	/*
	sf::RectangleShape rect(sf::Vector2f(1920, 1080));
	rect.setPosition(0, sh - 1080);
	rect.setTexture(&m_rtex);
	wnd->draw(rect);
	*/
#if NOVIDEO == 0
	/*
	wnd->draw(m_movieTop);
	wnd->draw(m_movieBot);*/
#endif

	/* Render background texture */
	/*
	sf::RectangleShape rect(sf::Vector2f(sw, sh));
	rect.setPosition(0, 0);
	rect.setFillColor(sf::Color(18, 22, 25, 255));
	wnd->draw(rect);
	*/

	for (unsigned int id = 0; id < BG_CLOUDS_NB; id++)
	{
		if (m_clouds[id].state == BGEST_STARTED)
		{
			sf::RectangleShape rect(sf::Vector2f(512, 512));
			rect.setOrigin(256 / 2, 256 / 2);
			rect.setScale(m_clouds[id].scale, m_clouds[id].scale);
			rect.setTexture(core->GetRenderer()->GetTexture(TEX_BG_CLOUD));
			rect.setPosition(m_clouds[id].x, m_clouds[id].y);
			if (m_clouds[id].alpha > 255)
				rect.setFillColor(sf::Color(255, 255, 255, MAX(0, MIN(255, 600 - m_clouds[id].alpha))));
			else
				rect.setFillColor(sf::Color(255, 255, 255, MAX(0, MIN(255, m_clouds[id].alpha * 1.4))));
			wnd->draw(rect);
		}
	}

	for (unsigned int id = 0; id < BG_BUBBLES_NB; id++)
	{
		if (m_clouds[id].state == BGEST_STARTED)
		{
			sf::RectangleShape rect(sf::Vector2f(16, 16));
			rect.setOrigin(64 / 2, 64 / 2);
			rect.setScale(m_bubbles[id].scale, m_bubbles[id].scale);
			rect.setTexture(core->GetRenderer()->GetTexture(TEX_BG_BUBBLE));
			rect.setPosition(m_bubbles[id].x, m_bubbles[id].y);
			if (m_bubbles[id].alpha > 255)
				rect.setFillColor(sf::Color(255, 255, 255, MAX(0, MIN(255, 600 - m_bubbles[id].alpha))));
			else
				rect.setFillColor(sf::Color(255, 255, 255, MAX(0, m_bubbles[id].alpha)));
			wnd->draw(rect);
		}
	}

	/* Render debugging FPS counter */
#if SHOW_FPS
	sf::Text tx;
	tx.setFont(core->GetRenderer()->GetFont(FONT_TITLE_TX));
	tx.setCharacterSize(12);
	tx.setColor(sf::Color(255, 255, 255, 255));

	tx.setPosition(5,5);
	tx.setString("FPS: " + std::to_string(core->m_currentFps));
	wnd->draw(tx);

	tx.setPosition(5,25);
	tx.setString("Clock: " + std::to_string(time(NULL)));
	wnd->draw(tx);
#endif
}

void Background::RenderCredits(JRExe *core)
{
	int sw = core->GetParams()->m_params.window_width;
	int sh = core->GetParams()->m_params.window_height;
	sf::RenderWindow *wnd = core->GetRenderer()->GetRenderWindow();
	float ratio = core->GetRenderer()->GetRenderingRatio();
	float sph = core->GetParams()->m_params.btn_top_offset;

	sf::Text tx;
	tx.setFont(core->GetRenderer()->GetFont(FONT_TITLE_TX));
	tx.setCharacterSize(24 * ratio);
	tx.setColor(sf::Color(255, 255, 255, 255));
	tx.setPosition(sw / 2, sh - sw - sph - 30 * ratio);
	tx.setString("CREDITS: " + std::to_string(core->m_credits.Value()));
	tx.setOrigin(tx.getLocalBounds().width / 2, 0);
	wnd->draw(tx);
}

void Background::RenderGridOverlay(JRExe *core)
{
	int sw = core->GetParams()->m_params.window_width;
	int sh = core->GetParams()->m_params.window_height;
	sf::RenderWindow *wnd = core->GetRenderer()->GetRenderWindow();

	float dx1 = core->GetParams()->m_params.btn_side_step;
	float btnw = core->GetParams()->m_params.btn_size;
	float k = core->GetParams()->m_params.btn_step;
	float sph = core->GetParams()->m_params.btn_top_offset;
	float errval = sw - (4.0 * btnw + 3.0 * k + 2.0 * dx1);

	sf::RectangleShape rect(sf::Vector2f(k, sw));
	rect.setFillColor(sf::Color(0, 0, 0, 255));

	// Render vertical grid
	rect.setPosition(-k + dx1, sh - sw);
	wnd->draw(rect);
	rect.setPosition(dx1 + btnw, sh - sw);
	wnd->draw(rect);
	rect.setPosition(dx1 + btnw * 2 + k, sh - sw);
	wnd->draw(rect);
	rect.setPosition(dx1 + btnw * 3 + k * 2, sh - sw);
	wnd->draw(rect);
	rect.setPosition(dx1 + btnw * 4 + k * 3, sh - sw);
	wnd->draw(rect);

	rect.setSize(sf::Vector2f(sw, k)); 
	// Render hor grid
	rect.setPosition(0, sh - dx1);
	wnd->draw(rect);
	rect.setPosition(0, sh - dx1 - k - btnw);
	wnd->draw(rect);
	rect.setPosition(0, sh - dx1 - k * 2 - btnw * 2);
	wnd->draw(rect);
	rect.setPosition(0, sh - dx1 - k * 3 - btnw * 3);
	wnd->draw(rect);
	rect.setPosition(0, sh - dx1 - k * 4 - btnw * 4);
	wnd->draw(rect);

	// Render top hide bar
	rect.setPosition(0, sh - sw - sph);
	rect.setSize(sf::Vector2f(sw, sph));
	wnd->draw(rect);
}

void Background::Update(JRExe *core)
{
	for (int i = 0; i < m_generateCloudsCount; i++)
	{
		BGCloud& res = m_clouds[i];

		if (res.state == BGEST_STARTED && (int)res.alpha == 600)
		{
			res.state = BGEST_COMPLETE;
		}
		else if (res.state == BGEST_COMPLETE)
		{
			GenerateCloud();
		}
	}

	for (int i = 0; i < m_generateBubblesCount; i++)
	{
		BGBubble& res = m_bubbles[i];

		if (res.state == BGEST_STARTED && (int)res.alpha == 600)
		{
			res.state = BGEST_COMPLETE;
		}
		else if (res.state == BGEST_COMPLETE)
		{
			GenerateBubble();
		}
	}
#if NOVIDEO == 0
	if (m_movieTop.getStatus() == sfe::Movie::Status::Stopped)
	{
		//m_movieTop.openFromFile("Data/Video/drops_flares.ogv");
		m_movieTop.stop();
		m_movieTop.play();
	}
	if (m_movieBot.getStatus() == sfe::Movie::Status::Stopped)
	{
		//m_movieBot.openFromFile("Data/Video/drops_flares.ogv");
		m_movieBot.stop();
		m_movieBot.play();
	}
#endif
}