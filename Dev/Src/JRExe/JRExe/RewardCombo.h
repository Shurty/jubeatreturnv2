#pragma once

#include "JRExe.h"

class RewardCombo
{
private:
	unsigned int m_currentCombo;
	BEATRESULT m_minimalBeat;
	unsigned int m_comboToReach;
	bool m_didReach;
	// TODO: add some way to notify graphics that we triggered a reward

public:
	Player* m_player;

	RewardCombo();
	~RewardCombo(void);

	void selectedMusic(const std::string& musicName, SONGLEVEL level);
	void hasScoredBeat(BEATRESULT res);//, unsigned int pointsForBeat, unsigned int currentCombo);
	void hasRegisteredFinalScore(unsigned int score);
};

