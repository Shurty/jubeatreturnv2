#include <JREngine/jrshared.h>
#include "GameModeBomb.h"
#include "Background.h"

GamemodeBomb::GamemodeBomb(JRExe* core) 
	: IGamemode(), m_core(core)
{
	m_name = "Bomb";
	m_selectSndName = "gm_select_unlimited.ogg";
	m_isActive = false;
	m_isGameRunning = false;
	m_isGameEnding = false;
	m_gameEndTimestamp = 0;
	m_selectLogo = core->m_render.GetTexture(TEXTURESDIR + std::string("Gamemodes/gmBomb.png"));
	m_bombTexture = new sf::Texture;
	m_bombTexture->loadFromFile(TEXTURESDIR + std::string("Gamemodes/BombMode/bomb.png"));
}

GamemodeBomb::~GamemodeBomb(void)
{

}

void 	GamemodeBomb::saveScore()
{
	m_core->m_playersManager.savePlayers();
}

void 	GamemodeBomb::loadScore()
{
	// FIXME: this function erase current player (we should load then we connect the player, but the player is already connected at this point)
	// m_core->m_playersManager.loadPlayers();
}

GMMODE_ID GamemodeBomb::GetId()
{
	return GMMD_BOMB;
}

// FIXME: this is not called ?
void GamemodeBomb::Init()
{
	// Here is the init of the gamemode, called on creation. For game init, go to gamestart callback
	m_isGameRunning = false;
	m_isGameEnding = false;
}

void GamemodeBomb::Update()
{
	static int graphTimestamp = 0;
	static int graphCount = 0;
	int timestamp = m_gameClock.getElapsedTime().asMilliseconds();

	if (m_isGameRunning)
	{
		float pressTime = m_core->m_params.m_params.press_range / 2;
		while (m_beatit != m_selectedBeatmap->beatsList.end() &&
			(*m_beatit).time <= timestamp + m_pressStartOffset)
		{
			//std::cout << "Press #" << (int)((*m_beatit).activate) << " at " << (*m_beatit).time << std::endl;
			if (rand() % 100 < 10) 
				m_activatedBomb[(*m_beatit).activate] = true;
			m_core->m_gui.BroadcastEvent(TRIGGERED, (void *)((*m_beatit).activate));
			++m_beatit;
			graphCount++;
		}
		if (timestamp > graphTimestamp)
		{
			//std::cout << "Graph value = " << graphCount << " at " << graphTimestamp << std::endl;
			graphTimestamp += m_selectedBeatmap->graphStep;
			graphCount = 0;
		}
		if (m_beatit == m_selectedBeatmap->beatsList.end())
		{
			m_gameEndTimestamp = timestamp + 4000;
			m_isGameRunning = false;
			m_isGameEnding = true;
			m_core->m_sounds.FadeOutMusic();
		}
	}
	else if (m_isGameEnding && timestamp > m_gameEndTimestamp)
	{
		m_gameEndTimestamp = 0;
		graphTimestamp = 0;
		graphCount = 0;
		m_core->m_sounds.GetMusic().stop();
		OnGameEnd();
	}
}

void GamemodeBomb::Render()
{

}

void GamemodeBomb::OnIconRender(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
{
	sf::RectangleShape rect(sf::Vector2f(x2 - x1, y2 - y1));

	rect.setPosition(x1, y1);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	rect.setTexture(m_selectLogo);
	target->GetRenderWindow()->draw(rect);
}

void GamemodeBomb::OnSelectedTopRender(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
{
	float ratio = target->GetRenderingRatio();

	sf::RectangleShape rect;
	sf::Text text;
	sf::FloatRect textRect;

	/* Panel title */
	text.setPosition(x1, y1);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(60 * ratio);
	text.setString("Bomb Mode");
	target->GetRenderWindow()->draw(text);

	text.setPosition(x1, y1 + 80 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(40 * ratio);
	text.setString("Be carefull not not touch the bomb");
	target->GetRenderWindow()->draw(text);
}

void GamemodeBomb::OnPlayKeyboardButtonInit(GUIKeyboard *kb, GUIWidget *wdg)
{
	Beat *bt = &m_selectedBeatmap->beatsList.front();
	std::deque<Beat>::iterator it;

	it = m_selectedBeatmap->beatsList.begin();
	while (it->time == bt->time)
	{
		if (it->activate == wdg->GetLinkedKey())
		{
			wdg->ReceiveEvent(FIRSTBTN_SET, NULL);
		}
		++it;
	}

	// Example of use for this callback
	wdg->LinkUserData(this);
	wdg->SetRender([] (GUIWidget *wdg, RenderingEngine *tgt) {
		GamemodeBomb *gm = (GamemodeBomb*)wdg->GetUserData();
		sf::Vector2f pos, size;

		if (wdg->IsTriggered() && gm->m_activatedBomb[wdg->GetLinkedKey()]) {
			pos = wdg->GetPosition();
			size = wdg->GetSize();
			sf::Texture *actTex = gm->m_bombTexture;
			sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

			rect.setOutlineColor(sf::Color(0, 0, 0, 255));
			rect.setTexture(actTex);
			rect.setPosition(pos.x, pos.y);
			tgt->GetRenderWindow()->draw(rect);
		}
	});
}

void GamemodeBomb::OnDebriefingTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2)
{

}

void GamemodeBomb::OnPlayTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2)
{

}

void GamemodeBomb::OnSetActive()
{

}

void GamemodeBomb::OnUnload()
{

}

bool GamemodeBomb::IsActive() const
{
	return m_isActive;
}

// bomb
void GamemodeBomb::OnBeatAction(BEATRESULT res, GUIWidget *widget)
{
	if (!isGameRunning() && !m_isGameEnding)
		return ;

	if (m_core->m_params.m_params.simulation)
	{
		if (res != BEATRESULT::BR_PERFECT) {
			if (m_beatit != m_selectedBeatmap->beatsList.end()) {
				Logger::Log << "WARNING: Simulation resulted on a non perfect result on beat press:" << (int)m_beatit->activate <<
					"-time:" << m_beatit->time << "-#combo:" << m_scoreManager.getBonusNotesCount() << std::endl;
			}
		}
	}

	m_scoreManager.scoreBeat(res, m_activatedBomb[widget->GetLinkedKey()]);
	m_activatedBomb[widget->GetLinkedKey()] = false;
	m_rewardsManager.hasScoredBeat(res);
	double scM = m_scoreManager.getBonusScoreMultiplier();
	if (scM == 1) {
		m_core->m_gui.m_mainBg->m_generateCloudsCount = MAX(0, MIN(BG_CLOUDS_NB, BG_CLOUDS_NB * scM));
	} else {
		m_core->m_gui.m_mainBg->m_generateCloudsCount = MAX(0, MIN(BG_CLOUDS_NB, BG_CLOUDS_NB * scM * 0.7));
	}
	m_core->m_gui.m_mainBg->m_generateBubblesCount = MAX(0, MIN(BG_BUBBLES_NB, BG_BUBBLES_NB * scM));
}

void GamemodeBomb::OnGamemodeSelected()
{
	m_core->m_sounds.PlaySnd(SOUNDDIR + GetSoundName());
	loadScore();
}

void GamemodeBomb::OnGamemodeConfirmed()
{
	m_isActive = true;
	m_rewardsManager.m_rewardCombo.m_player = m_core->m_playersManager.getCurrentPlayer();
}

void GamemodeBomb::OnGameSet(Beatmap *res)
{
	// Panel will swap to gamestate "Playing", you don't need to
	//m_scoreManager.reset(res->beats);
	m_selectedBeatmap = res;
	m_core->m_sounds.StopPreviewMusic();
	m_beatit = m_selectedBeatmap->beatsList.begin();
	m_core->m_params.m_params.press_range = m_core->m_params.m_params.press_durations[m_selectedBeatmap->level];
	m_scoreManager.reset(m_selectedBeatmap->beats);
	m_core->m_gui.m_mainBg->m_generateCloudsCount = 0;
	m_core->m_gui.m_mainBg->m_generateBubblesCount = 0;
	//m_core->m_gui.m_mainBg->m_minBubbleGenTime = 1000 / m_selectedBeatmap->originalBpm * 1000;

	int targetDt = m_core->m_params.m_params.press_range / 2;
	m_pressStartOffset = targetDt +
		m_core->m_params.m_params.press_offset +
		targetDt * m_core->m_params.m_params.press_img_delay;
}

void GamemodeBomb::OnMusicSelect(Beatmap *res)
{
	if (m_core->m_params.m_params.wav_mode) {
		m_core->m_sounds.PlayPreviewMusic(MUSICDIR + res->src + MUSICEXTWAV);
	}
	else {
		m_core->m_sounds.PlayPreviewMusic(MUSICDIR + res->src + MUSICEXT);
	}
	m_selectedBeatmap = res;
}

void GamemodeBomb::OnGameStart()
{
	if (m_core->m_params.m_params.wav_mode) {
		m_core->m_sounds.PlayMusic(MUSICDIR + m_selectedBeatmap->src + MUSICEXTWAV);
	}
	else {
		m_core->m_sounds.PlayMusic(MUSICDIR + m_selectedBeatmap->src + MUSICEXT);
	}
	m_gameClock.restart();
	m_isGameRunning = true;
	m_isGameEnding = false;
	for (int i = 0; i < KB_KEYS_COUNT; i++) {
		m_activatedBomb[i] = false;
	}
}

void GamemodeBomb::OnGameEnd()
{
	Player* p = m_core->m_playersManager.getCurrentPlayer();
	std::cout << "current score: " << m_scoreManager.getScoreWithCombo() << " || previous Score: " << p->getHighScoreFor(m_selectedBeatmap->name, m_selectedBeatmap->songLevel) << std::endl;
	p->saveScoreIfHigher(m_selectedBeatmap->name, m_selectedBeatmap->songLevel, m_scoreManager.getScoreWithCombo());

	// now save the new highscore !
	// FIXME: we rewrite the whole file of every player scores...
	this->saveScore();
	this->m_scoreManager.logScores();

	// Not panel controlled - Switch to debriefing here
	m_isGameRunning = false;
	m_isGameEnding = false;
	m_core->m_gsm.ChangeState(GMST_DEBRIEFING);
	m_core->m_gui.m_mainBg->m_generateCloudsCount = BG_CLOUDS_NB;
	m_core->m_gui.m_mainBg->m_generateBubblesCount = BG_BUBBLES_NB;

}

void GamemodeBomb::OnDebriefingExit()
{
	m_core->m_gsm.ChangeState(GMST_BETAMSG);
}

bool GamemodeBomb::isGameRunning()
{
	return m_isGameRunning;
}

ScoreManager& GamemodeBomb::GetScoreManager()
{
	return (m_scoreManager);
}

unsigned int GamemodeBomb::GetCreditPrice() { return (2); }
