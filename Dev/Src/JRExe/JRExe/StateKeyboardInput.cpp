
#include <JREngine/jrshared.h>
#include "StateKeyboardInput.h"

StateKeyboardInput::StateKeyboardInput(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_KEYBOARD_INPUT;
	m_name = "Keyboard input";
}

void StateKeyboardInput::OnInit(Parameters &pm)
{
	Logger::Log << "StateKeyboardInput: OnInit" << std::endl;

}

void StateKeyboardInput::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateKeyboardInput::OnRender(Parameters &pm)
{
}

void StateKeyboardInput::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateKeyboardInput: update" << std::endl;
}

void StateKeyboardInput::OnEnd(Parameters &pm)
{
}

void StateKeyboardInput::OnUnload(Parameters &pm)
{
}
