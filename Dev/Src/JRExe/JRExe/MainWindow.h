
#pragma once

#include <Windows.h>

class MainWindow
{
private:
	HWND				m_handle;
	HINSTANCE			m_instance;
	WNDCLASSEX			m_wcex;

public:
	MainWindow();
	~MainWindow();

	HWND GetHandle();
};