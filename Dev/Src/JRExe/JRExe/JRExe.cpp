
#include <iostream>
#include <string>
#include "JRExe.h"

#include "StateBoot.h"
#include "StateGameSelect.h"
#include "StateSplash.h"
#include "StateRegister.h"
#include "StateMusicSelect.h"
#include "StatePlaying.h"
#include "StateDebriefing.h"
#include "StateBetaMessage.hpp"

#include "UnitSFML.hpp"

JRExe::JRExe()
	: IInterfacable(), m_gsm(m_params), m_render(m_params), m_sounds(m_params),
	m_valid(m_params.m_params.jrexe_valid), m_testLvl(TLVL_ALL), m_playersManager("Data/Scores/"), 
	m_network(this)
{
	srand(time(NULL));
	m_gmm = new GamemodeManager();
	m_valid = true;
//	m_playersManager.connectPlayer("Shurty");
}

JRExe::~JRExe()
{
}

void JRExe::OnStart()
{
	/* States init */
	m_gsm.PushState(new StateBoot(*this));
	m_gsm.PushState(new StateSplash(*this));
	m_gsm.PushState(new StateRegister(*this));
	m_gsm.PushState(new StateGameSelect(*this));
	m_gsm.PushState(new StateMusicSelect(*this));
	m_gsm.PushState(new StatePlaying(*this));
	m_gsm.PushState(new StateDebriefing(*this));
	m_gsm.PushState(new StateBetaMessage(*this));

	m_gsm.ChangeState(GMST_BOOT);

	m_input->SetBtnPressedCallback(&IInterfacable::OnKeyPressed);
	m_input->SetBtnReleasedCallback(&IInterfacable::OnKeyReleased);
}

void JRExe::OnUpdate()
{
	m_gsm.UpdateState();
	m_gmm->Update();
}

void JRExe::OnRender()
{
	m_gsm.RenderState();
}

void JRExe::OnPostRender()
{

}

void JRExe::OnExit()
{
	this->m_render.OnExit(m_params);
}

void JRExe::RunTests()
{

}


bool JRExe::IsValid() {
	return (m_valid);
}

void JRExe::SetValid(bool re) {
	m_valid = re;
}


void JRExe::OnKeyPressed(int key)
{
	#if _DEBUG
		Logger::Log << "CoreController: Key in received " << key << std::endl;
	#endif
	m_gui.BroadcastEvent(PRESSED, (void *)key);

	/* DEBUG */
	switch (key)
	{
	case 17:
		m_params.Reload();
		break;
	case 18:
		m_sounds.FadeOutMusic();
		m_gmm->currentMode()->OnGameEnd();
		break;
	default:
		break;
	}
}

void JRExe::OnKeyReleased(int key)
{
	#if _DEBUG
		Logger::Log << "CoreController: Key off received " << key << std::endl;
	#endif	
	m_gui.BroadcastEvent(RELEASED, (void *)key);
}

void JRExe::setCurentFps(int framesPerSecond)
{
    m_currentFps = framesPerSecond;
}

RenderingEngine* JRExe::GetRenderer() { return (&m_render); }
GUIController* JRExe::GetGUI() { return (&m_gui); }
sf::Clock* JRExe::GetMainClock() { return (&m_globClock); }
Parameters* JRExe::GetParams() { return (&m_params); }
void JRExe::SetTestLevel(TESTLVL lvl) { m_testLvl = lvl;  }
TESTLVL JRExe::GetTestLevel() const { return (m_testLvl);  }

