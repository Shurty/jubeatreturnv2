
#pragma once

#include "JRExe.h"

class StateMods : public GameState
{
private:
	JRExe&	m_core;
public:
	StateMods(JRExe&);

	void OnInit(Parameters &pm);
	void OnStart(Parameters &pm);
	void OnRender(Parameters &pm);
	void OnUpdate(Parameters &pm);
	void OnEnd(Parameters &pm);
	void OnUnload(Parameters &pm);

};