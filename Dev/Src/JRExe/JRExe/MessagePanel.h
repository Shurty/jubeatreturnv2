#pragma once

#include <SFML/Audio.hpp>
#include <JREngine/jrgui.h>

#include "GUIPanel.h"

//#define	MSG_WELCOME_DELAY		2.0f
//#define	MSG_NOHIT_DELAY			7.0f
//#define	MSG_INFO_DELAY			500

#define	MSG_WELCOME_DELAY		2.0f
#define	MSG_NOHIT_DELAY			4.0f
#define	MSG_INFO_DELAY			500

class JRExe;
class RenderingEngine;

class MessagePanel : public GUIPanel 
{
private:
	GUIKeyboard m_keyboard;
	JRExe *m_gm;

	// Position tweens
	float m_posX;
	float m_alpha;
	float m_posXWdg;
	float m_alphaWdg;
	float m_displayTimer;

	AnimatedSprite m_safetyWarning;

	enum {
		MSG_ST_WELCOME,
		MSG_ST_NOHIT,
		MSG_ST_INFO,
	} m_state;

	bool m_started;
	bool m_delayComplete;

	void RenderHeader(RenderingEngine *target);

	sf::Clock m_startClock;
	float m_startTickSec;
	float m_startMaxTime;

public:
	MessagePanel(JRExe *gm);
	~MessagePanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();

	void DrawWelcome(RenderingEngine *target);
	void DrawNohit(RenderingEngine *target);
	void DrawInfo(RenderingEngine *target);

	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
	virtual void OnStart(GUIWidget *btn);
};