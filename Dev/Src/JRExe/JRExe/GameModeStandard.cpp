#include <JREngine/jrshared.h>
#include "GameModeStandard.h"
#include "Background.h"

GamemodeStandard::GamemodeStandard(JRExe* core)
: IGamemode(), m_core(core)
{
	m_name = "Standard Mode";
	m_selectSndName = "gm_select_standard.ogg";
	m_isActive = false;
	m_isGameRunning = false;
	m_isGameEnding = false;
	m_isGameMusicStarted = false;
	m_musicsPlayed = 0;
	m_gameEndTimestamp = 0;
	m_maxMusics = 3;
	m_selectLogo = core->m_render.GetTexture(TEXTURESDIR + std::string("Gamemodes/gmStandard.png"));
}

GamemodeStandard::~GamemodeStandard(void)
{

}

void 	GamemodeStandard::saveScore()
{
	/*
	Player* p = m_core->m_playersManager.getCurrentPlayer();
	Logger::Log << "current score: " << m_scoreManager.getScoreWithCombo() << " || previous Score: " << p->getHighScoreFor(m_selectedBeatmap->name, m_selectedBeatmap->songLevel) << std::endl;
	if (p->saveScoreIfHigher(m_selectedBeatmap->name, m_selectedBeatmap->songLevel, m_scoreManager.getScoreWithCombo()))
	{
		m_core->m_playersManager.saveScore(m_scoreManager.getScoreWithCombo(), p, m_selectedBeatmap->name, m_selectedBeatmap->songLevel);
		m_core->m_playersManager.savePlayer(p);
	}
	*/
	Player* p = m_core->m_playersManager.getCurrentPlayer();
	Logger::Log << "current score: " << m_scoreManager.getScoreWithCombo() << " || previous Score: " << p->getHighScoreFor(m_selectedBeatmap->name, m_selectedBeatmap->songLevel) << std::endl;

	// fill these details like it's first time we do it.
	Player::MusicScoreRecord::Details details;
	details.bestScore = m_scoreManager.getScoreWithCombo();
	details.maxCombo = m_scoreManager.getMaxCombo();
	details.maxGoodBeat = m_scoreManager.getNbBeatResult(BEATRESULT::BR_GOOD) +
		m_scoreManager.getNbBeatResult(BEATRESULT::BR_GREAT) +
		m_scoreManager.getNbBeatResult(BEATRESULT::BR_PERFECT);
	details.maxPerfect = m_scoreManager.getNbBeatResult(BEATRESULT::BR_PERFECT);
	details.timesCleared = ((m_scoreManager.getPotentialGrade() >= GRADE::GRADE_C) ? 1 : 0);
	details.timesPlayed = 1;
	// details.timestampHighscore = std::time(nullptr); // these 2 lines are not checked by function p->updateWithDetails
	// details.timestampLastPlayed = std::time(nullptr); 
	details.totalScore = m_scoreManager.getPotentialScore();

	// then pass these details to player, he will save or ignore everything depending of its past
	const Player::MusicScoreRecord& musicRecordUpdated = p->updateWithDetails(details, m_selectedBeatmap->name, m_selectedBeatmap->songLevel);

	m_core->m_playersManager.saveMusicRecordForPlayer(musicRecordUpdated, p, m_selectedBeatmap->name, m_selectedBeatmap->songLevel);

	//if (p->saveScoreIfHigher(m_selectedBeatmap->name, m_selectedBeatmap->songLevel, m_scoreManager.getScoreWithCombo()))
	//{
	//	m_core->m_playersManager.saveScore(m_scoreManager.getScoreWithCombo(), p, m_selectedBeatmap->name, m_selectedBeatmap->songLevel);
	//}
}

void 	GamemodeStandard::loadScore()
{
	// FIXME: this function erase current player (we should load then we connect the player, but the player is already connected at this point)
	// m_core->m_playersManager.loadPlayers();
}

GMMODE_ID GamemodeStandard::GetId()
{
	return GMMD_STANDARD3;
}

// FIXME: this is not called ?
void GamemodeStandard::Init()
{
	// Here is the init of the gamemode, called on creation. For game init, go to gamestart callback
	m_isGameRunning = false;
	m_isGameEnding = false;
	m_isGameMusicStarted = false;
	m_musicsPlayed = 0;
}

void GamemodeStandard::Update()
{
	static int graphTimestamp = 0;
	static int graphCount = 0;
	int timestamp = GetPlayTime();

	if (m_isGameRunning)
	{
		if (m_isGameMusicStarted == false && timestamp > 0)
		{
			BeginPlayMusic();
		}
		float pressTime = m_core->m_params.m_params.press_range / 2;
		while (m_beatit != m_selectedBeatmap->beatsList.end() &&
			(*m_beatit).time <= timestamp +
				m_core->m_params.m_params.press_offset)
		{

			//Logger::Log << "Press #" << (int)((*m_beatit).activate) << " at " << (*m_beatit).time << std::endl;
			m_core->m_gui.BroadcastEvent(TRIGGERED, (void *)((*m_beatit).activate));
			m_core->m_gui.BroadcastEvent(TRIGGERBEAT, (void *)&(*m_beatit));
			++m_beatit;
			graphCount++;
		}
		if (timestamp > graphTimestamp)
		{
			//Logger::Log << "Graph value = " << graphCount << " at " << graphTimestamp << std::endl;
			graphTimestamp += m_selectedBeatmap->graphStep;
			graphCount = 0;
		}
		if (m_beatit == m_selectedBeatmap->beatsList.end())
		{
			m_gameEndTimestamp = timestamp + 4000;
			m_isGameRunning = false;
			m_isGameEnding = true;
			m_core->m_sounds.FadeOutMusic();
		}
	}
	else if (m_isGameEnding && timestamp > m_gameEndTimestamp)
	{
		m_gameEndTimestamp = 0;
		graphTimestamp = 0;
		graphCount = 0;
		m_core->m_sounds.GetMusic().stop();
		OnGameEnd();
	}
}

void GamemodeStandard::Render()
{

}

void GamemodeStandard::OnIconRender(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
{
	sf::RectangleShape rect(sf::Vector2f(x2 - x1, y2 - y1));

	rect.setPosition(x1, y1);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	rect.setTexture(m_selectLogo);
	target->GetRenderWindow()->draw(rect);
}

void GamemodeStandard::OnSelectedTopRender(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
{
	float ratio = target->GetRenderingRatio();

	sf::RectangleShape rect;
	sf::Text text;
	sf::FloatRect textRect;

	/* Panel title */
	text.setPosition(x1, y1);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(60 * ratio);
	text.setString("Standard Mode");
	target->GetRenderWindow()->draw(text);

	text.setPosition(x1, y1 + 80 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(40 * ratio);
	text.setString("Play a set of 3 musics of your choice\
		\nUnlock new musics, score points and achievements\
		\nCompete against other players online");
	target->GetRenderWindow()->draw(text);
}


void GamemodeStandard::OnSetActive()
{
	m_core->m_gmm->GetDifficulty()->LoadConfig(&m_core->m_params);
}

void GamemodeStandard::OnUnload()
{

}

bool GamemodeStandard::IsActive() const
{
	return m_isActive;
}

void GamemodeStandard::OnBeatAction(BEATRESULT res, GUIWidget *widget)
{
	if (!isGameRunning() && !m_isGameEnding)
		return;


	if (m_core->m_params.m_params.simulation)
	{
		if (res != BEATRESULT::BR_PERFECT) {
			if (m_beatit != m_selectedBeatmap->beatsList.end()) {
				Logger::Log << "WARNING: Simulation resulted on a non perfect result on beat press:" << (int)m_beatit->activate <<
					"-time:" << m_beatit->time << "-#combo:" << m_scoreManager.getBonusNotesCount() << std::endl;
			}
		}
	}

	m_scoreManager.scoreBeat(res);
	m_rewardsManager.hasScoredBeat(res);

	double scM = m_scoreManager.getBonusScoreMultiplier();
	if (scM == 1) {
		m_core->m_gui.m_mainBg->m_generateCloudsCount = MAX(0, MIN(BG_CLOUDS_NB, BG_CLOUDS_NB * scM));
	}
	else {
		m_core->m_gui.m_mainBg->m_generateCloudsCount = MAX(0, MIN(BG_CLOUDS_NB, BG_CLOUDS_NB * scM * 0.7));
	}
	m_core->m_gui.m_mainBg->m_generateBubblesCount = MAX(0, MIN(BG_BUBBLES_NB, BG_BUBBLES_NB * scM));
}

void GamemodeStandard::OnGamemodeSelected()
{
	m_core->m_sounds.PlaySnd(SOUNDDIR + GetSoundName());
	loadScore();
}

void GamemodeStandard::OnGamemodeConfirmed()
{
	m_isActive = true;
	m_musicsPlayed = 0;
	m_rewardsManager.m_rewardCombo.m_player = m_core->m_playersManager.getCurrentPlayer();
}

void GamemodeStandard::OnGameSet(Beatmap *res)
{
	// Panel will swap to gamestate "Playing", you don't need to
	//m_scoreManager.reset(res->beats);
	m_selectedBeatmap = res;
	m_core->m_sounds.StopPreviewMusic();
	m_beatit = m_selectedBeatmap->beatsList.begin();
	//m_core->m_params.m_params.press_range = MAX(MIN(120000.0 / (float)m_selectedBeatmap->originalBpm, 900), 900);
	m_core->m_params.m_params.press_range = m_core->m_params.m_params.press_durations[m_selectedBeatmap->level];
	
	m_scoreManager.reset(m_selectedBeatmap->beats);
	m_core->m_gui.m_mainBg->m_generateCloudsCount = 0;
	m_core->m_gui.m_mainBg->m_generateBubblesCount = 0;
	//m_core->m_gui.m_mainBg->m_minBubbleGenTime = 1000 / m_selectedBeatmap->originalBpm * 1000;

	int targetDt = m_core->m_params.m_params.press_range / 2;
	m_pressStartOffset = targetDt +
		m_core->m_params.m_params.press_offset +
		targetDt * m_core->m_params.m_params.press_img_delay;
}

void GamemodeStandard::OnMusicSelect(Beatmap *res)
{
	if (m_core->m_params.m_params.wav_mode) {
		m_core->m_sounds.PlayPreviewMusic(MUSICDIR + res->src + MUSICEXTWAV);
	}
	else {
		m_core->m_sounds.PlayPreviewMusic(MUSICDIR + res->src + MUSICEXT);
	}
	m_selectedBeatmap = res;
}

void GamemodeStandard::BeginPlayMusic()
{
	if (m_core->m_params.m_params.wav_mode) {
		m_core->m_sounds.PlayMusic(MUSICDIR + m_selectedBeatmap->src + MUSICEXTWAV);
	}
	else {
		m_core->m_sounds.PlayMusic(MUSICDIR + m_selectedBeatmap->src + MUSICEXT);
	}
	m_isGameMusicStarted = true;
	Logger::Log << "Music start at timestamp " << GetPlayTime() << std::endl;
}

void GamemodeStandard::OnGameStart()
{
	m_gameClock.restart();
	m_isGameRunning = true;
	m_isGameEnding = false;
	m_isGameMusicStarted = false;
}

void GamemodeStandard::OnGameEnd()
{
	// now save the new highscore !
	this->saveScore();
	this->m_scoreManager.logScores();
	// Not panel controlled - Switch to debriefing here
	m_isGameRunning = false;
	m_isGameEnding = false;
	m_core->m_gsm.ChangeState(GMST_DEBRIEFING);
	m_core->m_gui.m_mainBg->m_generateCloudsCount = BG_CLOUDS_NB;
	m_core->m_gui.m_mainBg->m_generateBubblesCount = BG_BUBBLES_NB / 2;
	m_musicsPlayed++;
}

void GamemodeStandard::OnDebriefingExit()
{
	if (m_musicsPlayed == m_maxMusics) {
		m_core->m_gsm.ChangeState(GMST_SPLASH);
	} else {
		m_core->m_gsm.ChangeState(GMST_MUSIC_SELECT);
	}
}

bool GamemodeStandard::isGameRunning()
{
	return m_isGameRunning;
}

ScoreManager& GamemodeStandard::GetScoreManager()
{
	return (m_scoreManager);
}

void GamemodeStandard::OnPlayKeyboardButtonInit(GUIKeyboard *kb, GUIWidget *wdg)
{
	Beat *bt = &m_selectedBeatmap->beatsList.front();
	std::deque<Beat>::iterator it;

	it = m_selectedBeatmap->beatsList.begin();
	while (it->time == bt->time)
	{
		if (it->activate == wdg->GetLinkedKey())
		{
			wdg->ReceiveEvent(FIRSTBTN_SET, NULL);
		}
		++it;
	}
}

void GamemodeStandard::OnDebriefingTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2)
{

}

void GamemodeStandard::OnPlayTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2)
{

}
