
#include <JREngine/jrshared.h>
#include "StateMarkers.h"

StateMarkers::StateMarkers(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_MARKERS;
	m_name = "Markers";
}

void StateMarkers::OnInit(Parameters &pm)
{
	Logger::Log << "StateMarkers: OnInit" << std::endl;
}

void StateMarkers::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateMarkers::OnRender(Parameters &pm)
{
}

void StateMarkers::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateMarkers: update" << std::endl;
}

void StateMarkers::OnEnd(Parameters &pm)
{
}

void StateMarkers::OnUnload(Parameters &pm)
{
}
