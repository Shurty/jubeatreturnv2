
#include <JREngine/jrshared.h>
#include "StateDebriefing.h"
#include "Background.h"

StateDebriefing::StateDebriefing(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_DEBRIEFING;
	m_name = "Debriefing";
}

void StateDebriefing::OnInit(Parameters &pm)
{
	Logger::Log << "StateDebriefing: OnInit" << std::endl;

}

void StateDebriefing::OnStart(Parameters &pm)
{
	Logger::Log << "StateDebriefing: Begin" << std::endl;
	m_core.GetGUI()->HideAll();
	m_core.GetGUI()->Show(PANEL_DEBRIEFING);
}

void StateDebriefing::OnRender(Parameters &pm)
{
	m_core.m_render.OnPreRender(pm);
	m_core.m_gui.m_mainBg->Render(&m_core);
	m_core.m_gui.Render();
	m_core.m_gui.m_mainBg->	RenderGridOverlay(&m_core);
	m_core.m_render.OnRender(pm);
}

void StateDebriefing::OnUpdate(Parameters &pm)
{
	m_core.m_input->PollEvents(&m_core.m_render, &m_core);
	m_core.m_network.Update();
	m_core.m_songs.Update();
	m_core.m_sounds.Update();
	m_core.m_gui.m_mainBg->Update(&m_core);
	m_core.m_gui.Update();
	m_core.m_tweener.step(m_core.GetMainClock()->getElapsedTime().asMilliseconds());
}

void StateDebriefing::OnEnd(Parameters &pm)
{
}

void StateDebriefing::OnUnload(Parameters &pm)
{
}
