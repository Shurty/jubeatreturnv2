#pragma once

#include <sstream>
#include "JRExe.h"

#include "RewardsManager.h"

class GamemodeStandard :
	public IGamemode
{
protected:
	JRExe*							m_core;
	bool							m_isActive;
	bool							m_isGameRunning;
	bool							m_isGameEnding;
	bool							m_isGameMusicStarted;
	ScoreManager					m_scoreManager;
	RewardsManager					m_rewardsManager;
	int								m_gameEndTimestamp;

	bool							isGameRunning();
	std::deque<Beat>::iterator		m_beatit;

	void saveScore();
	void loadScore();

	sf::Texture						*m_selectLogo;
	unsigned int					m_musicsPlayed;
	unsigned int					m_maxMusics;
	float							m_pressStartOffset;
	
	void BeginPlayMusic();

public:
	GamemodeStandard(JRExe *);
	~GamemodeStandard(void);

	virtual GMMODE_ID GetId();

	virtual void Init();
	virtual void Update();
	virtual void Render();
	virtual void OnSetActive();
	virtual void OnUnload();
	virtual bool IsActive() const;
	virtual ScoreManager& GetScoreManager();

	virtual void OnPlayKeyboardButtonInit(GUIKeyboard *kb, GUIWidget *wdg);
	virtual void OnDebriefingTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2);
	virtual void OnPlayTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2);

	virtual void OnIconRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2);
	virtual void OnSelectedTopRender(RenderingEngine *, float m_alpha, float x1, float y1, float x2, float y2);
	virtual void OnBeatAction(BEATRESULT res, GUIWidget *widget);
	virtual void OnGamemodeSelected();
	virtual void OnGamemodeConfirmed();
	virtual void OnGameSet(Beatmap *res);
	virtual void OnMusicSelect(Beatmap *);
	virtual void OnGameStart();
	virtual void OnGameEnd();
	virtual void OnDebriefingExit();
};

