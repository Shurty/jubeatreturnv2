
#include <JREngine/jrshared.h>
#include "StatePlaying.h"
#include "Background.h"

StatePlaying::StatePlaying(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_PLAYING;
	m_name = "Playing the game";
}

void StatePlaying::OnInit(Parameters &pm)
{
	Logger::Log << "StatePlaying: OnInit" << std::endl;

}

void StatePlaying::OnStart(Parameters &pm)
{
	Logger::Log << "StatePlaying: Begin" << std::endl;
	m_core.GetGUI()->HideAll();
	m_core.GetGUI()->Show(PANEL_PLAY);
}

void StatePlaying::OnRender(Parameters &pm)
{
	m_core.m_render.OnPreRender(pm);
	m_core.m_gui.m_mainBg->Render(&m_core);
	m_core.m_gui.Render();
	m_core.m_gui.m_mainBg->	RenderGridOverlay(&m_core);
	m_core.m_render.OnRender(pm);
}

void StatePlaying::OnUpdate(Parameters &pm)
{
	m_core.m_input->PollEvents(&m_core.m_render, &m_core);
	m_core.m_network.Update();
	m_core.m_songs.Update();
	m_core.m_sounds.Update();
	m_core.m_gui.m_mainBg->Update(&m_core);
	m_core.m_gui.Update();
	m_core.m_tweener.step(m_core.GetMainClock()->getElapsedTime().asMilliseconds());
}

void StatePlaying::OnEnd(Parameters &pm)
{
}

void StatePlaying::OnUnload(Parameters &pm)
{
}
