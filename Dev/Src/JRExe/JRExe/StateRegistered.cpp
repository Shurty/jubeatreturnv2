
#include "StateRegistered.h"

StateRegistered::StateRegistered(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_REGISTERED;
	m_name = "Registered";
}

void StateRegistered::OnInit(Parameters &pm)
{
	Logger::Log << "StateRegistered: OnInit" << std::endl;

}

void StateRegistered::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateRegistered::OnRender(Parameters &pm)
{
}

void StateRegistered::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateRegistered: update" << std::endl;
}

void StateRegistered::OnEnd(Parameters &pm)
{
}

void StateRegistered::OnUnload(Parameters &pm)
{
}
