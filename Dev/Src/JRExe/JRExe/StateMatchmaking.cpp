
#include <JREngine/jrshared.h>
#include "StateMatchmaking.h"

StateMatchmaking::StateMatchmaking(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_MATCHMAKING;
	m_name = "Matchmaking";
}

void StateMatchmaking::OnInit(Parameters &pm)
{
	Logger::Log << "StateMatchmaking: OnInit" << std::endl;

}

void StateMatchmaking::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateMatchmaking::OnRender(Parameters &pm)
{
}

void StateMatchmaking::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateMatchmaking: update" << std::endl;
}

void StateMatchmaking::OnEnd(Parameters &pm)
{
}

void StateMatchmaking::OnUnload(Parameters &pm)
{
}
