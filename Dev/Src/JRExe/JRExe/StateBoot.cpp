
#include <JREngine/jrshared.h>

#include "StateBoot.h"
#include "StateGameSelect.h"

#include "MusicSelectPanel.h"
#include "PlayPanel.h"
#include "DebriefingPanel.h"
#include "SplashPanel.h"
#include "GamemodePanel.h"
#include "ConfigPanel.h"
#include "InputPanel.h"
#include "MessagePanel.h"
#include "PreloadPanel.h"

#include "Background.h"
#include "GameModeUnlimited.h"
#include "GameModeStandard1.hpp"
#include "GameModeStandard3.hpp"
#include "GameModeStandard5.hpp"
#include "GameModeBomb.h"

StateBoot::StateBoot(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_BOOT;
	m_name = "Boot";
	Logger::Log << "State construct: " << m_name << std::endl;
}

void StateBoot::OnInit(Parameters &pm)
{
	/* Attempting Arduino inputs creation */
	ArduinoInput *res = new ArduinoInput(pm);

	/* Input init */
	if (res->IsConnected()) {
		m_core.m_input = res;
	} else {
		delete res;
		m_core.m_input = new InputController(pm);
	}

	/* Rendering engine init */
	m_core.m_render.OnInit(pm);
	Logger::Log << "State init: " << m_name << std::endl;
}

void StateBoot::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?

	/* Gamemodemngr init */
	m_core.m_gmm->OnInit();
	m_core.m_gmm->InitDifficulties(m_core.GetRenderer());
	//m_core.m_gmm->m_gamemodes.push_back(new GamemodeUnlimited(&m_core));
	m_core.m_gmm->m_gamemodes.push_back(new GamemodeStandard1(&m_core));
	m_core.m_gmm->m_gamemodes.push_back(new GamemodeStandard3(&m_core));
	m_core.m_gmm->m_gamemodes.push_back(new GamemodeStandard5(&m_core));
	//m_core.m_gmm->m_gamemodes.push_back(new GamemodeBomb(&m_core));

	/* Player init */
	//
	m_core.m_credits.Value(999);

	/* Calculate button offsets */
	int sw = pm.m_params.window_width;
	int sh = pm.m_params.window_height;

	// Sides padding
	float dx1 = sw * 6.5 / 290.0;
	// Button size w and h
	float btnw = sw * 52.0 / 290.0;
	// Interior grid width
	float k = sw * 23.0 / 290;
	// Top side height
	float sph = sh * 62.5 / 505.0;

	pm.m_params.btn_step = k;
	pm.m_params.btn_side_step = dx1;
	pm.m_params.btn_size = btnw;
	pm.m_params.btn_top_offset = sph;

	/* SLib init */
	m_core.m_sounds.Init(&m_core);
	m_core.m_songs.ParseAll(CBEATMAPSDIR);

	/* GUI Init */
	GUIPanel *resPanel;

	Logger::Log << "Init bg" << std::endl;
	m_core.m_gui.Init(&m_core);
	m_core.m_gui.m_mainBg->Init(&m_core);

	resPanel = new SplashPanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float) m_core.m_params.m_params.window_width, (float) m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);

	resPanel = new InputPanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float) m_core.m_params.m_params.window_width, (float) m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);

	resPanel = new GamemodePanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float) m_core.m_params.m_params.window_width, (float) m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);

	resPanel = new MusicSelectPanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float) m_core.m_params.m_params.window_width, (float) m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);
	
	resPanel = new PlayPanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float) m_core.m_params.m_params.window_width, (float) m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);
	
	resPanel = new DebriefingPanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float) m_core.m_params.m_params.window_width, (float) m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);
	
	resPanel = new ConfigPanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float) m_core.m_params.m_params.window_width, (float) m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);
	
	resPanel = new MessagePanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float) m_core.m_params.m_params.window_width, (float) m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);

	resPanel = new PreloadPanel(&m_core);
	resPanel->SetPosition(0, 0);
	resPanel->SetSize((float)m_core.m_params.m_params.window_width, (float)m_core.m_params.m_params.window_height);
	resPanel->Init();
	resPanel->SetState(HIDDEN);
	m_core.m_gui.PushPanel(resPanel);

	m_core.m_network.Connect();

	Logger::Log << "State started: " << m_name << std::endl;
}

void StateBoot::OnRender(Parameters &pm)
{
}

void StateBoot::OnUpdate(Parameters &pm)
{
	m_core.m_gsm.ChangeState(GMST_SPLASH);
}

void StateBoot::OnEnd(Parameters &pm)
{
}

void StateBoot::OnUnload(Parameters &pm)
{
}
