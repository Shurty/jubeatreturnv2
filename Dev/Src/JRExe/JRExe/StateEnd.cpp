
#include <JREngine/jrshared.h>
#include "StateEnd.h"

StateEnd::StateEnd(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_END;
	m_name = "Session end";
}

void StateEnd::OnInit(Parameters &pm)
{
	Logger::Log << "StateEnd: OnInit" << std::endl;

}

void StateEnd::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateEnd::OnRender(Parameters &pm)
{
}

void StateEnd::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateEnd: update" << std::endl;
}

void StateEnd::OnEnd(Parameters &pm)
{
}

void StateEnd::OnUnload(Parameters &pm)
{
}
