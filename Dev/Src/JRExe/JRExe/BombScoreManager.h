#pragma once

#include "JRExe.h"

class BombScoreManager : public ScoreManager
{
public:
	BombScoreManager(void);
	~BombScoreManager(void);
	void scoreBeat(BEATRESULT res, bool isBomb);
};

