#include "RewardCombo.h"

RewardCombo::RewardCombo() : m_player(NULL)
{
	m_currentCombo = 0;
	m_comboToReach = 100;
	m_minimalBeat = BEATRESULT::BR_GREAT;
	m_didReach = false;
}


RewardCombo::~RewardCombo(void)
{

}

void RewardCombo::selectedMusic(const std::string& musicName, SONGLEVEL level)
{
	m_currentCombo = 0;
}

void RewardCombo::hasScoredBeat(BEATRESULT res) //, unsigned int pointsForBeat, unsigned int currentCombo)
{
	if (!m_player)
	{
		Logger::Log << "WARNING: player not initialized in RewardCombo!";
		return ;
	}
	if (m_didReach)
		return ;
	if (res >= m_minimalBeat)
	{
		m_currentCombo++;
		if (m_currentCombo >= m_comboToReach)
		{
			Logger::Log << m_player->getPseudo() << " reached the reward of " << m_currentCombo  << " beat of quality " << (int)m_minimalBeat << "!!!" << std::endl;
			m_didReach = true;
			// TODO: send event triggered reward !
			// TODO: (Bonus) find something to save the reward (in player ?)
		}
	}
	else
	{
		m_currentCombo = 0;
	}
}

void RewardCombo::hasRegisteredFinalScore(unsigned int score)
{
	
}