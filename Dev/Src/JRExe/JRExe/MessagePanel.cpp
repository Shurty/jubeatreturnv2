#pragma once

#include <iostream>
#include <JREngine/jrgui.h>
#include "MessagePanel.h"
#include "GUIPanel.h"
#include "JRExe.h"

MessagePanel::MessagePanel(JRExe *gm) 
	: GUIPanel(PANEL_MESSAGE, gm), m_keyboard(*gm->GetParams()), m_gm(gm)
{
	m_started = false;
	for (int i = 0; i < 16; i++) {
		if (i == 15) {
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			btn->LinkAction(this, &AGUICallback::OnStart);
			btn->LinkUserData(this);
			btn->SetRender([] (GUIWidget* widget, RenderingEngine *render) {
				sf::Vector2f pos, size;
				sf::Text tx;
				sf::FloatRect sz;
				float ratio = render->GetRenderingRatio();

				MessagePanel *pane = (MessagePanel *)widget->GetUserData();

				pos = widget->GetPosition();
				//pos.x += pane->m_posXWdg;
				size = widget->GetSize();

				sf::RectangleShape rect(sf::Vector2f(size.x, size.y));

				rect.setPosition(pos.x, pos.y);
				rect.setFillColor(sf::Color(255, 255, 255, pane->m_alphaWdg));
				rect.setTexture(render->GetTexture(TEX_NEXT_NOLOGO_BTN));
				render->GetRenderWindow()->draw(rect);

				/* Time calculation for the start timer */
				float time = pane->m_startMaxTime - pane->m_startClock.getElapsedTime().asSeconds();
				if (time < 0)
					time = 0;
				std::string timeStr = std::to_string(time);
				std::string resStr = timeStr;
				if (time < 10)
					resStr = "0" + resStr;
				if (time == 0)
					resStr = "00:00";
				resStr = resStr.substr(0, 5);

				tx.setFont(render->GetFont(FONT_FIXED_SIZE_TX));
				tx.setString(resStr);
				if (time < 10)
					tx.setColor(sf::Color(255, 0, 0, pane->m_alphaWdg));
				else
					tx.setColor(sf::Color(255, 255, 255, pane->m_alphaWdg));
				tx.setCharacterSize(50 * ratio);
				sz = tx.getLocalBounds();
				tx.setPosition(pos.x - sz.width / 2 + size.x / 2, pos.y + size.y * 0.15);
				render->GetRenderWindow()->draw(tx);
			});
			m_keyboard.SetWidget(i, btn);
		}
		else {
			GUIActionButton *btn = new GUIActionButton(*gm->GetParams());
			m_keyboard.SetWidget(i, btn);
		}
	}
	m_safetyWarning.Set(gm->GetRenderer()->GetTexture(TEX_MSG_HIT_FORBIDDEN), 256, 256);
}

MessagePanel::~MessagePanel()
{

}

void MessagePanel::Init()
{
	int py = (int) (m_size.y - m_size.x);
	int sz = (int) m_size.x;

	m_keyboard.SetPosition(0, (float) py);
	m_keyboard.SetSize((float) sz, (float) sz);
	m_keyboard.Init();
}

void MessagePanel::Start()
{
	m_posX = 100;
	m_posXWdg = 100;
	m_alpha = 0;
	m_alphaWdg = 0;
	m_displayTimer = 0;
	m_started = false;
	m_delayComplete = false;
	//m_state = MSG_ST_WELCOME;
	m_state = MSG_ST_NOHIT;

	//tween::TweenerParam param(5000, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	//param.addProperty(&m_displayTimer, 200);
	//param.forceEnd = true;
	//m_gm->m_tweener.addTween(param);

	tween::TweenerParam param2(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
	param2.addProperty(&m_posX, 0);
	param2.addProperty(&m_alpha, 255);
	m_gm->m_tweener.addTween(param2);

	// Reset the state of all buttons
	for (int i = 0; i < 16; i++) {
		m_keyboard.GetWidget(i)->OnReleased();
	}
	m_startClock.restart();
	m_startMaxTime = 30.00;
	m_startTickSec = 20.00;
	/*m_gm->m_sounds.m_bgMusic.openFromFile(MUSICDIR + std::string("Menu/DoBeDoBeDo_Info.ogg"));
	m_gm->m_sounds.m_bgMusic.play();
	m_gm->m_sounds.m_bgMusic.setVolume(40);
	m_gm->m_sounds.m_bgMusic.setLoop(true);*/

	m_gm->m_sounds.m_bgMusic.openFromFile(MUSICDIR + std::string("Menu/DoBeDoBeDo_Info.ogg"));
	m_gm->m_sounds.m_bgMusic.setVolume(40);
	m_gm->m_sounds.m_bgMusic.play();
	m_gm->m_sounds.m_bgMusic.setLoop(true);

	m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/welcome.wav"));
}

void MessagePanel::Stop()
{
	m_gm->m_sounds.m_bgMusic.stop();
}

void MessagePanel::DrawWelcome(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	sf::RectangleShape rect(sf::Vector2f(sw, sh * 0.15));
	sf::Text text;
	sf::FloatRect textRect;

	rect.setPosition(0, sh * 0.5 - sh * 0.075);
	rect.setFillColor(sf::Color(0, 0, 0, m_alpha * 0.4));
	target->GetRenderWindow()->draw(rect);

	/* Panel title */
	text.setPosition(sw / 2, sh / 2 - sh * 0.05);
	text.setFont(target->GetFont(FONT_BUTTON_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha));
	text.setCharacterSize(sh * 0.075);
	text.setString("Welcome to Jubeat Return");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.width / 2, 0);
	target->GetRenderWindow()->draw(text);

}

void MessagePanel::DrawNohit(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	sf::RectangleShape rect(sf::Vector2f(sw, sh * 0.15));
	sf::Text text;
	sf::FloatRect textRect;

	rect.setPosition(0, sh * 0.2 - sh * 0.075);
	rect.setFillColor(sf::Color(0, 0, 0, m_alpha * 0.4));
	target->GetRenderWindow()->draw(rect);

	text.setPosition(sw / 2, sh * 0.2 - sh * 0.05);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha));
	text.setCharacterSize(sh * 0.075);
	text.setString("CAUTION");
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.width / 2, 0);
	target->GetRenderWindow()->draw(text);

	/* Panel warning */

	rect.setPosition(sw * 0.2, sh * 0.4);
	rect.setSize(sf::Vector2f(sw * 0.6, sw * 0.2));
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	target->GetRenderWindow()->draw(rect);

	/*
	rect.setTexture(target->GetTexture(TEX_MSG_HIT_FORBIDDEN), true);
	rect.setTextureRect(sf::IntRect(256 * 3, 256 * 3, 256, 256));
	rect.setPosition(sw * 0.2 + sw * 0.01, sh * 0.6 + sw * 0.01);
	rect.setSize(sf::Vector2f(sw * 0.18, sw * 0.18));
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha));
	target->GetRenderWindow()->draw(rect);
	*/
	m_safetyWarning.SetActiveFrame(m_alpha / 255.0);
	m_safetyWarning.Render(target, sw * 0.2 + sw * 0.01, sh * 0.4 + sw * 0.01,
		sw * 0.18, sw * 0.18);

	text.setPosition(sw * 0.41, sh * 0.4 + sw * 0.1);
	text.setFont(target->GetFont(FONT_BUTTON_TX));
	text.setColor(sf::Color(0, 0, 0, m_alpha));
	text.setCharacterSize(sw * 0.02);
	text.setString("Please be carefull with the device\n\
				   \nDo NOT hardly hit the device\
				   \nDo NOT apply extreme pressure to the device");
	textRect = text.getLocalBounds();
	text.setOrigin(0, textRect.height / 2);
	target->GetRenderWindow()->draw(text);
}

void MessagePanel::DrawInfo(RenderingEngine *target)
{
	float ratio = target->GetRenderingRatio();
	float sw = m_gm->m_params.m_params.window_width;
	float sh = m_gm->m_params.m_params.window_height;
	sh = sh - sw;
	sh = sh - m_params.m_params.btn_top_offset;

	sf::RectangleShape rect(sf::Vector2f(sw, sh * 0.7));
	sf::Text text;
	sf::FloatRect textRect;

	/* Panel title */
	text.setPosition(0, 0);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
	text.setOrigin(0, 0);
	text.setCharacterSize(sh * 0.15);
	text.setString("INFORMATIONS");
	target->GetRenderWindow()->draw(text);

	rect.setPosition(0, sh * 0.15);
	rect.setFillColor(sf::Color(0, 0, 0, m_alpha * 0.4));
	target->GetRenderWindow()->draw(rect);

	// padding between content and padding inside black bg : 24px

	float internalPadding = 24 * ratio;
	float x1 = internalPadding;
	float y1 = sh * 0.15 + internalPadding;
	float x2 = sw - internalPadding * 2;
	float y2 = sh * 0.7 - internalPadding * 2;

	/* Panel title */
	text.setPosition(x1, y1);
	text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 1.0));
	text.setOrigin(0, 0);
	text.setCharacterSize(60 * ratio);
	text.setString("WEB2DAY");
	target->GetRenderWindow()->draw(text);

	text.setPosition(x1, y1 + 120 * ratio);
	text.setFont(target->GetFont(FONT_TITLE_TX));
	text.setColor(sf::Color(255, 255, 255, m_alpha * 1.0));
	text.setOrigin(0, 0);
	text.setCharacterSize(28 * ratio);
	text.setString("\nBienvenue au salon web2day 2015\
				    \n\
				    \nTwitter  : @JubeatReturn\
				    \nSite Web : www.jubeat-return.com");
	target->GetRenderWindow()->draw(text);
	/*
	rect.setSize(sf::Vector2f(290 * ratio, 290 * ratio));
	rect.setPosition(sw - 290 * ratio - 50 * ratio, sh / 2 - 290 / 2 * ratio);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha * 1.0));
	rect.setTexture(NULL);
	target->GetRenderWindow()->draw(rect);
	*/
	rect.setSize(sf::Vector2f(300 * ratio, 300 * ratio));
	rect.setPosition(sw - (310 + (290 - 290) / 2) * ratio - 50 * ratio, sh / 2 - 150 * ratio);
	rect.setFillColor(sf::Color(255, 255, 255, m_alpha * 1.0));
	rect.setTextureRect(sf::IntRect(0, 0, 512, 512));
	rect.setTexture(target->GetTexture(TEX_ICON_IMPORTANT));
	target->GetRenderWindow()->draw(rect);

}

void MessagePanel::Draw(RenderingEngine *target)
{
	switch (m_state)
	{
	case MSG_ST_WELCOME:
		DrawWelcome(target);
		break;
	case MSG_ST_NOHIT:
		DrawNohit(target);
		break;
	case MSG_ST_INFO:
		DrawInfo(target);
		break;
	default:
		break;
	}
	

	return;
}

void MessagePanel::Update(sf::Clock *time)
{
	GUIPanel::Update(time);

	float dttime = m_startClock.getElapsedTime().asSeconds();

	if (m_started == false && m_startClock.getElapsedTime().asSeconds() > 30.00)
	{
		OnStart(NULL);
	}

	if (m_state == MSG_ST_WELCOME && dttime > MSG_WELCOME_DELAY)
	{
		m_state = MSG_ST_NOHIT;

		m_alpha = 0;
		tween::TweenerParam param2(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param2.addProperty(&m_alpha, 255);
		m_gm->m_tweener.addTween(param2);
		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/caution.wav"));
	}
	else if (m_state == MSG_ST_NOHIT && dttime > MSG_NOHIT_DELAY)
	{
		m_state = MSG_ST_INFO;

		m_displayTimer = 0;
		tween::TweenerParam param(MSG_INFO_DELAY, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_displayTimer, 200);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);

		m_alpha = 0;
		tween::TweenerParam param2(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param2.addProperty(&m_alpha, 255);
		m_gm->m_tweener.addTween(param2);

		m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("Voice/information.wav"));
	}

	if (m_delayComplete == false && (int)m_displayTimer == 200)
	{
		m_delayComplete = true;
		tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_posXWdg, 0);
		param.addProperty(&m_alphaWdg, 255);
		m_gm->m_tweener.addTween(param);
	}
	if (m_started == true && (int)m_alpha == 0)
	{
		m_gm->m_gsm.ChangeState(GMST_GAMEMODE_SELECT);
		//m_gm->m_gsm.ChangeState(GMST_MUSIC_SELECT);
		m_started = false;
	}
}

void MessagePanel::ReceiveEvent(GUIEVENT ev, void *param)
{
	m_keyboard.ReceiveEvent(ev, param);
	GUIPanel::ReceiveEvent(ev, param);
}

void MessagePanel::OnStart(GUIWidget *btn)
{
	if (m_started == false && m_delayComplete == true)
	{
		m_started = true;
		m_alpha = 255;

		tween::TweenerParam param(800, tween::CUBIC, tween::EASE_OUT); //2 seconds tween
		param.addProperty(&m_alpha, 0);
		param.addProperty(&m_alphaWdg, 0);
		param.forceEnd = true;
		m_gm->m_tweener.addTween(param);
		//m_gm->m_sounds.PlaySnd(SOUNDDIR + std::string("click.ogg"));
		IGamemode* gmmode = m_gm->m_gmm->GetGamemodeFromId(0);

		gmmode->OnGamemodeConfirmed();
		m_gm->m_credits.Value(m_gm->m_credits.Value() - gmmode->GetCreditPrice());
		m_gm->m_gmm->SetMode(gmmode->GetId());
	}
}