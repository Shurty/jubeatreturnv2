
#include "StateMusicEnd.h"

StateMusicEnd::StateMusicEnd(JRExe& core)
	: GameState(), m_core(core)
{
	m_id = GMST_MUSIC_END;
	m_name = "End of music";
}

void StateMusicEnd::OnInit(Parameters &pm)
{
	Logger::Log << "StateMusicEnd: OnInit" << std::endl;

}

void StateMusicEnd::OnStart(Parameters &pm)
{
	// FIXME: check if init has been correctly done ?
}

void StateMusicEnd::OnRender(Parameters &pm)
{
}

void StateMusicEnd::OnUpdate(Parameters &pm)
{
	Logger::Log << "StateMusicEnd: update" << std::endl;
}

void StateMusicEnd::OnEnd(Parameters &pm)
{
}

void StateMusicEnd::OnUnload(Parameters &pm)
{
}
