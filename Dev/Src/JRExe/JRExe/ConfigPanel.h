
#pragma once

#include <JREngine/jrgui.h>
#include <SFML/Audio.hpp>
#include "JRExe.h"
#include "GUIPanel.h"
#include "GUIBeatGraph.h"

#define MAXLAYOUT_ITEMS			24

class JRExe;
class RenderingEngine;
struct Beatmap;

class ConfigPanel : public GUIPanel 
{
private:
	GUIKeyboard m_keyboard;
	JRExe *m_gm;
	float m_posX;
	float m_posXItm;
	float m_alpha;
	float m_alphaItm;

	bool m_showBackBtn;

	int m_selectOffset;
	// When the Back or Start button is preseed
	bool m_goBack;
	// When a selection is done on a "changeLayout" or "changePanel" button
	bool m_selectAction;
	// When a selection is done, current button id clicked with offset applied
	int m_selectActionIdx;

	enum CFLAYOUTID
	{
		// Main menu
		CFLAY_CONFIG,
			// Subs
			CFLAY_FILTER,
			CFLAY_SORTING,
			CFLAY_ACTIVATORS,
			CFLAY_MODS,
			CFLAY_ACHIEVEMENTS,
			CFLAY_STATS,
			CFLAY_DEBUG,
	};

	struct ConfigLayoutItem
	{
		sf::Texture *tex;
		std::string text;
		bool hideLabel;
		bool set;

		ConfigLayoutItem()
		{
			hideLabel = false;
			set = false;
			tex = NULL;
		}
	};

	struct ConfigLayout
	{
		int entriesCnt;
		ConfigLayoutItem items[MAXLAYOUT_ITEMS];
		CFLAYOUTID id;

		ConfigLayout(JRExe *jre, CFLAYOUTID _id)
			: id(_id)
		{
			for (int i = 0; i < MAXLAYOUT_ITEMS; i++)
			{
				items[i].set = false;
			}
			entriesCnt = MAXLAYOUT_ITEMS;
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{

		}
	};

	ConfigLayout *m_activeLayout;

	struct MainMenuLayout : public ConfigLayout {
		MainMenuLayout(JRExe *jre)
		: ConfigLayout(jre, CFLAY_CONFIG)
		{
			int i = 0;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_FILTERS_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Filters";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_SORTING_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Sorting";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_ACTIVATORS_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Activators";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_MODS_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Mods";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_ACHIEVEMENTS_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Achievements";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_STATS_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Stats";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_GAMEMODES_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Gamemode";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_RESET_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Exit";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_MENU_DEBUG_BTN);
			items[i].set = true;
			items[i].hideLabel = true;
			items[i].text = "Debug";
			i++;

			entriesCnt = i;
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{
			float ratio = target->GetRenderingRatio();

			sf::RectangleShape rect;
			sf::Text text;
			sf::FloatRect textRect;

			/* Panel title */
			text.setPosition(x1, y1);
			text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(60 * ratio);
			text.setString("Select a category");
			target->GetRenderWindow()->draw(text);
		}

	} m_main;

	struct FilterLayout : public ConfigLayout {
		FilterLayout(JRExe *jre)
		: ConfigLayout(jre, CFLAY_FILTER)
		{
			items[0].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[0].set = true;
			items[0].text = "All";

			int i;
			for (i = 1; i <= 10; i++)
			{
				items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
				items[i].set = true;
				items[i].text = "Level " + std::to_string(i);
			}
			/*

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "Dubstep";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "JPop";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "Jubeat Classic";
			i++;
			*/
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{
			float ratio = target->GetRenderingRatio();

			sf::RectangleShape rect;
			sf::Text text;
			sf::FloatRect textRect;

			/* Panel title */
			text.setPosition(x1, y1);
			text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(60 * ratio);
			text.setString("Filters");
			target->GetRenderWindow()->draw(text);

			text.setPosition(x1, y1 + 80 * ratio);
			text.setFont(target->GetFont(FONT_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(40 * ratio);
			text.setString("Allows you to change the listing of musics\
						    \nDisplay only specific levels, music types, categories...");
			target->GetRenderWindow()->draw(text);
		}

	} m_filters;

	struct SortingLayout : public ConfigLayout {
		SortingLayout(JRExe *jre)
		: ConfigLayout(jre, CFLAY_SORTING)
		{
			int i = 0;

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "Reverse order";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "By difficulty";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "By name";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "By author";
			i++;
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{
			float ratio = target->GetRenderingRatio();

			sf::RectangleShape rect;
			sf::Text text;
			sf::FloatRect textRect;

			/* Panel title */
			text.setPosition(x1, y1);
			text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(60 * ratio);
			text.setString("Sorting");
			target->GetRenderWindow()->draw(text);

			text.setPosition(x1, y1 + 80 * ratio);
			text.setFont(target->GetFont(FONT_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(40 * ratio);
			text.setString("Allows you to change the display order of musics\
						  \nSort them by name, level, difficulty, score...");
			target->GetRenderWindow()->draw(text);
		}
	} m_sorting;

	struct ActivatorsLayout : public ConfigLayout {
		ActivatorsLayout(JRExe *jre)
		: ConfigLayout(jre, CFLAY_ACTIVATORS)
		{
			for (int i = 0; i < MAXACTIVATORS; i++)
			{
				items[i].tex = jre->GetRenderer()->GetActivatorTexture(i)->GetTexture();
				items[i].set = true;
				items[i].hideLabel = true;
				items[i].text = "Activator";
			}
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{
			float ratio = target->GetRenderingRatio();

			sf::RectangleShape rect;
			sf::Text text;
			sf::FloatRect textRect;

			/* Panel title */
			text.setPosition(x1, y1);
			text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(60 * ratio);
			text.setString("Activators");
			target->GetRenderWindow()->draw(text);

			text.setPosition(x1, y1 + 80 * ratio);
			text.setFont(target->GetFont(FONT_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(40 * ratio);
			text.setString("Select a custom PRESS button when playing !\
						  \nYou can also customize your animated background\
						  \nSome activators and backgrounds are unlocked by achievements");
			target->GetRenderWindow()->draw(text);
		}
	} m_activators;

	struct ModsLayout : public ConfigLayout {
		ModsLayout(JRExe *jre)
		: ConfigLayout(jre, CFLAY_MODS)
		{
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{
			float ratio = target->GetRenderingRatio();

			sf::RectangleShape rect;
			sf::Text text;
			sf::FloatRect textRect;

			/* Panel title */
			text.setPosition(x1, y1);
			text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(60 * ratio);
			text.setString("Modifiers");
			target->GetRenderWindow()->draw(text);

			text.setPosition(x1, y1 + 80 * ratio);
			text.setFont(target->GetFont(FONT_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(40 * ratio);
			text.setString("Increase or decrase the difficulty of the game\
						  \nAdd some special events, special conditions or actions\
						  \nModifiers depends of the gamemode you selected");
			target->GetRenderWindow()->draw(text);
		}
	} m_mods;

	struct AchievementsLayout : public ConfigLayout {
		AchievementsLayout(JRExe *jre)
		: ConfigLayout(jre, CFLAY_ACHIEVEMENTS)
		{
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{
			float ratio = target->GetRenderingRatio();

			sf::RectangleShape rect;
			sf::Text text;
			sf::FloatRect textRect;

			/* Panel title */
			text.setPosition(x1, y1);
			text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(60 * ratio);
			text.setString("Achievements");
			target->GetRenderWindow()->draw(text);

			text.setPosition(x1, y1 + 80 * ratio);
			text.setFont(target->GetFont(FONT_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(40 * ratio);
			text.setString("Check your progress on Jubeat Return Achievements\
						  \nRemember some achievements are hidden");
			target->GetRenderWindow()->draw(text);
		}
	} m_achievements;

	struct StatsLayout : public ConfigLayout {
		StatsLayout(JRExe *jre)
		: ConfigLayout(jre, CFLAY_STATS)
		{
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{
			float ratio = target->GetRenderingRatio();

			sf::RectangleShape rect;
			sf::Text text;
			sf::FloatRect textRect;

			/* Panel title */
			text.setPosition(x1, y1);
			text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(60 * ratio);
			text.setString("Statistics");
			target->GetRenderWindow()->draw(text);

			text.setPosition(x1, y1 + 80 * ratio);
			text.setFont(target->GetFont(FONT_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(40 * ratio);
			text.setString("Check your play informations and stats\
						  \nAll the statistics of your playtime are bound to your JReturn account");
			target->GetRenderWindow()->draw(text);
		}
	} m_stats;

	struct DebugLayout : public ConfigLayout {
		DebugLayout(JRExe *jre)
		: ConfigLayout(jre, CFLAY_DEBUG)
		{
			int i = 0;
			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "Parameters\nReload()";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "SongsLibrary\nParseAll(c.beatmaps)";
			i++;

			items[i].tex = jre->m_render.GetTexture(TEX_COVER_BG_SMALL);
			items[i].set = true;
			items[i].text = "SongsLibrary\nParseAll(beatmaps)";
			i++;
		}

		virtual void Render(RenderingEngine *target, float m_alpha, float x1, float y1, float x2, float y2)
		{
			float ratio = target->GetRenderingRatio();

			sf::RectangleShape rect;
			sf::Text text;
			sf::FloatRect textRect;

			/* Panel title */
			text.setPosition(x1, y1);
			text.setFont(target->GetFont(FONT_TOP_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(60 * ratio);
			text.setString("Debug");
			target->GetRenderWindow()->draw(text);

			text.setPosition(x1, y1 + 80 * ratio);
			text.setFont(target->GetFont(FONT_TITLE_TX));
			text.setColor(sf::Color(255, 255, 255, m_alpha * 0.4));
			text.setOrigin(0, 0);
			text.setCharacterSize(40 * ratio);
			text.setString("Debug and dev functions\
				\nCRUD on elements and data");
			target->GetRenderWindow()->draw(text);
		}
	} m_debug;


	/*
	MainMenuLayout m_main;
	SortingLayout m_sort;*/

public:
	ConfigPanel(JRExe *gm);
	~ConfigPanel();

	virtual void Init();
	virtual void Start();
	virtual void Stop();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
	virtual void OnStart(GUIWidget *btn);
	virtual void OnSelect(GUIWidget *btn);

	virtual void OnNext(GUIWidget *btn);
	virtual void OnPrevious(GUIWidget *btn);
};