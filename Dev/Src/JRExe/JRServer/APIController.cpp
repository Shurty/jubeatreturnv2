/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

APIController::APIController()
: m_port(52040)
{

	// TCP init
	Logger::Log << "APIController: TPC Server init started" << std::endl;
	while (m_local.listen(m_port) != sf::Socket::Done) {
		Logger::Log << "APIController: Port " << m_port << " unavailable" << std::endl;
		m_port++;
	}
	Logger::Log << "APIController: TPC Listener Started on " << m_port << std::endl;
	m_local.setBlocking(false);
}

APIController::~APIController()
{
	while (!m_clients.empty()) {
		delete m_clients.back();
		m_clients.pop_back();
	}

	while (!m_commands.empty()) {
		delete m_commands.back();
		m_commands.pop_back();
	}
}

void APIController::Init(ServerController* sc)
{
	// Commands
	Logger::Log << "APIController: Setting up commands" << std::endl;
	m_commands.push_back(new CommandPing());
	m_commands.push_back(new CommandHello(sc));
	m_commands.push_back(new APIGetScore(sc));
}

void APIController::AcceptClients()
{
	if (m_clients.size() == 0) {
		m_clients.push_back(new Client(0));
	}
	Client *client = m_clients.back();
	if (m_local.accept(client->GetSock()) == sf::Socket::Done)
	{
		client->OnConnect();
		Logger::Log << "APIController: Client #" << client->GetId() << " connection from " << client->GetSock().getRemoteAddress().toString() << std::endl;
		Logger::Log << "APIController: Login timeout started on client #" << client->GetId() << std::endl;

		// New inactive client for next operation
		m_clients.push_back(new Client(client->GetId() + 1));
	}
}

void APIController::VerifyClients()
{
	/* Main socket verification */
	std::for_each(m_clients.begin(), --m_clients.end(), [this](Client *cl) {
		if (m_selector.isReady(cl->GetSock())) {
			// Client is still connected
			cl->GetTimeoutClock().restart();
		}
		else if (cl->GetTimeoutClock().getElapsedTime().asSeconds() > 10) {
			cl->MarkAsDisconnected();
		}
	});

	std::vector<Client *>::iterator it;
	std::vector<Client *>::iterator endIt = --m_clients.end();

	/* Timeout application */
	it = m_clients.begin();
	while (it != endIt)
	{
		if ((*it)->IsConnected() == false) {
			Logger::Log << "APIController: Client #" << (*it)->GetId() << " timed out" << std::endl;
			delete (*it);
			m_clients.erase(it);
			it = m_clients.begin();
			endIt = --m_clients.end();
		}
		else {
			++it;
		}
	}
}

void APIController::Run(ServerController* sc)
{
	std::stringstream res;

	// Accept new connections
	AcceptClients();

	std::for_each(m_clients.begin(), --m_clients.end(), [this](Client *cl) {
		m_selector.add(cl->GetSock());
	});
	m_selector.wait(SRV_SELECT_DT);

	// Verify active connections (timeout login/dc)
	VerifyClients();

	// Receive incoming messages
	ReceiveMsgs();

	// Update clients on their current task
	UpdateClients();

	m_selector.clear();
}

void APIController::ReceiveMsgs()
{
	std::for_each(m_clients.begin(), --m_clients.end(), [this](Client *cl) {
		if (m_selector.isReady(cl->GetSock())) {

			char buffer[1024];
			std::size_t received = 0;
			std::stringstream ss;
			//sf::Packet m_packet;
			std::string cmd;
			sf::Socket::Status st;

			st = cl->GetSock().receive(buffer, sizeof(buffer), received);
			if (st == sf::Socket::Disconnected) {
				cl->MarkAsDisconnected();
				return;
			}
			if (received > 1024)
				received = received - 1;
			buffer[received] = 0;
			ss.str(buffer);
			ss >> cmd;
			this->ParsePacket(cl, cmd, ss);
		}
	});
}

void APIController::UpdateClients()
{

}

void APIController::ParsePacket(Client *cl, std::string const &id, std::stringstream &pct)
{
	std::for_each(m_commands.begin(), m_commands.end(), [&cl, &id, &pct, this](Command *cmd)
	{
		if (cmd->GetID() == id) {
			if (id != "PING") { // ignore ping logging
				Logger::Log << "APIController: " << cmd->GetID().toAnsiString() << " from Client " << cl->GetId() << std::endl;
			}
			cmd->ReceiveAPIAction(cl, pct);
		}
	});
}