/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <functional>
#include <windows.h>
#include <JREngine\jrshared.h>

#include "SongLibrary.h"
#include "APIController.h"
#include "ServerController.h"

#include "Matchmaking.hpp"
#include "Ping.hpp"
#include "APIGetScore.hpp"
#include "Hello.hpp"

// TODO: reference additional headers your program requires here
