
#include "stdafx.h"

SongLibrary::SongLibrary()
{
	m_parsers["txt"] = new MemoParser();
	m_parsers["jcm"] = m_parsers["txt"];
	m_parsers["jrb"] = new JRBParser();
	Logger::Log << "Module construct: SongLibrary OK" << std::endl;
}

SongLibrary::~SongLibrary()
{
}

void SongLibrary::ParseAll(std::string const &dirsrc)
{
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;
	Beatmap *bmp;
	std::string activeSrc;

	Logger::Log << "SongLibrary: Loading handle " << dirsrc << std::endl;
	hFind = FindFirstFile((dirsrc + "*").c_str(), &ffd);
	if (INVALID_HANDLE_VALUE == hFind)
	{
		Logger::Log << "SongLibrary Error: Invalid handle " << dirsrc << std::endl;
		// No files
		return;
	}
	else
	{
		do {
			activeSrc = ffd.cFileName;
			if (activeSrc[0] != '.') {
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					ParseAll(dirsrc + "" + activeSrc + "/");
				}
				else {
					std::string ext = activeSrc.substr(activeSrc.length() - 3, 3);

					if (!m_parsers[ext])
						ext = "txt";
					bmp = m_parsers[ext]->ParseBeatmap(dirsrc + "" + activeSrc, false);
					bmp->GenerateHash();
					Logger::Log << "SongLibrary: Added " << std::string(bmp->name.begin(), bmp->name.end()) << " " << bmp->level << std::endl;
					m_beatmaps.push_back(bmp);
				}
			}
		} while (FindNextFile(hFind, &ffd) != 0);
	}
}

void SongLibrary::ParseFreemaps()
{
	Json::Value root;   // will contains the root value after parsing
	Json::Reader reader;
	std::ifstream file("Data/beatmaps_config.txt");


	if (!file)
	{
		Logger::Log << "Data/beatmaps_config.txt reading failed" << std::endl;
		return;
	}
	bool parsingSuccessful = reader.parse(file, root);
	file.close();
	if (!parsingSuccessful)
	{
		// report to the user the failure and their locations in the document.
		Logger::Log << "Failed to parse beatmaps_config\n"
			<< reader.getFormattedErrorMessages() << std::endl;
		return ;
	}

	SongLibrary* me = this;
	std::for_each(root["free"]["src"].begin(), root["free"]["src"].end(), [&me](Json::Value val)
	{
		std::for_each(me->m_beatmaps.begin(), me->m_beatmaps.end(), [&me, &val](Beatmap *bt) {
			if (bt->src == val.asString()) {
				me->m_freeBeatmaps.push_back(bt);
				Logger::Log << "Beatmap from source " << bt->src << " added to 'Freelist'" << std::endl;
			}
		});
	});
}

void SongLibrary::Update()
{

}

void SongLibrary::Filter(bool(*sorter)(Beatmap *))
{
	m_filteredBeatmaps.clear();
	std::for_each(m_beatmaps.begin(), m_beatmaps.end(), [&sorter, this](Beatmap *) {

	});
}

void SongLibrary::Clear()
{
	while (!m_beatmaps.empty())
	{
		delete m_beatmaps.back();
		m_beatmaps.pop_back();
	}
}

Beatmap *SongLibrary::GetBeatmap(int idx)
{
	/*
	idx = idx % m_beatmaps.size();
	if (idx < 0)
	idx += m_beatmaps.size();
	*/
	if (idx < 0 || idx >= m_filteredBeatmaps.size())
		return (NULL);
	return (m_filteredBeatmaps[idx]);
}
