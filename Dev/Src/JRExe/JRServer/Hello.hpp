/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class CommandHello : public AHello
{
private:
	ServerController* m_srv;

public:
	CommandHello(ServerController* srv)
		: AHello(), m_srv(srv)
	{

	}

	CommandHello::~CommandHello()
	{

	}

	virtual void Receive(Client *cl, std::string const &hashcode, bool isapi)
	{
		if (isapi == false)
		{
			if (m_srv->ValidateClient(cl, hashcode)) {
				cl->SetValid();
				Logger::Log << "CommandHello: Confirmed validity for client #" << cl->GetId() << std::endl;
				Send(cl, cl->GetHashcode(), false);
			}
			else {
				Logger::Log << "CommandHello: Incorrect hash for client #" << cl->GetId() << std::endl;

				//Command* cmd = (Command*)m_srv->GetCommand("INVALID_CLIENT");
				//sf::Packet pck;

				//pck << CL_ERR_INVALID_HASH << "Invalid license hash"; 
				//cmd->SendAction(cl, pck);

				cl->MarkAsDisconnected();
			}
		}
		else
		{
			cl->SetValid();
			Send(cl, "anon", isapi);
		}
	}

	virtual void Send(Client *cl, std::string const &hashcode, bool isapi)
	{
		if (isapi == false)
		{
			sf::Packet pck;

			std::cout << "Sending cl valid token " << GetID().toAnsiString() << std::endl;
			pck << GetID() << hashcode;
			cl->GetSock().send(pck);
		}
		else
		{
			Json::StyledWriter writer;
			Json::Value res;

			res["valid"] = true;
			writer.write(res);
			cl->GetSock().send(res.toStyledString().c_str(), res.toStyledString().size() + 1);
		}
	}
};
