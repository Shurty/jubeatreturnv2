/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

ServerController::ServerController()
	: m_port(52000)
{
	// Commands
	Logger::Log << "ServerController: Setting up commands" << std::endl;
	m_commands.push_back(new CommandPing());
	m_commands.push_back(new CommandHello(this));

	// UDP init
	Logger::Log << "ServerController: UDP Server init started" << std::endl;

	// TCP init
	Logger::Log << "ServerController: TPC Server init started" << std::endl;
	while (m_local.listen(m_port) != sf::Socket::Done) {
		Logger::Log << "Port " << m_port << " unavailable" << std::endl;
		m_port++;
	}
	Logger::Log << "ServerController: TPC Listener Started on " << m_port << std::endl;
	m_local.setBlocking(false);

	m_api.Init(this);
	m_songs.ParseAll(CBEATMAPSDIR);
	m_songs.ParseFreemaps();
}

ServerController::~ServerController()
{
	while (!m_clients.empty()) {
		delete m_clients.back();
		m_clients.pop_back();
	}

	while (!m_commands.empty()) {
		delete m_commands.back();
		m_commands.pop_back();
	}
}

void ServerController::AcceptClients()
{
	if (m_clients.size() == 0) {
		m_clients.push_back(new Client(0));
	}
	Client *client = m_clients.back();
	if (m_local.accept(client->GetSock()) == sf::Socket::Done)
	{
		client->OnConnect();
		Logger::Log << "ServerController: Client #" << client->GetId() << " connection from " << client->GetSock().getRemoteAddress().toString() << std::endl;
		Logger::Log << "ServerController: Login timeout started on client #" << client->GetId() << std::endl;

		// New inactive client for next operation
		m_clients.push_back(new Client(client->GetId() + 1));
	}
}

void ServerController::VerifyClients()
{
	/* Main socket verification */
	std::for_each(m_clients.begin(), --m_clients.end(), [this](Client *cl) {
		if (m_selector.isReady(cl->GetSock())) {
			// Client is still connected
			if (cl->IsValid())
				cl->GetTimeoutClock().restart();
		}
		else if (cl->GetTimeoutClock().getElapsedTime().asSeconds() > 10) {
			cl->MarkAsDisconnected();
		}
	});

	std::vector<Client *>::iterator it;
	std::vector<Client *>::iterator endIt = --m_clients.end();

	/* Timeout application */
	it = m_clients.begin();
	while (it != endIt)
	{
		if ((*it)->IsConnected() == false) {
			Logger::Log << "ServerController: Client #" << (*it)->GetId() << " timed out" << std::endl;
			delete (*it);
			m_clients.erase(it);
			it = m_clients.begin();
			endIt = --m_clients.end();
		} else {
			++it;
		}
	}
}

void ServerController::Run()
{
	std::stringstream res;
	
	Logger::Log << "Start main rountine" << std::endl;
	while (1)
	{
		// Accept new connections
		AcceptClients();

		if (m_clients.size() > 1)
		{
			std::for_each(m_clients.begin(), --m_clients.end(), [this](Client *cl) {
				m_selector.add(cl->GetSock());
			});
		}
		m_selector.wait(SRV_SELECT_DT);

		// Verify active connections (timeout login/dc)
		VerifyClients();

		// Receive incoming messages
		ReceiveMsgs();

		// Update clients on their current task
		UpdateClients();

		m_selector.clear();

		m_api.Run(this);
	}
}

void ServerController::ReceiveMsgs()
{
	std::for_each(m_clients.begin(), --m_clients.end(), [this](Client *cl) {
		if (m_selector.isReady(cl->GetSock())) {
			sf::Packet m_packet;
			sf::String cmd;
			sf::Socket::Status st;
			
			st = cl->GetSock().receive(m_packet);
			if (st == sf::Socket::Disconnected) {
				cl->MarkAsDisconnected();
				return ;
			}
			m_packet >> cmd;
			this->ParsePacket(cl, cmd, m_packet);
		}
	});
}

void ServerController::UpdateClients()
{

}

void ServerController::ParsePacket(Client *cl, std::string const &id, sf::Packet &pct)
{
	std::for_each(m_commands.begin(), m_commands.end(), [&cl, &id, &pct, this](Command *cmd)
	{
		if (cmd->GetID() == id) {
			if (id != "PING") { // ignore ping logging
				Logger::Log << "ServerController: " << cmd->GetID().toAnsiString() << ": from Client " << cl->GetId() << std::endl;
			}
			cmd->ReceiveAction(cl, pct);
		}
	});
}

unsigned int ServerController::GetBoothScore(int booth)
{
	static int score = 0;

	score += rand() % 10000;
	return (score);
}

bool ServerController::ValidateClient(Client *cl, std::string const &hash)
{
	// ignore for now
	return (true);
}

