/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class Licenser
{
private:

	std::string m_lcHash[] = {
		"ec*A(7877f8we0fDS&*FDS7g8q09fdsvc87v79rw^RF",
		"poiqicwj)(*U#@*ijoev)(UGHJVO091du83HJCOey7(",
		"k9-02UF09Jiov0V(uf*#@pAVK_s[-=2F9EWUIJVPosD",
	};

public:
	Licenser(int id = -1);
	~Licenser();


};
