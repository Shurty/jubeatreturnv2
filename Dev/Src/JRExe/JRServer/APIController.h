/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

#define SRV_SELECT_DT		sf::milliseconds(40)

class ServerController;

class APIController
{
private:
	int m_port;
	sf::TcpListener m_local;
	std::vector<Client *> m_clients;
	std::vector<Command *> m_commands;
	sf::SocketSelector m_selector;

public:
	APIController();
	~APIController();

	void Init(ServerController* sc);
	void VerifyClients();
	void ReceiveMsgs();
	void UpdateClients();
	void AcceptClients();
	void ParsePacket(Client *cl, std::string const &id, std::stringstream &pct);
	void Run(ServerController* sc);
};
