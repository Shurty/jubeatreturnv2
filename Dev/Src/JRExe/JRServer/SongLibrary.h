#pragma once

//#include <functional>
#include "stdafx.h"

#define PREVIEW_MAX_LENGTH			20

class SongLibrary
{
private:
	std::map<std::string, Parser *>										m_parsers;

	// This is the source beatmaps list (all maps)
	std::vector<Beatmap *>												m_beatmaps;
	// This is the list of beatmaps available for free
	std::vector<Beatmap *>												m_freeBeatmaps;
	// This is the list of beatmaps as filtered
	std::vector<Beatmap *>												m_filteredBeatmaps;


public:

	SongLibrary();
	~SongLibrary();

	// Apply a filter on available selection system
	void Filter(bool(*sorter)(Beatmap *));

	void ParseFreemaps();
	void ParseAll(std::string const &dir);
	void Update();
	void Clear();

	Beatmap *GetBeatmap(int idx);
};
