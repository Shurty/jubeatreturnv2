/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class CommandPing : public ACommandPing
{
private:

public:
	CommandPing() 
		: ACommandPing()
	{

	}

	CommandPing::~CommandPing()
	{

	}

	virtual void Receive(Client *cl, bool isapi)
	{
		sf::Packet pct;

		if (!cl->IsValid())
			return;
		Logger::Log << "PING from Client " << cl->GetId() << std::endl;
		cl->GetTimeoutClock().restart();
		if (isapi) {
			Send(cl, isapi);
		}
		else {
			SendAction(cl, pct);
		}
	}

	virtual void Send(Client *cl, bool isapi)
	{
		if (isapi)
		{
			Json::StyledWriter writer;
			Json::Value res;

			res["ping"] = "pong";
			writer.write(res);
			cl->GetSock().send(res.toStyledString().c_str(), res.toStyledString().size() + 1);
		}
		else {
			sf::Packet pck;

			pck << GetID() << "PONG";
			cl->GetSock().send(pck);
		}
	}
};
