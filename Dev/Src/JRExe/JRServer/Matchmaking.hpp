/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class CommandMatchmaking : public AMatchmaking
{
private:

public:
	CommandMatchmaking() 
		: AMatchmaking()
	{

	}

	CommandMatchmaking::~CommandMatchmaking()
	{

	}

	virtual void Receive(Client *cl, std::string const &pl, int song)
	{
		Logger::Log << "Matchmaking started for player " << cl->GetId() << " on #" << song << std::endl;
		cl->OnMatchmakingBegin();
		cl->SetMatchmaking(song);
	}

	virtual void Send(Client *cl, std::string const &pl, int song)
	{
		sf::Packet pck;

		Logger::Log << "Matchmaking player " << cl->GetId() << " with " << pl << std::endl;
		pck << GetID() << pl;
		cl->GetSock().send(pck);
	}
};