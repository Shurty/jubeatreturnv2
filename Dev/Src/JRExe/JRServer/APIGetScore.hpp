/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class APIGetScore : public AAPIGetScore
{
private:
	ServerController* m_srv;

public:
	APIGetScore(ServerController* srv)
		: AAPIGetScore(), m_srv(srv)
	{

	}

	APIGetScore::~APIGetScore()
	{

	}

	virtual void Receive(Client *cl, unsigned int id)
	{
		Logger::Log << "Score request recieved " << cl->GetId() << " for booth #" << id << std::endl;

		if (!cl->IsValid())
			return;
		unsigned int score = m_srv->GetBoothScore(id);
		Send(cl, score);
	}

	virtual void Send(Client *cl, unsigned int song)
	{
		Logger::Log << "Sending booth score to " << cl->GetId() << std::endl;
		
		Json::StyledWriter writer;
		Json::Value res;

		res["score"] = song;
		writer.write(res);
		cl->GetSock().send(res.toStyledString().c_str(), res.toStyledString().size() + 1);
	}
};