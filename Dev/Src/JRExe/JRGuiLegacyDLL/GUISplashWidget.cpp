#include <JREngine/jrgui.h>

GUISplashWidget::GUISplashWidget(int ex)
	: m_delayIdt(ex)
{

}

GUISplashWidget::~GUISplashWidget()
{

}


void GUISplashWidget::Init()
{

}

void GUISplashWidget::Draw(GraphicsController *target)
{
	sf::RectangleShape rectangle(m_size);
	int dt = m_clk.getElapsedTime().asMilliseconds() % 8145;

	if (m_parent)
		rectangle.setPosition(m_position + m_parent->GetPosition());
	else
		rectangle.setPosition(m_position);

	rectangle.setFillColor(sf::Color(0, 0, 0, 50));
	
	if (dt > m_delayIdt && dt < m_delayIdt + 180) {
		rectangle.setFillColor(sf::Color(50, 50, 50, 50));
	}

	if (m_pressed) {
		rectangle.setOutlineThickness(2);
		rectangle.setOutlineColor(sf::Color(255, 100, 60, 150));
	} else {
		/*
		rectangle.setOutlineThickness(0);
		rectangle.setOutlineColor(sf::Color(100, 100, 100, 150));
		*/
	}
	
	target->GetWindow()->draw(rectangle);
}

void GUISplashWidget::Update(sf::Clock *time)
{
	
}

void GUISplashWidget::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
}