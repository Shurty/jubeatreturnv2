#include <JREngine/jrgui.h>

GUIConfigButton::GUIConfigButton()
{
	m_textSize = 20;
	m_config = NULL;
	m_font.loadFromFile("Fonts/ARIALN.ttf");
}

GUIConfigButton::~GUIConfigButton()
{
}

void GUIConfigButton::Init()
{

}

void GUIConfigButton::DrawBackground(GraphicsController *target)
{
	sf::VertexArray triangle(sf::TrianglesStrip, 4);
	sf::VertexArray lines(sf::LinesStrip, 5);
	sf::Vector2f size = GetSize();
	sf::Vector2f pos = GetPosition();
	sf::RectangleShape rectangle(m_size);

	if (m_parent) {
		pos += GetParent()->GetPosition();
	}

	//if (m_pressed) {
	//	
	//}

	rectangle.setPosition(pos);
	//rectangle.setFillColor(sf::Color(0, 0, 0, 50));
	//rectangle.setOutlineThickness(2);
	//rectangle.setOutlineColor(sf::Color(255, 100, 60, 150));
	rectangle.setTexture(&(m_config->texture));
	target->GetWindow()->draw(rectangle);

	/*lines[0].position = pos + sf::Vector2f(3, 3);
	lines[1].position = pos + sf::Vector2f(3, size.y - 3);
	lines[2].position = pos + sf::Vector2f(size.x - 3, size.y - 3);
	lines[3].position = pos + sf::Vector2f(size.x - 3, 3);
	lines[4].position = pos + sf::Vector2f(3, 3);
	lines[0].color = sf::Color(120, 120, 120, 160);
	lines[1].color = sf::Color(80, 80, 80, 160);
	lines[2].color = sf::Color(120, 120, 120, 160);
	lines[3].color = sf::Color(80, 80, 80, 160);
	lines[4].color = sf::Color(120, 120, 120, 160);
	target->GetWindow()->draw(lines);

	triangle[0].position = pos + sf::Vector2f(5, 5);
	triangle[1].position = pos + sf::Vector2f(5, size.y - 5);
	triangle[2].position = pos + sf::Vector2f(size.x - 5, 5);
	triangle[3].position = pos + sf::Vector2f(size.x - 5, size.y - 5);
	triangle[0].color = sf::Color(50, 50, 50, 140);
	triangle[1].color = sf::Color(110, 110, 110, 140);
	triangle[2].color = sf::Color(110, 110, 110, 140);
	triangle[3].color = sf::Color(50, 50, 50, 140);
	target->GetWindow()->draw(triangle);*/
}

void GUIConfigButton::Draw(GraphicsController *target)
{
	sf::Vector2f pos = GetPosition();

	if (m_parent)
		pos += GetParent()->GetPosition();

	if (m_config) {
		sf::RectangleShape rectangle(m_size);
		sf::FloatRect textRect;
		sf::Text tx;

		DrawBackground(target);

		// Temporary text size scaling depending of the width
		m_textSize = (int) (m_size.x / 5.5f);

		tx.setCharacterSize(m_textSize);
		tx.setFont(m_font);
		tx.setColor(sf::Color(255, 255, 255, 255));
		tx.setString(*m_config->valueIt);
		tx.setCharacterSize((unsigned int) (m_textSize / 1.25f));
		textRect = tx.getLocalBounds();
		tx.setOrigin(textRect.left + textRect.width/2.0f,
			textRect.top + textRect.height/2.0f);
		tx.setPosition(pos + sf::Vector2f(m_size.x / 2.0f, m_size.y * 0.65f));
		target->GetWindow()->draw(tx);
	}
}

void GUIConfigButton::Update(sf::Clock *time)
{
	GUIWidget::Update(time);
}

void GUIConfigButton::OnPressed()
{
	GUIWidget::OnPressed();
	
	if (!m_config)
		return ;
	m_config->valueIt++;
	if (m_config->valueIt == m_config->values.end()) {
		m_config->valueIt = m_config->values.begin();
	}
	/* Update config value */
}

void GUIConfigButton::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
}

void GUIConfigButton::SetConfigEntry(ConfigEntry *res) {
	m_config = res;
}

ConfigEntry *GUIConfigButton::GetConfigEntry() const { return (m_config); }

