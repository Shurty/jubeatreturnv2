#include <JREngine/jrgui.h>

GUIActionImageButton::GUIActionImageButton(std::string const& image) : GUIActionButton()
{
	m_bg_texture.loadFromFile("Textures/Design/back_square.png");
	m_texture.loadFromFile(image);
}

void GUIActionImageButton::Draw(GraphicsController *target)
{
	sf::Vector2f size = GetSize();
	sf::Vector2f pos = GetPosition();

	sf::RectangleShape rectangleBg(m_size);
	sf::RectangleShape rectangle(m_size);

	if (m_parent)
		pos += GetParent()->GetPosition();

	rectangleBg.setPosition(pos);
	rectangleBg.setTexture(&m_bg_texture);
	rectangle.setPosition(pos);
	rectangle.setTexture(&m_texture);
	target->GetWindow()->draw(rectangleBg);
	target->GetWindow()->draw(rectangle);
}