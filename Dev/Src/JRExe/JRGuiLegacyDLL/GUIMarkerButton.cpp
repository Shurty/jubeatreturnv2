#include <JREngine/jrgui.h>

GUIMarkerButton::GUIMarkerButton()
{
	m_config = NULL;
}

GUIMarkerButton::~GUIMarkerButton()
{
}

void GUIMarkerButton::Init()
{

}

void GUIMarkerButton::DrawBackground(GraphicsController *target)
{
	sf::VertexArray triangle(sf::TrianglesStrip, 4);
	sf::VertexArray lines(sf::LinesStrip, 5);
	sf::Vector2f size = GetSize();
	sf::Vector2f pos = GetPosition();
	sf::RectangleShape rectangle(m_size);

	if (m_parent)
		pos += GetParent()->GetPosition();

	if (m_pressed) {
		rectangle.setPosition(pos);
		rectangle.setFillColor(sf::Color(0, 0, 0, 50));
		rectangle.setOutlineThickness(2);
		rectangle.setOutlineColor(sf::Color(255, 100, 60, 150));
		target->GetWindow()->draw(rectangle);
	}

	if (m_config->selected != true) {
		return ;
	}
	lines[0].position = pos + sf::Vector2f(3, 3);
	lines[1].position = pos + sf::Vector2f(3, size.y - 3);
	lines[2].position = pos + sf::Vector2f(size.x - 3, size.y - 3);
	lines[3].position = pos + sf::Vector2f(size.x - 3, 3);
	lines[4].position = pos + sf::Vector2f(3, 3);
	lines[0].color = sf::Color(120, 120, 120, 160);
	lines[1].color = sf::Color(80, 80, 80, 160);
	lines[2].color = sf::Color(120, 120, 120, 160);
	lines[3].color = sf::Color(80, 80, 80, 160);
	lines[4].color = sf::Color(120, 120, 120, 160);
	target->GetWindow()->draw(lines);

	triangle[0].position = pos + sf::Vector2f(5, 5);
	triangle[1].position = pos + sf::Vector2f(5, size.y - 5);
	triangle[2].position = pos + sf::Vector2f(size.x - 5, 5);
	triangle[3].position = pos + sf::Vector2f(size.x - 5, size.y - 5);
	triangle[0].color = sf::Color(50, 50, 50, 140);
	triangle[1].color = sf::Color(110, 110, 110, 140);
	triangle[2].color = sf::Color(110, 110, 110, 140);
	triangle[3].color = sf::Color(50, 50, 50, 140);
	target->GetWindow()->draw(triangle);
}

void GUIMarkerButton::Draw(GraphicsController *target)
{
	sf::Vector2f pos = GetPosition();
	sf::Vector2f size = GetSize();
	sf::RectangleShape rectangle(m_size - sf::Vector2f(20, 20));
	int activeFrame = 15;
	int activeCol;
	int activeRow;

	if (m_parent)
		pos += GetParent()->GetPosition();
	if (!m_config)
		return ;

	DrawBackground(target);

	if (m_config->selected == true) {
		float activeBpm = 30.f;
		int inLength = (int) ((BAD_DELAY + GOOD_DELAY + GREAT_DELAY + PERFECT_DELAY) / activeBpm);
		int outLength = (int) ((GREAT_DELAY + GOOD_DELAY + BAD_DELAY) / 2.0f / activeBpm);
		int totalLength = inLength + outLength;
		int dt = m_clk.getElapsedTime().asMilliseconds() % (totalLength + 1000) - 1000;
		if (dt < 0) {
			dt = 0;
		}
		activeFrame = (int) (dt * 25.0f / totalLength);
	}
	activeCol = activeFrame % 5;
	activeRow = activeFrame / 5;
	rectangle.setPosition(pos + sf::Vector2f(10, 10));
	rectangle.setTexture(&m_config->tex);
	rectangle.setTextureRect(sf::IntRect(activeCol * 100, activeRow * 100, 100, 100));
	rectangle.setFillColor(sf::Color(255, 255, 255, 255));

	target->GetWindow()->draw(rectangle);

}

void GUIMarkerButton::Update(sf::Clock *time)
{
	GUIWidget::Update(time);
}

void GUIMarkerButton::OnPressed()
{
	GUIWidget::OnPressed();
	
	m_clk.restart();
	if (!m_config)
		return ;
	m_config->selectCb->ResetMarkerSelection();
	m_config->selected = true;
}

void GUIMarkerButton::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
}

void GUIMarkerButton::SetMarkerEntry(MarkerEntry *res) {
	m_config = res;
}

MarkerEntry *GUIMarkerButton::GetMarkerEntry() const { return (m_config); }

