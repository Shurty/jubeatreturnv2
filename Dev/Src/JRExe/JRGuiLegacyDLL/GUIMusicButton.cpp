#include <JREngine/jrgui.h>

GUIMusicButton::GUIMusicButton()
{
	m_cb = NULL;
	m_music = NULL;
	m_selected = false;
	m_textSize = 10;
	m_font.loadFromFile("Fonts/ARIALN.ttf");
	m_bg.loadFromFile("Textures/Design/song.png");
}

GUIMusicButton::~GUIMusicButton()
{
}

void GUIMusicButton::LinkAction(AGUICallback *cblInst, void (AGUICallback::*clb)(GUIWidget *))
{
	m_cb = cblInst;
	m_cbMethod = clb;
}

void GUIMusicButton::Init()
{

}


void GUIMusicButton::DrawBG(GraphicsController *target)
{
	float mvRatio = 60.0f / m_music->originalBpm;
	sf::VertexArray triangle(sf::TrianglesStrip, 4);
	sf::VertexArray lines(sf::LinesStrip, 5);
	sf::Vector2f size = GetSize();
	sf::Vector2f pos = GetPosition();
	sf::Vector2f previousPos;
	sf::Vector2f targetPos;
	sf::RectangleShape rectangle(m_size);
	float delta = m_animationTime.getElapsedTime().asMilliseconds() % (int)(4000.0f * mvRatio) / mvRatio / 4;

	if (m_parent) {
		pos += GetParent()->GetPosition();
	}

	rectangle.setTexture(&m_bg);
	rectangle.setPosition(pos);
	target->GetWindow()->draw(rectangle);
}

void GUIMusicButton::Draw(GraphicsController *target)
{
	int x_diff = (int) m_size.x * 14.24 / 100;
	int y_diff_up = (int) m_size.x * 1.156 * 13.8 / 100;
	int y_diff_down = (int) m_size.x * 1.156 * 10.4 / 100;

	sf::RectangleShape rectangle(m_size);
	sf::RectangleShape rectangleTex(m_size - sf::Vector2f(x_diff * 2, y_diff_up + y_diff_down));
	sf::Text tx;
	sf::Vector2f pos = GetPosition();

	if (m_parent)
		pos += GetParent()->GetPosition();

	tx.setCharacterSize(m_textSize);
	tx.setFont(m_font);
	tx.setColor(sf::Color(255, 255, 255, 255));

	sf::Vector2f	rectangleTexPosition = pos + sf::Vector2f(x_diff, y_diff_up);
	sf::Vector2f	txPosition = pos + sf::Vector2f(m_textSize + 5.f, 0.f);

	rectangleTex.setPosition(rectangleTexPosition);
	rectangle.setFillColor(sf::Color(0, 0, 0, 50));

	if (m_music != NULL) {
		tx.setString(m_music->name.substr(0, 20));

		if (m_music->titleTextureValid) {
			rectangleTex.setTexture(&m_music->titleTexture);
		}
		else {
			rectangleTex.setFillColor(sf::Color(150, 0, 0, 50));
		}
		
	} else {
		rectangle.setFillColor(sf::Color(0, 0, 0, 50));
	}

	tx.setPosition(txPosition);

	sf::FloatRect textRect = tx.getLocalBounds();
	tx.setOrigin(textRect.width/2,tx.getOrigin().y);
	tx.setPosition(pos.x + (m_size.x / 2), tx.getPosition().y);

	if (m_pressed) {
		rectangle.setOutlineThickness(2);
		rectangle.setOutlineColor(sf::Color(255, 100, 60, 150));
	}
	
	if (m_music != NULL) {
		DrawBG(target);
		target->GetWindow()->draw(rectangleTex);
		target->GetWindow()->draw(tx);
		DrawLevel(target);
	}
}

void GUIMusicButton::DrawLevel(GraphicsController *target)
{
	sf::RectangleShape rectangle(sf::Vector2f(35, 35));
	sf::Vector2f pos = GetPosition();

	if (m_parent)
		pos += GetParent()->GetPosition();

	pos.y += m_size.y - 35;
	rectangle.setPosition(pos);
	rectangle.setTexture(&m_level_texture);
	target->GetWindow()->draw(rectangle);
}

void GUIMusicButton::Update(sf::Clock *time)
{
	GUIWidget::Update(time);
}

void GUIMusicButton::OnPressed()
{
	GUIWidget::OnPressed();
	if (m_cb) {
		(m_cb->*m_cbMethod)(this);
	}
}

void GUIMusicButton::ReceiveEvent(GUIEVENT ev, void *param)
{
	GUIWidget::ReceiveEvent(ev, param);
}

void GUIMusicButton::SetBeatmap(Beatmap *res) {
	std::string level_texture_name;
	std::stringstream ss;

	m_music = res;
	if (m_music) {
		ss << m_music->level;
		level_texture_name = "Textures/Design/level_" + ss.str() + ".png";
		m_level_texture.loadFromFile(level_texture_name);
	}
}

Beatmap *GUIMusicButton::GetBeatmap() const { return (m_music); }

void GUIMusicButton::SetSelected(bool res) { m_selected = res; }
void GUIMusicButton::SetTextSize(int sz) { m_textSize = sz; }

