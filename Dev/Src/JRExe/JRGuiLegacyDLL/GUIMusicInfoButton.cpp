#include <JREngine/jrgui.h>

GUIMusicInfoButton::GUIMusicInfoButton() : GUIMusicButton()
{
	//set font for japanese characters?
	//m_font.loadFromFile("Fonts/kaiu.ttf");
	m_bg.loadFromFile("Textures/Design/current_song.png");
}

GUIMusicInfoButton::~GUIMusicInfoButton()
{
}

void GUIMusicInfoButton::DrawBG(GraphicsController *target)
{
	sf::Vector2f size = GetSize();
	sf::Vector2f pos = GetPosition();	
	sf::RectangleShape rectangle(sf::Vector2f(m_size.x, (int) m_size.x * 1.156));

	if (m_parent) {
		pos += GetParent()->GetPosition();
	}

	rectangle.setTexture(&m_bg);
	rectangle.setPosition(pos);
	target->GetWindow()->draw(rectangle);
}

void GUIMusicInfoButton::Draw(GraphicsController *target)
{
	sf::RectangleShape rectangle(m_size);
	sf::RectangleShape rectangle_level(sf::Vector2f(50, 50));
	sf::Text tx;
	sf::Text songlvlTx;
	sf::Text authorTx;
	sf::Vector2f pos = GetPosition();

	if (m_parent)
		pos += GetParent()->GetPosition();

	tx.setCharacterSize(m_textSize);
	tx.setFont(m_font);
	tx.setColor(sf::Color(255, 255, 255, 255));

	authorTx.setCharacterSize((int) (m_textSize / 1.5));
	authorTx.setFont(m_font);
	authorTx.setColor(sf::Color(255, 255, 255, 255));

	songlvlTx.setCharacterSize(m_textSize);
	songlvlTx.setFont(m_font);

	sf::Vector2f	txPosition = pos +
		sf::Vector2f((float) (m_textSize + 5), (float) (m_size.y * 1.156 - m_textSize * 2));
	sf::Vector2f	authorTxPosition = pos +
		sf::Vector2f((float) (m_textSize + 5), (float) m_size.y * 1.156 - m_textSize);

	sf::Vector2f	songlvlTxPosition = pos +
		sf::Vector2f((float) (m_size.x + m_textSize / 2), m_size.y / 4 + 130);

	int x_diff = (int) m_size.x * 19 / 100;
	int y_diff = (int) m_size.x * 1.156 * 29.5 / 100;
	int y_diff_up = (int) m_size.x * 1.156 * 10.47 / 100;
	sf::Vector2f	rectangleTexPosition = pos + sf::Vector2f(x_diff / 2, y_diff_up);

	sf::RectangleShape rectangleTex(sf::Vector2f(m_size.x - x_diff, m_size.x * 1.156 - y_diff));
	rectangleTex.setPosition(rectangleTexPosition);

	if (m_music != NULL) {
		std::stringstream ss;
		ss << m_music->level;

		tx.setString(m_music->name.substr(0, 20));
		authorTx.setString(m_music->author.substr(0, 20));

		switch (m_music->songLevel) {
			case (BASIC) :
				songlvlTx.setString("BASIC");
				songlvlTx.setColor(sf::Color(0, 255, 0, 255));
				break;
			case (INTERMEDIATE) :
				songlvlTx.setString("ADVANCED");
				songlvlTx.setColor(sf::Color(255, 255, 0, 255));
				break;
			case (EXTREME) :
				songlvlTx.setString("EXTREME");
				songlvlTx.setColor(sf::Color(255, 0, 0, 255));
				break;
		}

		if (m_music->titleTextureValid) {
			rectangleTex.setTexture(&m_music->titleTexture);
		}
		else {
			rectangleTex.setFillColor(sf::Color(150, 0, 0, 50));
		}
		
	} else {
		rectangle.setFillColor(sf::Color(0, 0, 0, 50));
	}

	tx.setPosition(txPosition);
	authorTx.setPosition(authorTxPosition);
	songlvlTx.setPosition(songlvlTxPosition);

	if (m_pressed) {
		rectangle.setOutlineThickness(2);
		rectangle.setOutlineColor(sf::Color(255, 100, 60, 150));
	}

	if (m_music != NULL) {
		DrawBG(target);
		target->GetWindow()->draw(rectangleTex);
		target->GetWindow()->draw(tx);
		target->GetWindow()->draw(songlvlTx);
		target->GetWindow()->draw(authorTx);
		DrawLevel(target);
	}
}

void GUIMusicInfoButton::DrawLevel(GraphicsController *target)
{
	sf::RectangleShape rectangle(sf::Vector2f(130, 130));
	sf::Vector2f pos = GetPosition();

	if (m_parent)
		pos += GetParent()->GetPosition();

	pos.x += m_size.x;
	pos.y += m_size.y / 4;
	rectangle.setPosition(pos);
	rectangle.setTexture(&m_level_texture);
	target->GetWindow()->draw(rectangle);
}