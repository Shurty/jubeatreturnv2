#include <JREngine/jrgui.h>

void GUIActionTextButton::SetTitle(std::string const &res) { m_title = res; }
std::string GUIActionTextButton::GetTitle() const { return (m_title); }

GUIActionTextButton::GUIActionTextButton() : GUIActionButton()
{
	m_font.loadFromFile("Fonts/blank-Regular.otf");
}

void GUIActionTextButton::DrawBackground(GraphicsController *target)
{
	sf::VertexArray triangle(sf::TrianglesStrip, 4);
	sf::VertexArray lines(sf::LinesStrip, 5);
	sf::Vector2f size = GetSize();
	sf::Vector2f pos = GetPosition();
	sf::RectangleShape rectangle(m_size);

	if (m_parent)
		pos += GetParent()->GetPosition();

	if (m_pressed) {
		rectangle.setPosition(pos);
		rectangle.setFillColor(sf::Color(0, 0, 0, 50));
		rectangle.setOutlineThickness(2);
		rectangle.setOutlineColor(sf::Color(255, 100, 60, 150));
		target->GetWindow()->draw(rectangle);
	}


	lines[0].position = pos + sf::Vector2f(3, 3);
	lines[1].position = pos + sf::Vector2f(3, size.y - 3);
	lines[2].position = pos + sf::Vector2f(size.x - 3, size.y - 3);
	lines[3].position = pos + sf::Vector2f(size.x - 3, 3);
	lines[4].position = pos + sf::Vector2f(3, 3);
	lines[0].color = sf::Color(120, 120, 120, 120);
	lines[1].color = sf::Color(80, 80, 80, 120);
	lines[2].color = sf::Color(120, 120, 120, 120);
	lines[3].color = sf::Color(80, 80, 80, 120);
	lines[4].color = sf::Color(120, 120, 120, 120);
	target->GetWindow()->draw(lines);

	triangle[0].position = pos + sf::Vector2f(5, 5);
	triangle[1].position = pos + sf::Vector2f(5, size.y - 5);
	triangle[2].position = pos + sf::Vector2f(size.x - 5, 5);
	triangle[3].position = pos + sf::Vector2f(size.x - 5, size.y - 5);
	triangle[0].color = sf::Color(50, 50, 50, 100);
	triangle[1].color = sf::Color(110, 110, 110, 100);
	triangle[2].color = sf::Color(110, 110, 110, 100);
	triangle[3].color = sf::Color(50, 50, 50, 100);
	target->GetWindow()->draw(triangle);
}

void GUIActionTextButton::Draw(GraphicsController *target)
{
	sf::Text text(m_title, m_font);
	sf::FloatRect textRect;

	DrawBackground(target);

	if (m_parent)
		text.setPosition(m_position + m_parent->GetPosition() + m_size * 0.5f);
	else
		text.setPosition(m_position + m_size * 0.5f);

	text.setCharacterSize(32);
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width/2.0f,
		textRect.top + textRect.height/2.0f);
	text.setColor(sf::Color(100, 100, 100, 255));
	target->GetWindow()->draw(text);
}
