#include "stdafx.h"
#include "CppUnitTest.h"
#include <iostream>
#include "JREngine/jrshared.h"
#include <functional>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JREUnitTests
{
	TEST_CLASS(PlayersManagerUnitTest)
	{
	public:

		TEST_METHOD(Database)
		{
			std::string music1_string("music1");
			std::wstring music1_wstring(std::wstring(music1_string.begin(), music1_string.end()));
			std::string music2_string("music2");
			std::wstring music2_wstring(std::wstring(music2_string.begin(), music2_string.end()));

			PlayersManager pm("./");
			Player* p;
			
			p = pm.connectPlayer("PlayerExtreme1");
			//auto scores = pm.loadScoresForMusic(music1_wstring, SONGLEVEL::SL_EXTREME);
			//Assert::AreEqual(-1, PlayersManager::getPlayerPosition(scores, p->getPseudo()));
			// TODO: asserts that getting best and worst players on this music returns null
			if (pm.getCurrentPlayer()->saveScoreIfHigher(music1_wstring, SONGLEVEL::SL_EXTREME, 400000))
			{
				pm.saveScore(400000, p , music1_wstring, SONGLEVEL::SL_EXTREME);
			}
			auto newscores = pm.loadScoresForMusic(music1_wstring, SONGLEVEL::SL_EXTREME);
			Assert::AreEqual(0, PlayersManager::getPlayerPosition(*newscores, p->getPseudo()));

			pm.savePlayer(p);
			pm.unloadPlayers();

			pm.connectPlayer("PlayerBoss");
			if (pm.getCurrentPlayer()->saveScoreIfHigher(music1_wstring, SONGLEVEL::SL_BASIC, 1000000))
			{
				pm.saveScore(1000000, pm.getCurrentPlayer(), music1_wstring, SL_BASIC);
			}
			pm.savePlayer(pm.getCurrentPlayer());
			pm.unloadPlayers();

			pm.connectPlayer("PlayerNoob");
			if (pm.getCurrentPlayer()->saveScoreIfHigher(music1_wstring, SONGLEVEL::SL_BASIC, 1))
			{ // seems like duplicating code eh.
				pm.saveScore(1, pm.getCurrentPlayer(), music1_wstring, SONGLEVEL::SL_BASIC);
			}
			pm.savePlayer(pm.getCurrentPlayer());
			//auto almostlastscores = pm.loadScoresForMusic(music1_wstring, SONGLEVEL::SL_BASIC);
			//Assert::AreEqual(1, PlayersManager::getPlayerPosition(almostlastscores, pm.getCurrentPlayer()->getPseudo()));
			Player* middle = pm.connectPlayer("PlayerMiddle");
			// save locally detailed scores (should be detailed)
			if (pm.getCurrentPlayer()->saveScoreIfHigher(music1_wstring, SONGLEVEL::SL_BASIC, 500000))
			{
				// I choose to load scores in anticipation of a multiplayer shit, so we don't erase everything locally
				pm.saveScore(500000, middle, music1_wstring, SL_BASIC);
			}
			//auto lastscores = pm.loadScoresForMusic(music1_wstring, SONGLEVEL::SL_BASIC);

			// save detailed scores
			pm.savePlayer(middle);
			pm.unloadPlayers();

			p = pm.connectPlayer("PlayerOther2");
			// save locally ? no real use actually
			if (pm.getCurrentPlayer()->saveScoreIfHigher(music2_wstring, SONGLEVEL::SL_BASIC, 500002))
			{
				// save to files (
				pm.saveScore(500002, p, music2_wstring, SL_BASIC);
			}

			pm.savePlayer(p);
			pm.unloadPlayers();

			p = pm.connectPlayer("PlayerMiddle");

			Assert::IsNotNull(p);
			Assert::AreEqual((unsigned int)500000, p->m_scores[0].m_basic.bestScore);

			std::hash<std::wstring> hash;
			Assert::AreEqual(std::to_string(hash(music1_wstring)), p->m_scores[0].m_hashmusicName);
			Assert::AreEqual((unsigned int)500000, p->getHighScoreFor(music1_wstring, SONGLEVEL::SL_BASIC));

			auto finalscores = pm.loadScoresForMusic(music1_wstring, SONGLEVEL::SL_BASIC);
			pm.setNewScoreSession();

			pm.updateCurrentLoadedScoresForPlayer("PlayerMiddle", 0);
			Assert::AreEqual(pm.getCurrentLoadedScores()->size() - 1, pm.getCurrentPlayerPositionInScoreSession()); // should be last
			pm.updateCurrentLoadedScoresForPlayer("PlayerMiddle", 10);
			Assert::AreEqual(pm.getCurrentLoadedScores()->size() - 2, pm.getCurrentPlayerPositionInScoreSession()); // should be better than playernoob
			pm.updateCurrentLoadedScoresForPlayer("PlayerMiddle", 499999);
			Assert::AreEqual(pm.getCurrentLoadedScores()->size() - 2, pm.getCurrentPlayerPositionInScoreSession()); // should not have moved position
			pm.updateCurrentLoadedScoresForPlayer("PlayerMiddle", 500001);
			Assert::AreEqual(pm.getCurrentLoadedScores()->size() - 3, pm.getCurrentPlayerPositionInScoreSession()); // should be better than previous self highscore
			pm.updateCurrentLoadedScoresForPlayer("PlayerMiddle", 1000001); // impossible but for tests bah
			Assert::AreEqual((unsigned int)0, pm.getCurrentPlayerPositionInScoreSession());
			
			// All this works but we need to delete files in order to make previous asserts work next time we run the tests...
			/*
			if (pm.getCurrentPlayer()->saveScoreIfHigher(music1_wstring, SONGLEVEL::SL_BASIC, 1000001))
			{
				// save to files (
				pm.saveScore(1000001, p, music1_wstring, SL_BASIC);
			}
			pm.savePlayer(p);
			finalscores = pm.loadScoresForMusic(music1_wstring, SONGLEVEL::SL_BASIC);
			Assert::AreEqual(0, PlayersManager::getPlayerPosition(*finalscores, "PlayerMiddle"));
			Assert::AreEqual(1, PlayersManager::getPlayerPosition(*finalscores, "PlayerBoss"));
			*/
			// FIXME: Delete files is not working !
			pm.deleteFiles();
			pm.unloadPlayers();
		}
	};
}