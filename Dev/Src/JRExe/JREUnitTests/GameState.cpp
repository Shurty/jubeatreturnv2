#include "stdafx.h"
#include "CppUnitTest.h"
#include "JREngine/jrshared.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

class StateBoot : public GameState
{
public:
	StateBoot() {}
	virtual ~StateBoot() { }

	GMSTATEID GetId();

	virtual void OnInit(Parameters &pm) { }
	virtual void OnStart(Parameters &pm) { }
	virtual void OnRender(Parameters &pm) { }
	virtual void OnUpdate(Parameters &pm) { }
	virtual void OnEnd(Parameters &pm) { }
	virtual void OnUnload(Parameters &pm) { }
};

namespace JREUnitTests
{
	TEST_CLASS(GameState)
	{
	public:

		TEST_METHOD(GameStateOpTest)
		{
			// TODO: Your test code here
			Parameters pm;
			GameStateManager gsm(pm);
			::GameState* st = new StateBoot();
			::GameState* st2 = new StateBoot();

			gsm.PushState(st2);
			gsm.PushState(st);
			Assert::AreEqual((int)gsm.GetActiveState(), NULL);
			gsm.ChangeState(st);
			Assert::AreEqual((int)gsm.GetActiveState(), (int)st);
			gsm.ChangeState(st2);
			Assert::AreEqual((int)gsm.GetActiveState(), (int)st);
			gsm.UpdateState();
			Assert::AreEqual((int)gsm.GetActiveState(), (int)st2);
		}

	};
}