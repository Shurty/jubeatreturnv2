#include "stdafx.h"
#include "CppUnitTest.h"
#include "JREngine/jrshared.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JREUnitTests
{
	TEST_CLASS(MemoParser)
	{
	public:
		
		TEST_METHOD(NotesCountTest)
		{
			::MemoParser parser;
			::Beatmap *bmp = parser.ParseBeatmap("Data/Beatmaps/OnlyMyRailgun_ADV.txt");

			Assert::AreEqual(bmp->beats, (int)bmp->beatsList.size());
		}

	};
}