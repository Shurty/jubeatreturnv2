#include "stdafx.h"
#include "CppUnitTest.h"

// FIXME: that's ew...
#include "../../Includes/JREngine/ScoreManager.h"
#include <iostream>
#include <ctime>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JREUnitTests
{
	TEST_CLASS(ScoreManagerTest)
	{
	public:
		
		TEST_METHOD(FullPerfect)
		{

			ScoreManager m = ScoreManager();
			// FIXME: this is pretty random and might miss some tests, but well, it's too long if you test everything
			for (unsigned int numberOfNotes = 1; numberOfNotes < 1000; numberOfNotes += rand() % (100 - numberOfNotes / 500) + 1) {
				m.reset(numberOfNotes);
				for (int i = 0; i < numberOfNotes; i++)
				{
					m.scoreBeat(BEATRESULT::BR_PERFECT);
					Assert::AreEqual((unsigned int)GRADE::GRADE_EXC, m.getPotentialGrade());
				}
				unsigned int score = m.getScoreWithCombo();
				// Logger::Log << "score for " << numberOfNotes << " notes : " <<  << std::endl;
				//Logger::WriteMessage(std::to_string(m.getScoreWithCombo()).c_str());
				// if (m.getScoreWithoutCombo() != (unsigned int)900000 || m.getScoreWithCombo() != (unsigned int)1000000)
				Assert::AreEqual((unsigned int)900000, m.getScoreWithoutCombo()); 
				Assert::AreEqual((unsigned int)1000000, m.getScoreWithCombo());
			}
		}

		TEST_METHOD(FirstHalfPerfect)
		{
			// TODO: Your test code here

			// First half perfect, should be (1 million - totalmaxBonusScore) / 2
			ScoreManager m = ScoreManager();
			// FIXME: this random is pretty random and might miss some tests, but well, it's too long if you test everything
			for (unsigned int numberOfNotes = 102; numberOfNotes < 1000; numberOfNotes += rand() % (100 - numberOfNotes / 500) + 1) {
				numberOfNotes += numberOfNotes % 2; // can't test with odd numbers.
				Logger::WriteMessage("Begginning of test with number of notes :");
				Logger::WriteMessage(std::to_string(numberOfNotes).c_str());
				m.reset(numberOfNotes, true);
				Logger::WriteMessage("First half perfect:");
				unsigned int i = 0;
				for (; i < numberOfNotes / 2; i++)
				{
					m.scoreBeat(BEATRESULT::BR_PERFECT);
					Logger::WriteMessage("potential score:");
					Logger::WriteMessage(std::to_string(m.getPotentialScore()).c_str());
					Logger::WriteMessage("potential bonus:");
					Logger::WriteMessage(std::to_string(m.computePotentialBonusScore()).c_str());
					Assert::AreEqual((unsigned int)GRADE::GRADE_EXC, m.getPotentialGrade());
				}
				Logger::WriteMessage("Second half missed:");
				for (; i < numberOfNotes; i++)
				{
					m.scoreBeat(BEATRESULT::BR_MISS);
					Logger::WriteMessage("potential score:");
					Logger::WriteMessage(std::to_string(m.getPotentialScore()).c_str());
					Logger::WriteMessage("potential bonus:");
					Logger::WriteMessage(std::to_string(m.computePotentialBonusScore()).c_str());
					//Logger::WriteMessage("grade:");
					//Logger::WriteMessage(std::to_string(m.getGradeForFinalScore(m.getPotentialScore())).c_str());
					Assert::AreNotEqual((unsigned int)GRADE::GRADE_EXC, m.getPotentialGrade());
				}
				Assert::AreEqual(i, numberOfNotes);
				unsigned int score = m.getScoreWithCombo();
				// Logger::Log << "score for " << numberOfNotes << " notes : " <<  << std::endl;
				//Logger::WriteMessage(std::to_string(m.getScoreWithCombo()).c_str());
				// if (m.getScoreWithoutCombo() != (unsigned int)900000 || m.getScoreWithCombo() != (unsigned int)1000000)
				Assert::AreEqual((unsigned int)0, m.getBonusScore());
				Assert::AreEqual((unsigned int)450000, m.getScoreWithoutCombo()); 
				Assert::AreEqual((unsigned int)450000, m.getScoreWithCombo());
				const std::string& filepath = m.logScores();
				auto scores = m.readScores(filepath);
				Assert::AreEqual(numberOfNotes, scores.size()); // TODO: more verification ? (to check if we really load correctly ? 
			}
		}
		TEST_METHOD(SecondHalfPerfect)
		{
			// TODO: Your test code here

			// second half perfect, should be (1 million) / 2
			ScoreManager m = ScoreManager();
			// FIXME: this random is pretty random and might miss some tests, but well, it's too long if you test everything
			for (unsigned int numberOfNotes = 102; numberOfNotes < 1000; numberOfNotes += rand() % (100 - numberOfNotes / 500) + 1) {
				numberOfNotes += numberOfNotes % 2; // can't test with odd numbers.
				Logger::WriteMessage("Begginning of test with number of notes :");
				Logger::WriteMessage(std::to_string(numberOfNotes).c_str());
				m.reset(numberOfNotes);
				Logger::WriteMessage("First half missed:");
				unsigned int i = 0;
				for (; i < numberOfNotes / 2; i++)
				{
					m.scoreBeat(BEATRESULT::BR_MISS);
					Logger::WriteMessage("potential score:");
					Logger::WriteMessage(std::to_string(m.getPotentialScore()).c_str());
					Logger::WriteMessage("potential bonus:");
					Logger::WriteMessage(std::to_string(m.computePotentialBonusScore()).c_str());
					Assert::AreEqual((unsigned int)GRADE::GRADE_E, m.getPotentialGrade());
				}
				Logger::WriteMessage("Second half perfect:");
				for (; i < numberOfNotes; i++)
				{
					m.scoreBeat(BEATRESULT::BR_PERFECT);
					Logger::WriteMessage("potential score:");
					Logger::WriteMessage(std::to_string(m.getPotentialScore()).c_str());
					Logger::WriteMessage("potential bonus:");
					Logger::WriteMessage(std::to_string(m.computePotentialBonusScore()).c_str());
					//Logger::WriteMessage("grade:");
					//Logger::WriteMessage(std::to_string(m.getGradeForFinalScore(m.getPotentialScore())).c_str());
				}
				Assert::AreEqual(i, numberOfNotes);
				unsigned int score = m.getScoreWithCombo();
				// Logger::Log << "score for " << numberOfNotes << " notes : " <<  << std::endl;
				//Logger::WriteMessage(std::to_string(m.getScoreWithCombo()).c_str());
				// if (m.getScoreWithoutCombo() != (unsigned int)900000 || m.getScoreWithCombo() != (unsigned int)1000000)
				Assert::AreEqual((unsigned int)100000, m.getBonusScore());
				Assert::AreEqual((unsigned int)450000, m.getScoreWithoutCombo()); 
				Assert::AreEqual((unsigned int)550000, m.getScoreWithCombo());
			}
		}
		// to plot with gnuplot you can use: plot "testRandomNotes.dat" using 1:3, "testRandomNotes.dat" using 1:4, "testRandomNotes.dat" using 1:5, "testRandomNotes.dat" using 1:6
		TEST_METHOD(RandomToTestGraph)
		{
			ScoreManager m = ScoreManager();
			srand(time(nullptr));
			int numberOfNotes = rand() % 500 + 500; // between 500 and 1000 notes
			numberOfNotes = 185;
			m.reset(numberOfNotes, true);
			for (int i = 0; i < numberOfNotes; i++)
			{
				if ((rand() % 3) != 0) // so we have more high score
					m.scoreBeat((BEATRESULT)(rand() % 2 + 3));
				else
					m.scoreBeat((BEATRESULT)(rand() % 5));
			}
			m.logScores("Data/Logs/testRandomNotes.dat");
			unsigned int score = m.getScoreWithCombo();
		}
	};
}