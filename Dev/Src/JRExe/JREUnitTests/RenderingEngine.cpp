#include "stdafx.h"
#include "CppUnitTest.h"
#include "JREngine/jrshared.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace JREUnitTests
{
	TEST_CLASS(RenderingEngine)
	{
	public:

		TEST_METHOD(SFMLWindowTest)
		{
			// Create SFML window
			sf::RenderWindow wnd(sf::VideoMode(100, 100, 32), "UnitWindow");
			wnd.setPosition(sf::Vector2i(50, 50));
			wnd.clear(sf::Color(18, 22, 25, 255));
			wnd.display();

			// Unloading
			wnd.close();
		}

		TEST_METHOD(SFMLPrimitivesTest)
		{
			// Create SFML window
			sf::RenderWindow wnd(sf::VideoMode(100, 100, 32), "UnitWindow");
			wnd.setPosition(sf::Vector2i(50, 50));
			wnd.clear(sf::Color(18, 22, 25, 255));
			wnd.display();

			// Create SFML primitives
			wnd.clear(sf::Color(18, 22, 25, 255));
			sf::RectangleShape rect(sf::Vector2f(100, 100));
			rect.setPosition(0, 0);
			rect.setFillColor(sf::Color(0, 255, 0, 255));
			wnd.draw(rect);
			sf::CircleShape circ(100);
			circ.setPosition(0, 0);
			circ.setFillColor(sf::Color(255, 0, 0, 255));
			wnd.draw(circ);
			wnd.display();

			// Unloading
			wnd.close();
		}

		TEST_METHOD(SFMLAssetsTest)
		{
			// Create SFML window
			sf::RenderWindow wnd(sf::VideoMode(100, 100, 32), "UnitWindow");
			wnd.setPosition(sf::Vector2i(50, 50));
			wnd.clear(sf::Color(18, 22, 25, 255));
			wnd.display();

			sf::RectangleShape rect(sf::Vector2f(100, 100));
			rect.setPosition(0, 0);
			rect.setFillColor(sf::Color(0, 255, 0, 255));
			wnd.draw(rect);

			// Load texture
			wnd.clear(sf::Color(18, 22, 25, 255));
			sf::Texture tex;
			Assert::AreNotEqual(tex.loadFromFile(TEXTURESDIR + std::string("bgBubble.png")), false);
			rect.setTexture(&tex);
			wnd.draw(rect);
			wnd.display();

			// Load font
			wnd.clear(sf::Color(18, 22, 25, 255));
			sf::Font font;
			Assert::AreNotEqual(font.loadFromFile(FONTSDIR + std::string("ARIALN.TTF")), false);

			// SFML text
			sf::Text tx;
			tx.setPosition(0, 0);
			tx.setFont(font);
			tx.setString("ABCDEF");
			tx.setCharacterSize(33);
			wnd.draw(tx);
			wnd.display();

			// Unloading
			wnd.close();
		}

	};
}