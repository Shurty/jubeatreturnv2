/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <JREngine\jrshared.h>
#include <JREngine\jrgui.h>
#include <SFML\Audio.hpp>
#include <SoundTouch\SoundTouch.h>

// TODO: reference additional headers your program requires here
#include "JBMusic.h"
#include "Core.h"
#include "GUIBeatButton.h"
#include "FileBrowser.h"
#include "BeatMapper.h"

