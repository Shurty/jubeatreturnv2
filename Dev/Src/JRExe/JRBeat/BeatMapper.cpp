/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

BeatMapper::BeatMapper(Core *cr)
: core(cr)
{
	curTime = 0;
	core->pm.m_params.press_range = 900;
}

BeatMapper::~BeatMapper()
{
}

void BeatMapper::Update()
{
	if (core->st == ST_MAP)
	{
		BeatMapper* me = this;
		curTime = core->time.getElapsedTime().asMilliseconds();
		std::for_each(mappings.begin(), mappings.end(), [me](Beatmapping map) {
			std::for_each(map.beats.begin(), map.beats.end(), [me](Beat bt) {
				int tm = bt.time - me->core->pm.m_params.press_range / 2;
				if (tm >= me->prevTime && tm <= me->curTime) {
					me->core->kb.GetWidget(bt.activate - 1)->OnTriggered();
				}
				tm -= me->core->pm.m_params.press_range / 2;
				if (tm >= me->prevTime && tm <= me->curTime) {
					((GUIBeatButton*)me->core->kb.GetWidget(bt.activate - 1))->OnPreTrigger();
				}
			});
		});
		prevTime = curTime;
		if (core->music.getStatus() == sf::Music::Status::Stopped)
		{
			if (curMap->beats.size() > 0 && curMap->beats.back().time + 2000 < curTime)
			{
				EndMapping();
			}
		}
	}
}

void BeatMapper::EndMapping()
{
	core->st = ST_IDLE;
	curBeat = curMap->beats.begin();
	core->music.stop();
}

void BeatMapper::Draw(RenderingEngine *re)
{
	DrawActions();

	sf::RectangleShape rect;
	int px, py;
	int gridHeight = (mappings.size() - 1) * 32 + 128;
	sf::Text tx;
	float sw = core->pm.m_params.window_width;
	float sh = core->pm.m_params.window_height;

	py = 50;
	px = 0;

	int tm = curTime;
	int secIdx = 0;


	std::for_each(mappings.begin(), mappings.end(), [=, &px, &py, &tx](Beatmapping &ent)
	{
		px = 0;
		sf::RectangleShape rect;
		sf::FloatRect tsize;
		BeatMapper* me = this;
		float sw = core->pm.m_params.window_width;
		float sh = core->pm.m_params.window_height;
		int sy = 32;

		if (curMap->id == ent.id)
		{
			sy *= 4;
		}
		rect.setPosition(px - 5, py - 2);
		rect.setSize(sf::Vector2f(sw - px - 5, sy));
		rect.setTexture(NULL);
		if (curMap->id == ent.id)
			rect.setFillColor(sf::Color(255, 255, 0, 100));
		else if (ent.id % 2 == 0)
			rect.setFillColor(sf::Color(80, 80, 80, 100));
		else
			rect.setFillColor(sf::Color(50, 50, 50, 100));
		core->render.GetRenderWindow()->draw(rect);

		for (int i = 1; i < 16; i++)
		{
			rect.setPosition(px - 5, py - 2 + i * (sy / 16.0));
			rect.setSize(sf::Vector2f(sw - px - 5, 1));
			rect.setTexture(NULL);
			if (curMap->id == ent.id)
				rect.setFillColor(sf::Color(80, 80, 80, 100));
			else if (ent.id % 2 == 1)
				rect.setFillColor(sf::Color(80, 80, 80, 100));
			else
				rect.setFillColor(sf::Color(0, 0, 0, 100));
			core->render.GetRenderWindow()->draw(rect);
		}

		tx.setFont(core->render.GetFont(FONT_SCORING_TX));
		tx.setString(std::to_string(ent.id) + "\n" + std::to_string(ent.beats.size()));
		tx.setCharacterSize(12);
		tx.setPosition(px, py + 2);
		tx.setColor(sf::Color(255, 255, 255, 255));
		core->render.GetRenderWindow()->draw(tx);

		px = 0;

		/* Render beatmap beats */
		std::for_each(ent.beats.begin(), ent.beats.end(), [this, &me, &px, &py, &tx, &ent](Beat bt)
		{
			sf::RectangleShape rect;
			sf::FloatRect tsize;
			float sw = core->pm.m_params.window_width;
			float sh = core->pm.m_params.window_height;
			int sy = 32;
			if (curMap->id == ent.id)
			{
				sy *= 4;
			}
			float beatDuration = BT_BEATTIMELEN / BT_NBTIMES;
			float beatSizePx = (float)BT_SECSIZE / (float)BT_NBTIMES / ((float)1000.0 / BT_BEATTIMELEN);
			float posOffsetX = (curTime - BT_BEATTIMELEN * 4) / 1000.0 * (float)BT_SECSIZE;
			float msPerPx = beatSizePx / beatDuration;
			float totalPressLengthPxW = this->core->pm.m_params.press_range * msPerPx;
			/*if (curTime < BT_BEATTIMELEN * 4)
			posOffsetX = 0;
			*/
			float x = px + bt.time / beatDuration * beatSizePx - posOffsetX;

			if (x < 0 || x > sw)
				return;

			/* render press range */
			rect.setPosition(x - totalPressLengthPxW / 2.0, py - 2 + (sy / 16.0) * (bt.activate - 1));
			rect.setSize(sf::Vector2f(totalPressLengthPxW, sy / 16.0));
			rect.setTexture(NULL);
			if (0)
				rect.setFillColor(sf::Color(255, 0, 0, 255));
			else if (curMap->id == ent.id)
				rect.setFillColor(sf::Color(255, 255, 0, 80));
			else
				rect.setFillColor(sf::Color(255, 255, 0, 80));
			core->render.GetRenderWindow()->draw(rect);

			/* render perfect range */
			rect.setPosition(x, py - 2 + (sy / 16.0) * (bt.activate - 1));
			rect.setSize(sf::Vector2f(beatSizePx, sy / 16.0));
			rect.setTexture(NULL);
			if (0)
				rect.setFillColor(sf::Color(255, 0, 0, 255));
			else if (curMap->id == ent.id)
				rect.setFillColor(sf::Color(255, 255, 255, 255));
			else
				rect.setFillColor(sf::Color(255, 255, 255, 120));
			core->render.GetRenderWindow()->draw(rect);

		});

		py += sy;
	});

	// X move = 2px = 100ms
	py = 50;
	px = 0;

	//if (curTime > BT_BEATTIMELEN * 4) {
	px -= (int)(tm / 1000.0 * (float)BT_SECSIZE * BT_BEATTIMELEN / 1000) % BT_SECSIZE;
	secIdx = tm / 1000 - (BT_BEATTIMELEN / 1000.0 * 4.0) - 1;
	//}

	int mpx = px;
	while (mpx < sw)
	{
		rect.setPosition(mpx, py);
		rect.setFillColor(sf::Color(200, 200, 200, 50));
		rect.setSize(sf::Vector2f(1, gridHeight));
		core->render.GetRenderWindow()->draw(rect);
		mpx += (BT_SECSIZE / BT_NBTIMES) / (1000 / BT_BEATTIMELEN);
	}

	mpx = px;
	while (mpx < sw)
	{
		if (mpx < 0)
		{
			mpx += BT_SECSIZE;
			secIdx++;
			continue;
		}
		rect.setPosition(mpx, py);
		rect.setFillColor(sf::Color(255, 255, 255, 100));
		rect.setSize(sf::Vector2f(1, gridHeight));
		core->render.GetRenderWindow()->draw(rect);
		secIdx++;

		tx.setPosition(mpx, py - 15);
		tx.setFont(core->render.GetFont(FONT_TITLE_TX));
		tx.setCharacterSize(12);
		tx.setString(std::to_string(secIdx) + "s");
		tx.setColor(sf::Color(255, 255, 255, 255));
		core->render.GetRenderWindow()->draw(tx);
		mpx += BT_SECSIZE;
	}

	tx.setPosition(sw - 100, 10);
	tx.setFont(core->render.GetFont(FONT_TITLE_TX));
	tx.setCharacterSize(12);
	tx.setString(std::to_string(core->time.getElapsedTime().asMilliseconds()) + "ms");
	tx.setColor(sf::Color(255, 255, 255, 255));
	core->render.GetRenderWindow()->draw(tx);

	px = 0;
	int ctmOffset = tm / (float)1000 * (float)BT_SECSIZE;

	rect.setPosition(px + BT_SECSIZE * 4 * BT_BEATTIMELEN / 1000, py);
	rect.setFillColor(sf::Color(255, 255, 0, 200));
	rect.setSize(sf::Vector2f(2, gridHeight));
	core->render.GetRenderWindow()->draw(rect);



}

void BeatMapper::DrawActions()
{
	sf::Text tx;
	sf::RectangleShape rect;
	sf::FloatRect tsize;
	int px, py;
	float sw = core->pm.m_params.window_width;
	float sh = core->pm.m_params.window_height;

	py = sh - 20;
	rect.setSize(sf::Vector2f(140, 20));
	rect.setFillColor(sf::Color(50, 50, 50, 255));
	tx.setFont(core->render.GetFont(FONT_SCORING_TX));
	tx.setCharacterSize(13);
	tx.setColor(sf::Color(255, 255, 255, 255));

	px = 20;
	tx.setString("Beatmap " + std::to_string(curMap->id));
	tx.setPosition(px + 5, py + 3);
	tsize = tx.getLocalBounds();
	rect.setPosition(px, py);
	rect.setSize(sf::Vector2f(tsize.width + 10, 20));
	core->render.GetRenderWindow()->draw(rect);
	core->render.GetRenderWindow()->draw(tx);

	int bpm = PRESS_BPM;

	px += tsize.width + 20;
	tx.setString("BPM: " + std::to_string(bpm));
	tx.setPosition(px + 5, py + 3);
	tsize = tx.getLocalBounds();
	rect.setPosition(px, py);
	rect.setSize(sf::Vector2f(tsize.width + 10, 20));
	core->render.GetRenderWindow()->draw(rect);
	core->render.GetRenderWindow()->draw(tx);

	if (core->st == ST_MAP)
	{
		px += tsize.width + 20;
		tx.setString("Press 5 to force stop");
		tx.setPosition(px + 5, py + 3);
		tx.setColor(sf::Color(255, 150, 0, 255));
		tsize = tx.getLocalBounds();
		rect.setPosition(px, py);
		rect.setSize(sf::Vector2f(tsize.width + 10, 20));
		core->render.GetRenderWindow()->draw(rect);
		core->render.GetRenderWindow()->draw(tx);
	}

}

void BeatMapper::Start(std::string const &path, std::string const &nm)
{
	src = path;
	name = nm;
	core->music.openFromFile(path);
	core->music.setVolume(100);
	totalTime = core->music.getDuration().asMilliseconds();
	core->st = ST_IDLE;

	mappings.clear();
	mappings.push_back(Beatmapping(0));
	mappings.push_back(Beatmapping(1));
	mappings.push_back(Beatmapping(2));
	mappings.push_back(Beatmapping(3));
	mappings.push_back(Beatmapping(4));
	mappings.push_back(Beatmapping(5));
	mappings.push_back(Beatmapping(6));
	curMap = mappings.begin();
	curTime = 0;
	curBeat = curMap->beats.begin();
	if (curMap->beats.size() != 0)
		curTime = curBeat->time;
	if (nm.find("_x075")) {
		Logger::Log << "Found a low-speed music (0.75x speed)" << std::endl;
	}
}

void BeatMapper::CursorUp()
{
	if (mappings.size() == 0)
		return;
	if (curMap == mappings.begin())
		curMap = mappings.end();
	curMap--;
	curBeat = curMap->beats.begin();
	if (curMap->beats.size() == 0)
		return;
	curTime = curBeat->time;
}
void BeatMapper::CursorDown()
{
	if (mappings.size() == 0)
		return;
	curMap++;
	if (curMap == mappings.end())
		curMap = mappings.begin();
	curBeat = curMap->beats.begin();
	if (curMap->beats.size() == 0)
		return;
	curTime = curBeat->time;
}
void BeatMapper::CursorRight()
{
	if (curMap->beats.size() == 0)
		return;
	curBeat++;
	if (curBeat == curMap->beats.end())
		curBeat = curMap->beats.begin();
	curTime = curBeat->time;
}
void BeatMapper::CursorLeft()
{
	if (curMap->beats.size() == 0)
		return;
	if (curBeat == curMap->beats.begin())
		curBeat = curMap->beats.end();
	curBeat--;
	curTime = curBeat->time;
}

void BeatMapper::CursorSelect()
{
}
void BeatMapper::CursorRestart()
{
	curMap->beats.clear();
	curBeat = curMap->beats.begin();
	CursorStart();
}
void BeatMapper::CursorStart()
{
	core->st = ST_MAP;
	core->music.play();
	core->time.restart();
	prevTime = 0;
}
void BeatMapper::KeyPress(int key)
{
}
void BeatMapper::KeyReleased(int key)
{
	if (key == 17)
	{
		EndMapping();
		return;
	}
	if (key > 16)
		return;
	Beat beat;
	beat.activate = key;
	beat.bpm = PRESS_BPM;
	beat.time = core->time.getElapsedTime().asMilliseconds();
	int division = BT_BEATTIMELEN / BT_NBTIMES;
	int spacing = beat.time % (int)division;
	int divisor = beat.time / division;
	beat.time = divisor * division;
	if (spacing > division / 2) {
		beat.time += division;
	}

	GUIBeatButton* bt = (GUIBeatButton*)core->kb.GetWidget(key - 1);
	if (bt->m_duration > 500)
	{
		beat.type = BTY_HOLD;
		beat.duration = bt->m_duration;
	}
	std::cout << "Beat at: " << beat.time << "; duration: " << bt->m_duration << std::endl;
	beat.time -= bt->m_duration;
	if (beat.time <= 0)
		return;
	curMap->beats.push_back(beat);

	bt->OnReleased();
	bt->OnTriggered();
	bt->Update(&core->time);
	bt->OnPressed();
	bt->Update(&core->time);
	bt->OnReleased();
}

void BeatMapper::CursorBack()
{
	core->st = ST_LOAD;
}

void BeatMapper::CursorSave()
{
	Beatmap bp;
	std::string Sname = name + " Beatmap";

	bp.name = std::wstring(Sname.begin(), Sname.end());
	bp.src = name;
	bp.author = std::wstring(L"Jubeat Return");
	bp.delay = 0;
	bp.level = 10;
	bp.originalBpm = PRESS_BPM;
	bp.songLevel = SL_EXTREME;

	std::vector<Beat> endBeatsList;
	std::for_each(mappings.begin(), mappings.end(), [=, &endBeatsList](Beatmapping &map)
	{
		std::for_each(map.beats.begin(), map.beats.end(), [=, &endBeatsList](Beat &item)
		{
			endBeatsList.push_back(item);
		});
	});
	bp.beats = endBeatsList.size();
	std::sort(endBeatsList.begin(), endBeatsList.end(), [](const Beat &bmp1, const Beat &bmp2) -> bool
	{
		return (bmp1.time < bmp2.time);
	});
	std::for_each(endBeatsList.begin(), endBeatsList.end(), [=, &bp](Beat &map)
	{
		bp.beatsList.push_back(map);
	});
	bp.speedMul = 1;
	if (bp.src.find("_x075")) {
		bp.speedMul = 0.75;
	}
	bp.Export("Data/Beatmaps/" + bp.src + ".jrb");
}

