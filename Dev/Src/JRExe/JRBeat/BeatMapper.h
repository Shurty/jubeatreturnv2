/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

#define BT_NBTIMES			(8)
#define BT_BEATTIMELEN		1000.0
#define BT_SECSIZE			80 * 1000 / BT_BEATTIMELEN
#define PRESS_BPM			(30000 / (BT_BEATTIMELEN / BT_NBTIMES))

class Core;

struct Beatmapping
{
	int id;
	std::list<Beat> beats;
	Beatmapping(int _id = 0)
	{
		id = _id;
	}
};

class BeatMapper
{
private:
	Core* core;
	std::string name;
	std::string src;
	std::list<Beatmapping> mappings;
	std::list<Beatmapping>::iterator curMap;
	std::list<Beat>::iterator curBeat;
	int curTime;
	int prevTime;
	int totalTime;

public:
	BeatMapper(Core*);
	~BeatMapper();
	
	void Draw(RenderingEngine *);
	void Update();
	void DrawActions();
	void Start(std::string const &path, std::string const &name);
	void EndMapping();

	// actions
	void CursorUp();
	void CursorSelect();
	void CursorDown();
	void CursorLeft();
	void CursorRight();
	void CursorBack();
	void CursorSave();
	void CursorStart();
	void CursorRestart();
	void KeyPress(int key);
	void KeyReleased(int key);
};