/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

struct DirEntry
{
	std::string name;
	std::string src;
	bool isDir;
	unsigned int id;
};

class FileBrowser
{
private:
	Core* core;
	int idx;
	std::list<DirEntry> files;
	std::list<DirEntry>::iterator filePtr;

public:
	FileBrowser(Core *);
	~FileBrowser();
	
	void FileBrowse(std::string const &dir);
	void Draw(RenderingEngine *re);
	void DrawActions();

	// actions
	void CursorUp();
	void CursorSelect();
	void CursorDown();
};
