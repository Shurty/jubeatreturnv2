/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

Core::Core()
	: pm(), render(pm), kb(pm)
{
	browser = new FileBrowser(this);
	mapper = new BeatMapper(this);
}

Core::~Core()
{
	delete browser;
	delete mapper;
}

void Core::Start()
{
	/* Attempting Arduino inputs creation */
	ArduinoInput *res = new ArduinoInput(pm);

	st = ST_LOAD;

	/* Input init */
	if (res->IsConnected()) {
		input = res;
	} else {
		delete res;
		input = new InputController(pm);
	}
	input->SetBtnPressedCallback(&IInterfacable::OnKeyPressed);
	input->SetBtnReleasedCallback(&IInterfacable::OnKeyReleased);
	render.OnInit(pm);

	browser->FileBrowse("Data/Musics/");

	for (int i = 0; i < 16; i++)
	{
		GUIBeatButton* wdg = new GUIBeatButton(this);
		wdg->LinkUserData(this);
		wdg->SetRender([] (GUIWidget *wdg, RenderingEngine *render) {

			bool usable = false;
			sf::Text tx;
			sf::RectangleShape rect;
			sf::FloatRect tsize;
			Core *core = (Core*)wdg->GetUserData();

			tx.setFont(render->GetFont(FONT_TITLE_TX));
			switch (wdg->GetLinkedKey())
			{
			case 1: tx.setString("1"); break;
			case 2: tx.setString("2"); break;
			case 3: tx.setString("3"); break;
			case 4: tx.setString("4"); break;
			case 5: tx.setString("Q"); break;
			case 6: tx.setString("W"); break;
			case 7: tx.setString("E"); break;
			case 8: tx.setString("R"); break;
			case 9: tx.setString("A"); break;
			case 10: tx.setString("S"); break;
			case 11: tx.setString("D"); break;
			case 12: tx.setString("F"); break;
			case 13: tx.setString("Z"); break;
			case 14: tx.setString("X"); break;
			case 15: tx.setString("C"); break;
			case 16: tx.setString("V"); break;
			default: tx.setString("?"); break;
			}
			tx.setCharacterSize(10);
			tx.setPosition(wdg->GetPosition().x + wdg->GetSize().x / 2, wdg->GetPosition().y);
			tx.setColor(sf::Color(255, 255, 255, 255));
			render->GetRenderWindow()->draw(tx);

			tx.setFont(render->GetFont(FONT_TITLE_TX));
			/* Render special states */
			switch (core->st)
			{
			case ST_LOAD:
			case ST_IDLE:
				switch (wdg->GetLinkedKey())
				{
				case 2:
					usable = true;
					tx.setString("/\\");
					break;
				case 5:
					if (core->st != ST_IDLE)
						break;
					usable = true;
					tx.setString("<");
					break;
				case 6:
					usable = true;
					tx.setString("SEL");
					break;
				case 7:
					if (core->st != ST_IDLE)
						break;
					usable = true;
					tx.setString(">");
					break;
				case 10:
					usable = true;
					tx.setString("V");
					break;
				case 13:
					if (core->st != ST_IDLE)
						break;
					usable = true;
					tx.setString("<--");
					break;
				case 14:
					if (core->st != ST_IDLE)
						break;
					usable = true;
					tx.setString("SAV");
					break;
				case 15:
					if (core->st != ST_IDLE)
						break;
					usable = true;
					tx.setString("RE");
					break;
				case 16:
					if (core->st != ST_IDLE)
						break;
					usable = true;
					tx.setString("GO");
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}

			if (usable == true)
			{
				tx.setCharacterSize(16);
				tsize = tx.getLocalBounds();
				tx.setPosition(wdg->GetPosition().x + wdg->GetSize().x / 2 - tsize.width / 2, 
					wdg->GetPosition().y + wdg->GetSize().y / 2 - tsize.height / 2);
				tx.setColor(sf::Color(0, 255, 0, 255));
				render->GetRenderWindow()->draw(tx);

				rect.setPosition(wdg->GetPosition().x, wdg->GetPosition().y);
				rect.setSize(sf::Vector2f(wdg->GetSize().x, wdg->GetSize().y));
				rect.setTexture(NULL);
				rect.setFillColor(sf::Color(255, 255, 0, 50));
				render->GetRenderWindow()->draw(rect);
			}


		});
		kb.SetWidget(i, wdg);
	}
	float sw = pm.m_params.window_width;
	float sh = pm.m_params.window_height;

	/* Calculate button offsets */
	float dx1 = sw * 2.5 / 290.0;
	float btnw = sw * 60.0 / 290.0;
	float k = sw * 15.0 / 290;
	float sph = sh * 52.5 / 505.0;

	pm.m_params.btn_step = k;
	pm.m_params.btn_side_step = dx1;
	pm.m_params.btn_size = btnw;
	pm.m_params.btn_top_offset = sph;

	kb.SetPosition(0, sh-sw);
	kb.SetSize(sw, sw);
	kb.Init(false);
}

void Core::Update()
{
	input->PollEvents(&render, this);
	kb.ReceiveEvent(UPDATE, &time);
	tweener.step(time.getElapsedTime().asMilliseconds());

	switch (st)
	{
	case ST_IDLE:
	case ST_MAP:
		mapper->Update();
		break;
	default:
		break;
	}
	
}

void Core::Render()
{
	float sw = pm.m_params.window_width;
	float sh = pm.m_params.window_height;
	render.OnPreRender(pm);

	sf::Text tx;
	sf::RectangleShape rect;

	tx.setFont(render.GetFont(FONT_TITLE_TX));
	tx.setString("Jubeat Return BeatMapper");
	tx.setCharacterSize(32);
	tx.setPosition(10, 0);
	tx.setColor(sf::Color(255, 255, 255, 255));
	render.GetRenderWindow()->draw(tx);

	rect.setPosition(0,sh-sw);
	rect.setSize(sf::Vector2f(sw, sw));
	rect.setTexture(render.GetTexture(TEX_PLAYBOX_BG));
	rect.setFillColor(sf::Color(255, 255, 255, 255));
	render.GetRenderWindow()->draw(rect);

	kb.ReceiveEvent(DRAW, &render);

	switch (st)
	{
	case ST_LOAD:
		browser->Draw(&render);
		break;
	case ST_IDLE:
	case ST_MAP:
		mapper->Draw(&render);
		break;
	default:
		break;
	}

	render.OnRender(pm);
}

void Core::End()
{
	render.OnExit(pm);
}

void Core::OnKeyPressed(int key)
{
	kb.ReceiveEvent(PRESSED, (void *)key);
	switch (st)
	{
	case ST_LOAD:
		switch (key)
		{
		case 2:
			browser->CursorUp();
			break;
		case 6:
			browser->CursorSelect();
			break;
		case 10:
			browser->CursorDown();
			break;
		default:
			break;
		}
		break;
	case ST_IDLE:
		switch (key)
		{
		case 2:
			mapper->CursorUp();
			break;
		case 5:
			mapper->CursorLeft();
			break;
		case 6:
			mapper->CursorSelect();
			break;
		case 7:
			mapper->CursorRight();
			break;
		case 10:
			mapper->CursorDown();
			break;
		case 13:
			mapper->CursorBack();
			break;
		case 14:
			mapper->CursorSave();
			break;
		case 15:
			mapper->CursorRestart();
			break;
		case 16:
			mapper->CursorStart();
			break;
		default:
			break;
		}
		break;
	case ST_MAP:
		mapper->KeyPress(key);
		break;
	default:
		break;
	}
}
void Core::OnKeyReleased(int key)
{
	kb.ReceiveEvent(RELEASED, (void *)key);
	switch (st)
	{
	case ST_MAP:
		mapper->KeyReleased(key);
		break;
	default:
		break;
	}
}