/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

enum DETECT
{
	MISS = 0,
	BAD,
	GOOD,
	GREAT,
	PERFECT
};

class RenderingEngine;

class GUIBeatButton : public GUIActionButton
{
private:
	sf::Clock m_triggerTime;
	bool m_isDummy;
	bool m_isFirst;
	bool m_pretriggered;
private:
	int m_pressTime;
public:
	int m_duration;

	Core *m_core;

	// Click simulation
	bool m_clickSimulateDone;

	// Post click animation
	bool m_animateEnd;
	float m_endFrameId;
	BEATRESULT m_lastpress;

	sf::Color animOutColor;
	float m_animOutAlpha;

public:
	GUIBeatButton(Core *);
	virtual ~GUIBeatButton();

	void SetDummy(bool dum);
	void SetFirst(bool bl);

	virtual void Init();
	virtual void Draw(RenderingEngine *target);
	virtual void Update(sf::Clock *time);
	virtual void OnTriggered();
	void OnPreTrigger();
	virtual void OnPressed();
	virtual void OnReleased();
	virtual void ReceiveEvent(GUIEVENT ev, void *param);
};