/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class FileBrowser;
class BeatMapper;

enum State
{
	ST_LOAD,
	ST_IDLE,
	ST_MAP,
	ST_MAP_ACTIVATORS,
	ST_SAVE
};

class Core : public IInterfacable
{
public:
	Parameters pm;
	RenderingEngine render;
	InputController* input;
	GUIKeyboard kb;
	sf::Clock time;
	State st;
	tween::Tweener tweener;

	FileBrowser *browser;
	BeatMapper *mapper;

	/* st_load params */
	std::string cwd;

	/* st_idle params */
	//sf::Music music;
	JBMusic music;
	soundtouch::SoundTouch musicCtrl;

	/* st_map params */
	std::deque<Beat> beats;

	/* st_save params */
	Beatmap bmp;

public:
	Core();
	virtual ~Core();

	void Start();
	void Update();
	void Render();
	void End();

	virtual void OnKeyPressed(int key);
	virtual void OnKeyReleased(int key);
};