/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

// JRBeat.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Logger::Log << "Jubeat Return Beatmapper " << VERSION << std::endl;
	Logger::Log << "Begin-" << std::endl;
	Core core;

	Logger::Log << "Start-" << std::endl;
	core.Start();
	Logger::Log << "Op-" << std::endl;
	while (1)
	{
		core.Update();
		core.Render();
	}
	Logger::Log << "End-" << std::endl;
	core.End();
	Logger::Log << "Complete-" << std::endl;

	return 0;
}

