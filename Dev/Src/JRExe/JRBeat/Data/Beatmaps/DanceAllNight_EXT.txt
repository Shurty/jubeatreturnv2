﻿###HEADER###
Name: Dance All Night
Author: Sota Fujimori
Src: DANCEALLNIGHT

Difficulty: EXTREME
Level: 9

BPM: 135
Notes: 846
Delay: -180
###BODY###
1
口口口口 |－－－－|
口口口口 |－－－－|
口口口口 |－－－－|
口口口口 |－－－－|
2
③口口② |①－②－|
⑦⑥⑤④ |③－－－④⑤⑥⑦|
口口口口
口口口①

口⑫⑫口
口口口口
口⑨⑧口 |⑧⑨⑩⑪|
⑪口口⑩ |⑫－－－|
3
①④⑦② |①－－－|
⑥②①⑤ |②－③－|
①④③口 |④－－⑤|
⑥②①④ |⑥－⑦－|
4
①④⑤① |①－②－|
⑧②③⑦ |③－④－|
③⑦⑤① |⑤－⑥－|
⑥口③⑦ |⑦－⑧－|
5
①④④② |①－－－|
⑥②①⑤ |②－③－|
①④③① |④－－⑤|
⑥②口⑦ |⑥－⑦－|
6
口③⑤① |①－②－|
④⑥③⑦ |③－④－|
②⑦⑤① |⑤－⑥－|
⑧①③⑦ |⑦－⑧－|
7
①④⑦② |①－－－|
⑥②①⑤ |②－③－|
①④③口 |④－－⑤|
⑥②①④ |⑥－⑦－|
8
①④⑤① |①－②－|
⑧②③⑦ |③－④－|
③⑦⑤① |⑤－⑥－|
⑥口③⑦ |⑦－⑧－|
9
口④④② |①－－－|
⑥②①⑤ |②－③－|
口④③① |④－－⑤|
⑥②口⑦ |⑥－⑦－|
10
口③⑤① |①－②－|
④⑥③⑦ |③－④－|
②⑦⑤① |⑤－⑥－|
⑧①③⑦ |⑦－⑧－|
11
④⑤⑥口 |①－－－|
①口①口 |②－－③|
口⑦口③ |－④⑤⑥|
口②②口 |⑦－－－|
12
⑤口口④ |①－②－|
口②②口 |③－④⑤|
⑤口口④
③③①①

口⑨⑩口
⑧⑫口⑪
口⑦⑥口 |⑥⑦⑧⑨|
口口口口 |⑩⑪－⑫|
13
②④口② |①－②－|
⑦⑧⑥⑤ |③－－④|
口⑦⑤口 |⑤－⑥－|
③③①① |⑦－⑧－|
14
⑤⑥⑦⑧ |①②③④|
⑨⑩⑪⑫ |⑤⑥⑦⑧|
⑨⑩⑪⑫ |⑨－⑩－|
④③②① |⑪－⑫－|
15
③①①口 |①－－－|
②②③口 |②－－③|
口③口口
口口口③

口⑦口口
口口口口
口口口口 |－④⑤⑥|
④⑤⑥口 |⑦－－－|
16
⑤口②④ |①－②－|
口①②① |③－④⑤|
⑤口口④
③③口口

口⑨⑩口
⑧⑫口⑪
口⑦⑥口 |⑥⑦⑧⑨|
口口口口 |⑩⑪－⑫|
17
②口口② |①－②－|
④⑦⑤口 |③－－④|
⑦⑧⑥⑤ |⑤－⑥－|
③③①① |⑦－⑧－|
18
④③②① |①②③④|
口口⑤⑤ |⑤－⑥－|
口口口口
口口⑥⑥

口⑩⑨⑧
⑦口口口
口⑩⑨⑧ |⑦－⑧－|
⑦口口口 |⑨－⑩－|
19
③⑧⑦口 |①－－－|
②⑧⑦② |②－－－|
口⑥⑤④ |③④⑤⑥|
口①①口 |⑦－⑧－|
20
①口口① |－－①－|
②口口② |－－②－|
③口口③ |－－③－|
④口口④ |－－④－|
21
口口口口 |①－－②|
①口③② |③－④－|
④①口④
口口口口

口⑤⑥口
口口口口
口⑦⑦口 |⑤⑥－－|
口口口口 |⑦－－－|
22
⑥⑦⑧口 |①②③④|
口④③② |⑤⑥⑦⑧|
口口口口
①口口⑤

口口口口
⑨⑬⑬口
⑫⑪⑩⑫ |⑨⑩⑪⑫|
⑬⑫⑫⑬ |－－⑬－|
23
①②②① |①－－－|
③⑧⑦口 |②－－－|
⑧②②⑦ |③④⑤⑥|
口⑥⑤④ |⑦－⑧－|
24
①口口① |－－①－|
②口口② |－－②－|
③口口③ |－－③－|
④口口④ |－－④－|
25
⑥⑥①④ |①－②③|
②③口⑤ |－－④－|
口口①④ |⑤⑥－－|
口口⑦⑦ |⑦－－－|
26
口口口口 |①②③④|
①②③④ |⑤⑥⑦⑧|
⑤⑥⑦⑧
口口口口

⑪⑩⑩⑫
⑪口口⑫
口口口口 |⑨－⑩－|
口⑨⑨口 |⑪－⑫－|
27
口口⑥口 |①②－－|
⑤⑦口口 |－－－－|
④①②口 |－③－④|
③口口口 |－⑤⑥⑦|
28
口口口③ |①②－－|
⑤口④口 |－－③④|
口⑥口口 |－－－－|
口②①口 |－－⑤⑥|
29
口⑥口口 |①②－－|
口口⑦⑤ |－－－－|
口口口④ |－③－④|
口②①③ |－⑤⑥⑦|
30
③口口口 |①②－－|
⑥④口⑥ |－－③④|
口口⑤口 |⑤－－⑥|
⑦①②⑦ |－－⑦－|
31
⑤③口口 |①②③④|
口口②⑥ |⑤⑥⑦⑧|
⑦①④口
口口⑧口

口口⑫口
⑮⑨⑭口
口⑬口⑩ |⑨⑩⑪⑫|
⑪口口⑯ |⑬⑭⑮⑯|
32
⑤①⑥⑯ |①②③④|
⑮⑦⑧② |⑤⑥⑦⑧|
③⑨⑭⑩ |⑨⑩⑪⑫|
⑪⑬④⑫ |⑬⑭⑮⑯|
33
⑤①口口 |①②③④|
口③⑥② |⑤⑥⑦⑧|
口⑦口④
口口⑧口

口⑬口⑫
⑪口口⑭
⑮口⑩口 |⑨⑩⑪⑫|
⑨口口⑯ |⑬⑭⑮⑯|
34
⑤口④口 |①②③④|
口③⑥口 |⑤⑥⑦⑧|
口⑦口②
口①⑧口

口口口⑩
⑨口口口
口口口⑩ |⑨－－⑩|
⑨口口口 |－－－－|
35
口①②口 |①－－－|
口口口口 |②－－－|
口②①口
口口口口

⑧⑨⑩口
口⑥⑤④
口口口口 |③④⑤⑥|
③口口⑦ |⑦⑧⑨⑩|
36
口口口⑤ |①②③－|
口口口口 |④⑤－－|
口③②口
④①口口

⑥口⑩口
口⑨⑧⑦
⑪⑫⑬口 |⑥⑦⑧⑨|
口口口口 |⑩⑪⑫⑬|
37
④②口⑥ |①－②－|
口口口③ |③－－④|
口口口口 |⑤－⑥－|
⑦口⑤① |⑦－－－|
38
③口①口 |①－②－|
口③口① |③－④－|
②口④口
口②口④

口口口⑧
⑨口⑥⑦
口⑤口⑩ |⑤⑥⑦－|
⑪口口口 |⑧⑨⑩⑪|
39
②①①口 |①－－－|
③口②口 |②－－③|
口②口口
口①①②

口⑦口⑥
口⑤口⑧
⑨口④口 |④⑤⑥－|
口口口⑩ |⑦⑧⑨⑩|
40
④口口口 |①②③－|
口口⑤口 |④⑤⑥⑦|
口⑥③②
口①⑦口

口⑪⑩⑨
⑬⑭⑮口
口口口口 |⑧⑨⑩⑪|
⑧口口⑫ |⑫⑬⑭⑮|
41
②口口④ |①－②－|
口口口口 |③－－④|
②①①③
口口③口

口⑧⑥口
⑦口口⑥
口⑦口口 |⑤－⑥－|
⑤⑤口口 |⑦－－⑧|
42
④口口④ |①－②－|
口②②口 |③－－④|
③⑥⑤① |⑤⑥⑦⑧|
③⑧⑦① |－－－－|
43
口①①口 |①－－－|
口②口口 |②－－－|
①③口① |③－－－|
口④口口 |④－－－|
44
口口口口 |①－－－|
口口口口 |②－－－|
口口②口
口口①口

口④口口
口口③④
⑤口口口 |③－④－|
口口⑤口 |⑤－－－|
45
③口③① |①－②－|
口②口口 |③－－－|
口口③①
①口口口

口④口口
⑤⑥④⑥
口口口口 |④－⑤－|
口口④⑥ |⑥－－－|
46
①③④① |①－②－|
口口口口 |③－④－|
①③②③
口口口口

口口口⑥
⑦⑤⑦口
口口口⑧ |⑤－⑥－|
⑦⑤口口 |⑦－⑧－|
47
②⑯①⑮ |①②③④|
⑭④⑬③ |⑤⑥⑦⑧|
⑥⑫⑤⑪ |⑨⑩⑪⑫|
⑩⑧⑨⑦ |⑬⑭⑮⑯|
48
⑧口⑦口 |①②③④|
口口口口 |⑤⑥⑦⑧|
④⑥③⑤
口②口①

口口口口
⑫⑪⑩⑨
口口口口 |⑨⑩⑪⑫|
⑯⑮⑭⑬ |⑬⑭⑮⑯|
49
③①③① |①－②－|
口口口口 |③－－－|
口②③①
口口口口

口⑤口口
⑥⑥④⑥
口口口口 |④－⑤－|
口④④⑥ |⑥－－－|
50
①③④① |①－②－|
⑦⑤口⑥ |③－④－|
①③②③ |⑤－⑥－|
⑦⑤⑦⑧ |⑦－⑧－|
51
②④③① |①－－②|
④②⑤⑥ |③－④－|
①①③① |⑤－－－|
口口⑤⑥ |⑥－－－|
52
①②口口 |①－－－|
④③③④ |②－－－|
①②口口 |③－－－|
④③③④ |④－－－|
53
③口③① |①－②－|
口②口口 |③－－－|
①口③①
口口口口

口④口口
⑤⑥④⑥
口口口口 |④－⑤－|
口口④⑥ |⑥－－－|
54
①③④① |①－②－|
口口口口 |③－④－|
①③②③
口口口口

口口口⑥
⑦⑤⑦口
口口口⑧ |⑤－⑥－|
⑦⑤口口 |⑦－⑧－|
55
②⑯①⑮ |①②③④|
⑭④⑬③ |⑤⑥⑦⑧|
⑥⑫⑤⑪ |⑨⑩⑪⑫|
⑩⑧⑨⑦ |⑬⑭⑮⑯|
56
⑧口⑦口 |①②③④|
口口口口 |⑤⑥⑦⑧|
④⑥③⑤
口②口①

口口口口
⑫⑪⑩⑨
口口口口 |⑨⑩⑪⑫|
⑯⑮⑭⑬ |⑬⑭⑮⑯|
57
③①③① |①－②－|
口口口口 |③－－－|
口②③①
口口口口

口⑤口口
⑥⑥④⑥
口口口口 |④－⑤－|
口④④⑥ |⑥－－－|
58
①③④① |①－②－|
⑦⑤口⑥ |③－④－|
①③②③ |⑤－⑥－|
⑦⑤⑦⑧ |⑦－⑧－|
59
②④③① |①－－②|
④②⑤⑥ |③－④－|
①①③① |⑤－－－|
口口⑤⑥ |⑥－－－|
60
①口④① |①－②③|
口⑤口口 |④－⑤－|
①③口①
口口②口

⑧⑧口口
口口⑥⑥
口口⑧⑧ |⑥⑦－－|
⑦⑦口口 |⑧－－－|
61
口口①口 |①－－－|
口①口口 |②－－③|
口口口③
口②②口

④⑤⑥口
口口口口
口⑦口口 |－④⑤⑥|
口口口口 |⑦－－－|
62
⑤口口④ |①－②－|
口②②口 |③－④⑤|
⑤口口④
③③①①

口⑨⑩口
⑧⑫口⑪
口⑦⑥口 |⑥⑦⑧⑨|
口口口口 |⑩⑪－⑫|
63
②④口② |①－②－|
⑧⑦⑥⑤ |③－－④|
⑧⑦⑥⑤ |⑤－⑥－|
③③①① |⑦－⑧－|
64
口⑤⑤口 |①②③④|
口口口口 |⑤－⑥－|
口⑥⑥口
④③②①

⑨口口⑨
⑦⑦⑧⑧
⑦口口⑧ |⑦－⑧－|
口⑩⑩口 |⑨－⑩－|