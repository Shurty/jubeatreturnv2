/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JRBeat" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

FileBrowser::FileBrowser(Core *cr)
	: core(cr)
{
}

FileBrowser::~FileBrowser()
{
}

void FileBrowser::Draw(RenderingEngine *re)
{
	DrawActions();
	int px, py;
	sf::Text tx;

	py = 50;
	px = 220;

	std::for_each(files.begin(), files.end(), [=, &px, &py, &tx] (DirEntry &ent)
	{
		
		sf::RectangleShape rect;
		sf::FloatRect tsize;
		float sw = core->pm.m_params.window_width;
		float sh = core->pm.m_params.window_height;

		if (filePtr->id == ent.id)
		{
			rect.setPosition(px - 5, py - 2);
			rect.setSize(sf::Vector2f(200, 20));
			rect.setTexture(NULL);
			rect.setFillColor(sf::Color(50, 50, 50, 255));
			core->render.GetRenderWindow()->draw(rect);
		}

		tx.setFont(core->render.GetFont(FONT_TITLE_TX));
		tx.setString(ent.name);
		tx.setCharacterSize(16);
		tx.setPosition(px, py);
		if (ent.isDir)
			tx.setColor(sf::Color(0, 255, 0, 255));
		else
			tx.setColor(sf::Color(255, 255, 255, 255));
		core->render.GetRenderWindow()->draw(tx);
		py += 20;
		if (py > 300)
		{
			py = 50;
			px += 200;
		}
	});

	tx.setFont(core->render.GetFont(FONT_TITLE_TX));
	tx.setString(std::to_string(files.size()) + " files");
	tx.setCharacterSize(16);
	tx.setPosition(px + 20, py);
	tx.setColor(sf::Color(255, 255, 0, 255));
	core->render.GetRenderWindow()->draw(tx);
}

void FileBrowser::FileBrowse(std::string const &dirsrc)
{
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;
	Beatmap *bmp;
	std::string activeSrc;
	
	core->cwd = dirsrc;
	Logger::Log << "Browse " << dirsrc << std::endl;
	hFind = FindFirstFile((dirsrc + "*").c_str(), &ffd);
	if (INVALID_HANDLE_VALUE == hFind) 
	{
		Logger::Log << "Browse Error: Invalid handle " << dirsrc << std::endl;
		// No files
		return ;
	}
	else 
	{
		files.clear();
		activeSrc = dirsrc;
		if (dirsrc.length() > 0)
		{
			files.push_back(DirEntry());
			files.back().isDir = true;
			files.back().src = "..";
			files.back().name = "..";
			files.back().id = 0;
		}
		do {
			activeSrc = ffd.cFileName;
			if (activeSrc[0] != '.') {
				unsigned int pid = 0;
				if (files.size() > 0) {
					pid = files.back().id + 1;
				}
				files.push_back(DirEntry());
				files.back().src = dirsrc + "" + activeSrc;
				files.back().name = activeSrc;
				files.back().id = pid;
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					files.back().isDir = true;
				} else {
					files.back().isDir = false;
				}
			}
		} while (FindNextFile(hFind, &ffd) != 0);
		filePtr = files.begin();
	}
}

void FileBrowser::DrawActions()
{
	sf::Text tx;
	sf::RectangleShape rect;
	sf::FloatRect tsize;
	int px, py;
	float sw = core->pm.m_params.window_width;
	float sh = core->pm.m_params.window_height;

	py = sh - 20;
	rect.setSize(sf::Vector2f(140, 20));
	rect.setFillColor(sf::Color(50, 50, 50, 255));
	tx.setFont(core->render.GetFont(FONT_SCORING_TX));
	tx.setCharacterSize(13);
	tx.setColor(sf::Color(255, 255, 255, 255));

	px = 20;
	tx.setString("CWD: /" + core->cwd);
	tx.setPosition(px + 5, py + 3);
	tsize = tx.getLocalBounds();
	rect.setPosition(px, py);
	rect.setSize(sf::Vector2f(tsize.width + 10, 20));
	core->render.GetRenderWindow()->draw(rect);
	core->render.GetRenderWindow()->draw(tx);

	px += tsize.width + 20;
	tx.setString("Filter: txt,bpm,bmp,jrb,jbm");
	tx.setPosition(px + 5, py + 3);
	tsize = tx.getLocalBounds();
	rect.setPosition(px, py);
	rect.setSize(sf::Vector2f(tsize.width + 10, 20));
	core->render.GetRenderWindow()->draw(rect);
	core->render.GetRenderWindow()->draw(tx);

}

void FileBrowser::CursorUp()
{
	if (files.size() == 0)
		return ;
	if (filePtr == files.begin())
		filePtr = files.end();
	filePtr--;
}
void FileBrowser::CursorSelect()
{
	if (files.size() == 0)
		return ;
	if (filePtr->isDir)
	{
		if (filePtr->src[0] == '.')
		{
			core->cwd += "../";
		}
		else
		{
			core->cwd = filePtr->src + "/";
		}
		FileBrowse(core->cwd);
	}
	else
	{
		core->mapper->Start(filePtr->src, filePtr->name);
	}
}
void FileBrowser::CursorDown()
{
	if (files.size() == 0)
		return ;
	filePtr++;
	if (filePtr == files.end())
		filePtr = files.begin();
}