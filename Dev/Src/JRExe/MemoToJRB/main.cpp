/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
// For license details please check LICENSE.TXT
//
// Copyright Jeremy DUBUC 2014-2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <JREngine\jrshared.h>
#include <iostream>

void ParseFolder(std::string const &src, std::map<std::string, Parser *>& m_parsers)
{
	const std::string srcFrom = BEATMAPSDIR;
	const std::string srcTo = CBEATMAPSDIR;

	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;
	Beatmap *bmp;
	std::string activeSrc;

	Logger::Log << "Begin: Loading handle " << srcFrom << src << std::endl;
	
	hFind = FindFirstFile((srcFrom + src + "*").c_str(), &ffd);
	if (INVALID_HANDLE_VALUE == hFind)
	{
		Logger::Log << "SongLibrary Error: Invalid handle " << srcFrom << src << std::endl;
		return ;
	}
	else
	{
		do {
			activeSrc = ffd.cFileName;
			if (activeSrc[0] != '.') {
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					if (activeSrc[0] != '_')
						ParseFolder(src + activeSrc + "/", m_parsers);
				}
				else {
					std::string ext = activeSrc.substr(activeSrc.length() - 3, 3);
					if (!m_parsers[ext])
						ext = "txt";
					bmp = m_parsers[ext]->ParseBeatmap(srcFrom + src + "" + activeSrc);
					Logger::Log << "SongLibrary: Parsed " << std::string(bmp->name.begin(), bmp->name.end()) << " " << bmp->level << std::endl;
					/* Compile and copy to target dir */
					bmp->Export(srcTo + "" + activeSrc.substr(0, activeSrc.length() - 3) + "jrb");
				}
			}
		} while (FindNextFile(hFind, &ffd) != 0);
	}
}

int main(int ac, char **av)
{
	MemoParser memoParser;
	JRBParser jrbParser;
	std::map<std::string, Parser *> m_parsers;

	m_parsers["txt"] = &memoParser;
	m_parsers["jcm"] = m_parsers["txt"];
	m_parsers["jrb"] = &jrbParser;

	ParseFolder("", m_parsers);

	Logger::Log << "Complete." << std::endl;
	return (0);
}

