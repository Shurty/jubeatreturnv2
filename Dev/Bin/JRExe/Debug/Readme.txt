Jubeat Return Project
Release Version WIN32

Authors:
Jeremy DUBUC
Thierry BERGER
Quentin GUAY
Thimothee MAURIN

Dependancies:
SFML
MSVC110

Available builds:
WIN32

Documentation will be available online as soon as possible.
Bitbucket access can be provided on request - contact me

For requests or issues, please contact jeremy.dubuc@gmail.com
For licensing, please check LICENSE.TXT
