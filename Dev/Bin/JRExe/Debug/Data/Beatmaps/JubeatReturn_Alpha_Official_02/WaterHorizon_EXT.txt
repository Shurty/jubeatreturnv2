﻿###HEADER###
Name: Water Horizon
Author: 青龍
Src: WaterHorizon

Difficulty: EXTREME
Level: 10

BPM: 191
Notes: 780
Delay: -1256
###BODY###
1
口口口口 |－－－－|
口口口口 |－－－－|
口口口口 |－－－－|
口口口口 |－－－－|
2
口①口口 |①－－－|
口口①口 |－－－－|
口①口口 |－－－－|
口口①口 |－－－－|
3
口口口口 |－－－－|
口口口口 |－－－－|
口口口口 |－－－－|
口口口口 |－－－－|
4
④⑩②口 |①－②－|
⑧⑥口⑨ |③－④－|
⑤①⑦③ |⑤⑥⑦－|
口口口口 |⑧⑨－⑩|
5
口口⑤口 |①－②－|
口⑤口口 |③－④－|
口①⑥② |⑤－－－|
④⑥口③ |⑥－－－|
6
口①口② |①－②－|
口口①④ |③－④－|
③①口口
口口①口

⑦⑩口口
口⑤口口
口口⑨⑥ |⑤⑥⑦－|
口⑧口口 |⑧⑨－⑩|
7
⑥口⑦② |①－②－|
口⑧口⑤ |③④－－|
③①⑨口 |⑤⑥⑦－|
口口④口 |⑧－⑨－|
8
⑧④⑦③ |①－②－|
口口口口 |③－④－|
口口口口 |⑤－⑥－|
⑥②⑤① |⑦－⑧－|
9
⑧④⑦③ |①－②－|
口口口口 |③－④－|
口口口口 |⑤－⑥－|
⑥②⑤① |⑦－⑧－|
10
⑦口口口 |①－②－|
⑧③⑥口 |③－④－|
口④⑤② |⑤－⑥－|
口口口① |⑦－⑧－|
11
⑦口口口 |①－②－|
⑧③⑥口 |③－④－|
口④⑤② |⑤－⑥－|
口口口① |⑦－⑧－|
12
⑮①⑯② |①②③④|
⑬③⑭④ |⑤⑥⑦⑧|
⑤⑥⑨⑩ |⑨⑩⑪⑫|
⑦⑧⑪⑫ |⑬⑭⑮⑯|
13
口②口① |①－－－|
口①口③ |－－－－|
②口口口 |②－－－|
口口③口 |③－－－|
14
⑤口口② |①－②－|
①①④⑦ |③－④－|
⑥口口口 |⑤－⑥－|
③口口⑧ |⑦－⑧－|
15
口⑤口口 |①－②－|
⑧①④⑦ |③－④－|
⑤③②⑥ |⑤－⑥－|
口口⑦口 |⑦－⑧－|
16
⑤口口② |①－②－|
口①④⑦ |③－④－|
⑥口口口 |⑤－⑥－|
③口口⑧ |⑦－⑧－|
17
口⑤⑥口 |①－②－|
口①④⑦ |③－④－|
⑤③②口 |⑤－⑥－|
口⑧⑦口 |⑦－⑧－|
18
⑤口口② |①－②－|
①①④⑦ |③－④－|
⑥口口口 |⑤－⑥－|
③口口⑧ |⑦－⑧－|
19
口⑤口口 |①－②－|
⑧①④⑦ |③－④－|
⑤③②⑥ |⑤－⑥－|
口口⑦口 |⑦－⑧－|
20
⑤口口② |①－②－|
口①④⑦ |③－④－|
⑥口口口 |⑤－⑥－|
③口口⑧ |⑦－⑧－|
21
口⑦口② |①－②－|
口③④⑧ |③④⑤⑥|
⑦⑤⑥口 |⑦－－－|
口①⑧口 |⑧－－－|
22
⑤口①② |①－②－|
①口④⑦ |③－④－|
⑥口口口 |⑤－⑥－|
③口口⑧ |⑦－⑧－|
23
⑤⑥⑦① |①－②－|
口①④⑧ |③－④－|
口③②口 |⑤⑥⑦－|
口⑨口口 |⑧－⑨－|
24
⑤口口② |①－②－|
口①④⑦ |③－④－|
⑥口①口 |⑤－⑥－|
③口口⑧ |⑦－⑧－|
25
⑤⑥⑦① |①－②－|
口①④⑧ |③－④－|
口③②口 |⑤⑥⑦－|
口⑨口口 |⑧－⑨－|
26
⑤口口② |①－②－|
口①④⑦ |③－④－|
⑥口①口 |⑤－⑥－|
③口口⑧ |⑦－⑧－|
27
⑤⑥⑦① |①－②－|
口①④⑧ |③－④－|
口③②口 |⑤⑥⑦－|
口⑨口口 |⑧－⑨－|
28
⑤口口② |①－②－|
口①④⑦ |③－④－|
⑥口①口 |⑤－⑥－|
③口口⑧ |⑦－⑧－|
29
口⑤口① |①－②－|
口①④⑥ |③－④－|
⑤③②口 |⑤－－－|
⑥口⑥口 |⑥－－－|
30
口⑦⑥② |①－②－|
①⑤⑧口 |③④⑤－|
口③④口 |⑥⑦⑧－|
⑨口口⑩ |⑨－⑩－|
31
④口口③ |①－②－|
②口口① |③－④－|
口口口口
口口口口

口口口口
口口口⑦
⑥口口⑤ |⑤－－－|
口口口口 |⑥－⑦－|
32
口⑦⑥② |①－②－|
①⑤⑧口 |③④⑤－|
⑨③④⑩ |⑥⑦⑧－|
口口口口 |⑨－⑩－|
33
口口口③ |①－②－|
口口口② |③－④－|
口④口口
口①口口

口⑥⑤口
口口口口
口口口口 |⑤－⑥－|
口⑧⑦口 |⑦－⑧－|
34
口⑦⑥② |①－②－|
①⑤⑧口 |③④⑤－|
口③④口 |⑥⑦⑧－|
⑨口口⑩ |⑨－⑩－|
35
④口口③ |①－②－|
②口口① |③－④－|
口口口口
口口口口

口口口口
口口口⑦
⑥口口⑤ |⑤－－－|
口口口口 |⑥－⑦－|
36
口⑦⑥② |①－②－|
①⑤⑧口 |③④⑤－|
⑨③④⑩ |⑥⑦⑧－|
口口口口 |⑨－⑩－|
37
口口口③ |①－②－|
口口口② |③－④－|
口④口口
口①口口

口⑥⑤口
口口口口
口口口口 |⑤－⑥－|
口⑧⑦口 |⑦－⑧－|
38
口⑥⑤④ |①－②－|
③口⑦② |③－④－|
①口口口 |⑤⑥⑦－|
⑧口口⑨ |⑧－⑨－|
39
④口口③ |①－②－|
②口口① |③－④－|
口口口口
口口口口

口口口口
口口口⑦
⑥口口⑤ |⑤－－－|
口⑥口口 |⑥－⑦－|
40
口⑥⑤② |①－②－|
①④⑦口 |③－④－|
⑧口口⑨ |⑤⑥⑦－|
①口③口 |⑧－⑨－|
41
口口口③ |①－②－|
口口口② |③－④－|
口④口口
口①口口

口⑥⑤口
口口口口
口口口口 |⑤－⑥－|
口⑧⑦口 |⑦－⑧－|
42
口⑥⑤④ |①－②－|
③口⑦② |③－④－|
①口口口 |⑤⑥⑦－|
⑧口口⑨ |⑧－⑨－|
43
④口口③ |①－②－|
②口口① |③－④－|
口口口口
口口口口

口口口口
口口口⑦
⑥口口⑤ |⑤－－－|
口口口口 |⑥－⑦－|
44
口⑥⑤② |①－②－|
①④⑦口 |③－④－|
⑧口口⑨ |⑤⑥⑦－|
口口③口 |⑧－⑨－|
45
口口口③ |①－②－|
口口口② |③－④－|
口④口口
口①口口

口⑥⑤口
口口口口
口口口口 |⑤－⑥－|
口⑧⑦口 |⑦－⑧－|
46
口⑦口口 |①－②－|
⑥③②① |③－④－|
④口口口 |⑤－－－|
⑤口口口 |⑥－⑦－|
47
口口①② |①－②－|
口④③口 |③④⑤⑥|
口⑥⑤口 |⑦－⑧－|
⑧⑩⑨⑦ |⑨－⑩－|
48
口⑦口口 |①－②－|
⑥③②① |③－④－|
④口口口 |⑤－－－|
⑤口口口 |⑥－⑦－|
49
口口①② |①－②－|
⑨③④⑩ |③④⑤⑥|
口⑤⑥口 |⑦－⑧－|
⑦口口⑧ |⑨－⑩－|
50
②口⑦① |①－②－|
⑨⑥⑤⑧ |③－④－|
④口口③ |⑤⑥⑦－|
口口口口 |⑧－⑨－|
51
②③口口 |①－②－|
⑧口④口 |③－④－|
口⑦⑤口 |⑤－⑥－|
口⑥口① |⑦－⑧－|
52
②口⑦① |①－②－|
⑨⑥⑤⑧ |③－④－|
④口口③ |⑤⑥⑦－|
口口口口 |⑧－⑨－|
53
②③口口 |①－②－|
口口④口 |③－④－|
口⑦口⑤ |⑤－⑥－|
口口⑥① |⑦－－－|
54
口口口口 |①－－－|
口①口口 |－－－－|
①口口② |②－－－|
①口②口 |－－－－|
55
口口②口 |①－－－|
①口口② |－－－－|
口①口口 |②－－－|
口口口口 |－－－－|
56
口①口口 |①－－－|
①口②口 |－－－－|
口口口② |②－－－|
口口口口 |－－－－|
57
口口口口 |①－－－|
口口口口 |－－－－|
①口口② |②－－－|
口①②口 |－－－－|
58
口口口口 |①－－－|
①口口口 |－－－－|
口①口② |②－－－|
口口②口 |－－－－|
59
口口②口 |①－－－|
①口口② |－－－－|
口①口口 |②－－－|
口口口口 |－－－－|
60
口①口口 |①－－－|
①口②口 |－－－－|
口口口② |②－－－|
口口口口 |－－－－|
61
口口口口 |①－－－|
口口口口 |－－－－|
①口口② |②－－－|
口①②口 |－－－－|
62
口口⑥⑤ |①－②－|
⑧⑦②① |③－④－|
④③口口 |⑤－⑥－|
口口口口 |⑦－⑧－|
63
口①口口 |－－①－|
口口②口 |②－③－|
口③口④ |④－－－|
口口口口 |－－－－|
64
口口⑧⑦ |①－②－|
⑤⑥②① |③－④－|
④③口口 |⑤－⑥－|
口口口口 |⑦－⑧－|
65
⑥⑦口口 |－－①－|
口⑤②① |②－③－|
口③④口 |④－⑤－|
口口口口 |⑥－⑦－|
66
口口⑥⑤ |①－②－|
⑧⑦②① |③－④－|
④③口口 |⑤－⑥－|
口口口口 |⑦－⑧－|
67
口①⑦口 |－－①－|
⑥⑤②口 |②－③－|
口③④口 |④－⑤－|
口口口口 |⑥－⑦－|
68
口口⑥口 |①－－－|
④⑤口② |②－③－|
①口③口 |－－④－|
口口口口 |⑤－⑥－|
69
④③口① |①－②－|
口口②口 |③－④－|
口口口口 |－－－－|
口口口口 |－－－－|
70
口口⑥⑤ |①－②－|
⑧⑦②① |③－④－|
④③口口 |⑤－⑥－|
口口口口 |⑦－⑧－|
71
口①口口 |－－①－|
口口②口 |②－③－|
口③口④ |④－－－|
口口口口 |－－－－|
72
口口⑧⑦ |①－②－|
⑤⑥②① |③－④－|
④③口口 |⑤－⑥－|
口口口口 |⑦－⑧－|
73
⑥⑦口口 |－－①－|
口⑤②① |②－③－|
口③④口 |④－⑤－|
口口口口 |⑥－⑦－|
74
口口⑥⑤ |①－②－|
⑧⑦②① |③－④－|
④③口口 |⑤－⑥－|
口口口口 |⑦－⑧－|
75
口①⑦口 |－－①－|
⑥⑤②口 |②－③－|
口③④口 |④－⑤－|
口口口口 |⑥－⑦－|
76
口口⑥口 |①－－－|
④⑤口② |②－③－|
①口③口 |－－④－|
口口口口 |⑤－⑥－|
77
④③口① |①－②－|
口口②口 |③－④－|
⑦⑨⑩⑧ |⑤⑥⑦⑧|
⑪⑤⑥⑫ |⑨⑩⑪⑫|
78
③口①② |①－－－|
③①口口 |－－－－|
口口口口 |②－－－|
口口口口 |③－－－|
79
口口⑥⑤ |①－②－|
⑧⑦②① |③－④－|
④③口口 |⑤－⑥－|
①③⑤⑦ |⑦－⑧－|
80
口②口口 |①－②－|
口口③口 |③－④－|
口④口⑤ |⑤－－－|
①③⑤⑥ |⑥－－－|
81
口口⑧⑦ |①－②－|
⑤⑥②① |③－④－|
④③口口 |⑤－⑥－|
①③⑤⑦ |⑦－⑧－|
82
口⑥口口 |①－②－|
⑤⑦③② |③－④－|
口④⑧口 |⑤－⑥－|
①③⑤⑦ |⑦－⑧－|
83
口口⑥⑤ |①－②－|
⑧⑦②① |③－④－|
④③口口 |⑤－⑥－|
①③⑤⑦ |⑦－⑧－|
84
口②⑧口 |①－②－|
⑦⑥③口 |③－④－|
口④⑤口 |⑤－⑥－|
①③⑤⑦ |⑦－⑧－|
85
口口⑦口 |①－－－|
⑤⑥口② |②－③－|
①口③口 |④－⑤－|
①②④⑥ |⑥－⑦－|
86
④③口① |①－②－|
口口②口 |③－④－|
口口口口 |⑤－－－|
①③⑤⑥ |⑥－－－|
87
口⑥口口 |①－－－|
⑤口⑦② |②－③－|
①口③口 |④－⑤－|
①②④⑥ |⑥－⑦－|
88
口口④⑤ |①－②－|
口③⑦⑥ |③－④－|
②口⑧口 |⑤－⑥－|
①③⑤⑦ |⑦－⑧－|
89
口口②口 |①－－－|
口③口⑦ |②－③－|
①⑤⑥口 |④－⑤－|
①②④⑥ |⑥－⑦－|
90
④口口① |①－②－|
⑤③②⑧ |③－④－|
口⑥⑦口 |⑤－⑥－|
①③⑤⑦ |⑦－⑧－|
91
口口口口 |①－－－|
口③②⑦ |②－③－|
①⑤⑥口 |④－⑤－|
①②④⑥ |⑥－⑦－|
92
②①口⑤ |①②③－|
③⑦④⑨ |④－⑤－|
⑥口⑧口 |⑥－⑦－|
①④⑥⑧ |⑧－⑨－|
93
④②①口 |①－②－|
③口口口 |③－④－|
口口口③
①口④口

口口口口
口口⑤口
口⑥⑦口 |－－⑤－|
⑦⑤口⑥ |⑥－⑦－|
94
⑤口③⑤ |－－①－|
口①④② |②－③－|
②口口① |④－－－|
口④③口 |⑤－－－|
95
口口口口 |①－－－|
口口口口 |－－－－|
口①①口 |－－－－|
①口口① |－－－－|