﻿###HEADER###
Name: PONPONPON
Author: きゃりーぱみゅぱみゅ
Src: PONPONPON

Difficulty: INTERMEDIATE
Level: 5

BPM: 128
Notes: 337
Delay: -2578
###BODY###
1
口口口口 |－－－－|
口口口口 |－－－－|
口口口口 |－－－－|
口口口口 |－－－－|
2
口口口口 |－－－－|
口①口口 |－－①－|
口口①口 |－－－－|
口口口口 |－－－－|
3
口口④口 |①－②－|
③口⑤口 |③－－－|
②口口口 |④－⑤－|
①口⑥⑥ |－－⑥－|
4
口口口口 |－－－－|
①②③④ |－－①－|
口口口⑤ |②－③－|
口口口口 |④－⑤－|
5
⑤⑦⑥④ |①－②－|
口口口口 |③－－－|
③②①口 |④－⑤－|
口口口口 |⑥－⑦－|
6
口口口口 |－－－－|
①②③④ |－－①－|
口口口⑤ |②－③－|
口口口口 |④－⑤－|
7
口口⑥⑥ |①－②－|
口口口口 |③－－－|
③②①⑤ |④－⑤－|
口口口④ |－－⑥－|
8
口口口口 |－－－－|
①②③④ |－－①－|
口口口⑤ |②－③－|
口口口口 |④－⑤－|
9
口⑧⑧口 |①－②－|
口口口口 |③－④－|
③②①口 |⑤－⑥－|
④⑥⑦⑤ |⑦－⑧－|
10
④口口③ |－－①－|
⑥口口口 |－－②－|
②①①⑤ |③－④－|
口口口口 |⑤－⑥－|
11
口口口口 |①－②－|
口①②③ |③－④－|
口口口口
④口口口

口口口口
⑥⑥口口
口口⑦⑦ |⑤－⑥－|
口口口⑤ |－－⑦－|
12
①口口口 |－－－－|
口②口⑤ |－－①－|
③口口口 |②－③－|
口④口口 |④－⑤－|
13
口口口口 |①－②－|
③②①口 |③－④－|
口口口口
口口口④

口口口口
口口⑥⑥
⑦⑦口口 |⑤－⑥－|
⑤口口口 |－－⑦－|
14
②③④⑤ |－－－－|
①口口口 |－－①－|
口口口口 |②－③－|
口口口口 |④－⑤－|
15
口口口口 |①－②－|
②口口① |③－④－|
④口口③
口口口口

口口口口
口口口口
口口⑦⑦ |⑤－⑥－|
⑥⑥口⑤ |－－⑦－|
16
口口口口 |－－－－|
④口口③ |－－①②|
⑥①②⑤ |③－④－|
口口口口 |⑤－⑥－|
17
口口口口 |①－②－|
口⑥⑤口 |③－④－|
口⑦⑦口 |⑤－⑥－|
②④③① |－－⑦－|
18
①口口④ |－－①－|
口口口口 |②－③－|
③口口② |④－－－|
口口口口 |－－－－|
19
①①口口 |①－－－|
口口②② |②－－－|
③④⑤口 |③－④－|
口口口口 |⑤－－－|
20
口口口⑦ |①－②－|
①③⑤⑥ |③－④－|
口口口口 |⑤－⑥－|
②④口口 |⑦－－－|
21
①①口口 |①－－－|
口口②② |②－－－|
口⑤④③ |③－④－|
口口口口 |⑤－－－|
22
①口口口 |①－②－|
②口口口 |③－④－|
③口⑥口 |⑤－⑥－|
④⑦⑤口 |⑦－－－|
23
口口①① |①－－－|
②②口口 |②－－－|
口⑤④③ |③－④－|
口口口口 |⑤－－－|
24
⑦口口口 |①－②－|
⑥⑤③① |③－④－|
口口口口 |⑤－⑥－|
口口④② |⑦－－－|
25
口口①① |①－－－|
②②口口 |②－－－|
③④⑤口 |③－④－|
口口口口 |⑤－－－|
26
口口口① |①－②－|
口口口② |③－④－|
口⑥口③ |⑤－⑥－|
口⑤⑦④ |⑦－－－|
27
口口④④ |－－①－|
③口口口 |②－③－|
②口口口 |④－－－|
①口口口 |－－－－|
28
①口口口 |－－①－|
②口口口 |②－③－|
③口口口 |④－－－|
口口④④ |－－－－|
29
口口口口 |－－①－|
口口口⑤ |②－③－|
口口口口 |④－－－|
①②③④ |⑤－－－|
30
②①⑤⑤ |①－②－|
③口口口 |③－④－|
④口口口 |⑤－－－|
口口口口 |－－－－|
31
④④口口 |－－①－|
口口口③ |②－③－|
口口口② |④－－－|
口口口① |－－－－|
32
口口口① |－－①－|
口口口② |②－③－|
口口口③ |④－－－|
④④口口 |－－－－|
33
口口口口 |－－①－|
口⑤⑤口 |②－③－|
④③②① |④－－－|
口口口口 |⑤－－－|
34
口口口口 |①－－－|
口口口口 |－－②－|
③口口② |③－④－|
⑤①①④ |⑤－－－|
35
①①口口 |①－－－|
口口②② |②－－－|
③④⑤口 |③－④－|
口口口口 |⑤－－－|
36
口口口⑦ |①－②－|
①③⑤⑥ |③－④－|
口口口口 |⑤－⑥－|
②④口口 |⑦－－－|
37
①①口口 |①－－－|
口口②② |②－－③|
口⑥⑤④ |④－⑤－|
口③口口 |⑥－－－|
38
①口口口 |①－②－|
②口口口 |③－④－|
③口⑥口 |⑤－⑥－|
④⑦⑤口 |⑦－－－|
39
口口①① |①－－－|
②②口口 |②－－－|
口⑤④③ |③－④－|
口口口口 |⑤－－－|
40
⑦口口口 |①－②－|
⑥⑤③① |③－④－|
口口口口 |⑤－⑥－|
口口④② |⑦－－－|
41
口口①① |①－－－|
②②口口 |②－－－|
③④⑤口 |③－④－|
口口口口 |⑤－－－|
42
口口口① |①－②－|
口口口② |③－④－|
口⑥口③ |⑤－⑥－|
口⑤⑦④ |⑦－－－|
43
①口口① |①－－－|
②口口② |②－－－|
口口口口 |③－④－|
③④⑤口 |⑤－－－|
44
①①口口 |①－②－|
口口②② |③－④－|
③口口口
口口④④

⑦⑦⑥⑥
⑤口口口
口口口口 |⑤－⑥－|
口口口口 |⑦－－－|
45
口口口口 |①－－－|
口③③口 |②－－－|
①④④② |③－④－|
口⑤⑤口 |⑤－－－|
46
①口口口 |①－②－|
③③口⑥ |③－④－|
⑦口口② |⑤－⑥－|
⑤⑤口④ |⑦－－－|
47
①口口① |①－－－|
②口口② |②－－－|
口口口口 |③－④－|
口⑤④③ |⑤－－－|
48
口口①① |①－②－|
②②口口 |③－④－|
口口口③
④④口口

⑥⑥⑦⑦
口口口⑤
口口口口 |⑤－⑥－|
口口口口 |⑦－－－|
49
口口口口 |①－－－|
口③③口 |②－－－|
②④④① |③－④－|
口⑤⑤口 |⑤－－－|
50
⑥口口① |①－②－|
口口③③ |③－④－|
②口口⑦ |⑤－⑥－|
④口⑤⑤ |⑦－－－|