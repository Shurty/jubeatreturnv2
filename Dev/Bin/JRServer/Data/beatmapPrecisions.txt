
------- Official beatmaps list for 0.1a -------

DailyLunch_EASY: Perfect (0ms)
DailyLunch_ADV: Perfect (0ms)
DailyLunch_EXT: Perfect (0ms)
DanceAllNight_EASY: Perfect (0ms)
DanceAllNight_ADV: Perfect (0ms)
DanceAllNight_EXT: Perfect (0ms)
Dota2S1_EXT: Terrible - REDO NEEDED
encounter_EXT: Perfect (0ms)
FollowTomorrow_EXT: Near perfect (100ms)
HYDRA_EASY : Good (perfect but some places invalid)
HYDRA_EXT : Good (perfect but some places invalid)
OnlyMyRailgun_EXT : near perfect (20ms)
OnlyMyRailgun_ADV : near perfect (20ms)
OnlyMyRailgun_EASY : near perfect (20ms)
PiaNoJac1_EXT: Perfect (0ms)
PONPONPON_EASY : Perfect (0ms fix song)
PONPONPON_ADV : Perfect (0ms fix song)
PONPONPON_EXT : Perfect (0ms fix song)
RomancingLayer_EXT : Perfect (0ms fix song)
SecondHeaven_EXT : Perfect (0ms fix song)
SecondHeaven_ADV : Perfect (0ms fix song)
SecondHeaven_EASY : Perfect (0ms fix song)
SistersNoise_ADV : perfect (0ms)
SistersNoise_EASY : perfect (0ms)
SistersNoise_EXT : perfect (0ms)
TheFatRat_Unity_EASY(2, lvl2): Mediun (0ms, redo beatmap)
TheFatRat_Unity_(EASY1, lvl4): Near Perfect (0ms, redo some)
TheFatRat_Unity_EXT(lvl8): Perfect (0ms, but redo some notes)
TheFatRat_Unity_ADV(MED lvl6): Perfect (0ms, redo some)
TheIslandPart_EXT: Terrible (bad mapping)
TrueBlue_ADV : Near prefect (50ms)
TrueBlue_EASY : Near prefect (50ms)
Unisonote_EXT: Perfect (0ms)
Unisonote_EASY: Perfect (0ms)
Unisonote_ADV: Perfect (0ms)
V_EASY: Perfect (0ms)
V_ADV: Perfect (0ms)
V_EXT: Perfect (0ms)
ZZ_EXT: Perfect (0ms)


------- Menu/Info musics -------
Intro: KONAMI (to be replaced)
Input: TheFatRat IfSo
GM Select: TheFatRat IfSo
Info: TheFatRat DaBeDoBeDo Info
Music select: TheFatRat DaBeDoBeDo Info
Config select: -
Networking Start: TheFatRat DaBeDoBeDo
Music debrief: TheFatRat InfinitePower
Game End: -